[![pipeline status](https://gitlab.windenergy.dtu.dk/TOPFARM/pywake_ellipsys_examples/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/TOPFARM/cuttingedge/pywake/pywake_ellipsys/commits/master)
[![coverage report](https://gitlab.windenergy.dtu.dk/TOPFARM/pywake_ellipsys_examples/badges/master/coverage.svg)](https://gitlab.windenergy.dtu.dk/TOPFARM/cuttingedge/pywake/pywake_ellipsys/commits/master)

![PyWakeEllipSys](https://gitlab.windenergy.dtu.dk/TOPFARM/pywake_ellipsys_examples/raw/master/logo.png)

# PyWake_EllipSys_Examples

This repository contains examples of PyWake_EllipSys.
PyWake_EllipSys is a closed-sourced Reynolds-averaged Navier-Stokes based
wake model that is set up as a plugin to PyWake. PyWake is an open source AEP calculator
including a library of (engineering) wake models:

https://gitlab.windenergy.dtu.dk/TOPFARM/PyWake

The RANS model in PyWake_EllipSys uses the general purpose flow solver EllipSys3D,
initially developed by Jess A. Michelsen at DTU Fluid
Mechanics<sup>[1](#Michelsen1992)</sup><sup>[,2](#Michelsen1994)</sup> and
Niels N. Sørensen in connection with his PhD study<sup>[3](#Sorensen1995)</sup>.
EllipSys3D is closed source licensed software and it is based on Fortran and
MPI. PyWake_EllipSys uses PyEllipSys, which is a direct memory Python interface to
EllipSys. This means that it is possible to import EllipSys3D as a Python
object and change flow variables during a simulation.

The wind turbines are represented by actuator disks (AD) and the inflow is an
atmospheric surface layer, following Monin-Obukohv Similarity Theory (MOST) or
an idealized atmospheric boundary layer (ABL) with or without Coriolis forces.
The non-neutral wake interactions are not yet validated. The main setup uses flat
terrain, with a homogeous roughness length, but the RANS model can also be run
with terrain.

PyWake_EllipSys can be used to simulate flow cases, perform parametric studies and
calculate the AEP.

## Documentation, installation, etc:

[https://topfarm.pages.windenergy.dtu.dk/cuttingedge/pywake/pywake_ellipsys/](https://topfarm.pages.windenergy.dtu.dk/cuttingedge/pywake/pywake_ellipsys/)

## References

<a name="Michelsen1992">1</a>:
Michelsen, J. A.,
*Basis3D - a Platform for Development of Multiblock PDE Solvers*,
AFM 92-05, Department of Fluid Mechanics, Technical University of Denmark, December 1994.  
<a name="Michelsen1994">2</a>:
Michelsen, J. A.,
Block structured Multigrid solution of 2D and 3D Elliptic PDEs,
AFM 94-06, Department of Fluid Mechanics, Technical University of Denmark, May 1994.  
<a name="Sorensen1995">3</a>:
Sørensen, N. N.,
*General Purpose Flow Solver Applied to Flow Over Hills*,
Risø-R-827, Risø National Laboratory, Roskilde, Denmark 1995.   
