import numpy as np
import sys
import os
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysWTs
from py_wake.site._site import UniformWeibullSite


class UniformSite(UniformWeibullSite):
    def __init__(self, A, k, nsector=12.0, shear=None):
        UniformWeibullSite.__init__(self, np.ones(int(nsector)) / float(nsector), [A] * int(nsector), [k] * int(nsector), .1, shear=shear)


def run_MR(run_machine, queue, coarse=False, grid=False, cal=False, run=False,
           aep=False, plotfmt='pdf'):
    from py_wake_ellipsys_examples.data.turbines.dtu10mw import DTU10MW
    from py_wake_ellipsys_examples.data.turbines.dtu2p5mw import DTU2p5MW

    # Multi-rotor wind farm simulations, as published in DOI: 10.1088/1742-6596/1256/1/012011
    # The wind farm consists of 4x4 10 MW multi-rotor wind turbines, each equiped with 4 2.5 MW
    # rotors based on a down-scaled DTU 10 MW reference rotor. The AEP of the multi-rotor wind farm
    # is compared with a single-rotor wind farm of 4x4 DTU-10 MW wind turbines.
    # The grid here is different in the height direction, where a taller refinement wake domain (2D -> 3D)
    # and domain height (25D -> 50D) are used compared to the published work, but the effects are negligible.
    # The example calculates an AEP of 466.87 GWh and 472.52 GWh for the single- and multi-rotor wind farm, respectively.
    # This AEP of single-rotor wind farm that was published had a lower AEP of 464.83, which needs further investigation.
    # However, the published AEP of the multi-rotor wind farm is the almost the same (472.87 GWh), differences
    # can be associated to the difference in grid settings.
    # The multi-rotor has zero toe out, but one could add this using the optional variable toe_i, which
    # is a list or array with a toe out angle for each rotor similar to the yaw_i variable:
    # sim_res = wfm(..., toe_i=toe_i) or run_windfarm(..., toe_i=toe_i)
    # For single-rotor wts, yaw_i=toe_i, since the arm is zero.

    # DTU 10 MW rotor:
    D10 = 178.3
    zH10 = 119.0

    # Down-scaled DTU 2.5 MW rotor
    D = D10 * 0.5

    # Relative rotor locations 4R-MR
    arm = 0.5 * 1.05 * D
    wt_x_mr_rel = [0.0, 0.0, 0.0, 0.0]
    wt_y_mr_rel = [-arm, arm, -arm, arm]
    # SR layout, same as MR layout
    s = 3.0  # Regular spacing normalized by D10
    wt_x_sr = np.array([0.0, 1.0, 2.0, 3.0,
                        0.0, 1.0, 2.0, 3.0,
                        0.0, 1.0, 2.0, 3.0,
                        0.0, 1.0, 2.0, 3.0]) * s * D10
    wt_y_sr = np.array([0.0, 0.0, 0.0, 0.0,
                        1.0, 1.0, 1.0, 1.0,
                        2.0, 2.0, 2.0, 2.0,
                        3.0, 3.0, 3.0, 3.0]) * s * D10

    nWT = len(wt_x_sr)
    nR = len(wt_x_mr_rel)
    nAD = nR * nWT
    wt_x_mr = np.zeros((nAD))
    wt_y_mr = np.zeros((nAD))
    # yawc_x and yawc_y are the yaw centers of the multi-rotor rotors to acount for the arms. For single-rotor wts, yawc_x=wt_x and yawc_y=wt_y
    yawc_x = np.zeros((nAD))
    yawc_y = np.zeros((nAD))
    type_i_mr = np.zeros((nAD))
    type_i_mr_rel = np.array([0, 0, 1, 1])  # The multi-rotor is based on two wt types that only differ in hub height.
    for i in range(nWT):
        for j in range(nR):
            k = j + i * nR
            wt_x_mr[k] = wt_x_sr[i] + wt_x_mr_rel[j]
            wt_y_mr[k] = wt_y_sr[i] + wt_y_mr_rel[j]
            yawc_x[k] = wt_x_sr[i]
            yawc_y[k] = wt_y_sr[i]
            type_i_mr[k] = type_i_mr_rel[j]

    # Multi-rotor 4R-2p5MW
    h1 = zH10 - 0.525 * D
    h2 = zH10 + 0.525 * D
    mr_wt1 = DTU2p5MW(zH=h1)
    mr_wt2 = DTU2p5MW(zH=h2)

    # Single-rotor: 10MW
    sr = DTU10MW()

    # All rotor types
    wt = EllipSysWTs.from_WindTurbines([mr_wt1, mr_wt2, sr])

    type_i_sr = np.zeros((nWT))
    type_i_sr[:] = 2

    TI = 0.056  # TI based on TKE ~ 0.8 * TI_u = sigma_U/U
    zRef = 119.0
    # A single flow case
    # wd = [270.0]
    # ws = [8.0]
    ws = np.arange(4.0, 26.0, 1.0)
    wd = np.arange(0.0, 360.0, 3.0)

    # Constant A and k values for all wind directions
    # A and k based on HR II
    Site = UniformSite(A=9.0, k=2.2)
    # A and k based on FINO3
    # Site = UniformSite(A=13.0, k=2.4)

    # A multi-rotor and equivalent single rotor case are run
    casenames = ['MR4x4-%gD' % s, 'SR4x4-%gD' % s]
    type_i_all = [type_i_mr, type_i_sr]
    wt_x_all = [wt_x_mr, wt_x_sr]
    wt_y_all = [wt_y_mr, wt_y_sr]
    yawc_x_all = [yawc_x, wt_x_sr]
    yawc_y_all = [yawc_y, wt_y_sr]

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 1e-2
        grid_cells1_Ds = [2.0, 4.0]
        adgrid_nr = 5
        grid_bsize = 48
        maxnodes = 3
        walltime_wf_run = '0:30:00'
        # Overwrite flow cases with fewer cases for testing
        ws = np.arange(4.0, 26.0, 4.0)
        wd = np.arange(0.0, 360.0, 15.0)
    else:
        e3d_reslim = 1e-5
        grid_cells1_Ds = [8.0, 16.0]
        adgrid_nr = 64
        # If one can afford the resources:
        grid_bsize = 32
        maxnodes = 42
        walltime_wf_run = '48:00:00'
        # Otherwise:
        # grid_bsize = 48
        # maxnodes = 12
        # walltime_wf_run = '72:00:00'

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1001', grid=ADGrid(nr=adgrid_nr, ntheta=32))
    # Wind farm grid small rotor
    wfgrid_D = FlatBoxGrid(D,
                           cells1_D=grid_cells1_Ds[0],
                           radius_D=5e4 / D, zFirstCell_D=0.5 / D,
                           zWakeEnd_D=3.0, bsize=grid_bsize, zlen_D=50.0,
                           cluster=Cluster(walltime='0:20:00'))
    # Wind farm grid large rotor
    wfgrid_D10 = FlatBoxGrid(D10,
                             cells1_D=grid_cells1_Ds[1],
                             radius_D=5e4 / D, zFirstCell_D=0.5 / D,
                             zWakeEnd_D=3.0, bsize=grid_bsize, zlen_D=50.0,
                             cluster=Cluster(walltime='0:20:00'))
    wfgrids = [wfgrid_D, wfgrid_D10]
    e3d = E3D(reslim=e3d_reslim)
    # Wind farm run
    wfrun = WFRun(casename=casenames[0], write_restart=False,
                  march_dir='ws', wscutoff='auto', wdsym='auto',
                  cluster=Cluster(walltime=walltime_wf_run))

    wfm = EllipSys(Site, wt, wfgrids[0], TI, zRef,
                   e3d=e3d, ad=ad, wfrun=wfrun)
    wfm.calrun.cluster.walltime = '1:00:00'

    # Run step by step:
    if grid:
        # Create AD grid
        for p in range(len(casenames)):
            wfm.grid = wfgrids[p]
            for i in np.unique(type_i_all[p]):
                wfm.create_adgrid(i)
            # Create calibration grid per wt type
            for i in np.unique(type_i_all[p]):
                wfm.create_calibration_grid(i)

        # Create wind farm grid, the single rotor has a twice as fine grid relative to the rotor diameter, so we have get the same effective grid size.
        wfm.grid = wfgrids[1]
        wfm.wfrun.casename = casenames[1]
        wfm.create_windfarm_grid(wt_x_sr, wt_y_sr)
        # Grid name
        gridname = wfm.grid.get_grid_name(casenames[1])
        # We use the exact same wind farm grid for the multi-rotor wind farm
        # Wait for the single rotor wind farm grid to be finished
        wfm.grid.cluster.wait_for_cluster(gridname, gridname, 'grid')
        wfm.grid = wfgrids[0]
        wfm.wfrun.casename = casenames[0]
        gridname2 = wfm.grid.get_grid_name(casenames[0])
        print('ln -s ' + gridname + ' ' + gridname2)
        os.system('ln -s ' + gridname + ' ' + gridname2)

    if cal or run:
        # Run flow cases
        for p in range(len(casenames)):
            wfm.run_grid = False
            wfm.run_cal = cal
            wfm.run_wf = run
            wfm.run_post = False
            wfm.grid = wfgrids[p]
            wfm.wfrun.casename = casenames[p]
            sim_res = wfm(wt_x_all[p], wt_y_all[p], wd=wd, ws=ws, type=type_i_all[p], yawc_x=yawc_x_all[p], yawc_y=yawc_y_all[p])

    if aep:
        AEPs = []
        for p in range(len(casenames)):
            wfm.run_grid = False
            wfm.run_cal = False
            wfm.run_wf = False
            wfm.run_post = True
            wfm.grid = wfgrids[p]
            wfm.wfrun.casename = casenames[p]

            # There is a bug in PyWake's AEP calculator without wake effects if type_i is not np.zeros(I) using DeprecatedWindTurbines class.
            # This is because EllipSysWT is based on DeprecatedWindTurbines, which no longer supports the from_WindTurbines() method.
            # To avoid this we switch the wt class for sr and mr.
            if p == 0:
                wt = EllipSysWTs.from_WindTurbines([mr_wt1, mr_wt2, sr])
                wfm.windTurbines = wt
                type_i = type_i_all[p]
            elif p == 1:
                wfm.windTurbines = sr
                type_i = np.zeros(nWT)
            sim_res = wfm(wt_x_all[p], wt_y_all[p], wd=wd, ws=ws, type=type_i, yawc_x=yawc_x_all[p], yawc_y=yawc_y_all[p])
            aep = float(sim_res.aep(with_wake_loss=True).sum())
            aep_NoWake = float(sim_res.aep(with_wake_loss=False).sum())
            wake_loss = (aep - aep_NoWake) / aep_NoWake
            print('AEP:', aep)
            print('AEP no wake:', aep_NoWake)
            print('Wake loss:', wake_loss)
            AEPs.append(aep)
        print('Difference MR and SR:', (AEPs[0] - AEPs[1]) / AEPs[1])


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Run MR and SR wind farm cases using a very coarse setup if coarse=True
    run_MR(run_machine, queue, coarse=True, grid=True, cal=True, run=True,
           aep=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
