import numpy as np
import sys
import os
import time
import xarray
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *


def run_V80(run_machine, queue, grid=False, run=False, post=False, write=False,
            write_LES=False, plot=False, plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.hornsrev1 import Hornsrev1Site
    from py_wake_ellipsys.utils.wtgeneric_cnst import WTgen_Cnst
    from py_wake_ellipsys_examples.V80.data import data_path
    from py_wake_ellipsys_examples.plotwfres import plotSingleWake, get_downstream_label
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    nWT = 1
    wt_x = [0.0]
    wt_y = [0.0]
    type_i = np.zeros((nWT))

    # Create V80 with uniform thrust distribution similar to reference LES model
    zRef = 70.0
    Dref = 80.0
    wt = WTgen_Cnst(ctval=0.77, D=Dref, zH=zRef, loading='uniform', name='V80')

    # A single flow case
    wd = [270.0]
    ws = [8.0]
    TI = 0.057
    Uref = 1.0

    today = date.today()
    case = 'V80'

    # Extraction positions at hub height for several downsteam distances as function of relative wd
    sDown_D = np.array([2.5, 5.0, 7.5])

    maxnodes = 2
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1000', grid=ADGrid(nr=32, ntheta=32),
            old=True,  # e3d earsm test case was run with old AD model
            )
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, zFirstCell_D=0.00004, radius_D=5000 / Dref, cells1_D=8.0,
                         zlen_D=25.0, m1_w_D=3.0, m1_e_D=13, m1_s_D=2.0, m1_n_D=2.0)
    # Wind farm run
    wfrun = WFRun(casename=case, write_restart=True, restart_stress_out=True, cluster=Cluster(walltime='0:10:00'))

    # Explicit Algebraic Reynolds-Stress k-epsilon model and constants from Wallin and Johansson (2000)
    e3d = E3D(turbmodel='keWJ')
    #         turbmodel_earsm_momsource=False)

    cnst = Cnst(CR=1.8, ce2=1.82, kappa=0.38)
    # set Cmu by CR: Cmu=0.0871818320 for CR=1.8
    cnst.cmu = cnst.WJ_cmu_eff_log()
    wfpostflow = WFPostFlow(outputformat='netCDF', single_precision_netCDF=True,
                            clip_netCDF=[1200, 200, 405], restart_stress_out=True)
    # Initiate EllipSys3D model
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   cnst=cnst, ad=ad, e3d=e3d, wfrun=wfrun, wfpostflow=wfpostflow)

    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)

    if run:
        # Run flow cases
        wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)

    if post:
        # Post process flow
        wfm.post_windfarm_flow(wd, ws)

    if write:
        # Write velocity deficit and TI to a file
        folder = wfm.get_name()
        infile = '%s/post_flow_wd%g_ws%g/flowdata.nc' % (folder, wd[0], ws[0])
        print('Wait for flowdata.nc')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        data = xarray.open_dataset(infile)
        # Extract profiles of data at various downstream distances
        data_profile = data.interp(x=sDown_D * Dref, z=zRef).where(np.abs(data.y) < 2 * Dref, drop=True)
        # Extraction positions at hub height for several downsteam distances as function of y
        for i in range(sDown_D.size):
            label = get_downstream_label(sDown_D[i])
            outfile = case + '_RANS_%sD.dat' % label
            f = open(outfile, 'w')
            f.write('# EllipSys3D-RANS, %s\n' % wfm.e3d.turbmodel)
            f.write('# Based on DOI: 10.5194/wes-7-1975-2022, rerun date=%s\n' % today)
            f.write('# Spanwise distance y/D, U/U0, TI (sqrt(2/3 tke)/U0), sqrt(uu)/U0, sqrt(vv)/U0, sqrt(ww)/U0\n')
            for j in range(len(data_profile.y)):
                # print(data_profile.y[i,:])
                # sys.exit()
                f.write('%8.6f %8.6e %8.6e %8.6e %8.6e %8.6e\n' % (data_profile.y[j] / Dref, data_profile.U[i, j] / Uref, np.sqrt(2.0 / 3.0 * data_profile.tke[i, j]) / Uref,
                        np.sqrt(data_profile.uu[i, j]) / Uref, np.sqrt(data_profile.vv[i, j]) / Uref, np.sqrt(data_profile.ww[i, j]) / Uref))
            f.close()

    if write_LES:
        # Write LES velocity deficit and TI to a file
        infile = 'ellipsys3d_earsm_testcases/asl_wake/les_data/mean.nc'
        data = xarray.open_dataset(infile)
        infile = 'ellipsys3d_earsm_testcases/asl_wake/les_data/stresses.nc'
        data_stress = xarray.open_dataset(infile)

        # Extract profiles of data at various downstream distances
        data_profile = data.interp(x=sDown_D * Dref, z=zRef).where(np.abs(data.y) < 2 * Dref, drop=True)
        data_profile_stress = data_stress.interp(x=sDown_D * Dref, z=zRef).where(np.abs(data_stress.y) < 2 * Dref, drop=True)

        # Extraction positions at hub height for several downsteam distances as function of y
        Uref_LES = 8.0
        for i in range(sDown_D.size):
            label = get_downstream_label(sDown_D[i])
            outfile = case + '_LES_%sD.dat' % label
            f = open(outfile, 'w')
            f.write('# LES from Mahdi Abkar\n')
            f.write('# Based on DOI: 10.5194/wes-7-1975-2022, rerun date=%s\n' % today)
            f.write('# Spanwise distance y/D, U/U0, TI (sqrt(2/3 tke)/U0), sqrt(uu)/U0, sqrt(vv)/U0, sqrt(ww)/U0\n')
            for j in range(len(data_profile.y)):
                # print(data_profile.y[i,:])
                # sys.exit()
                f.write('%8.6f %8.6e %8.6e %8.6e %8.6e %8.6e\n' % (data_profile.y[j] / Dref, data_profile.U[i, j] / Uref_LES, np.sqrt(2.0 / 3.0 * data_profile.tke[i, j]) / Uref_LES,
                        np.sqrt(data_profile_stress.uu[i, j]) / Uref_LES, np.sqrt(data_profile_stress.vv[i, j]) / Uref_LES, np.sqrt(data_profile_stress.ww[i, j]) / Uref_LES))
            f.close()

    if plot:
        # plot
        lesfiles = []
        modelfiles = []
        modelfilesdown = []
        for i in range(sDown_D.size):
            label = get_downstream_label(sDown_D[i])
            lesfiles.append(data_path + case + '_LES_%sD.dat' % label)
            modelfilesdown.append(rans_path + case + '_RANS_%sD.dat' % label)
        modelfiles.append(lesfiles)
        modelfiles.append(modelfilesdown)

        # Compare with old results
        # modelfilesdown = []
        # for i in range(sDown_D.size):
        #     label = get_downstream_label(sDown_D[i])
        #     modelfilesdown.append(data_path + 'data/' + case + '_RANS_%sD.dat' % label)
        # modelfiles.append(modelfilesdown)
        # modellabels = ['LES', 'RANS', 'RANS old']
        # modellabels = ['LES', 'RANS No MomS', 'RANS old']
        # modelcolors = ['b', 'g', 'r']
        modellabels = ['LES', 'RANS']
        modelcolors = ['b', 'g']
        outfile = case + '_deficit.' + plotfmt
        plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='y')
        outfile = case + '_ti.' + plotfmt
        plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='y', ylabel=r'$\frac{\sqrt{2/3k}}{U_0}$', modeliU=2, ylim=0.12)
        outfile = case + '_uu.' + plotfmt
        plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='y', ylabel=r"$\frac{\sqrt{\overline{u'u'}}}{U_0}$", modeliU=3, ylim=0.18)
        outfile = case + '_vv.' + plotfmt
        plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='y', ylabel=r"$\frac{\sqrt{\overline{v'v'}}}{U_0}$", modeliU=4, ylim=0.12)
        outfile = case + '_ww.' + plotfmt
        plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='y', ylabel=r"$\frac{\sqrt{\overline{w'w'}}}{U_0}$", modeliU=5, ylim=0.12)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq,workq'
    starttime = time.time()
    run_V80(run_machine, queue,
            grid=True,
            run=True,
            post=True,
            write=True,
            # write_LES=True,
            plot=True,
            plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
