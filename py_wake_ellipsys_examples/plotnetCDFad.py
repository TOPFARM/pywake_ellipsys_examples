import numpy as np
import xarray
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.pyplot import cm
import glob


# PLOT STYLE ##################################
mpl.style.use('classic')
plt.rcParams['font.family'] = 'STIXGeneral'
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1
plt.rcParams['grid.linestyle'] = ':'  # Dotted gridlines
mpl.rcParams['lines.linewidth'] = 2
yd = dict(rotation=0, ha='right')     # Shortcut to flip y-label
plt.close('all')
mpl.rcParams['axes.formatter.useoffset'] = False

'''
DESCRIPTION:
Extracting disk-averaged variable from flowdata.nc, where variable:
    - U (default, change in calc_UAD if needed)
    - V
    - W
    - P
    - tke
    - epsilon
    - muT

LIMITATIONS:
Only works when flow is in the x-direction.
Only works for non-yawed and non-tilted AD's.

VERIFICATION:
Verification was made with UAD-file produced by pywake_ellipsys:
Compared to UAD-file, there was an error of +/- 0.2 %,
so if more precision needed, then use UAD-file instead.
Only one verification test was made, so the above is only rule of thumb.
'''


# HELPER FUNCTION ###################
def calc_UAD(data, x, y, z, D, n=128, var='U'):
    '''
    Calculate <U_AD> at each position.
    A n by n rectangular grid is masked by sqrt(y^2 + z^2) < R.
    I recommend using n=128 (can increase to minimize geometric error).
    Same resolution and diameter used for all disks.
    Important that x, y and z have the same length!
    Input:
        - data: netcdf data       (xarray Dataset)
        - x: streamwise positions (array)
        - y: lateral positions    (array)
        - z: vertical positions   (array)
        - n: resolution           (scalar)
        - D: diameter             (scalar)
    '''
    R = D / 2
    results = np.zeros(len(x))

    for i in range(len(x)):
        # Extract rectangular data
        y_rec = np.linspace(-R, R, n) + y[i]
        z_rec = np.linspace(-R, R, n) + z[i]
        A = (y_rec[1] - y_rec[0]) * (z_rec[1] - z_rec[0])    # Rectangular shape element
        rec_data = data.interp(x=x[i], y=y_rec, z=z_rec)  # This is fast

        # Mask operation. If mask condition not fulfilled, set value to 0.0.
        rec_data_masked = rec_data.where(np.sqrt((rec_data.y - y[i])**2 + (rec_data.z - z[i])**2) < R,
                                         other=0.0)

        # Each velocity contribution is weighted by the shape element area
        results[i] = np.sum(rec_data_masked[var].values * A) / (np.pi * R**2)

    return results


# PARAMETERS AND DATA ###################################
D = 126.0
zh = 90.0
UH = 8
x = np.arange(-7, 7.01, 0.25) * D
y = np.zeros(x.shape)
z = np.ones(x.shape) * zh
data = xarray.open_dataset('flowdata.nc')
print('file succesfully loaded')


# CALCULATE DISK-AVERAGED QUANTITIES ################
ADvar = calc_UAD(data, x, y, z, D)


# PLOT XY-plane #############################
fig, axes = plt.subplots(2, 2, sharex='col', figsize=(12, 5),
                         gridspec_kw={'width_ratios': (30, 1), 'height_ratios': (1, 2)})
plt.subplots_adjust(wspace=0.05)
ax = axes[:, 0]      # write as column
axes[1, 1].remove()  # remove unneeded Axes instance:
zplane_data = data.interp(z=zh)
X, Y = np.meshgrid(zplane_data.x, zplane_data.y)
p = ax[0].contourf(X / D, Y / D, zplane_data['U'].T / UH, np.linspace(0.3, 1.05, 10), cmap=cm.jet)
comcolRANS = plt.colorbar(p, cax=axes[0, 1], orientation='vertical', aspect=100)
comcolRANS.set_label('$U/U_H$', rotation=0, ha='left')
comcolRANS.set_ticks(np.arange(0.3, 1.05, 0.1))
ax[0].axis('equal')
ax[0].set_xlim(left=-10, right=10)
ax[0].set_ylim(bottom=-1.25, top=1.25)
ax[0].set_ylabel('$y/D$', yd)
ax[0].set_title('At $z = z_{hub}$')
# (optional: plot disks)
plot_disks = 0
if (plot_disks):
    x_AD = [-3.5 * D, 1.5 * D]  # Look in adinput.dat to find the AD coordinates
    for i in range(len(x_AD)):
        ax[0].plot([x_AD[i] / D, x_AD[i] / D], [-0.5, 0.5], 'k')


# PLOT DISK-AVERAGED QUANTITY ###########################
# (optional: load and plot UAD file produced by pywake_ellipsys)
load_uad = 0
if (load_uad):
    uad_file = glob.glob('UAD*')[0]
    wf_radius = 6.5 * D   # for a row of wind turbines, wf_radius = (x_ad[-1] - x_ad[0])/2
    uad_data = np.genfromtxt(uad_file, skip_header=2)
    ax[1].plot((uad_data[:, 1] - wf_radius) / D, uad_data[:, 4] / UH, label=r'EllipSys UAD file')

# plot disk-averaged quantity
ax[1].plot(x / D, ADvar / UH, 'go', label=r'flowdata')
ax[1].set_ylabel(r'$\dfrac{ \langle U_{AD} \rangle}{U_{H}}$ [-]', yd)
ax[1].set_xlabel('$x/D$')
ax[1].grid()
ax[1].legend(loc='upper right', fontsize=10)


# SAVE PICTURE ###################################
fig.savefig('AD.pdf', bbox_inches='tight')
