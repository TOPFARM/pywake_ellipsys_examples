import numpy as np
import sys
import os
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *


def run_NREL5MW(run_machine, queue, coarse=True, grid=False, run=False, post=False, write=False,
                plot=False, plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.hornsrev1 import Hornsrev1Site
    from py_wake_ellipsys.utils.wtgeneric_cnst import WTgen_Cnst
    from py_wake_ellipsys_examples.plotwfres import plotSingleWake, get_downstream_label
    from py_wake_ellipsys_examples.NREL5MW.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # NREL-5MW single wake simulations, as published in DOI: 10.1002/we.1736
    # The grid uses a different z-distribution that is more smooth, but requires more cells.
    # The height of the domain has been increased from 16D to 25D.
    # Old results where accidentally generated with a top BC as symmetry (should be inlet),
    # which sped up the inflow slightly for the high TI case.
    nWT = 1
    wt_x = [0.0]
    wt_y = [0.0]
    type_i = np.zeros((nWT))
    Dref = 126.0
    zRef = 90.0
    wt = WTgen_Cnst(D=Dref, zH=zRef, ctval=0.79, cpval=0.47, rpmval=9.0)
    TIs = [0.04, 0.128]  # Two TI cases where TI based on TKE ~ 0.8 * TI_u = sigma_U/U
    subcases = ['_TIlow', '_TIhigh']
    case = 'NREL-5MW'
    # A single flow case
    wd = [270.0]
    ws = [8.0]  # The wind speed does not matter since we use constant forces.
    # Extraction positions at hub height for several downsteam distances as function of relative wd
    sDown_D = np.array([2.5, 5.0, 7.5])
    wd_rel = np.linspace(-30, 30, 61)
    cos = np.cos(wd_rel)
    sin = np.sin(wd_rel)
    ext_points = np.zeros((3, 61, 3))
    ext_points[:, :, 2] = zRef
    for i in range(sDown_D.size):
        for j in range(wd_rel.size):
            ext_points[i, j, 0] = sDown_D[i] * wt.diameter() * np.cos(wd_rel[j] * np.pi / 180.0)
            ext_points[i, j, 1] = sDown_D[i] * wt.diameter() * np.sin(wd_rel[j] * np.pi / 180.0)

    ext_points = np.reshape(ext_points, (sDown_D.size * wd_rel.size, 3))
    today = date.today()

    maxnodes = 3
    # Set global cluster variables
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1100', grid=ADGrid(nr=64, ntheta=32))
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, zFirstCell_D=0.5 / Dref, radius_D=1e4 / Dref, cells1_D=10.0,
                         zlen_D=25.0, m1_w_D=3, m1_e_D=12, m1_s_D=1.5, m1_n_D=1.5)
    # Wind farm run
    wfrun = WFRun(casename=case, write_restart=True, restart_stress_out=True, cluster=Cluster(walltime='0:10:00'))
    wfpostflow = WFPostFlow(restart_stress_out=True)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TIs[0], zRef,
                   ad=ad, wfrun=wfrun, wfpostflow=wfpostflow)

    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)

    if run:
        # Run flow cases
        for TI in TIs:
            wfm.TI = TI
            wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)

    if post:
        # Post process lines
        for TI in TIs:
            wfm.TI = TI
            wfm.post_windfarm_flow(wd, ws, points=ext_points)

    if write:
        # Write speed up factor and TI to a file
        for k in range(len(TIs)):
            wfm.TI = TIs[k]
            folder = wfm.get_name()
            infile = '%s/post_flow_points_wd%g_ws%g/extracted.dat' % (folder, wd[0], ws[0])
            print('Wait for extracted.dat')
            while not os.path.exists(infile):
                time.sleep(5)
                sys.stdout.write('.', )
                sys.stdout.flush()
            nvar = 21
            iu = 5
            itke = 10
            flowdata = np.reshape(np.genfromtxt(infile, skip_header=True), (sDown_D.size, wd_rel.size, nvar))
            for i in range(sDown_D.size):
                label = get_downstream_label(sDown_D[i])
                outfile = case + subcases[k] + '_RANS_%sD.dat' % label
                f = open(outfile, 'w')
                f.write('# EllipSys3D-RANS, k-epsilon-fP\n')
                f.write('# Based on DOI: 10.1002/we.1736, rerun date=%s\n' % today)
                f.write('# Relative wd [deg], U/U0, TI (sqrt(2/3 tke)/U0)\n')
                for j in range(wd_rel.size):
                    f.write('%8.6f %8.6f %8.6f\n' % (wd_rel[j], flowdata[i, j, iu], np.sqrt(2.0 / 3.0 * flowdata[i, j, itke])))
                f.close()

    if plot:
        # plot
        for k in range(len(TIs)):
            lesfiles = []
            modelfiles = []
            modelfilesdown = []
            caseout = case + subcases[k]
            for i in range(sDown_D.size):
                label = get_downstream_label(sDown_D[i])
                lesfiles.append(data_path + caseout + '_LES_%sD.dat' % label)
                modelfilesdown.append(rans_path + caseout + '_RANS_%sD.dat' % label)
            modelfiles.append(modelfilesdown)
            # Compare with old results
            # modelfilesdown = []
            # for i in range(sDown_D.size):
            #     label = get_downstream_label(sDown_D[i])
            #     modelfilesdown.append(data_path + caseout + '_RANS_%sD.dat' % label)
            # modelfiles.append(modelfilesdown)
            # modellabels = ['RANS', 'RANS old']
            # modelcolors = ['g', 'r']
            modellabels = ['RANS']
            modelcolors = ['g']
            outfile = caseout + '.' + plotfmt
            plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile,
                           lesfiles=lesfiles, dataerrorbar=True)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    run_NREL5MW(run_machine, queue, grid=True, run=True, post=True, write=True, plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
