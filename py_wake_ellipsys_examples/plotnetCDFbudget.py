import numpy as np
import xarray
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.pyplot import cm


# PLOT STYLE ##################################
mpl.style.use('classic')
plt.rcParams['font.family'] = 'STIXGeneral'
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1
plt.rcParams['grid.linestyle'] = ':'
mpl.rcParams['lines.linewidth'] = 2
yd = dict(rotation=0, ha='right')
plt.close('all')

'''
Calculate TKE budget terms from flowdata.nc.
Only works for flat terrain simulations.

Buoyancy-related terms (activated with keMO=True):
Here you need to specify ustar and L inside calc_budget. Also, several formulations
of Sk and B exist, which you must choose between.
'''


# HELPER FUNCTIONS ##########################
def calc_budget(data, keMO='False', sigma_k=1, rho=1.225):
    # Store budget terms in this dictionary
    data_vars = {}

    # 1st derivatives
    diffx = data.differentiate(coord='x')
    diffy = data.differentiate(coord='y')
    diffz = data.differentiate(coord='z')

    # Calculate k-diffusion
    dkdx = data['muT'] / (rho * sigma_k) * diffx['tke']
    dkdy = data['muT'] / (rho * sigma_k) * diffy['tke']
    dkdz = data['muT'] / (rho * sigma_k) * diffz['tke']
    data_vars['Dk'] = dkdx.differentiate(coord='x') + \
        dkdy.differentiate(coord='y') + \
        dkdz.differentiate(coord='z')

    # Calculate advection term
    data_vars['adv'] = data['U'] * diffx['tke'] + \
        data['V'] * diffy['tke'] + \
        data['W'] * diffz['tke']

    # Calculate production
    uv = -data['muT'] / rho * (diffy['U'] + diffx['V'])
    vu = uv
    uw = -data['muT'] / rho * (diffz['U'] + diffx['W'])
    wu = uw
    vw = -data['muT'] / rho * (diffz['V'] + diffy['W'])
    wv = vw
    uu = 2. / 3. * data['tke'] - data['muT'] / rho * 2 * diffx['U']
    vv = 2. / 3. * data['tke'] - data['muT'] / rho * 2 * diffy['V']
    ww = 2. / 3. * data['tke'] - data['muT'] / rho * 2 * diffz['W']
    data_vars['P'] = - (uu * diffx['U'] + uv * diffy['U'] + uw * diffz['U']) \
                     - (vu * diffx['V'] + vv * diffy['V'] + vw * diffz['V']) \
                     - (wu * diffx['W'] + wv * diffy['W'] + ww * diffz['W'])

    # Calculate TKE dissipation
    data_vars['epsilon'] = data['epsilon']

    # Buoyancy related terms
    if keMO is True:
        # Need to change these parameters manually!!
        L = -180
        ustar = 3.85419962346641e-01
        gamma_1 = 16
        beta = 5
        kappa = 0.4
        cmu = 0.03
        C_kD = kappa**2 / (sigma_k * np.sqrt(cmu))
        zeta = data.z / L             # 1D array
        ones = data['U'] / data['U']  # 3D DataArray full of 1's

        # Calculate source term
        # (here based on B=cst in freestream)
        # (if based on B=cst*phi_m, then terms will look like van der Laan et. al 2017)
        if (L < 0):
            f_un = (2 - zeta) + gamma_1 / 2 * (1 - 12 * zeta + 7 * zeta**2) \
                - gamma_1**2 / 16 * zeta * (3 - 54 * zeta + 35 * zeta**2)
            phi_m = (1 - gamma_1 * zeta)**(-0.25)
            phi_eps = 1 - zeta
            Sk = ustar**3 / (kappa * L) * \
                (zeta**(-1) * (phi_m - phi_eps) - 1 - C_kD / 4 * phi_m**(6.5) * phi_eps**(-1.5) * f_un)
            data_vars['Sk'] = ones[:, :] * Sk  # Convert from 1D to 3D
        else:
            f_st = (2 - zeta) - 2 * beta * zeta * (1 - 2 * zeta + 2 * beta * zeta)
            phi_m = 1 + beta * zeta
            phi_eps = phi_m - zeta
            Sk = ustar**3 / (kappa * L) * (- C_kD / 4 * phi_m**(-3.5) * phi_eps**(-1.5) * f_st)
            data_vars['Sk'] = ones[:, :] * Sk  # Convert from 1D to 3D

        # Calculate buoyancy term
        # 1 = "Velocity gradient based" (currently implemented in EllipSys)
        # 2 = "Velocity gradient based" (but reduces to B=cst in freestream)
        # 3 = "Constant"                (independent of wake)
        bmethod = 1
        if (bmethod == 1):
            tmp = ones[:, :] * zeta / 1  # Convert from 1D to 3D
            data_vars['B'] = - data['muT'] / rho * (diffz['U']**2 + diffz['V']**2) * tmp
        elif (bmethod == 2):
            tmp = ones[:, :] * zeta / phi_m  # Convert from 1D to 3D
            data_vars['B'] = - data['muT'] / rho * (diffz['U']**2 + diffz['V']**2) * tmp
        elif (bmethod == 3):
            data_vars['B'] = - ustar**3 / (kappa * L) * ones

    # Assemble DataArrays to one DataSet
    budget_terms = xarray.Dataset(data_vars=data_vars)
    return budget_terms


# PARAMETERS ########################################
D = 126.0
zh = 90.0
UH = 8
x_ex = [-9 * D, -5 * D, 0 * D]   # At these x-positions a vertical budget profile is extracted
data = xarray.open_dataset('flowdata.nc')
budget_terms = calc_budget(data, keMO=False)
bl = budget_terms.interp(x=x_ex, y=0)  # "Vertical budget lines"


# PLOT XY-plane #############################
fig, ax = plt.subplots(1, 2, sharex='col', figsize=(12, 6),
                       gridspec_kw={'width_ratios': (30, 1)})
xyplane = data.interp(z=zh)
X, Y = np.meshgrid(xyplane.x, xyplane.y)
p = ax[0].contourf(X / D, Y / D, xyplane['U'].T / UH, np.linspace(0.4, 1.05, 10), cmap=cm.jet)
comcolRANS = plt.colorbar(p, cax=ax[1], orientation='vertical', aspect=100)
comcolRANS.set_label('$U/U_H$', rotation=0, ha='left')
comcolRANS.set_ticks(np.arange(0.3, 1.05, 0.1))
ax[0].axis('scaled')
ax[0].set_xlim(left=-10, right=5)
ax[0].set_ylim(bottom=-2, top=2)
ax[0].set_xlabel('$x/D$')
ax[0].set_ylabel('$y/D$', yd)
ax[0].set_title('At $z = z_{hub}$')
# Plot where budgets are extracted:
for i in range(len(x_ex)):
    ax[0].plot([x_ex[i] / D], [0], 'ko', markersize=8, label='extract budget' if i == 1 else '')
ax[0].legend(fontsize=12)


# PLOT BUDGETS ####################################
fig, ax = plt.subplots(1, len(x_ex), sharey=True, figsize=(12, 4))

for i in range(len(x_ex)):
    ax[i].plot(bl['Dk'][i], bl.z, linewidth=3, label=r'$D_k$' if i == 0 else "")
    ax[i].plot(bl['P'][i], bl.z, label=r'$P$' if i == 0 else "")
    ax[i].plot(-bl['epsilon'][i], bl.z, label=r'$- \varepsilon$' if i == 0 else "")
    ax[i].plot(bl['adv'][i], bl.z, label=r'$adv$' if i == 0 else "")
    if 'B' in list(budget_terms.keys()):
        ax[i].plot(bl['B'][i], bl.z, label='$B$' if i == 0 else "")
        ax[i].plot(-bl['Sk'][i], bl.z, label='$-S_k$' if i == 0 else "")
        ax[i].plot(bl['P'][i] + bl['B'][i] - bl['epsilon'][i] + bl['Dk'][i] - bl['Sk'][i],
                   bl.z, 'k', label=r'$D_k + P - \varepsilon + B - S_k$' if i == 0 else "")
    else:
        ax[i].plot(bl['P'][i] + - bl['epsilon'][i] + bl['Dk'][i], bl.z,
                   'k', label=r'$D_k + P - \varepsilon$' if i == 0 else "")
    ax[i].set_xlim(left=-0.05, right=0.06)
    ax[i].set_ylim(top=4 * zh)
    ax[i].plot(np.array(ax[i].get_xlim()), [zh + D / 2, zh + D / 2], 'k--')
    ax[i].plot(np.array(ax[i].get_xlim()), [zh - D / 2, zh - D / 2], 'k--')
    ax[i].set_xlabel('term [m$^2$/s$^3$]')
    ax[i].grid()
    ax[i].set_title('$x/D = %.1f$' % (x_ex[i] / D))

ax[0].set_ylabel('$z$ [m]', yd)
fig.legend(bbox_to_anchor=(0.5, 1.02), loc='lower center', fontsize=10,
           ncol=7, fancybox=True, shadow=True, scatterpoints=1, handlelength=2)
fig.savefig('Budget.pdf', bbox_inches='tight')
