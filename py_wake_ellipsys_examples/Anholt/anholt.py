import numpy as np
import sys
import os
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *


def get_wdWF_from_wdA15(wfm, wdNoWF, wdWF, ws, nWT, cases):
    # Load wind farm precursor results to rebin wd cases based on wd of A15
    # return wds to be run for simulation with wind farm
    wfm.set_subattr('ad.force', '0000')
    wfm.wfrun.casename = cases[0]
    folder = wfm.get_name()
    infile = folder + '/finished'
    print('Wait for precursor.dat files')
    while not os.path.exists(infile):
        time.sleep(5)
        sys.stdout.write('.', )
        sys.stdout.flush()

    L = np.array(wdNoWF).size
    wdA15 = np.zeros((L))
    ad_wd_ws = np.zeros((nWT, 2, L))
    for l in range(L):
        flowcase = wfm.wfrun.get_flowcase_name(wdNoWF, ws, l, 0)
        infile = folder + '/' + flowcase + '/precursor.dat'
        # infile = folder + '/wd' + str(wdNoWF[l]) + '_ws' + str(ws[0]) + '/precursor.dat'
        ad_wd_ws[:, :, l] = np.genfromtxt(infile, skip_header=True)
        wdA15[l] = np.round(ad_wd_ws[14, 0, l])  # We round to a deg

    LWF = np.array(wdWF).size
    wdWFrun = np.zeros((LWF))
    for l in range(LWF):
        wdWFrun[l] = wdNoWF[np.where(wdWF[l] == wdA15)[0][0]]
    return wdWFrun, ad_wd_ws


def run_Anholt(run_machine, queue, coarse=False, turbmodel='kefP', grid=False, prelib=False, pre=False,
               cal=False, runNoWF=False, runWF=False, write=False, plot=False, plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.hornsrev1 import Hornsrev1Site
    from py_wake_ellipsys_examples.data.farms.anholt import wt_x, wt_y
    from py_wake_ellipsys_examples.data.turbines.swt3p6_120 import SWT_3p6_120
    from py_wake.validation.validation_lib import GaussianFilter
    from py_wake_ellipsys_examples.plotwfres import plotWFrow, plotWFpower
    from py_wake_ellipsys_examples.Anholt.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Anholt offshore wind farm including coastal effects, based on DOI: 10.1088/1742-6596/854/1/012046
    # The terrain elavation is zero but the coast is modeled as a roughness change.
    # The chosen grid is a terrain o-grid instead of the rectangular grid as used in the original article.
    # When an ABL inflow model is used, we can encouter numerical problems for large domains
    # including a roughness change. The new setup using an o-grid seems to be more troublesome
    # and we cannot get convergence results when an ABL inflow is employed. Therefore, the default setup
    # in the example is a neutral surface layer inflow using the kefP turbulence model.
    # The present example is set up to make plots of the inflow wind speed gradient of the first row
    # of wind turbines (row A) and wind turbine power of the entire wind farm for wd=250+-5 deg.
    # SCADA and WRF data is also available for wd=250+-5, wd=260+-5, wd=270+-5 and wd=280+-5 deg.
    # We use a higher land roughness (0.2 m instead of 0.05) compared to the original article so
    # the terrain elevation is also represented by the land roughness.
    nWT = len(wt_x)
    type_i = np.zeros((nWT))
    wt = SWT_3p6_120()
    Dref = wt.diameter()
    zRef = 81.6
    # High ambient values following original article
    wdNoWF = np.arange(230.0, 271.0, 1.0)
    wdWF = np.arange(230.0, 275.0, 5.0)
    ws = [9.5]
    cases = ['Anholt_NoWFgrid', 'Anholt']
    ncases = len(cases)
    sigma = 5.0  # Estimated wind direction uncertainty [deg]
    zlen_D = 25.0
    linewidth = 1.5
    cRANS = 'g'
    today = date.today()
    z0Inlet = 0.0001
    if turbmodel in ['ke', 'keMO', 'kefP', 'keMOfP']:
        # ASL inflow, TI based on roughness only
        kappa = 0.4
        cmu = 0.03
        TI = kappa * np.sqrt(2.0 / 3.0) / (cmu ** 0.25 * np.log((zRef + z0Inlet) / z0Inlet))
        fcori = np.nan
        run_ws_con = True
        zlen_D = 25.0
    elif turbmodel in ['keABLc', 'keABLcfP', 'keABLp', 'keABLpfP']:
        # ABL inflow, TI based on roughness and lmax (ABL height)
        TI = 0.046
        latitude = 56.6
        fcori = 2.0 * 7.292115 * 10 ** (-5) * np.sin(latitude / 180.0 * np.pi)
        run_ws_con = False
        # A low domain height is used to avoid numerical problems of the inflow in combination with
        # roughness changes, although the current ABL setup still lead to convergence issues.
        zlen_D = 7.0
    else:
        print('ERROR:', turbmodel, 'unknown')
        sys.exit()

    rundir = os.getcwd()
    if grid:
        os.system('tar -xzf ' + data_path + 'map.tar.gz')
    terrain_map = rundir + '/anholt_coastlineDKonly_UTM32.map'
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 5e-2
        maxit = 10
        tolOpt = 1e-1
        grid_cells1_Ds = [0.1, 1.0]
        # grid_cells1_D = 2.0
        grid_cells2_D = 0.1
        zdistrs = ['exp', 'exp']
        grid_zWakeEnd_Ds = [3.0, 3.0]
        adgrid_nr = 5
        grid_bsizes = [32, 32]
        grid_bsize_cal = 48
        maxnodes = 4
        walltime_wf_run = '3:00:00'
        e3d_relaxu_wf = 0.9
    else:
        e3d_reslim = 1e-5
        maxit = 50
        tolOpt = 1e-3
        grid_cells1_Ds = [0.2, 8.0]
        grid_bsizes = [32, 64]
        grid_cells2_D = 0.2
        zdistrs = ['exp', 'uniwake']
        grid_zWakeEnd_Ds = [25.0, 3.0]
        adgrid_nr = 10
        grid_bsize_cal = 32
        maxnodes = 21
        walltime_wf_run = '36:00:00'
        e3d_relaxu_wf = 0.9

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1001', grid=ADGrid(nr=adgrid_nr, ntheta=32), run_pre=True)
    # Wind farm grid
    wfgrid = TerrainGrid(Dref, type='terrainogrid',
                         zFirstCell_D=0.5 / Dref, radius_D=2e5 / Dref, cells1_D=grid_cells1_Ds[0],
                         zlen_D=zlen_D, zdistr=zdistrs[0], bsize=grid_bsizes[0],
                         m1_w_D=5.0, m1_e_D=20.0,
                         m1_n_D=15.0, m1_s_D=5.0,  # To resolve the wind farm wake for 230.0 < wd < 270
                         cells2_D=grid_cells2_D,
                         m2_w_D=500.0, m2_e_D=100.0,
                         m2_n_D=100.0, m2_s_D=250.0,
                         # grid_terrain_surf_only=True,
                         terrain_map=terrain_map,
                         terrain_map_smooth=False,
                         terrain_map_farfield_z0=z0Inlet,
                         terrain_map_farfield_h=0.0,
                         terrain_map_dxy_inner=100.0, terrain_map_dxy_outer=400.0,
                         terrain_map_Lxy_inner=100000.0, terrain_map_Lxy_outer=600000.0,
                         terrain_surfgen='sfhill',
                         terrain_3dgen='extrude3d',
                         cluster=Cluster(walltime='1:00:00'))
    # Constants
    cnst = Cnst(fcori=fcori, z0=z0Inlet)
    # ABL inflow precursor optimizer
    preopt = PreOpt(maxit=maxit, tolOpt=tolOpt, cluster=Cluster(walltime='0:30:00'))
    # Wind farm run
    wfrun = WFRun(casename=cases[0], cluster=Cluster(walltime=walltime_wf_run))
    # EllipSys 3D parameters
    e3d = E3D(turbmodel=turbmodel, reslim=e3d_reslim)
    # Constants
    cnst = Cnst(fcori=fcori, z0=z0Inlet)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   cnst=cnst, e3d=e3d, ad=ad, wfrun=wfrun,
                   run_wd_con=False, run_ws_con=run_ws_con)
    wfm.calrun.cluster.walltime = '10:00:00'

    # Create calibration grid (use smaller block size to speed up calibration)
    wfm.calrun.grid.bsize = grid_bsize_cal
    wfm.calrun.grid.cells1_D = grid_cells1_Ds[1]
    wfm.calrun.grid.zWakeEnd_D = grid_zWakeEnd_Ds[1]
    wfm.calrun.grid.zdistr = zdistrs[1]

    # Run step by step:
    if grid:
        # Create precursor grid
        # wfm.create_precursor_grid()
        # Create AD grid
        for type in np.unique(type_i):
            print(type)
            wfm.create_adgrid(type)
        # Create calibration grid (use smaller block size to speed up calibration)
        for type in np.unique(type_i):
            wfm.create_calibration_grid(type)
        # Create wind farm grids with and without inner refined grid
        for i in range(ncases):
            wfm.wfrun.casename = cases[i]
            grid = wfm.grid
            grid.zdistr = zdistrs[i]
            grid.cells1_D = grid_cells1_Ds[i]
            grid.bsize = grid_bsizes[i]
            grid.zWakeEnd_D = grid_zWakeEnd_Ds[i]
            wfm.grid = grid
            wfm.create_windfarm_grid(wt_x, wt_y)

    if prelib and turbmodel in ['keABLc', 'keABLcfP', 'keABLp', 'keABLpfP']:
        # Inflow precursor library for ABL models
        Ro0 = 10**np.arange(5.0, 10.2, 0.2)
        Rol = 10**np.append(np.arange(2.0, 3.5, 0.1), np.arange(3.5, 4.7, 0.05))
        wfm.run_precursor_lib(Ro0, Rol, 'cori')
        if turbmodel in ['keABLp', 'keABLpfP']:
            Ro0_tilde = 10**np.arange(5.0, 10.2, 0.2)
            Rol_tilde = 10**np.append(np.arange(2.0, 3.5, 0.1), np.arange(3.5, 4.7, 0.05))
            wfm.run_precursor_lib(Ro0_tilde, Rol_tilde, 'press')

    if pre and turbmodel in ['keABLc', 'keABLcfP', 'keABLp', 'keABLpfP']:
        # We need an inflow profile for each wind speed in the force calibration run
        # ws_pre = np.arange(4.0, 13.0, 1.0)
        wfm.pre = preopt
        ws_pre = np.array([4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 9.28, 10.0, 11.0, 12.0])
        wfm.run_precursor_opt(ws_pre)

    if cal:
        # Run force control calibration
        wfm.casename = cases[1]
        wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[1])
        # Set lower cut out wind speed for faster force calibration simulations when an ABL inlow.
        # This is possible because we only interested in a 9.0 m/s flow case
        wt.cutout = [12.0, 12.0]
        wfm.wt = wt
        for type in np.unique(type_i):
            wfm.run_calibration(type)
        wt.cutout = [25.0, 25.0]
        wfm.wt = wt

    if runNoWF:
        # Run flow parametric run of wds to find the wind direction at wt A15 as precursor to the wind farm simulations
        wfm.set_subattr('ad.force', '0000')
        grid = wfm.grid
        grid.cells1_D = grid_cells1_Ds[0]
        grid.bsize = grid_bsizes[0]
        wfm.grid = grid
        wfm.wfrun.casename = cases[0]
        wfm.run_windfarm(wt_x, wt_y, wdNoWF, ws, type_i)

    if runWF:
        # Run wind farm simulation using the wind directions based on wt A15
        wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[0])
        wdWFrun, ad_wd_ws = get_wdWF_from_wdA15(wfm, wdNoWF, wdWF, ws, nWT, cases)
        wfm.set_subattr('ad.force', '1001')
        wfm.wfrun.casename = cases[1]
        wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[1])
        wfm.set_subattr('grid.bsize', grid_bsizes[1])
        print(wdWFrun)
        wfm.set_subattr('e3d.relaxu', e3d_relaxu_wf)
        wfm.run_windfarm(wt_x, wt_y, wdWFrun, ws, type_i)
        wfm.set_subattr('e3d.relaxu', 0.9)

    if write:
        # Write results of disk averaged inflow wind speed for row A
        wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[0])
        wdWFrun, ad_wd_ws = get_wdWF_from_wdA15(wfm, wdNoWF, wdWF, ws, nWT, cases)
        L = np.array(wdNoWF).size
        A_ws_ave = np.zeros((30))
        n = 0
        for l in range(L):
            A15_wd = np.round(ad_wd_ws[14, 0, l])  # We round to a deg
            if 245.0 <= A15_wd and A15_wd < 255.0:
                A_ws_ave = A_ws_ave + ad_wd_ws[0:30, 1, l]
                n = n + 1
        A_ws_ave = A_ws_ave / float(n)
        # Output
        filename = 'Anholt_RANS_RowA_wd250.dat'
        f = open(filename, 'w')
        f.write('# EllipSys3D-RANS-AD %s\n' % wfm.e3d.turbmodel)
        f.write('# Based on DOI: 10.1002/we.1804, rerun date=%s\n' % today)
        f.write('# wd=250 +- 5 based on wd at A15\n')
        f.write('# ws_norm = Wind speed normalized by wind speed at A15\n')
        f.write('# wt A_i, ws_norm\n')
        for i in range(30):
            f.write('%3i %8.6f\n' % (i + 1, A_ws_ave[i] / A_ws_ave[14]))
        f.close()

        # Write results of wind farm power
        wfm.set_subattr('ad.force', '1001')
        wfm.wfrun.casename = cases[1]
        wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[1])

        WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
        wdbin = 250.0
        # Gaussian averaging
        l = np.where(wdWF == wdbin)[0][0]
        powerGA = np.zeros(power_ilk.shape)
        lGA1 = l - 4
        lGA2 = l + 5
        lAve1 = l - 1
        lAve2 = l + 2
        for i in range(nWT):
            powerGA[i, :, 0] = GaussianFilter(power_ilk[i, lGA1:lGA2, 0], wdWF[lGA1:lGA2], 3, sigma)
        # Average over 3 wds:
        ProwAve = power_ilk[:, lAve1:lAve2, 0].sum(axis=1)
        ProwAveGA = powerGA[:, lAve1:lAve2, 0].sum(axis=1)

        # Output
        filename = 'Anholt_RANS_Power_wd%i.dat' % int(wdbin)
        f = open(filename, 'w')
        f.write('# EllipSys3D-RANS-AD %s\n' % wfm.e3d.turbmodel)
        f.write('# Based on DOI: 10.1088/1742-6596/854/1/012046, rerun date=%s\n' % today)
        f.write('# GA with sigma=%6.4f deg\n' % sigma)
        f.write('# Linear average for wd=%i+-5 deg\n' % int(wdbin))
        f.write('# P15=%10.2f, P15GA=%10.2f\n' % (ProwAve[14], ProwAveGA[14]))
        f.write('# WT, Pi/P15,     Pi/P15+GA\n')
        for i in range(nWT):
            f.write('%-5i %10.8f %10.8f\n' % (i + 1, ProwAve[i] / ProwAve[14], ProwAveGA[i] / ProwAveGA[14]))
        f.close()

    if plot:
        # Row A plot
        datafile = data_path + 'Anholt_WFdata_wd250.dat'
        modelfiles = [data_path + 'Anholt_WRF_wd250.dat', rans_path + 'Anholt_RANS_RowA_wd250.dat']
        modellabels = ['WRF', 'RANS']
        modelcolors = ['r', 'g']
        outfile = 'Anholt_RowA_wd250.' + plotfmt
        plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, np.linspace(0, 29, 30), wt.diameter(), 250.0, ylabel='$U_i/U_{15}$', GA=False, figx=9.0, xticks=np.arange(5, 31, 5).tolist(), wfy=0.7)

        # Wind turbine power plot
        # Convert data from ws to power and write to a temporary file
        data = np.genfromtxt(datafile, skip_header=True)
        dataNorm = 8.553
        dataPower = wt.power(data[:, 1] * dataNorm)
        datafilepower = 'Anholt_WFdata_power_wd250.dat'
        f = open(datafilepower, 'w')
        f.write('Interpolated Power\n')
        for i in range(nWT):
            f.write('%-5i %10.2f\n' % (data[i, 0], dataPower[i] / dataPower[14]))
        f.close()
        modelfiles = [rans_path + 'Anholt_RANS_Power_wd250.dat']
        modellabels = ['RANS']
        outfile = 'Anholt_Power_wd250.' + plotfmt
        plotWFpower(datafilepower, modelfiles, modellabels, outfile, (np.array(wt_x) - wt_x[14]) / wt.diameter(), (np.array(wt_y) - wt_y[14]) / wt.diameter(), 250.0, xlabel='$(x-x_{A15})/D$', ylabel='$(y-y_{A15})/D$', clabel='$P_i/P_{A15}$')


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Run with a very coarse setup if coarse=True (which still takes an hour to run).
    # It recommended to use the kefP turbulence model. One could try to use keABLcfP turbulence model, as used
    # in the orignal article but convergence problems are likely to occur.
    run_Anholt(run_machine, queue, coarse=True, turbmodel='kefP', grid=True, prelib=False, pre=False,
               cal=True, runNoWF=True, runWF=True, write=True, plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
