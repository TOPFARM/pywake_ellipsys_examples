import numpy as np
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake.examples.data.hornsrev1 import Hornsrev1Site
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular


def my_own_wind_farm_simulation():
    # Define arrays for C_T, Power and RPM as function of the freestream wind speed.
    # For example: ct_curve = np.array([[3.0, 0.0], [4.0, 0.8], ..., [25.0, 0.05]])
    ct_curve = np.array([[]])
    cp_curve = np.array([[]])
    power_curve = np.array([[]])
    rpm_curve = np.array([[]])

    # Alternatively, one could use existing aerodynamic data:
    from py_wake_ellipsys_examples.data.turbines.nrel5mw_reduced import nrel5mw_ct_curve as ct_curve
    from py_wake_ellipsys_examples.data.turbines.nrel5mw_reduced import nrel5mw_cp_curve as cp_curve
    from py_wake_ellipsys_examples.data.turbines.nrel5mw_reduced import nrel5mw_power_curve as power_curve
    from py_wake_ellipsys_examples.data.turbines.nrel5mw_reduced import nrel5mw_rpm_curve as rpm_curve

    # Define wt_name, diameter, hub height, rated_powers, cutin and cutout
    class mywt(EllipSysOneTypeWT):
        def __init__(self):
            EllipSysOneTypeWT.__init__(self, name='',
                                       diameter=,
                                       hub_height=,
                                       cutin=4.0,
                                       cutout=25.0,
                                       dws=1.0,
                                       rated_power=,
                                       rotdir='cw',
                                       powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                      power=power_curve[:, 1], power_unit='kW',
                                                                      ct=ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                       cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                       rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1], left=0.0, right=0.0),
                                       # The input below is not needed for the ad_force=2111,
                                       pitch_func='',
                                       bladeloading_func='',
                                       airfoildata_file='',
                                       airfoildata_path='',
                                       airfoildata_file_type='',  # 1=flex5, 2=hawc2
                                       bladegeo_file='',
                                       bladegeo_path='')

    # Define the wind farm layout
    wt_x = []
    wt_y = []
    type_i = np.array([0, ..., 0])

    # Define the wind farm simulation
    wt = mywt()
    Dref = wt.diameter()
    zRef =  # Reference height at which the turbulence intensity is defined, normally the hub height.
    Ti =  # Turbulence intensity based on the turbulent kinetic energy, i.e. 0.1 is 10%.

    # AD
    ad = AD(force='2111')
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref,
                         cells1_D=2.0, zFirstCell_D=0.005,
                         m1_w_D=2, m1_e_D=12, m1_s_D=1.5,
                         m1_n_D=1.5, radius_D=500.0)
    # Wind farm simulation
    wfrun = WFRun(casename='MyWindFarmSim', write_restart=True, cluster=Cluster(walltime='0:19:00'))
    # Post flow with netCDF output
    wfpostflow = WFPostFlow(outputformat='netCDF', single_precision_netCDF=True)
    # EllipSys params
    e3d = E3D(turbmodel='kefP', reslim=1e-3)
    # Instantiate
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, Ti, zRef,
                   e3d=e3d, ad=ad, wfrun=wfrun, wfpostflow=wfpostflow)

    # Set global cluster params
    maxnodes = 1

    # Gbar
    run_machine = 'gbar'
    corespernode = 24
    queue = 'hpc'

    # Sophia
    # run_machine = 'sophia'
    # corespernode = 32
    # queue = 'windq'

    # Run local (do not execute this script in the login node but use an interactide node:
    # Sophia: srun -N 1 --ntasks-per-node=32 --time=0:59:00 --partition=workq --pty bash
    # run_machine = 'local'

    set_cluster_vars(run_machine, True, queue, corespernode, maxnodes)

    # Run step by step:
    # Create AD grid
    wfm.create_adgrid(type_i[0])
    # Create calibration grid
    wfm.create_calibration_grid(type_i[0])
    # Create wind farm grid
    wfm.create_windfarm_grid(wt_x, wt_y)
    # Run force control calibration
    wfm.run_calibration(type_i[0])
    # Run flow cases (wind direction and wind speeds)
    wd = []
    ws = []
    wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)
    WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
    # Store 3D flow data as a netCDF file for each flow case
    wfm.post_windfarm_flow(wd, ws)
    # One could plot the results using py_wake_ellipsys_examples/plotnetCDFall.py


if __name__ == '__main__':
    my_own_wind_farm_simulation()
