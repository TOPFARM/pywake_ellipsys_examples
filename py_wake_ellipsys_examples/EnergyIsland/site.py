from py_wake.site._site import UniformWeibullSite
import numpy as np
from py_wake_ellipsys_examples.EnergyIsland import EnergyIsland_path


# NEWA data taken at lat, long = 56.4, 6.3 and z=150 m
infile = 'site_newa2006_2018_dwd5.dat'
data = np.genfromtxt(EnergyIsland_path + infile, skip_header=True)


class EnergyIslandSite(UniformWeibullSite):
    def __init__(self, ti=0.047):
        p_wd = data[:, 0]
        f = data[:, 4]
        a = data[:, 2]
        k = data[:, 1]
        UniformWeibullSite.__init__(self, np.array(f) / np.sum(f), a, k, ti)
