import numpy as np
import sys
import os
import time
import xarray
from datetime import date
from py_wake.tests import npt
from py_wake_ellipsys.utils.lut import calc_lut, define_lut, run_lut
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.lib import awf_ctcp_to_netcdf
from py_wake.wind_farm_models.wind_farm_model import WindFarmModel


def run_and_save_lut(wfm, zRef, Dref, Prated, ws, wd, wt_x, wt_y, type_i, ADcontrolfile, flowmap_maxpoints, outname='results.nc'):
    nWT = len(wt_x)
    L = len(wd)
    K = len(ws)
    power_ilk = np.zeros((nWT, L, K))
    ct_ilk = np.zeros((nWT, L, K))
    WS_eff_ilk = np.zeros((nWT, L, K))
    TI_eff_ilk = np.zeros((nWT, L, K))
    for l in range(L):
        for k in range(K):
            print('outname=', outname, 'wd=', wd[l], 'ws=', ws[k])
            sys.stdout.flush()
            power_i, ct_i, WS_eff_i = run_lut(wfm, zRef, Dref, Prated, [ws[k]], [wd[l]], wt_x, wt_y,
                                              type_i, ADcontrolfile, flowmap_maxpoints=flowmap_maxpoints)
            # print(power_i.shape, power_ilk.shape)
            power_ilk[:, l, k] = power_i[:, 0, 0]
            ct_ilk[:, l, k] = ct_i[:, 0, 0]
            WS_eff_ilk[:, l, k] = WS_eff_i[:, 0, 0]
    # Store results
    data = xarray.Dataset({'power': xarray.DataArray(power_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                                 ('wd', wd),
                                                                 ('ws', ws)]),
                           'ct': xarray.DataArray(ct_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                           ('wd', wd),
                                                           ('ws', ws)]),
                           'WS_eff': xarray.DataArray(WS_eff_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                                   ('wd', wd),
                                                                   ('ws', ws)]),
                           'TI_eff': xarray.DataArray(TI_eff_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                                   ('wd', wd),
                                                                   ('ws', ws)])})
    data.to_netcdf(path=outname)


def calc_ctcp_post(ws, wd, wf_type_i, Dref, test=False):
    if test:
        awf_type = np.asarray(np.where(awf_wf_type_i_all == 0)[0], dtype=int)
        wf_type_i = wf_type_i[awf_type]
    Cnst = 0.5 * 1.225 * Dref ** 2 * 0.25 * np.pi
    Imax = int(max([len(np.asarray(np.where(wf_type_i == type)[0], dtype=int)) for type in np.unique(wf_type_i)]))
    # CTCPwf = np.zeros((2, len(np.unique(wf_type_i)), Imax, len(wd), len(ws)))
    # CTCPwf[:, :, :, :, :] = np.nan
    CTCPwf = np.zeros((2, len(np.unique(wf_type_i)), len(wd), len(ws)))
    CTCPwt = np.zeros((2, len(np.unique(wf_type_i)), Imax, len(wd), len(ws)))
    CTCPwf[:, :, :, :] = np.nan
    CTCPwt[:, :, :, :, :] = np.nan
    for type in np.unique(wf_type_i):
        data = xarray.open_dataset('wf_results_type_%i.nc' % type)
        nWT = len(data['power'][:, 0, 0])
        I = data['ct'].shape[0]

        # CTwf
        CTCPwf[0, type, :, :] = (data['ct'] * data['WS_eff'] ** 2 / ws ** 2).mean(dim='wt')
        # CPwf
        CTCPwf[1, type, :, :] = data['power'].sum(dim='wt') / (nWT * Cnst * ws ** 3)
        # CTstar
        CTCPwt[0, type, 0:I, :, :] = data['ct']
        # CP
        CTCPwt[1, type, 0:I, :, :] = data['power'] / (Cnst * ws ** 3)

        # CTCPwf[0, type, 0:I, :, :] = (data['ct'] * data['WS_eff'] ** 2 / ws ** 2)
        # CTCPwf[1, type, 0:I, :, :] = (data['power'] / (Cnst * ws ** 3))
    if test:
        # Test first WF (type=0), wd=240 and  ws=10
        CTCPwf_expected = np.array([0.72532678, 0.38944476])
        # npt.assert_array_almost_equal(np.nanmean(CTCPwf, axis=2)[:, 0, 0, 0], CTCPwf_expected, 6)
        npt.assert_array_almost_equal(CTCPwf[:, 0, 0, 0], CTCPwf_expected, 6)

    awt_ctcp_from_lut = awf_ctcp_to_netcdf(CTCPwf, CTCPwt, ws, wd)
    awt_ctcp_from_lut.to_netcdf(path='awf_ctcp_type_all.nc', mode='w')


class MyLUT(WindFarmModel):
    def __init__(self, site, windTurbines, casename='', assemble='ws'):
        WindFarmModel.__init__(self, site, windTurbines)
        self.casename = casename
        self.assemble = assemble

    def calc_wt_interaction(self, x_ilk, y_ilk, h_i, type_i, wd, ws, **kwargs):
        if np.asarray(x_ilk).ndim == 3:
            x_i = x_ilk[:, 0, 0]
        else:
            x_i = x_ilk
        if np.asarray(y_ilk).ndim == 3:
            y_i = y_ilk[:, 0, 0]
        else:
            y_i = y_ilk
        wd_bin_size = 360 / len(np.atleast_1d(wd))
        localWind = self.site.local_wind(x=x_i, y=y_i, h=h_i, wd=wd, ws=ws, wd_bin_size=wd_bin_size)
        shape = (len(localWind.i), len(wd), len(ws))
        WS_eff_ilk = np.zeros(shape)
        power_ilk = np.zeros(shape)
        ct_ilk = np.zeros(shape)
        shape = (len(localWind.i), len(localWind.wd), len(localWind.ws))
        TI_eff_ilk = np.zeros(shape)

        if self.assemble == 'ws':
            for k in range(len(ws)):
                # data = xarray.open_dataset('%sresults1e7_ws%g.nc' % (self.casename, ws[k]))
                data = xarray.open_dataset('%s_ws%g.nc' % (self.casename, ws[k]))
                WS_eff_ilk[:, :, k] = data['WS_eff'][:, :, 0]
                TI_eff_ilk[:, :, k] = data['TI_eff'][:, :, 0]
                power_ilk[:, :, k] = data['power'][:, :, 0]
                ct_ilk[:, :, k] = data['ct'][:, :, 0]
        else:
            data = xarray.open_dataset('%s.nc' % self.casename)
            WS_eff_ilk = data['WS_eff'].values
            TI_eff_ilk = data['TI_eff'].values
            power_ilk = data['power'].values
            ct_ilk = data['ct'].values

        kwargs_ilk = {'type_i': type_i, 'x_ilk': x_ilk, 'y_ilk': y_ilk, 'h_ilk': h_i}

        return WS_eff_ilk, TI_eff_ilk, power_ilk, ct_ilk, localWind, kwargs_ilk


def post_aep(Site, wt, ws, wd, wt_x, wt_y, casename, assemble):
    wfm = MyLUT(Site, wt,
                casename=casename, assemble=assemble)
    simres = wfm(wt_x, wt_y, wd=wd, ws=ws)
    aep = float(simres.aep(with_wake_loss=True).sum())
    aep_NoWake = float(simres.aep(with_wake_loss=False).sum())
    wake_loss = (aep - aep_NoWake) / aep_NoWake
    return aep, aep_NoWake, wake_loss


if __name__ == '__main__':
    """
    Most of the RANS LUT methods are performed by saving data during a calculation in order to save memory

    Usage
    1) Test script
       python lutwf.py test
    2) Calculate wind farm CT and CP per wind farm with type=wf_type and also save wt power for aep per wf
       python lutwf.py ctcp_run <wf_type>
    3) Combine all .nc files from 2) to a single one
       python lutwf.py ctcp_post
    4) Calculate AEP per wind farm (without wf to wf effects) from 2)
       python lutwf.py aepperwf_post
    5) Calculate wt power the entire cluster per ws
       python lutwf.py aep_run <ws>
    6) Post process 5) to calculate cluster AEP
       python lutwf.pt aep_post
    """
    starttime = time.time()
    # Global setup
    from py_wake_ellipsys.utils.wtgeneric import WTgen
    from py_wake_ellipsys_examples.EnergyIsland.site import EnergyIslandSite
    from py_wake_ellipsys_examples.EnergyIsland import EnergyIsland_path
    Site = EnergyIslandSite()
    from py_wake.site.shear import LogShear
    Ti = 0.047
    cmu = 0.03
    kappa = 0.4
    zRef = 150.0
    Dref = 236.0
    uStar_UH = Ti * np.sqrt(1.5 * np.sqrt(cmu))
    z0 = zRef / (np.exp(kappa / uStar_UH) - 1)
    print('z0', z0)
    Site.shear = LogShear(h_ref=zRef, z0=z0, interp_method='linear')
    cnst_density = 1.225
    wt = WTgen(D=Dref, zH=zRef, P_rated=15e3, ct_rated=0.8, cp_rated=0.45, tsr_rated=8.0, ws_cutin=3.95, ws_cutout=25.0, dws=0.05)

    from py_wake_ellipsys_examples.EnergyIsland import EnergyIsland_path
    '''
    # The script can be quick tested by using 2 small wind farms
    nWF = 2
    awf_wt_x_all = np.array([0.0, 5 * Dref, 0.0, 5 * Dref])
    awf_wt_y_all = np.asarray([0.0, 0.0, 5 * Dref, 5 * Dref])
    awf_wt_type_i_all = np.array([0, 0, 0, 0], dtype=int)
    awf_wf_type_i_all = np.array([0, 0, 1, 1], dtype=int)
    '''

    nWF = 10
    nWT = nWF * 67
    awf_wt_type_i_all = np.zeros((nWT), dtype=int)
    awf_wf_type_i_all = []
    awf_wt_x_all = []
    awf_wt_y_all = []
    awf_name_all = []

    for i in range(nWF):
        filename = EnergyIsland_path + 'layout%i.dat' % (i + 1)
        layout = np.genfromtxt(filename)
        awf_wt_x_all = awf_wt_x_all + list(layout[:, 0])
        awf_wt_y_all = awf_wt_y_all + list(layout[:, 1])
        awf_name_all.append('WF%i' % (i + 1))
        awf_wf_type_i_all = awf_wf_type_i_all + [i] * 67

    awf_wt_x_all = np.asarray(awf_wt_x_all)
    awf_wt_y_all = np.asarray(awf_wt_y_all)
    awf_wt_type_i_all = np.asarray(awf_wt_type_i_all, dtype=int)
    awf_wf_type_i_all = np.asarray(awf_wf_type_i_all, dtype=int)

    path_LUT = '/groups/PyWakeEllipSys/LUT/EnergyIsland_WTgen15MW.nc'
    ADcontrolfile = EnergyIsland_path + 'LUTwf/WTgen_Jou_rot_shear_kefP_8cD_Ti0.047_zeta0.dat'
    Prated = 15e6

    wfm = define_lut(Site, wt, zRef, Dref, path_LUT, shearcor=False)

    if sys.argv[1] == 'test':
        # Test WF power and thrustWF power and thrust for 1 WF and flow case
        test = True
        ws = np.array([10.0])
        wd = np.array([240.0])
        type = 0
        nWF = 1
    elif sys.argv[1] in ['ctcp_run', 'ctcp_post', 'aepperwf_post', 'aep_run', 'aep_post']:
        # Calculate WF power and thrust per wind farm and store as netCDF files
        test = False
        wd = np.arange(0.0, 360.0, 5.0)
        ws = np.arange(4.0, 25.0 + 1.0, 1.0)
    if sys.argv[1] in ['ctcp_run', 'test']:
        if sys.argv[1] == 'ctcp_run':
            type = int(sys.argv[2])
        print('start awf_type=', type)
        sys.stdout.flush()
        awf_type = np.asarray(np.where(awf_wf_type_i_all == type)[0], dtype=int)
        outname = 'wf_results_type_%i.nc' % type
        run_and_save_lut(wfm, zRef, Dref, Prated, ws, wd,
                         awf_wt_x_all[awf_type], awf_wt_y_all[awf_type], awf_wt_type_i_all[awf_type],
                         ADcontrolfile, 20000, outname=outname)
    if sys.argv[1] in ['ctcp_post', 'test']:
        # Assemble netCDF files and calculate WF power and thrust coefficients
        calc_ctcp_post(ws, wd, awf_wf_type_i_all, Dref, test=test)
    if sys.argv[1] in ['aepperwf_post', 'test']:
        # Calculate AEP per single wind farm
        aep_all = np.zeros((nWF))
        aep_NoWake_all = np.zeros((nWF))
        for i in range(nWF):
            outname = 'wf_results_type_%i' % i
            awf_type = np.asarray(np.where(awf_wf_type_i_all == i)[0], dtype=int)
            aep, aep_NoWake, wake_loss = post_aep(Site, wt, ws, wd, awf_wt_x_all[awf_type], awf_wt_y_all[awf_type], outname, None)
            aep_all[i] = aep
            aep_NoWake_all[i] = aep_NoWake
            print('wf %i' % i)
            print('aep_NoWake', aep_NoWake)
            print('aep', aep)
            print('wake_loss', wake_loss)
        print('Total aep_NoWake', sum(aep_NoWake_all))
        print('Total aep', sum(aep_all))
        wake_loss_all = (sum(aep_all) - sum(aep_NoWake_all)) / sum(aep_NoWake_all)
        print('Total wake_loss, no WF to WF interaction', wake_loss_all)
        if test:
            # Test first WF (type=0) aep for wd=240 and  ws=10
            wake_loss_expected = -0.13456720050720772
            npt.assert_array_almost_equal(wake_loss_all, wake_loss_expected, 6)
    if sys.argv[1] in ['aep_run', 'test']:
        # Entire cluster per ws
        if sys.argv[1] == 'aep_run':
            ws = np.array([float(sys.argv[2])])
        # wd = np.array([0.0])
        outname = 'cluster_results_ws%g.nc' % ws[0]
        if sys.argv[1] == 'test':
            awf_wt_x_all = awf_wt_x_all[awf_type]
            awf_wt_y_all = awf_wt_y_all[awf_type]
            awf_wt_type_i_all = awf_wt_type_i_all[awf_type]
        run_and_save_lut(wfm, zRef, Dref, Prated, ws, wd,
                         awf_wt_x_all, awf_wt_y_all, awf_wt_type_i_all,
                         ADcontrolfile, 6030, outname=outname)
    if sys.argv[1] in ['aep_post', 'test']:
        # Calculate cluster AEP
        if sys.argv[1] == 'test':
            awf_wt_x_all = awf_wt_x_all[awf_type]
            awf_wt_y_all = awf_wt_y_all[awf_type]
            awf_wt_type_i_all = awf_wt_type_i_all[awf_type]
        # ws = np.arange(4.0, 25.0 + 1.0, 1.0)
        # ws = np.array([8.0])
        # wd = np.arange(0.0, 360.0, 5.0)
        outname = 'cluster_results'
        aep, aep_NoWake, wake_loss = post_aep(Site, wt, ws, wd, awf_wt_x_all, awf_wt_y_all, outname, 'ws')
        print('aep_NoWake', aep_NoWake)
        print('aep', aep)
        print('wake_loss', wake_loss)
        if test:
            # Test first WF (type=0) aep for wd=240 and  ws=10
            wake_loss_expected = -0.13456720050720772
            npt.assert_array_almost_equal(wake_loss, wake_loss_expected, 6)
        COWI_aep_NoWake = 5480 + 5477.9 + 5458.8 + 5500.1 + 5497.6 + 5481.3 + 5445 + 5422.7 + 5423.9 + 5409.2
        COWI_aep = 5214.4 + 5209.5 + 5192.8 + 5241.8 + 5227 + 5190.2 + 5182.9 + 5146.9 + 5145.1 + 5136.8
        print('COWI_aep_NoWake', COWI_aep_NoWake)
        COWI_wake_loss = (COWI_aep - COWI_aep_NoWake) / COWI_aep_NoWake
        print('COWI_wake_loss', COWI_wake_loss)
    endtime = time.time()
    # print('Total time:', endtime - starttime, 'sec')
    print('Total time, awf_type:', type, endtime - starttime, 'sec')
