import numpy as np
import sys
import os
import re
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.site._site import UniformWeibullSite


class UniformSite(UniformWeibullSite):
    def __init__(self, A, k, nsector=12.0, shear=None):
        UniformWeibullSite.__init__(self, np.ones(int(nsector)) / float(nsector), [A] * int(nsector), [k] * int(nsector), .1, shear=shear)


def run_farm(run_machine, queue, n, s, Dref, rho, zH_D, ws_rated, ct_rated, cp_rated, tsr_rated,
             TI, A, k, ws_cutin, ws_cutout, nws, nwd,
             coarse=False, grid=False, cal=False, run=False, postWFeff=False):
    from py_wake_ellipsys.utils.wtgeneric import WTgen
    # Run a square wind farm layout with a symmetric simulation setup (no wake rotation nor Coriolis).
    # We assume flat terrain and a constant TI for all ws and wd.
    # We a relative large fist cell height that causes a numerical error in wind speed with respect to the loglaw
    # We overcome this by using a numerical loglaw that is then used to correct for this error.
    # More info: https://doi.org/10.1088/1742-6596/2265/2/022030
    # The difference with the Torque article is that we use a force calibration instead of the 1D mom control
    # and the Torque article results for Table 2 were run with a new, faster AD model
    # TODO: we need to perform a grid refinement study for AEP

    # A and k based on a uniform site
    Site = UniformSite(A=A, k=k)

    # Flow cases
    wd = np.arange(0.0, 360.0, 360.0 / float(nwd))
    ws = np.linspace(ws_cutin, ws_cutout, nws)

    # A square layout
    wt_x = np.zeros((n ** 2))
    wt_y = np.zeros((n ** 2))
    for i in range(n):
        for j in range(n):
            k = i * n + j
            wt_x[k] = i * Dref * s
            wt_y[k] = j * Dref * s

    # A generic wind turbine including TI-smoothing to be more realistic and mitigate grid dependence.
    # Without smoothing, the cp and ct curves have a sharp transition around rated that requires a much fine grid than D/8
    # in order to get AEP grid effects in the order of 0.1-0.2%.
    zH = zH_D * Dref
    nWT = len(wt_x)
    h_i = np.array([zH] * nWT)
    type_i = np.zeros((nWT))
    wt = WTgen(ti=TI, D=Dref, zH=zH, P_rated=-1, ws_rated=ws_rated, ct_rated=ct_rated, cp_rated=cp_rated, tsr_rated=tsr_rated,
               rho=rho, ws_cutin=ws_cutin, ws_cutout=ws_cutout, dws=(ws_cutout - ws_cutin) / float(nws - 1),
               ct_method='power_law', ct_exp=3.2)
    print(wt.P_rated)
    zRef = zH
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 1e-2
        grid_cells1_D = 2.0
        adgrid_nr = 5
        maxnodes = 1
        walltime_wf_run = '0:20:00'
        grid_bsize = 48
        e3d_nstepmin = 1
    else:
        e3d_reslim = 1e-4
        grid_cells1_D = 8.0
        adgrid_nr = 10
        e3d_nstepmin = 10
        # If one can afford the resources:
        maxnodes = 53
        # walltime_wf_run = '200:00:00'
        walltime_wf_run = '5:00:00'
        grid_bsize = 32
        # Otherwise (for large WFs)
        # grid_bsize = 64
        # maxnodes = 4
        # walltime_wf_run = '100:00:00'
    today = date.today()

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # Constants
    cnst = Cnst(density=rho)
    # AD
    ad = AD(force='2011', grid=ADGrid(nr=adgrid_nr, ntheta=32))
    # Wind farm grid small rotor
    wfgrid = FlatBoxGrid(Dref,
                         cells1_D=grid_cells1_D,
                         zFirstCell_D=0.015,
                         bsize=grid_bsize,
                         cluster=Cluster(walltime='1:00:00'))
    e3d = E3D(reslim=e3d_reslim, nstepmin=e3d_nstepmin)
    # Wind farm run
    wfrun = WFRun(casename='wfsq_' + str(n) + '-' + str(s) + 'D', write_restart=False,
                  # We march in the ws and stop ws cases automatically when wf rated power is achieved
                  march_dir='ws', wscutoff='auto', wdsym='auto',
                  cluster=Cluster(walltime=walltime_wf_run))

    wfm = EllipSys(Site, wt, wfgrid, TI, zRef,
                   cnst=cnst, e3d=e3d, ad=ad, wfrun=wfrun,
                   run_grid=grid, run_cal=cal, run_wf=run)
    wfm.calrun.cluster.walltime = '1:00:00'

    # Make grid, run force calibration and wind farm
    wfm.calc_wt_interaction(wt_x, wt_y, h_i, type_i, wd, ws)

    # Post process WF AEP and wake effects
    if postWFeff:
        wfm.run_grid = False
        wfm.run_cal = False
        wfm.run_wf = False
        sim_res = wfm(wt_x, wt_y, wd=wd, ws=ws, type=type_i)
        aep = float(sim_res.aep(with_wake_loss=True).sum())
        aep_NoWake = float(sim_res.aep(with_wake_loss=False).sum())
        wake_loss = (aep - aep_NoWake) / aep_NoWake
        print('AEP:', aep)
        print('AEP no wake:', aep_NoWake)
        print('Wake loss:', wake_loss)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    # queue = 'workq'
    queue = 'windq'
    starttime = time.time()

    # All main parameters are listed here. If only wake losses are of interest
    # and the WT wind speeds are linked to the Weibull parameter A, then
    # we only need 10 parameters:
    # 1) 6 WT parameters (Dref is not a parameter for wake losses)
    # 2) 2 site parameters
    # 3) 2 wind farm layout parameters

    # Parameters that influence AEP but not wake losses due to Reynolds-number similarity
    rho = 1.225  # Air density, AEP scales linearly with rho
    cp_rated = 0.45  # Below-rated power coefficient (if ct power law is used above rated
    #                  then cp_rated does not influence the wake losses), AEP scales linearly cp_rated
    Dref = 100.0  # Rotor diameter, also used to scale grid parameters, AEP scales as Dref ** 2
    A = 10.0  # Weibull parameter A, AEP scales as A ** 3

    # A generic wind turbine with a cnst below rated regime
    ct_rated = 0.8  # Below-rated thrust coefficient
    zH_D = 0.7  # Ratio of hub height and diameter (ground clearance)
    tsr_rated = 8.0  # Below-rated tip speed ratio (for AD force distribution)
    ws_rated_A = 1.0  # Rated WT wind speed normalized by Weibull parameter A
    ws_cutin_A = 0.4  # Cut-in ws normalized by Weibull parameter A.
    ws_cutout_A = 2.5  # Cut-out ws normalized by Weibull parameter A.
    # For small values of ws_cutin_A and large values of ws_cutout_A relative to ws_rated_A, both the wake losses and the AEP is nearly unchanged

    # Cnst TI for all flow cases and a generic site shape Weibull parameters k
    #  and a uniform wind rose
    TI = 0.06  # TI based on TKE ~ 0.8 * TI_u = sigma_U/U
    k = 2.2

    # One could also provide a rated power and calculate ws_rated_A
    # P_rated = 3.0e+06
    # ws_rated_A = (8.0 * P_rated / (rho * np.pi * Dref ** 2 * cp_rated)) ** (1.0 / 3.0) / A

    # WT ws parameters in dimensional form
    ws_rated = ws_rated_A * A
    ws_cutin = ws_cutin_A * A
    ws_cutout = ws_cutout_A * A

    nws = 22  # Number of wind speeds
    nwd = 120  # Number of wind direction before symmetry (with symmetry times nwd = nwd / 8)

    # List of square wind farm cases
    ns = [2]  # Number of wts in one direction, i.e. 2 means 2x2
    ss = [4]  # WT inter spacing in rotor diameters
    for n in ns:
        for s in ss:
            print('Wind farm with: n', n, 's', s)
            # Run
            run_farm(run_machine, queue, n, s, Dref, rho, zH_D, ws_rated, ct_rated, cp_rated, tsr_rated,
                     TI, A, k, ws_cutin, ws_cutout, nws, nwd,
                     coarse=True, grid=True, cal=True, run=True, postWFeff=True)
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
