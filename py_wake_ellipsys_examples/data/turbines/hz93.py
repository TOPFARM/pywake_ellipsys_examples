import numpy as np

from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake_ellipsys_examples.data.turbines.nrel5mw import nrel5mw_bladeloading
from py_wake.validation.ecn_wieringermeer import power_curve, ct_curve
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular


# Model based power, ct and cp curves from Matias Sessarego
power_curve = np.array([[4.0, 89.9895],
                        [5.0, 194.6294],
                        [6.0, 343.0097],
                        [7.0, 548.7081],
                        [8.0, 826.8739],
                        [9.0, 1162.0075],
                        [10.0, 1521.1084],
                        [11.0, 1878.5056],
                        [12.0, 2000.0],
                        [13.0, 2000.0],
                        [14.0, 2000.0],
                        [15.0, 2000.0],
                        [16.0, 2000.0],
                        [17.0, 2000.0],
                        [18.0, 2000.0],
                        [19.0, 2000.0],
                        [20.0, 2000.0],
                        [21.0, 2000.0],
                        [22.0, 2000.0],
                        [23.0, 2000.0],
                        [24.0, 2000.0],
                        [25.0, 2000.0]])
cp_curve = np.stack((power_curve[:, 0], power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 93.0 ** 2 * np.pi * power_curve[:, 0] ** 3)), axis=1)
ct_curve = np.array([[4.0, 0.930642],
                     [5.0, 0.833722],
                     [6.0, 0.837716],
                     [7.0, 0.838956],
                     [8.0, 0.802836],
                     [9.0, 0.732175],
                     [10.0, 0.665714],
                     [11.0, 0.596719],
                     [12.0, 0.445337],
                     [13.0, 0.334879],
                     [14.0, 0.262498],
                     [15.0, 0.211303],
                     [16.0, 0.173528],
                     [17.0, 0.144852],
                     [18.0, 0.122573],
                     [19.0, 0.104975],
                     [20.0, 0.090860],
                     [21.0, 0.079382],
                     [22.0, 0.069920],
                     [23.0, 0.062042],
                     [24.0, 0.055434],
                     [25.0, 0.049829]])

rpm_curve = np.array([[4.0, 9.2],
                      [5.0, 9.87],
                      [6.0, 11.92],
                      [7.0, 13.94],
                      [8.0, 15.0],
                      [9.0, 15.0],
                      [10.0, 15.0],
                      [11.0, 15.0],
                      [12.0, 15.0],
                      [13.0, 15.0],
                      [14.0, 15.0],
                      [15.0, 15.0],
                      [16.0, 15.0],
                      [17.0, 15.0],
                      [18.0, 15.0],
                      [19.0, 15.0],
                      [20.0, 15.0],
                      [21.0, 15.0],
                      [22.0, 15.0],
                      [23.0, 15.0],
                      [24.0, 15.0],
                      [25.0, 15.0]])


class HZ93(EllipSysOneTypeWT):   # CSIC HZ93-2.0MW
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='HZ93',
                                   diameter=93.0,
                                   hub_height=67.0,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=2000.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1], power_unit='kW',
                                                                  ct=ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func='',
                                   bladeloading_func=lambda r: (np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 1], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 2], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 3], left=0.0, right=0.0)),
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type='',  # 1=flex5, 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
