import numpy as np
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular

# Power and CT curve from WAsP, most likely based on official curves
v112_power_curve = np.array([[3.0, 26.0],
                             [3.5, 73.0],
                             [4.0, 133.0],
                             [4.5, 207.0],
                             [5.0, 302.0],
                             [5.5, 416.0],
                             [6.0, 554.0],
                             [6.5, 717.0],
                             [7.0, 907.0],
                             [7.5, 1126.0],
                             [8.0, 1375.0],
                             [8.5, 1652.0],
                             [9.0, 1958.0],
                             [9.5, 2282.0],
                             [10.0, 2585.0],
                             [10.5, 2821.0],
                             [11.0, 2997.0],
                             [11.5, 3050.0],
                             [12.0, 3067.0],
                             [12.5, 3074.0],
                             [13.0, 3075.0],
                             [13.5, 3075.0],
                             [14.0, 3075.0],
                             [14.5, 3075.0],
                             [15.0, 3075.0],
                             [15.5, 3075.0],
                             [16.0, 3075.0],
                             [16.5, 3075.0],
                             [17.0, 3075.0],
                             [17.5, 3075.0],
                             [18.0, 3075.0],
                             [18.5, 3075.0],
                             [19.0, 3075.0],
                             [19.5, 3075.0],
                             [20.0, 3075.0],
                             [20.5, 3075.0],
                             [21.0, 3075.0],
                             [21.5, 3075.0],
                             [22.0, 3075.0],
                             [22.5, 3075.0],
                             [23.0, 3075.0],
                             [23.5, 3075.0],
                             [24.0, 3075.0],
                             [24.5, 3075.0],
                             [25.0, 3075.0]])
v112_cp_curve = np.stack((v112_power_curve[:, 0], v112_power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 112.0 ** 2 * np.pi * v112_power_curve[:, 0] ** 3)), axis=1)
v112_ct_curve = np.array([[3.0, 0.901],
                          [3.5, 0.847],
                          [4.0, 0.821],
                          [4.5, 0.815],
                          [5.0, 0.812],
                          [5.5, 0.808],
                          [6.0, 0.805],
                          [6.5, 0.801],
                          [7.0, 0.798],
                          [7.5, 0.795],
                          [8.0, 0.794],
                          [8.5, 0.795],
                          [9.0, 0.786],
                          [9.5, 0.76],
                          [10.0, 0.713],
                          [10.5, 0.642],
                          [11.0, 0.564],
                          [11.5, 0.478],
                          [12.0, 0.407],
                          [12.5, 0.352],
                          [13.0, 0.307],
                          [13.5, 0.27],
                          [14.0, 0.24],
                          [14.5, 0.215],
                          [15.0, 0.193],
                          [15.5, 0.174],
                          [16.0, 0.158],
                          [16.5, 0.144],
                          [17.0, 0.132],
                          [17.5, 0.121],
                          [18.0, 0.111],
                          [18.5, 0.102],
                          [19.0, 0.095],
                          [19.5, 0.088],
                          [20.0, 0.082],
                          [20.5, 0.077],
                          [21.0, 0.072],
                          [21.5, 0.067],
                          [22.0, 0.063],
                          [22.5, 0.059],
                          [23.0, 0.056],
                          [23.5, 0.053],
                          [24.0, 0.05],
                          [24.5, 0.047],
                          [25.0, 0.044]])

# Estimate RPM from a set tip speed ratio, and min and max rpm
TSR = 7.5
RPMmin = 6.2
RPMmax = 17.7
R = 61.0
U = np.arange(3.0, 25.5, 0.5)
rpm = np.maximum(np.minimum(TSR * U / R * 30 / np.pi, RPMmax), RPMmin)
rpm_curve = np.zeros((len(U), 2))
rpm_curve[:, 0] = U
rpm_curve[:, 1] = rpm


class V112(EllipSysOneTypeWT):  # V112
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='V112',
                                   diameter=112.0,
                                   hub_height=84.0,  # Can be from 84-119 m
                                   cutin=3.0,
                                   cutout=25.0,
                                   dws=0.5,
                                   rated_power=3075.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=v112_power_curve[:, 0],
                                                                  power=v112_power_curve[:, 1], power_unit='kW',
                                                                  ct=v112_ct_curve[:, 1], ws_cutin=3.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, v112_cp_curve[:, 0], v112_cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, v112_rpm_curve[:, 0], v112_rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func='',
                                   bladeloading_func='',
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type=2,  # 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
