import numpy as np

from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake.examples.data.lillgrund import power_curve, ct_curve
from py_wake_ellipsys_examples.data.turbines.nrel5mw import nrel5mw_bladeloading
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular


cp_curve = np.stack((power_curve[:, 0], power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 92.6 ** 2 * np.pi * power_curve[:, 0] ** 3)), axis=1)
# Measured rotor speed from Kurt Hansen from EERA-DTOC WP1-Lillgrund-SWT-2.3-93 report (2013)
# (Probably used a digitizer)
rpm_curve = np.array([[4.0, 7.63155],
                      [5.0, 8.72967],
                      [6.0, 10.8897],
                      [7.0, 12.8523],
                      [8.0, 14.9859],
                      [9.0, 15.9238],
                      [10.0, 16.0],
                      [11.0, 16.0],
                      [12.0, 16.0],
                      [13.0, 16.0],
                      [14.0, 16.0],
                      [15.0, 16.0],
                      [16.0, 16.0],
                      [17.0, 16.0],
                      [18.0, 16.0],
                      [19.0, 16.0],
                      [20.0, 16.0],
                      [21.0, 16.0],
                      [22.0, 16.0],
                      [23.0, 16.0],
                      [24.0, 16.0],
                      [25.0, 16.0]])


# SWT-2.3-93 with variable hub height, e.g. SWT-2.3-93-65 (Lillgrund) or SWT-2.3-93-68.5 (Roedsand II)
class SWT2p3_93(EllipSysOneTypeWT):
    def __init__(self, zH=65.0):
        EllipSysOneTypeWT.__init__(self, name='SWT-2p3-93-' + str(zH).replace('.', 'p'),
                                   diameter=92.6,
                                   hub_height=zH,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=2300.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1], power_unit='kW',
                                                                  ct=ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func='',
                                   bladeloading_func=lambda r: (np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 1], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 2], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 3], left=0.0, right=0.0)),
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type='',  # 1=flex5, 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
