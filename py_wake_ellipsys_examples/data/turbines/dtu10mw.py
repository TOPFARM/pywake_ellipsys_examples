import numpy as np
from py_wake.site._site import UniformWeibullSite
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake_ellipsys_examples.data.turbines.ADairfoil import ADairfoil_path
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular

wt_x = [0.0, 630.0, 0.0, 630.0]
wt_y = [0.0, 0.0, 630.0, 630.0]
type_i = np.array([0, 1, 1, 0])

# Electric power curve (assuming a loss of 6.4%, and stiff structure rpm (DTU Wind Energy Report-I-0092)
dtu10mw_power_curve = np.array([[4.0, 263.3],
                                [5.0, 751.0],
                                [6.0, 1440.5],
                                [7.0, 2355.4],
                                [8.0, 3506.3],
                                [9.0, 4992.3],
                                [10.0, 6848.2],
                                [11.0, 9114.9],
                                [12.0, 10000.0],
                                [13.0, 10000.0],
                                [14.0, 10000.0],
                                [15.0, 10000.0],
                                [16.0, 10000.0],
                                [17.0, 10000.0],
                                [18.0, 10000.0],
                                [19.0, 10000.0],
                                [20.0, 10000.0],
                                [21.0, 10000.0],
                                [22.0, 10000.0],
                                [23.0, 10000.0],
                                [24.0, 10000.0],
                                [25.0, 10000.0]])
dtu10mw_cp_curve = np.stack((dtu10mw_power_curve[:, 0], dtu10mw_power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 178.3 ** 2 * np.pi * dtu10mw_power_curve[:, 0] ** 3)), axis=1)
dtu10mw_ct_curve = np.array([[4.0, 0.923],
                             [5.0, 0.919],
                             [6.0, 0.904],
                             [7.0, 0.858],
                             [8.0, 0.814],
                             [9.0, 0.814],
                             [10.0, 0.814],
                             [11.0, 0.814],
                             [12.0, 0.577],
                             [13.0, 0.419],
                             [14.0, 0.323],
                             [15.0, 0.259],
                             [16.0, 0.211],
                             [17.0, 0.175],
                             [18.0, 0.148],
                             [19.0, 0.126],
                             [20.0, 0.109],
                             [21.0, 0.095],
                             [22.0, 0.084],
                             [23.0, 0.074],
                             [24.0, 0.066],
                             [25.0, 0.059]])
# rpm curve is need for RANS model when wake rotation is considered
dtu10mw_rpm_curve = np.array([[4.0, 6.0],
                              [5.0, 6.0],
                              [6.0, 6.0],
                              [7.0, 6.0],
                              [8.0, 6.426],
                              [9.0, 7.229],
                              [10.0, 8.032],
                              [11.0, 8.836],
                              [12.0, 9.6],
                              [13.0, 9.6],
                              [14.0, 9.6],
                              [15.0, 9.6],
                              [16.0, 9.6],
                              [17.0, 9.6],
                              [18.0, 9.6],
                              [19.0, 9.6],
                              [20.0, 9.6],
                              [21.0, 9.6],
                              [22.0, 9.6],
                              [23.0, 9.6],
                              [24.0, 9.6],
                              [25.0, 9.6]])
# Pitch curve is need for RANS model when airfoil data is used
dtu10mw_pitch_curve = np.array([[4.0, 2.751],
                                [5.0, 1.966],
                                [6.0, 0.896],
                                [7.0, 0.000],
                                [8.0, 0.000],
                                [9.0, 0.000],
                                [10.0, 0.000],
                                [11.0, 0.000],
                                [12.0, 4.502],
                                [13.0, 7.266],
                                [14.0, 9.292],
                                [15.0, 10.958],
                                [16.0, 12.499],
                                [17.0, 13.896],
                                [18.0, 15.200],
                                [19.0, 16.432],
                                [20.0, 17.618],
                                [21.0, 18.758],
                                [22.0, 19.860],
                                [23.0, 20.927],
                                [24.0, 21.963],
                                [25.0, 22.975]])

# Normalized blade load distributions from EllipSys3D rotor resolved DES, used for actuator disk grid in RANS
# Based on: https://rwt.windenergy.dtu.dk/dtu10mw/dtu-10mw-rwt/-/blob/master/CFD/3D_DTU_10MW_RWT/baseline/EllipSys3D/turb/wsp_8_spanwise_loads.dat
# columns:    y/R [-] fx/(rho*R*U**2) [-] fy/(rho*R*U**2) [-] fz/(rho*R*U**2) [-]
# (fx: tangential force, fy: radial force, fz: thrust force)
# U = 8.0 m/s, rho = 1.225 kg/m^3, RPM = 6.4259
dtu10mw_bladeloading = np.array([[0.000000000000e+00, 0.000000000000e+00, 0.000000000000e+00, 0.000000000000e+00],
                                 [4.486819966349e-02, 1.335386015315e-02, 0.000000000000e+00, 2.985964666293e-02],
                                 [5.608524957936e-02, 1.138542441654e-02, 0.000000000000e+00, 3.506447800657e-02],
                                 [6.730229949523e-02, 2.031562546499e-03, 0.000000000000e+00, 4.071056148202e-02],
                                 [7.851934941110e-02, -7.530774777662e-03, 0.000000000000e+00, 4.596799277759e-02],
                                 [8.973639932698e-02, -1.568917754988e-02, 0.000000000000e+00, 5.367622786636e-02],
                                 [1.009534492428e-01, -2.293452333261e-02, 0.000000000000e+00, 6.336113034670e-02],
                                 [1.121704991587e-01, -2.797332659929e-02, 0.000000000000e+00, 7.359818495542e-02],
                                 [1.233875490746e-01, -3.176926213559e-02, 0.000000000000e+00, 8.270484708185e-02],
                                 [1.346045989905e-01, -3.634126014399e-02, 0.000000000000e+00, 9.251871415981e-02],
                                 [1.458216489063e-01, -3.984067926105e-02, 0.000000000000e+00, 1.020582971831e-01],
                                 [1.570386988222e-01, -4.322347539689e-02, 0.000000000000e+00, 1.123190249179e-01],
                                 [1.682557487381e-01, -4.732904300251e-02, 0.000000000000e+00, 1.236761205604e-01],
                                 [1.794727986540e-01, -5.104445185253e-02, 0.000000000000e+00, 1.359105197042e-01],
                                 [1.906898485698e-01, -5.383218349033e-02, 0.000000000000e+00, 1.492629368068e-01],
                                 [2.019068984857e-01, -5.608261843717e-02, 0.000000000000e+00, 1.635409393707e-01],
                                 [2.131239484016e-01, -5.801897884785e-02, 0.000000000000e+00, 1.780637426030e-01],
                                 [2.243409983174e-01, -5.907876543775e-02, 0.000000000000e+00, 1.932421137272e-01],
                                 [2.355580482333e-01, -6.018104805018e-02, 0.000000000000e+00, 2.068178345371e-01],
                                 [2.467750981492e-01, -6.205803392585e-02, 0.000000000000e+00, 2.190942804491e-01],
                                 [2.579921480651e-01, -6.431503313608e-02, 0.000000000000e+00, 2.301486831412e-01],
                                 [2.692091979809e-01, -6.677942615633e-02, 0.000000000000e+00, 2.402531848410e-01],
                                 [2.804262478968e-01, -6.932568933350e-02, 0.000000000000e+00, 2.504794287317e-01],
                                 [2.916432978127e-01, -7.175825254387e-02, 0.000000000000e+00, 2.615540192521e-01],
                                 [3.028603477285e-01, -7.407383651722e-02, 0.000000000000e+00, 2.736605783648e-01],
                                 [3.140773976444e-01, -7.618042138336e-02, 0.000000000000e+00, 2.867528786613e-01],
                                 [3.252944475603e-01, -7.806328762576e-02, 0.000000000000e+00, 3.010084042030e-01],
                                 [3.365114974762e-01, -7.968992583012e-02, 0.000000000000e+00, 3.163291345703e-01],
                                 [3.477285473920e-01, -8.075544971213e-02, 0.000000000000e+00, 3.326290819188e-01],
                                 [3.589455973079e-01, -8.126184228599e-02, 0.000000000000e+00, 3.486184715053e-01],
                                 [3.701626472238e-01, -8.150677601383e-02, 0.000000000000e+00, 3.624539442810e-01],
                                 [3.813796971397e-01, -8.166036232216e-02, 0.000000000000e+00, 3.736468288942e-01],
                                 [3.925967470555e-01, -8.161586182426e-02, 0.000000000000e+00, 3.829931352799e-01],
                                 [4.038137969714e-01, -8.150584889031e-02, 0.000000000000e+00, 3.913616125082e-01],
                                 [4.150308468873e-01, -8.155195182392e-02, 0.000000000000e+00, 3.995021146428e-01],
                                 [4.262478968031e-01, -8.141584207996e-02, 0.000000000000e+00, 4.086900946582e-01],
                                 [4.374649467190e-01, -8.125566146257e-02, 0.000000000000e+00, 4.181852701821e-01],
                                 [4.486819966349e-01, -8.103146210812e-02, 0.000000000000e+00, 4.279634902194e-01],
                                 [4.598990465508e-01, -8.082122683622e-02, 0.000000000000e+00, 4.378662567102e-01],
                                 [4.711160964666e-01, -8.063718709581e-02, 0.000000000000e+00, 4.479254180640e-01],
                                 [4.823331463825e-01, -8.046278200007e-02, 0.000000000000e+00, 4.581647389747e-01],
                                 [4.935501962984e-01, -8.029400402898e-02, 0.000000000000e+00, 4.685719865624e-01],
                                 [5.047672462142e-01, -8.017492302586e-02, 0.000000000000e+00, 4.791010764934e-01],
                                 [5.159842961301e-01, -8.004612153330e-02, 0.000000000000e+00, 4.899130249408e-01],
                                 [5.272013460460e-01, -7.995851122277e-02, 0.000000000000e+00, 5.008432817883e-01],
                                 [5.384183959619e-01, -7.984196407110e-02, 0.000000000000e+00, 5.119079715453e-01],
                                 [5.496354458777e-01, -7.971802711550e-02, 0.000000000000e+00, 5.230698661966e-01],
                                 [5.608524957936e-01, -7.958911545549e-02, 0.000000000000e+00, 5.342988628429e-01],
                                 [5.720695457095e-01, -7.943423575263e-02, 0.000000000000e+00, 5.455862911626e-01],
                                 [5.832865956254e-01, -7.927003330777e-02, 0.000000000000e+00, 5.568854229858e-01],
                                 [5.945036455412e-01, -7.915652219946e-02, 0.000000000000e+00, 5.682941928875e-01],
                                 [6.057206954571e-01, -7.900118895006e-02, 0.000000000000e+00, 5.797060245859e-01],
                                 [6.169377453730e-01, -7.883618957959e-02, 0.000000000000e+00, 5.910525713370e-01],
                                 [6.281547952888e-01, -7.864890204539e-02, 0.000000000000e+00, 6.023064057367e-01],
                                 [6.393718452047e-01, -7.844193459773e-02, 0.000000000000e+00, 6.134403865304e-01],
                                 [6.505888951206e-01, -7.821580373596e-02, 0.000000000000e+00, 6.244160838761e-01],
                                 [6.618059450365e-01, -7.797136361555e-02, 0.000000000000e+00, 6.352033233372e-01],
                                 [6.730229949523e-01, -7.772588048119e-02, 0.000000000000e+00, 6.457762799455e-01],
                                 [6.842400448682e-01, -7.742118877837e-02, 0.000000000000e+00, 6.560846772809e-01],
                                 [6.954570947841e-01, -7.717142628223e-02, 0.000000000000e+00, 6.661153667861e-01],
                                 [7.066741446999e-01, -7.691928445523e-02, 0.000000000000e+00, 6.759443496973e-01],
                                 [7.178911946158e-01, -7.657958525530e-02, 0.000000000000e+00, 6.852754186363e-01],
                                 [7.291082445317e-01, -7.626315571097e-02, 0.000000000000e+00, 6.943125693912e-01],
                                 [7.403252944476e-01, -7.595845971591e-02, 0.000000000000e+00, 7.030194610093e-01],
                                 [7.515423443634e-01, -7.564083263704e-02, 0.000000000000e+00, 7.112994036650e-01],
                                 [7.627593942793e-01, -7.530624549315e-02, 0.000000000000e+00, 7.191632710291e-01],
                                 [7.739764441952e-01, -7.495968729612e-02, 0.000000000000e+00, 7.265516871359e-01],
                                 [7.851934941110e-01, -7.459137746518e-02, 0.000000000000e+00, 7.334460809001e-01],
                                 [7.964105440269e-01, -7.419960339716e-02, 0.000000000000e+00, 7.398002678357e-01],
                                 [8.076275939428e-01, -7.377079732622e-02, 0.000000000000e+00, 7.455800817242e-01],
                                 [8.188446438587e-01, -7.328989635675e-02, 0.000000000000e+00, 7.507234997196e-01],
                                 [8.300616937745e-01, -7.276286526950e-02, 0.000000000000e+00, 7.553035900283e-01],
                                 [8.412787436904e-01, -7.215057029542e-02, 0.000000000000e+00, 7.591593650921e-01],
                                 [8.524957936063e-01, -7.142862293543e-02, 0.000000000000e+00, 7.621072315634e-01],
                                 [8.637128435222e-01, -7.055825712225e-02, 0.000000000000e+00, 7.641119787792e-01],
                                 [8.749298934380e-01, -6.949308377305e-02, 0.000000000000e+00, 7.649311811096e-01],
                                 [8.861469433539e-01, -6.819186592192e-02, 0.000000000000e+00, 7.641573906624e-01],
                                 [8.973639932698e-01, -6.657941785800e-02, 0.000000000000e+00, 7.613003622649e-01],
                                 [9.085810431856e-01, -6.465664381288e-02, 0.000000000000e+00, 7.555156123021e-01],
                                 [9.197980931015e-01, -6.232323703458e-02, 0.000000000000e+00, 7.458300044639e-01],
                                 [9.310151430174e-01, -5.948752675495e-02, 0.000000000000e+00, 7.308617956437e-01],
                                 [9.422321929333e-01, -5.595826942667e-02, 0.000000000000e+00, 7.087694724553e-01],
                                 [9.450364554122e-01, -5.492203434935e-02, 0.000000000000e+00, 7.018177057699e-01],
                                 [9.478407178912e-01, -5.383048662538e-02, 0.000000000000e+00, 6.943251742649e-01],
                                 [9.506449803702e-01, -5.264571434294e-02, 0.000000000000e+00, 6.860628440945e-01],
                                 [9.534492428491e-01, -5.138064286287e-02, 0.000000000000e+00, 6.769522531391e-01],
                                 [9.562535053281e-01, -5.005719837009e-02, 0.000000000000e+00, 6.671780105761e-01],
                                 [9.590577678071e-01, -4.855961060812e-02, 0.000000000000e+00, 6.559159350785e-01],
                                 [9.618620302860e-01, -4.702377613973e-02, 0.000000000000e+00, 6.440225571440e-01],
                                 [9.646662927650e-01, -4.540209404008e-02, 0.000000000000e+00, 6.304840500418e-01],
                                 [9.674705552440e-01, -4.368657502261e-02, 0.000000000000e+00, 6.156851557224e-01],
                                 [9.702748177229e-01, -4.186115466938e-02, 0.000000000000e+00, 5.996054574382e-01],
                                 [9.730790802019e-01, -3.980596792839e-02, 0.000000000000e+00, 5.805067989058e-01],
                                 [9.758833426809e-01, -3.759406154498e-02, 0.000000000000e+00, 5.600873756682e-01],
                                 [9.786876051598e-01, -3.504478378564e-02, 0.000000000000e+00, 5.379824762210e-01],
                                 [9.814918676388e-01, -3.255562168782e-02, 0.000000000000e+00, 5.094736714091e-01],
                                 [9.842961301178e-01, -2.982838772076e-02, 0.000000000000e+00, 4.775610642462e-01],
                                 [9.871003925967e-01, -2.612859832660e-02, 0.000000000000e+00, 4.434747101308e-01],
                                 [9.899046550757e-01, -2.184822072407e-02, 0.000000000000e+00, 4.012793159889e-01],
                                 [9.927089175547e-01, -1.608145524054e-02, 0.000000000000e+00, 3.523294407499e-01],
                                 [9.955131800337e-01, -6.846444452711e-03, 0.000000000000e+00, 2.874046550757e-01],
                                 [9.983174425126e-01, -7.547630541280e-03, 0.000000000000e+00, 1.805117779024e-01],
                                 [1.001121704992e+00, 0.000000000000e+00, 0.000000000000e+00, 0.000000000000e+00]])


class DTU10MW(EllipSysOneTypeWT):   # DTU10MW
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='DTU10MW',
                                   diameter=178.3,
                                   hub_height=119.0,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=10000.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=dtu10mw_power_curve[:, 0],
                                                                  power=dtu10mw_power_curve[:, 1], power_unit='kW',
                                                                  ct=dtu10mw_ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, dtu10mw_cp_curve[:, 0], dtu10mw_cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, dtu10mw_rpm_curve[:, 0], dtu10mw_rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func=lambda ws: np.interp(ws, dtu10mw_pitch_curve[:, 0], dtu10mw_pitch_curve[:, 1], left=0.0, right=0.0),
                                   bladeloading_func=lambda r: (np.interp(r, dtu10mw_bladeloading[:, 0], dtu10mw_bladeloading[:, 1], left=0.0, right=0.0),
                                                                np.interp(r, dtu10mw_bladeloading[:, 0], dtu10mw_bladeloading[:, 2], left=0.0, right=0.0),
                                                                np.interp(r, dtu10mw_bladeloading[:, 0], dtu10mw_bladeloading[:, 3], left=0.0, right=0.0)),
                                   airfoildata_file='dtu10mw.pc',
                                   airfoildata_path=ADairfoil_path,
                                   airfoildata_file_type=2,  # 1=flex5, 2=hawc2
                                   bladegeo_file='dtu10mw.geo',
                                   bladegeo_path=ADairfoil_path)
