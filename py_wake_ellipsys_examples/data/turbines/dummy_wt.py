import numpy as np
from py_wake.site._site import UniformWeibullSite
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake_ellipsys_examples.data.turbines.ADairfoil import ADairfoil_path
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular

# Dummy wt using a default rotor diameter and hub height of 100 m
power_curve = np.array([[3.0, 0.0], [25.0, 0.0]])
cp_curve = np.array([[3.0, 0.0], [25.0, 0.0]])
ct_curve = np.array([[3.0, 0.0], [25.0, 0.0]])
rpm_curve = np.array([[3.0, 0.0], [25.0, 0.0]])
pitch_curve = np.array([[4.0, 0.000], [25.0, 0.000]])

bladeloading = np.array([[0.000000000e+00, 0.000000000e+00, 0.000000000e+00, 0.000000000e+00],
                         [0.500000000e+00, 0.500000000e+00, 0.000000000e+00, 0.500000000e+00],
                         [1.000000000e+00, 1.000000000e+00, 0.000000000e+00, 1.000000000e+00],
                         [1.500000000e+00, 0.000000000e+00, 0.000000000e+00, 0.000000000e+00]])


class Dummy(EllipSysOneTypeWT):   # Dummy WT
    def __init__(self, D=100.0, zH=100.0, cutin=3.0, cutout=25.0):
        EllipSysOneTypeWT.__init__(self, name='NoAD',
                                   diameter=D,
                                   hub_height=zH,
                                   cutin=cutin,
                                   cutout=cutout,
                                   dws=1.0,
                                   rated_power=0.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1], power_unit='kW',
                                                                  ct=ct_curve[:, 1]),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1]),
                                   pitch_func=lambda ws: np.interp(ws, pitch_curve[:, 0], pitch_curve[:, 1]),
                                   bladeloading_func=lambda r: (np.interp(r, bladeloading[:, 0], bladeloading[:, 1]),
                                                                np.interp(r, bladeloading[:, 0], bladeloading[:, 2]),
                                                                np.interp(r, bladeloading[:, 0], bladeloading[:, 3])),
                                   airfoildata_file='',
                                   airfoildata_path=ADairfoil_path,
                                   airfoildata_file_type=2,  # 1=flex5, 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path=ADairfoil_path)
