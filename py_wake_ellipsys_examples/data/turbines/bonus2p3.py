import numpy as np

from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake_ellipsys_examples.data.turbines.nrel5mw import nrel5mw_bladeloading
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular


power_curve = np.array([[4.0, 42.0],
                        [5.0, 136.0],
                        [6.0, 276.0],
                        [7.0, 470.0],
                        [8.0, 727.0],
                        [9.0, 1043.0],
                        [10.0, 1394.0],
                        [11.0, 1738.0],
                        [12.0, 2015.0],
                        [13.0, 2183.0],
                        [14.0, 2260.0],
                        [15.0, 2288.0],
                        [16.0, 2297.0],
                        [17.0, 2299.0],
                        [18.0, 2300.0],
                        [19.0, 2300.0],
                        [20.0, 2300.0],
                        [21.0, 2300.0],
                        [22.0, 2300.0],
                        [23.0, 2300.0],
                        [24.0, 2300.0],
                        [25.0, 2300.0]])
cp_curve = np.stack((power_curve[:, 0], power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 82.4 ** 2 * np.pi * power_curve[:, 0] ** 3)), axis=1)
ct_curve = np.array([[4.0, 0.856],
                     [5.0, 0.851],
                     [6.0, 0.838],
                     [7.0, 0.858],
                     [8.0, 0.886],
                     [9.0, 0.861],
                     [10.0, 0.763],
                     [11.0, 0.666],
                     [12.0, 0.592],
                     [13.0, 0.455],
                     [14.0, 0.347],
                     [15.0, 0.271],
                     [16.0, 0.221],
                     [17.0, 0.179],
                     [18.0, 0.152],
                     [19.0, 0.129],
                     [20.0, 0.109],
                     [21.0, 0.095],
                     [22.0, 0.082],
                     [23.0, 0.073],
                     [24.0, 0.063],
                     [25.0, 0.056]])

# The rotor speed varies between two settings (11.0 an 16.5 rpm) as shown
# by Nygaard 2017 (doi:10.1088/1742-6596/753/3/032020) and there is some
# hysteresis that causes an overlap in rpm values below rated.
# The sharp peak in the ct curve is caused by having two rpm values.
# In other words, there exist two ct curves below, one for each rpm value.
# For now we use a sharp transition of rpm values. An alternative approach
# would be to define two different bonus wind turbine types with different
# ct curves and constant rpm values and run them both.
rpm_curve = np.array([[4.0, 11.0],
                      [5.0, 11.0],
                      [6.0, 11.0],
                      [7.0, 11.0],
                      [8.0, 16.5],
                      [9.0, 16.5],
                      [10.0, 16.5],
                      [11.0, 16.5],
                      [12.0, 16.5],
                      [13.0, 16.5],
                      [14.0, 16.5],
                      [15.0, 16.5],
                      [16.0, 16.5],
                      [17.0, 16.5],
                      [18.0, 16.5],
                      [19.0, 16.5],
                      [20.0, 16.5],
                      [21.0, 16.5],
                      [22.0, 16.5],
                      [23.0, 16.5],
                      [24.0, 16.5],
                      [25.0, 16.5]])


class Bonus_2p3(EllipSysOneTypeWT):   # Bonus 2.3 MW
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='Bonus-2p3',
                                   diameter=82.4,
                                   hub_height=69.0,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=2300.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1], power_unit='kW',
                                                                  ct=ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func='',
                                   bladeloading_func=lambda r: (np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 1], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 2], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 3], left=0.0, right=0.0)),
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type='',  # 1=flex5, 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
