import numpy as np
from py_wake.site._site import UniformWeibullSite
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular
from py_wake_ellipsys_examples.data.turbines.ADairfoil import ADairfoil_path

from py_wake_ellipsys_examples.data.turbines.dtu10mw import dtu10mw_power_curve, dtu10mw_cp_curve, dtu10mw_ct_curve, dtu10mw_rpm_curve, dtu10mw_bladeloading

wt_x = [0.0, 630.0, 0.0, 630.0]
wt_y = [0.0, 0.0, 630.0, 630.0]
type_i = np.array([0, 1, 1, 0])

D10 = 178.3
zH10 = 119.0

# DownScale 10 MW power curve to 2.5 MW (constant CP, CT and TSR)
dtu2p5mw_power_curve = np.copy(dtu10mw_power_curve)
dtu2p5mw_power_curve[:, 1] = dtu10mw_power_curve[:, 1] * 0.25
dtu2p5mw_cp_curve = np.copy(dtu10mw_cp_curve)
dtu2p5mw_ct_curve = np.copy(dtu10mw_ct_curve)

# Same TSR
RPMmin = dtu10mw_rpm_curve[1, 0] * 0.5
RPMmax = dtu10mw_rpm_curve[1, -1] * 0.5
TSR_design = 7.5
dtu2p5mw_rpm_curve = np.copy(dtu10mw_rpm_curve)

# Calculate RPM from a set tip speed ratio, and min and max rpm
R = D10 * 0.25
U = np.arange(4.0, 26, 1.0)
rpm = np.maximum(np.minimum(TSR_design * U / R * 30 / np.pi, RPMmax), RPMmin)
dtu2p5mw_rpm_curve = np.zeros((22, 2))
dtu2p5mw_rpm_curve[:, 0] = U
dtu2p5mw_rpm_curve[:, 1] = rpm

D = D10 * 0.5
h1 = zH10 - 0.525 * D
h2 = zH10 + 0.525 * D


class DTU2p5MW(EllipSysOneTypeWT):   # DTU2p5MW for MR
    def __init__(self, zH=h1):
        EllipSysOneTypeWT.__init__(self, name=('DTU2p5MW-' + str('%g' % zH).replace('.', 'p'))[0:15],
                                   diameter=D,
                                   hub_height=zH,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=2500.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=dtu2p5mw_power_curve[:, 0],
                                                                  power=dtu2p5mw_power_curve[:, 1], power_unit='kW',
                                                                  ct=dtu2p5mw_ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, dtu2p5mw_cp_curve[:, 0], dtu2p5mw_cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, dtu2p5mw_rpm_curve[:, 0], dtu2p5mw_rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func='',
                                   bladeloading_func=lambda r: (np.interp(r, dtu10mw_bladeloading[:, 0], dtu10mw_bladeloading[:, 1], left=0.0, right=0.0),
                                                                np.interp(r, dtu10mw_bladeloading[:, 0], dtu10mw_bladeloading[:, 2], left=0.0, right=0.0),
                                                                np.interp(r, dtu10mw_bladeloading[:, 0], dtu10mw_bladeloading[:, 3], left=0.0, right=0.0)),
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type=2,  # 1=flex5, 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
