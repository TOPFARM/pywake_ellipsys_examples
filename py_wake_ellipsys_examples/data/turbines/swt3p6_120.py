import numpy as np
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake_ellipsys_examples.data.turbines.nrel5mw import nrel5mw_bladeloading
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular


# Estimate RPM from a set tip speed ratio, and min and max rpm
TSR = 7.5
RPMmin = 5.0
RPMmax = 13.0
R = 60
U = np.arange(4.0, 26, 1.0)
rpm = np.maximum(np.minimum(TSR * U / R * 30 / np.pi, RPMmax), RPMmin)
rpm_curve = np.zeros((22, 2))
rpm_curve[:, 0] = U
rpm_curve[:, 1] = rpm

power_curve = np.array([[4.0, 161.0],
                        [5.0, 351.0],
                        [6.0, 635.0],
                        [7.0, 1026.0],
                        [8.0, 1544.0],
                        [9.0, 2204.0],
                        [10.0, 2910.0],
                        [11.0, 3399.0],
                        [12.0, 3567.0],
                        [13.0, 3596.0],
                        [14.0, 3600.0],
                        [15.0, 3600.0],
                        [16.0, 3600.0],
                        [17.0, 3600.0],
                        [18.0, 3600.0],
                        [19.0, 3600.0],
                        [20.0, 3600.0],
                        [21.0, 3600.0],
                        [22.0, 3600.0],
                        [23.0, 3600.0],
                        [24.0, 3600.0],
                        [25.0, 3600.0]])
cp_curve = np.stack((power_curve[:, 0], power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 120.0 ** 2 * np.pi * power_curve[:, 0] ** 3)), axis=1)
ct_curve = np.array([[4.0, 0.858],
                     [5.0, 0.858],
                     [6.0, 0.858],
                     [7.0, 0.857],
                     [8.0, 0.858],
                     [9.0, 0.857],
                     [10.0, 0.804],
                     [11.0, 0.607],
                     [12.0, 0.418],
                     [13.0, 0.316],
                     [14.0, 0.248],
                     [15.0, 0.201],
                     [16.0, 0.165],
                     [17.0, 0.138],
                     [18.0, 0.117],
                     [19.0, 0.1],
                     [20.0, 0.087],
                     [21.0, 0.076],
                     [22.0, 0.067],
                     [23.0, 0.06],
                     [24.0, 0.053],
                     [25.0, 0.048]])


class SWT_3p6_120(EllipSysOneTypeWT):   # SWT 3.6 MW
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='SWT-3p6-120',
                                   diameter=120.0,
                                   hub_height=81.6,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=3600.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1], power_unit='kW',
                                                                  ct=ct_curve[:, 1], ws_cutin=4.0, ws_cutout=25.0),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func='',
                                   bladeloading_func=lambda r: (np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 1], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 2], left=0.0, right=0.0),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 3], left=0.0, right=0.0)),
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type='',  # 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
