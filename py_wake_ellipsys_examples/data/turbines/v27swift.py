import numpy as np
from py_wake.site._site import UniformWeibullSite
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake_ellipsys_examples.data.turbines.ADairfoil import ADairfoil_path
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular

# Data from OpenFAST model v3
# Power is based on a density of 1.0640320278 kg/m^3
# Loss factor based on OpenFAST	0.887
power_curve = np.array([[3.0, 4.63584253599958],
                        [4.0, 10.9841749557889],
                        [5.0, 21.4446994580893],
                        [6.0, 37.0564406635783],
                        [7.0, 58.8201979723131],
                        [8.0, 87.729756982402],
                        [9.0, 122.304433964459],
                        [10.0, 160.230305517596],
                        [11.0, 200.010310684876]])

cp_curve = np.stack((power_curve[:, 0], power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 27.0 ** 2 * np.pi * power_curve[:, 0] ** 3)), axis=1)

ct_curve = np.array([[3.0, 0.815113690200702],
                     [4.0, 0.814815186652044],
                     [5.0, 0.814815186652044],
                     [6.0, 0.814715685469159],
                     [7.0, 0.814317680737615],
                     [8.0, 0.805760579009434],
                     [9.0, 0.74814939411854],
                     [10.0, 0.687453672558185],
                     [11.0, 0.626061442717629]])

rpm_curve = np.array([[3.0, 16.6412],
                      [4.0, 22.1864],
                      [5.0, 27.7301],
                      [6.0, 33.2719],
                      [7.0, 38.8103],
                      [8.0, 43.5116],
                      [9.0, 43.5646],
                      [10.0, 43.6227],
                      [11.0, 43.6833]])

pitch_curve = np.array([[3.0, -0.75],
                        [4.0, -0.75],
                        [5.0, -0.75],
                        [6.0, -0.75],
                        [7.0, -0.75],
                        [8.0, -0.75],
                        [9.0, -0.75],
                        [10.0, -0.75],
                        [11.0, -0.75]])


class V27SWIFT(EllipSysOneTypeWT):   # V27 SWiFT
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='V27SWIFT',
                                   diameter=27.0,
                                   hub_height=32.1,
                                   cutin=3.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   # tilt=4.05, tilt has been moved to calc_wt_interactoin using tilt_ilk
                                   rated_power=225.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1], power_unit='kW',
                                                                  ct=ct_curve[:, 1], ws_cutin=3.0, ws_cutout=11.0),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1], left=0.0, right=0.0),
                                   pitch_func=lambda ws: np.interp(ws, pitch_curve[:, 0], pitch_curve[:, 1], left=0.0, right=0.0),
                                   bladeloading_func='',
                                   airfoildata_file='v27swift.pc',
                                   airfoildata_path=ADairfoil_path,
                                   airfoildata_file_type=2,  # 1=flex5, 2=hawc2
                                   bladegeo_file='v27swift.geo',
                                   bladegeo_path=ADairfoil_path)


def main():
    if __name__ == '__main__':
        wt = V27SWIFT()
        print('Diameter', wt.diameter())
        print('Hub height', wt.hub_height())
        ws = np.arange(3, 25)
        import matplotlib.pyplot as plt
        plt.plot(ws, wt.power(ws), '.-')
        plt.show()


main()
