import numpy as np

from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysOneTypeWT
from py_wake.examples.data.hornsrev1 import power_curve, ct_curve
from py_wake_ellipsys_examples.data.turbines.nrel5mw import nrel5mw_bladeloading
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular


cp_curve = np.stack((power_curve[:, 0], power_curve[:, 1] * 1e3 / (0.125 * 1.225 * 80.0 ** 2 * np.pi * power_curve[:, 0] ** 3)), axis=1)
# Estimate RPM from a set tip speed ratio, and min and max rpm
# Several sources exist that report different values
TSR = 8.0
RPMmin = 9.0
RPMmax = 19.0
R = 40
U = np.arange(4.0, 26, 1.0)
rpm = np.maximum(np.minimum(TSR * U / R * 30 / np.pi, RPMmax), RPMmin)
rpm_curve = np.zeros((23, 2))
rpm_curve[0, :] = [3.0, 0.0]
rpm_curve[1:, 0] = U
rpm_curve[1:, 1] = rpm


class V80(EllipSysOneTypeWT):   # V80
    def __init__(self):
        EllipSysOneTypeWT.__init__(self, name='V80',
                                   diameter=80.0,
                                   hub_height=70.0,
                                   cutin=4.0,
                                   cutout=25.0,
                                   dws=1.0,
                                   rated_power=2000.0,
                                   rotdir='cw',
                                   powerCtFunction=PowerCtTabular(ws=power_curve[:, 0],
                                                                  power=power_curve[:, 1] * 1e-3, power_unit='kW',
                                                                  ct=ct_curve[:, 1]),
                                   cp_func=lambda ws: np.interp(ws, cp_curve[:, 0], cp_curve[:, 1], left=0.0, right=0.0),
                                   rpm_func=lambda ws: np.interp(ws, rpm_curve[:, 0], rpm_curve[:, 1]),
                                   pitch_func='',
                                   bladeloading_func=lambda r: (np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 1]),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 2]),
                                                                np.interp(r, nrel5mw_bladeloading[:, 0], nrel5mw_bladeloading[:, 3])),
                                   airfoildata_file='',
                                   airfoildata_path='',
                                   airfoildata_file_type='',  # 1=flex5, 2=hawc2
                                   bladegeo_file='',
                                   bladegeo_path='')
