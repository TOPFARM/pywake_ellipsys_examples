import numpy as np
import math as mt
from py_wake_ellipsys.utils.terraingridutils import write_box_grd


def deg2std(height, max_slope):
    return height * np.exp(-0.5) / (mt.tan(max_slope / 180.0 * np.pi))


def gauss2d(x, y, mu_x, mu_y, sigma_x, sigma_y, height):
    return height * np.exp(-(x - mu_x) ** 2 / (2.0 * sigma_x ** 2) - (y - mu_y) ** 2 / (2.0 * sigma_y ** 2))


def save_grd_map(mapoutfile, mapname, x, y, z):
    nx = len(x)
    ny = len(y)
    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)
    zmin = z.min()
    zmax = z.max()
    f = open(mapoutfile, 'w')
    f.write(mapname + '\r\n')
    f.write('%i %i\r\n' % (nx, ny))
    f.write('%g %g\r\n' % (xmin, xmax))
    f.write('%g %g\r\n' % (ymin, ymax))
    f.write('%g %g\r\n' % (zmin, zmax))
    for j in range(ny):
        for i in range(nx):
            f.write('%g %s' % (z[j, i], ''))
        f.write('\n')
    f.close()


def save_terrain_height(filename, x, y, z):
    f = open(filename, 'w')
    for j in range(len(y)):
        for i in range(len(x)):
            f.write('%g %g %g\n' % (x[i], y[j], z[j, i]))
    f.close()
    return


def create_terrain(x, y, height, mu_x, mu_y, maxangle_x, maxangle_y, roughness_maps=False, zouter=0.0):
    # Create a 2D Gaussian grd file
    sigma_x = deg2std(height, maxangle_x)
    sigma_y = deg2std(height, maxangle_y)
    z = np.zeros((len(y), len(x)))
    for j in range(len(y)):
        for i in range(len(x)):
            z[j, i] = gauss2d(x[i], y[j], mu_x, mu_y, sigma_x, sigma_y, height) + zouter

    # Save grd file
    save_grd_map('gaussianhill_inner.grd', 'gaussianhill', x, y, z)

    # Write background grd file with outer height
    write_box_grd('gaussianhill_outer.grd', zouter, -1e6, 1e6, -1e7, 1e7)

    # Save inner terrain for plotting
    save_terrain_height('gaussianhill_inner.dat', x, y, z)

    # Write two grd files to make a simple box-shaped roughness change
    z0Outer = 0.03
    z0Inner = 0.1
    write_box_grd('gaussianhill_inner_z0.grd', z0Inner, 0.0, 1e6, 0.0, 1e7)
    write_box_grd('gaussianhill_outer_z0.grd', z0Outer, -1e6, 1e6, -1e7, 1e7)


def main():
    if __name__ == '__main__':
        create_terrain(np.arange(-1500.0, 4510.0, 10.0), np.arange(-4500.0, 5510.0, 10.0),
                       300.0, 1000.0, 500.0, 20.0, 10.0, zouter=100.0, roughness_maps=True)


main()
