import numpy as np
from numpy import newaxis as na
import sys
import os
import time
import xarray
from datetime import date
from scipy import interpolate
from py_wake.tests import npt
from py_wake.site._site import UniformWeibullSite
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysWTs
from py_wake_ellipsys.utils.wtgeneric import WTgen
from py_wake_ellipsys.utils.lut import calc_lut, define_lut, run_lut
from py_wake_ellipsys.utils.ad_integration import AD_integrated_var, cxzy_to_cxyzg, e3d_get_adgrid_params, AD_integrated_xline
from py_wake_ellipsys.utils.flatboxgridutils import get_wf_center
from py_wake_ellipsys_examples.LUT import LUTexample_path
from py_wake.site.shear import LogShear
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


class UniformSite(UniformWeibullSite):
    def __init__(self, A, k, TI, nsector=12.0, shear=None):
        UniformWeibullSite.__init__(self, np.ones(int(nsector)) / float(nsector), [A] * int(nsector), [k] * int(nsector), TI, shear=shear)


def set_coarse(wfm):
    wfm.set_subattr('e3d.reslim', 1e-2)
    wfm.set_subattr('pre.tolOpt', 5e-2)
    wfm.set_subattr('pre.maxit', 1)
    wfm.set_subattr('grid.cells1_D', 1.0)
    wfm.set_subattr('grid.z_cells1_D', 1.0)
    wfm.set_subattr('grid.bsize', 48)
    wfm.set_subattr('ad.grid.nr', 5)
    wfm.set_subattr('ad.grid.ntheta', 8)
    Cluster.maxnodes = 1
    # Cluster.corespernode = 1
    wfm.wfrun.cluster.walltime = '0:20:00'
    wfm.wfpostflow.cluster.job_exclusive = False


def run_RANS(Site, wt, TI, zRef, ws, wd, wt_x, wt_y, type_i, casename, run_machine, queue,
             coarse=True, grid=False, pre=False, cal=False, run=False, write=False, post=False, write_flow=False):

    set_cluster_vars(run_machine, True, queue, 32, 4)

    # AD
    ad = AD(force='2111', grid=ADGrid(nr=64, ntheta=64))
    # Wind farm grid small rotor
    Dref = wt.diameter()
    wfgrid = FlatBoxGrid(Dref,
                         cells1_D=12.0,
                         dwd=1.0,  # To reduce grid size, because we only simulate 270 deg
                         # z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area
                         bsize=64)
    e1d = E1D(reslim=1e-9)
    e3d = E3D(reslim=1e-7)
    # Wind farm run
    wfrun = WFRun(casename=casename, write_restart=True,
                  cluster=Cluster(walltime='3:59:00'))
    wfpostflow = WFPostFlow(outputformat='netCDF', rescale_netCDF=True)
    wfm = EllipSys(Site, wt, wfgrid, TI, zRef,
                   e1d=e1d, e3d=e3d, ad=ad, pre=PreMost(), wfrun=wfrun, wfpostflow=wfpostflow,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_wf=run)
    wfm.calrun.grid.bsize = 32
    wfm.calrun.cluster.walltime = '2:59:00'
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    h_i = [zRef] * len(wt_x)
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x, wt_y, h_i, type_i, wd, ws)

    if write:
        f = open('RANSdata_%s.dat' % casename, 'w')
        f.write('# x [m], y[m], Power [W], U_AD [m/s]\n')
        for i in range(len(wt_x)):
            f.write('%8.6e %8.6e %8.6e %8.6e %8.6e\n' % (wt_x[i], wt_y[i], power_ilk[i, 0, 0], WS_eff_ilk[i, 0, 0], TI_eff_ilk[i, 0, 0]))
        f.close()

    if post:
        wfm.post_windfarm_flow(wd, ws)

    if write_flow:
        infile = 'run_%s_Jou_rot_shear_con_kefP_%gcD_Ti%g_zeta0_wdGrid270/post_flow_wd%g_ws%g/flowdata.nc' % (casename, wfm.grid.cells1_D, TI, wd[0], ws[0])
        print('Wait for netCDF file')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        data = xarray.open_dataset(infile)

        # Calculate ad integrated U and tke over a line through the wind turbine row
        nr = 64
        ntheta = 64
        wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x, wt_y, type_i, wd[0], grid_wd=270.0)
        # WT positions only:
        # vars_ADint, pos = AD_integrated_xline(wt_x_cfd[0], wt_y_cfd[0], zRef, wt_x_cfd[-1], 5 * Dref, nr, ntheta, Dref, vars=['U', 'tke'], flowdata=data)
        # print(vars_ADint)
        vars_ADint, pos = AD_integrated_xline(wt_x_cfd[0] - 3 * Dref, wt_y_cfd[0], zRef, wt_x_cfd[-1] + 10 * Dref, 0.1 * Dref, nr, ntheta, Dref, flowdata=data, vars=['U', 'tke'])
        xWFc, yWFc = get_wf_center(wt_x, wt_y, True, 270.0)
        f = open('RANSdata_flow_%s.dat' % casename, 'w')
        f.write('# x/D, y/D, z/zH, U_AD/U0, tke_AD/U0**2\n')
        for i in range(len(pos[:, 0])):
            f.write('%8.6e %8.6e %8.6e %8.6e %8.6e\n' % ((pos[i, 0] + xWFc) / Dref, (pos[i, 1] + yWFc) / Dref, pos[i, 2] / zRef, vars_ADint[0][i] / ws[0], vars_ADint[1][i] / ws[0] ** 2))
        f.close()


def run_RANSlut(Site, wt, zRef, Dref, Prated, ws, wd, wt_x, wt_y, type_i, casename, path_LUT, ADcontrolfile, s_D,
                nr, ntheta, write=False, test_4WT_4D=False, test_UAD_1WT=False, plot=False, shearcor=False):
    wfm = define_lut(Site, wt, zRef, Dref, path_LUT, shearcor=False)
    power_ilk, ct_ilk, WS_eff_ilk = run_lut(wfm, zRef, Dref, Prated, ws, wd, wt_x, wt_y, type_i,
                                            ADcontrolfile, nr=nr, ntheta=ntheta)

    if write:
        # Store results
        data = xarray.Dataset({'power': xarray.DataArray(power_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                                     ('wd', wd),
                                                                     ('ws', ws)]),
                               'ct': xarray.DataArray(ct_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                               ('wd', wd),
                                                               ('ws', ws)]),
                               'WS_eff': xarray.DataArray(WS_eff_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                                       ('wd', wd),
                                                                       ('ws', ws)]),
                               'TI_eff': xarray.DataArray(TI_eff_ilk, [('wt', np.arange(0, nWT, 1, dtype=int)),
                                                                       ('wd', wd),
                                                                       ('ws', ws)])})
        data.to_netcdf('RANSLUT_results_UAD_ws%g.nc' % ws[0])

    if test_4WT_4D:
        # Check power, based in fine LUT and RANS results using 5 wts, with 5D spacing
        # print(power_ilk)
        power_ilk_expected = np.array([[[3433500.83803484]], [[1133995.588661]], [[1084488.52192273]], [[1029533.13709412]]]) * 1e-6
        npt.assert_array_almost_equal(power_ilk * 10 ** (-6), power_ilk_expected, 6)

    if test_UAD_1WT:
        # Test when using 1 WT and shear correction
        # Without the shear correction 0.5% error in power can occur for a single WT.
        # When the shear correction is used then this error is only 0.005%.
        # However, the shear correction does not work for multiple WTs.
        RANS = np.genfromtxt(LUTexample_path + 'DTU10MW/RANS/RANSdata_%s.dat' % casename, skip_header=True)
        RANSUAD = RANS[3]
        RANSpower = RANS[2]
        diff = (WS_eff_ilk[0, 0, 0] - RANSUAD) / RANSUAD * 100
        print('UAD_RANS', RANSUAD, 'UAD_RANS-LUT:', WS_eff_ilk[0, 0, 0])
        print('diff UAD  RANS-LUT vs RANS: ', diff, '[%]')
        diff_power = (power_ilk[0, 0, 0] - RANSpower) / RANSpower * 100
        print('Power_RANS', RANSpower, 'Power_RANS-LUT:', power_ilk[0, 0, 0])
        print('diff Power  RANS-LUT vs RANS: ', diff_power, '[%]')
        # 15MW WTgen example had a smaller error:
        # diff_expected = -0.0018131238769583766
        diff_expected = -0.03688518253755399
        npt.assert_array_almost_equal(diff * 10 ** 3, diff_expected * 10 ** 3, 6)

    if plot:
        RANSpower = np.genfromtxt(LUTexample_path + 'DTU10MW/RANS/RANSdata_%s.dat' % casename, skip_header=True)
        Pref = wt.power(ws=ws[0])
        fig, ax = plt.subplots(1, 1, figsize=(5.0, 5.0))
        ax.plot(np.arange(1, nWT + 1, 1), RANSpower[:, 2] / Pref, '-ok', lw=2, label='RANS')
        ax.plot(np.arange(1, nWT + 1, 1), power_ilk[:, 0, ] / Pref, '-or', lw=2, label='RANS LUT')
        ax.set_ylabel(r'$P_i/P_{\rm ref}$')
        ax.set_xlabel('WT nr.')
        ax.set_xticks([1, 2, 3, 4, 5])
        ax.grid(True)
        ax.legend(loc=0)
        filename = casename + '_power.pdf'
        fig.savefig(filename)

        # Plot AD averaged flow
        RANSflow = np.genfromtxt(LUTexample_path + 'DTU10MW/RANS/RANSdata_flow_%s.dat' % casename, skip_header=True)

        # Calculate ad integration parameters
        nr = 64
        ntheta = 64
        wfm = EllipSys(Site, wt, FlatBoxGrid(Dref), TI, zRef)
        wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x, wt_y, type_i, wd[0], grid_wd=270.0)
        simres = wfm(wt_x_cfd, wt_y_cfd, wd=270.0, ws=ws)
        vars_ADint, pos = AD_integrated_xline(wt_x_cfd[0] - 3 * Dref, wt_y_cfd[0], zRef, wt_x_cfd[-1] + 10 * Dref, 0.1 * Dref, nr, ntheta, Dref, wfm=wfm, simres=simres)
        xWFc, yWFc = get_wf_center(wt_x, wt_y, True, 270.0)

        fig, ax = plt.subplots(2, 1, figsize=(5.0, 5.0))
        ax[0].plot(RANSflow[:, 0] / s_D, RANSflow[:, 3] * 8.0, '-g', lw=2, label='RANS')
        ax[0].plot((pos[:, 0] + xWFc) / Dref / s_D, vars_ADint[0] / ws[0] * 8.0, '-r', lw=2, label='RANS-LUT')
        ax[0].set_ylabel(r'$U_{\rm AD}/U_0$')

        ax[1].plot(RANSflow[:, 0] / s_D, np.sqrt(2.0 / 3.0 * RANSflow[:, 4]) - TI, '-g', lw=2, label='RANS')
        ax[1].plot((pos[:, 0] + xWFc) / Dref / s_D, vars_ADint[1] - TI, '-r', lw=2, label='RANS-LUT')
        ax[1].set_ylabel(r'$I_{\rm AD}-I_0$')
        ax[1].set_xlabel(r'$x/s$')
        for i in range(2):
            ax[i].grid(True)
        ax[1].legend(loc=0)
        filename = casename + '_flow.pdf'
        fig.savefig(filename)

        # Differences
        print('Diff in power [%]:', (power_ilk[:, 0, 0] - RANSpower[:, 2]) / RANSpower[:, 2] * 100.0)
        # Find wt position in stream tube
        indices = np.zeros((nWT), dtype=int)
        for i in range(nWT):
            indices[i] = np.where(np.floor(pos[:, 0] * 1e4) * 1e-4 == np.floor(wt_x_cfd[i] * 1e4) * 1e-4)[0][0]
        print('Diff in UAD at wts [%]:', (vars_ADint[0][indices] / ws[0] - RANSflow[indices, 3]) / RANSflow[indices, 3] * 100)
        print('Diff in IAD at wts [%]:', (vars_ADint[1][indices] - np.sqrt(2.0 / 3.0 * RANSflow[indices, 4])) * 100)
        print('Diff in UAD, abs max [%]:', np.max(np.abs((vars_ADint[0][:] / ws[0] - RANSflow[:, 3]) / RANSflow[:, 3] * 100)))
        print('Diff in IAD, abs max [%]:', np.max(np.abs(vars_ADint[1][:] - np.sqrt(2.0 / 3.0 * RANSflow[:, 4] * 100))))


if __name__ == '__main__':
    starttime = time.time()
    run_machine = 'sophia'
    # run_machine = 'local'
    queue = 'windq,workq,windfatq,fatq'

    # Or DTU-10MW (the only difference for the LUTs would be the ground clearance zH/D, the blade forces
    # through cp and the tip speed ratio when the Joukowsky rotor model is used)
    from py_wake_ellipsys_examples.data.turbines.dtu10mw import DTU10MW
    wt = DTU10MW()
    zRef = wt.hub_height()
    Dref = wt.diameter()
    Prated = 10.0e6

    '''
    # Rerun LUT library
    # Range of TIs, here we also use 0.047 which was used to simulate the Energy Island with RANS-LUT
    TIs = np.array([0.05, 0.1, 0.2, 0.3])
    # Range of CTs
    cts = np.array([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])
    set_cluster_vars(run_machine, True, queue, 32, 1)
    calc_lut(TIs, cts, wt, zRef, Dref,
             grid_cells1_D=12.0, grid_z_cells1_D=None, nr=64, ntheta=64,
             coarse=False,  # For testing, set to False for actual LUT generation
             grid=True,
             pre=True,
             run=True,
             post=True,
             database=True,
             database_reduce='rotor'
             )
    '''

    # Wind farm case (Case 1 in Wake Conference article of Javier)
    TI = 0.05
    nWT = 4  # Number of turbines in a row
    s_D = 4.0  # Spacing normalized by the rotor diamete
    ws = np.array([8.0])

    # Wind farm case (Case 2 in Wake Conference article of Javier)
    # TI = 0.05
    # nWT = 4  # Number of turbines in a row
    # s_D = 7.0  # Spacing normalized by the rotor diamete
    # ws = np.array([8.0])

    # nWT = 1  # For single WT testing

    nr = 64
    ntheta = 64
    cmu = 0.03
    kappa = 0.4
    uStar_UH = TI * np.sqrt(1.5 * np.sqrt(cmu))
    z0 = zRef / (np.exp(kappa / uStar_UH) - 1)
    print('z0', z0)
    Site = UniformSite(A=10.0, k=2.4, TI=TI)
    Site.shear = LogShear(h_ref=zRef, z0=z0, interp_method='linear')
    wd = np.array([270.0])
    wt_x = np.arange(0, nWT, 1) * s_D * Dref
    wt_y = np.zeros((nWT))
    type_i = np.zeros((nWT), dtype=int)
    casename = '1x%gWT-%gD' % (nWT, s_D)

    '''
    # Rerun full RANS for reference and input
    run_RANS(Site, wt, TI, zRef, ws, wd, wt_x, wt_y, type_i, casename, run_machine, queue,
             coarse=True,
             grid=True,
             pre=True,
             cal=True,
             run=True,
             write=True,
             post=True,
             write_flow=True
             )
    '''

    # RANS-LUT
    # Change PATH to your RANS single wake library, AD control file and AD grid
    path_LUT = '/groups/PyWakeEllipSys/LUT/DTU10MW.nc'
    ADcontrolfile = LUTexample_path + 'DTU10MW/RANS/DTU10MW_Jou_rot_shear_kefP_12cD_Ti0.05_zeta0.dat'
    # ADcontrolfile = 'calibration_WTgen_Jou_rot_shear_kefP_8cD_Ti%g_zeta0/WTgen_Jou_rot_shear_kefP_8cD_Ti%g_zeta0.dat' % (TI, TI)

    # Calculate WT power per wind speed with LUT model and store as netCDF files
    # We use a slightly lower ws_cutin in PyWake to be consistent with PyWakeEllipSys
    # wt.ws_cutin = 3.95
    # for k in range(len(ws)):
    #     run_RANSlut(Site, wt, zRef, Dref, Prated, [ws[k]], wd, wt_x, wt_y, type_i, casename, path_LUT, ADcontrolfile,
    #                 write=True, test=False, plot=False, shearcor=True)
    run_RANSlut(Site, wt, zRef, Dref, Prated, ws, wd, wt_x, wt_y, type_i, casename, path_LUT, ADcontrolfile, s_D, nr, ntheta,
                write=False, test_4WT_4D=True, test_UAD_1WT=False, plot=True, shearcor=False)
    # Test shear correction for a single WT, which works well (set nWT=1)
    # run_RANSlut(Site, wt, zRef, Dref, Prated, ws, wd, wt_x, wt_y, type_i, casename, path_LUT, ADcontrolfile, s_D, nr, ntheta,
    #             write=False, test_4WT_4D=False, test_UAD_1WT=True, plot=False, shearcor=True)
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
