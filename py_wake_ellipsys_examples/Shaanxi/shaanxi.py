import numpy as np
import time
import os
import sys
import xarray
import time
from datetime import date

from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.examples.data.hornsrev1 import Hornsrev1Site


def run_Shaanxi(run_machine, queue, coarse=True, grid=False, cal=False, run=False,
                post=False, write=False, plot=False, plotfmt='pdf', rans_path=''):
    from py_wake_ellipsys_examples.data.farms.shaanxi import wt_x, wt_y, h0_i
    from py_wake_ellipsys_examples.data.turbines.hz93 import HZ93
    from py_wake_ellipsys_examples.plotwfres import plotWFrow, plotWFpower
    from py_wake_ellipsys.utils.terraingridutils import plot_x2d, read_x2d, write_x2d_noattr
    from py_wake.validation.validation_lib import GaussianFilter
    from py_wake_ellipsys_examples.Shaanxi.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Setup for calculating the power of on onshore wind farm in complex terrain

    wt = HZ93()
    Dref = wt.diameter()
    # domain_center = [316864.0, 4153557.0]  # WT10, could be used for sitting without wind turbines
    nWT = len(wt_x)
    type_i = np.zeros((nWT))
    h_i = wt.hub_height(type_i)  # Hub height of wts

    terrain_map = data_path + 'Shaanxi.map'
    # wd = [180.0]
    wdbinCenter = 180.0
    wd = np.arange(160.0, 205.0, 5.0)
    ws = [10.0]
    Uref_wt = 13  # In order to get 10 m/s at WT14
    zRef = 67.0
    z0Inlet = 0.1
    kappa = 0.4
    cmu = 0.03
    TI = kappa * np.sqrt(2.0 / 3.0) / (cmu ** 0.25 * np.log((zRef + z0Inlet) / z0Inlet))
    sigma = 5.0  # Guessed wind direction uncertainty [deg]
    today = date.today()
    if coarse:
        grid_cells1_D = 2.0
        e3d_reslim = 1e-5
        grid_bsize = 32
        walltime_wf_run = '0:30:00'
        maxnodes = 4
    else:
        grid_cells1_D = 8.0
        e3d_reslim = 1e-5
        grid_bsize = 48
        walltime_wf_run = '2:00:00'
        maxnodes = 16

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='2111', run_pre=True)
    # Wind farm grid
    wfgrid = TerrainGrid(Dref, type='terrainogrid',
                         dwd=360.0, cells1_D=grid_cells1_D,
                         bsize=grid_bsize,
                         zlen_D=100.0, zFirstCell_D=0.5 / Dref,
                         terrain_map=terrain_map, terrain_map_extrasmooth=True,
                         terrain_map_farfield_z0=z0Inlet,
                         terrain_map_farfield_h=-1,
                         terrain_hypsf_blendf=0.5,
                         radius_D=28000.0 / Dref,
                         # Could be changed:
                         terrain_map_smooth_rmin=7000.0,
                         terrain_map_smooth_rmax=17500.0,
                         # terrain_surf_only=True,
                         # Margin are relative to grid_wd=270, here we set them all equal
                         m1_e_D=20.0, m1_w_D=20.0,
                         m1_n_D=20.0, m1_s_D=20.0,
                         m2_w_D=0.0, m2_e_D=0.0,
                         m2_n_D=0.0, m2_s_D=0.0,
                         cells2_D=1.0, terrain_surfgen='hypgridsf',
                         cluster=Cluster(walltime='1:59:00'))
    e3d = E3D(reslim=e3d_reslim)
    # Wind farm run
    wfrun = WFRun(casename='Shaanxi', cluster=Cluster(walltime=walltime_wf_run),
                  write_restart=True, Uref_wt=Uref_wt)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   ad=ad, e3d=e3d, wfrun=wfrun, run_wd_con=False,
                   run_grid=grid, run_cal=cal, run_wf=run, run_post=post)
    # Make grid and run simulations
    # One could also use create_windfarm_grid() and run_windfarm() but then one should take care of grid_wd
    WS_eff_ilk, TI_effilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x, wt_y, h_i, type_i, wd, ws)

    if write:
        # Write wind turbine power
        # Gaussian averaging
        powerGA = np.zeros(power_ilk.shape)
        for i in range(nWT):
            powerGA[i, :, 0] = GaussianFilter(power_ilk[i, :, 0], wd, 3, sigma)

        filename = 'Shaanxi_RANS_power_wd%g_ws10.dat' % wdbinCenter
        f = open(filename, 'w')
        f.write('# EllipSys3D-RANS, %s\n' % wfm.e3d.turbmodel)
        f.write('# Rerun date=%s\n' % today)
        f.write('# P14=%10.2f [W]\n' % (power_ilk[13, 0, 0]))
        f.write('# GA with sigma=%g deg\n' % sigma)
        f.write('#       Pi/P14 -+5 deg\n')
        f.write('# WT_i, noGA, GA\n')
        l1 = np.where(wd == wdbinCenter - 5.0)[0][0]
        l2 = np.where(wd == wdbinCenter + 5.0)[0][0] + 1
        for i in range(nWT):
            f.write('%-5i %10.8f %10.8f\n' % (i + 1, power_ilk[i, l1:l2, 0].sum() / power_ilk[13, l1:l2, 0].sum(),
                    powerGA[i, l1:l2, 0].sum() / powerGA[13, l1:l2, 0].sum()))
        f.close()
        # Write coarse out file of inner surface grid
        gridname = wfm.grid.get_grid_name(wfm.wfrun.casename)
        x2dfile = gridname + '/surfacemesh/grid-270.X2D'
        xyz_ver, nblocks, bsize = read_x2d(x2dfile, True)
        nred = 4
        write_x2d_noattr('Shaanxi.X2D', xyz_ver[:, ::nred, ::nred, :], nblocks, int(bsize / nred), fmt='%6.4e')

    if plot:
        # Plot Power and elevation as contours
        # Data
        datafile = data_path + 'measurements/Shaanxi_WFdata_wd180_ws10.dat'
        dataPower = np.genfromtxt(datafile, skip_header=True)
        plot_x2d(rans_path + 'Shaanxi.X2D', ext=plotfmt, attr=False, contours=True, outname='Shaanxi_WFdata_power_and_elevation', contour_delta=10.0,
                 wt_x=wt_x, wt_y=wt_y, wt_c=dataPower[:, 1], wt_clabel=r'$P/P_{14}$', wt_cmin=0.4, wt_cmax=1.0, wt_i=True)
        # RANS (plus GA)
        Power = np.genfromtxt(rans_path + 'Shaanxi_RANS_power_wd180_ws10.dat', skip_header=True)
        plot_x2d(rans_path + 'Shaanxi.X2D', ext=plotfmt, attr=False, contours=True, outname='Shaanxi_RANS_power_and_elevation', contour_delta=10.0,
                 wt_x=wt_x, wt_y=wt_y, wt_c=Power[:, 2], wt_clabel=r'$P/P_{14}$', wt_cmin=0.4, wt_cmax=1.0, wt_i=True)
        # Power plot first row
        wt_i_plot = [17, 2, 4, 6, 7, 9, 11, 13, 16, 21]
        datafile = data_path + 'measurements/Shaanxi_WFdata_wd180_ws10.dat'
        modelfiles = [rans_path + 'Shaanxi_RANS_power_wd180_ws10.dat']
        modellabels = ['RANS']
        modelcolors = ['g']
        outfile = 'Shaanxi_Row1_wd180_ws10.' + plotfmt
        plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, wt_i_plot, wt.diameter(), 180.0,
                  ylabel='$P_i/P_{14}$', dataerrorbar=False, figx=9.0, xticklabels=np.asarray(wt_i_plot) + 1,
                  wfx=0.3, wfy=0.3, fulldata=True)
        # Power plot of a downstream row
        wt_i_plot = [0, 1, 3, 5, 8, 10, 12, 14, 19, 23]
        outfile = 'Shaanxi_Row2_wd180_ws10.' + plotfmt
        plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, wt_i_plot, wt.diameter(), 180.0,
                  ylabel='$P_i/P_{14}$', dataerrorbar=False, figx=9.0, xticklabels=np.asarray(wt_i_plot) + 1,
                  wfx=0.3, wfy=0.3, fulldata=True)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    starttime = time.time()
    queue = 'windq'
    run_Shaanxi(run_machine, queue, coarse=True, grid=True, cal=True, run=True,
                post=True, write=True, plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
