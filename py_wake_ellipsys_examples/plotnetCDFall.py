import numpy as np
import xarray
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import griddata, interp1d

mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


def plot_netCDF_flowvar_flatterrain(zPlot, D, points_D, Lx_D, Ly_D,
                                    interpolateflag=True, infile='flowdata.nc'):
    # Plot all flow variables of a netCDF file containing a flow data
    # of flat terrain wind farm simulation.
    # Each plot is horizontal contour plot at z=zplot, with
    # horizontal dimension Lx_D and Ly_D.

    # Open netCDF data file
    print('Load netCDF file')
    data = xarray.open_dataset(infile)
    print('netCDF file loaded')

    # List flow variables
    flowvarnames = list(data.keys())
    print(flowvarnames)

    # Number of flow variables
    nvar = len(flowvarnames)

    # Create a plot at hub height for each flow variable
    figx = 10.0
    figy = 10.0 * Ly_D / Lx_D * nvar
    fig, ax = plt.subplots(nvar, 1, sharey=True, sharex=True, figsize=(figx, figy))
    cmap = plt.cm.get_cmap('jet')
    nx = points_D * Lx_D + 1
    xi = np.linspace(-0.5 * Lx_D, 0.5 * Lx_D, nx)
    ny = points_D * Ly_D + 1
    yi = np.linspace(-0.5 * Ly_D, 0.5 * Ly_D, ny)
    Xi, Yi = np.meshgrid(xi, yi)

    for i in range(len(flowvarnames)):
        print('Plot ', flowvarnames[i])
        ax[i].set_title(flowvarnames[i] + ' [' + data.variables[flowvarnames[i]].attrs['units'] + ']')
        ax[i].set_aspect('equal')
        # Interpolate data around hub height
        if interpolateflag:
            U = np.array(data[flowvarnames[i]].interp(x=xi * D, y=yi * D, z=zPlot, method='linear'))
        else:
            # Or just take the closest index
            U = np.array(data[flowvarnames[i]].interp(x=xi * D, y=yi * D, z=zPlot, method='nearest'))
        # Make contour plot
        vmin = U.min()
        vmax = U.max()
        if vmin == vmax:
            vmin = vmax * 0.99
        levels = np.linspace(vmin, vmax, 15)
        cs = ax[i].contourf(Xi, Yi, U.T, levels, cmap=cmap, vmax=vmax, vmin=vmin)
        divider1 = make_axes_locatable(ax[i])
        cax1 = divider1.append_axes("right", size="2.5%", pad=0.05)
        cbar1 = plt.colorbar(cs, cax=cax1)
        ax[i].set_ylabel('$y/D$')
    ax[-1].set_xlabel('$x/D$')
    filename = 'Wake.pdf'
    fig.savefig(filename)


if __name__ == '__main__':
    plot_netCDF_flowvar_flatterrain(zPlot=90.0, D=126.0, points_D=8, Lx_D=20, Ly_D=5,
                                    interpolateflag=True)
