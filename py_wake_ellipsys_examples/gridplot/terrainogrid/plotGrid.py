import matplotlib.pyplot as plt
import numpy as np
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from scipy.interpolate import griddata
from matplotlib import rcParams
from matplotlib import gridspec

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

from py_wake_ellipsys.utils.flatboxgridutils import get_margin_coords
from py_wake_ellipsys.utils.terraingridutils import read_x2d

import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')

# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')

rcParams['xtick.direction'] = 'out'
rcParams['ytick.direction'] = 'out'


def plotgrid_terrainogrid(folder='.', outfile='terrainogrid.png'):

    surfacegrid = folder + '/surfacemesh/assembled.X2D'
    xyz_ver, nblocks, bsize = read_x2d(surfacegrid, True)

    D = 126.0
    radius = 50000.0 / D

    xWT = np.array([0.0, 630.0, 630.0, 0.0]) + 1000.0
    yWT = np.array([0.0, 0.0, 630.0, 630.0]) + 500.0

    xWFc = 0.5 * (min(xWT) + max(xWT)) / D
    yWFc = 0.5 * (min(yWT) + max(yWT)) / D

    # Read margins from gcf.dat
    m1, m2 = get_margin_coords('terrainogrid', 'sfhill', folder)
    m1 = m1 / D - np.array([[xWFc] * 5, [yWFc] * 5])
    m2 = m2 / D - np.array([[xWFc] * 5, [yWFc] * 5])

    # Read additional info from gcf.dat
    with open(folder + '/surfacemesh/gcf.dat', 'r') as f:
        for line in f.readlines():
            line = line.rstrip('\n')
            if line[0:22] == 'horizontal_domain_size':
                xlen = float(line.split(' ')[1]) / D
                ylen = float(line.split(' ')[2]) / D
            elif line[0:13] == 'domain_centre':
                xc = float(line.split(' ')[1]) / D
                yc = float(line.split(' ')[2]) / D
            elif line[0:6] == 'xshift':
                xshift = float(line.split(' ')[-1]) / D
            elif line[0:6] == 'yshift':
                yshift = float(line.split(' ')[-1]) / D
            elif line[0:10] == 'flow_angle':
                grid_wd = float(line.split()[-1])

    # Hypgridsf
    deg = (270.0 - float(grid_wd)) / 180.0 * np.pi
    cos = np.cos(deg)
    sin = np.sin(deg)

    ysplit = 0.5 * ylen  # Assumption!
    xc2 = - cos * xshift + sin * yshift + sin * (0.5 * ylen - ysplit)
    yc2 = - sin * xshift - cos * yshift - cos * (0.5 * ylen - ysplit)

    Lx = m2[0, 3] + m2[0, 2]
    Ly = m2[1, 3] + m2[1, 2]

    fig, ax = plt.subplots(2, 1, sharey=False, figsize=(7.5, 5))
    fig.subplots_adjust(right=0.5)
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    ax[0] = plt.subplot(gs[0])
    ax[1] = plt.subplot(gs[1])

    for n in range(nblocks):
        ax[0].plot(xyz_ver[n, :, :, 0] / D - xWFc, xyz_ver[n, :, :, 1] / D - yWFc, '-k', linewidth=0.1)
        ax[0].plot(np.transpose(xyz_ver[n, :, :, 0]) / D - xWFc, np.transpose(xyz_ver[n, :, :, 1]) / D - yWFc, '-k', linewidth=0.1)

    ax[0].set_aspect('equal')

    # Lines marking wake regions
    ax[0].plot(m2[0], m2[1], 'm', linewidth=0.5)
    ax[0].plot(m1[0], m1[1], 'b', linewidth=0.5)

    margin = 2000.0 / D
    rlen = np.sqrt(xlen ** 2 + ylen ** 2)
    xmin = xyz_ver[:, :, :, 0].min() / D
    xmax = xyz_ver[:, :, :, 0].max() / D
    ymin = xyz_ver[:, :, :, 1].min() / D
    ymax = xyz_ver[:, :, :, 1].max() / D
    ax[0].set_xlim(xmin - margin, xmax + margin)
    ax[0].set_ylim(ymin - margin, ymax + margin)

    padding = 1
    bbox = dict(facecolor='white', alpha=1, pad=padding)

    # grid center line
    size = max((xmax - xmin), (ymax - ymin)) * 0.8
    ax[0].plot([- cos * size, cos * size], [-sin * size, sin * size], '--k')
    ax[0].annotate('grid_wd ->', fontsize=10, xy=(0.7 * (xc2 - cos * size) + 0.3 * (xc2 + cos * size), 0.7 * (yc2 - sin * size) + 0.3 * (yc2 + sin * size)), color='k', bbox=bbox, ha='center', va='center', style='italic', rotation=270.0 - grid_wd)

    # grid_radius
    actual_radius = (xmax - xmin - xlen * cos - ylen * sin) * 0.5 * 1.07
    correction = 0.0
    ax[0].annotate('', xy=(0.5 * Lx, 0.5 * Ly), xytext=(0.5 * Lx - sin * actual_radius, 0.5 * Ly + cos * actual_radius), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    ax[0].annotate('grid_radius_D', fontsize=10, xy=(0.5 * (0.5 * Lx + 0.5 * Lx - sin * actual_radius), 0.5 * (0.5 * Ly + 0.5 * Ly + cos * actual_radius)), color='k', bbox=bbox, ha='center', va='center', style='italic', rotation=270.0 - grid_wd - 90.0)

    # Zoomed plot
    axins = zoomed_inset_axes(ax[0], 8, loc='upper right', bbox_to_anchor=(0.99, 0.92), bbox_transform=ax[0].figure.transFigure)
    for n in range(nblocks):
        axins.plot(xyz_ver[n, :, :, 0] / D - xWFc, xyz_ver[n, :, :, 1] / D - yWFc, '-k', linewidth=0.1)
        axins.plot(np.transpose(xyz_ver[n, :, :, 0]) / D - xWFc, np.transpose(xyz_ver[n, :, :, 1]) / D - yWFc, '-k', linewidth=0.1)

    margin = 1000.0 / D

    axins.set_ylim(-0.5 * rlen - margin, 0.5 * rlen + margin)
    axins.set_xlim(-0.5 * rlen - margin, 0.5 * rlen + margin)

    axins.set_aspect('equal')
    axins.plot(m2[0], m2[1], 'm', linewidth=1)
    axins.plot(m1[0], m1[1], 'b', linewidth=1)

    mark_inset(ax[0], axins, loc1=2, loc2=3, fc="none", ec="0.5")
    axins.plot([-cos * size, cos * size], [-sin * size, sin * size], '--k')

    axins.plot(np.append(xWT, xWT[0]) / D - xWFc, np.append(yWT, yWT[0]) / D - yWFc, '-r')

    ax[0].set_ylabel('$(y-y_{WF})/D$', rotation=90)
    ax[1].set_xlabel('$(x-x_{WF})/D$', rotation=0)
    ax[1].set_ylabel('$z/D$', rotation=90)

    h = 0.0
    for n in range(nblocks):
        ax[1].plot(xyz_ver[n, :, :, 0] / D - xWFc, xyz_ver[n, :, :, 2] / D - h / D, '-k', linewidth=0.1)
        ax[1].plot(np.transpose(xyz_ver[n, :, :, 0]) / D - xWFc, np.transpose(xyz_ver[n, :, :, 2]) / D - h / D, '-k', linewidth=0.1)

    ax[1].set_xlim(-20, 20)
    ax[1].set_ylim(0, 4)
    ax[1].set_yticks([0, 1, 2, 3, 4])

    fig.savefig(outfile, dpi=300)


def main():
    if __name__ == '__main__':
        plotgrid_terrainogrid()


main()
