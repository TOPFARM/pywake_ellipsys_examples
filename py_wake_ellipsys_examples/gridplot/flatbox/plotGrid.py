import matplotlib.pyplot as plt
import numpy as np
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from scipy.interpolate import griddata
from matplotlib import rcParams
from matplotlib import gridspec

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

from py_wake_ellipsys.utils.flatboxgridutils import get_margin_coords

import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')

# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')

rcParams['xtick.direction'] = 'out'
rcParams['ytick.direction'] = 'out'


def plotgrid_flatbox(folder='.', outfile='flatboxgrid.png'):

    xnodes = np.genfromtxt(folder + '/xDistribution.dat')
    ynodes = np.genfromtxt(folder + '/yDistribution.dat')
    znodes = np.genfromtxt(folder + '/zDistribution.dat')

    layout = np.genfromtxt(folder + '/layout.dat')

    lxly = np.genfromtxt(folder + '/lxly.dat')

    lx = lxly[0]
    ly = lxly[1]
    radius = 50000.0
    D = 126.0
    xWT = np.array([-2.5, 2.5])
    yWT = np.array([0.0, 0.0])

    # Read input.dat of Boxf90
    with open(folder + '/input.dat', 'r') as f:
        for line in f.readlines():
            line = line.rstrip('\n')
            if line[0:4] == 'xlen':
                xlen = float(line.split(' ')[-1])
            elif line[0:4] == 'ylen':
                ylen = float(line.split(' ')[-1])
            elif line[0:4] == 'zlen':
                zlen = float(line.split(' ')[-1])
            elif line[0:6] == 'xshift':
                xshift = float(line.split(' ')[-1])
            elif line[0:6] == 'yshift':
                yshift = float(line.split(' ')[-1])
            elif line[0:14] == 'z-distribution':
                zdist = np.array(line.split('  ')[-1].split())
                zdist = list(map(float, zdist))

    # Read margins
    m1, m2 = get_margin_coords('flatbox', 'boxf90', folder)

    x1 = xshift / D
    x2 = (xlen + xshift) / D
    y1 = yshift / D
    y2 = (ylen + yshift) / D
    z1 = 0
    z2 = zlen / D

    xT1 = m2[0, 0] / D
    xT2 = m2[0, 1] / D
    yT1 = m2[1, 0] / D
    yT2 = m2[1, 2] / D

    xW1 = m1[0, 0] / D
    xW2 = m1[0, 1] / D
    yW1 = m1[1, 0] / D
    yW2 = m1[1, 2] / D

    zW2 = zdist[4] * zlen / D

    # Add the first node
    xnode1 = xnodes[0, 0] - xnodes[0, 1]
    ynode1 = ynodes[0, 0] - ynodes[0, 1]
    znode1 = znodes[0, 0] - znodes[0, 1]
    xnodes = xnodes[:, 0]
    ynodes = ynodes[:, 0]
    znodes = znodes[:, 0]
    xnodes = np.insert(xnodes, 0, xnode1)
    ynodes = np.insert(ynodes, 0, ynode1)
    znodes = np.insert(znodes, 0, znode1)

    nx = len(xnodes)
    ny = len(ynodes)
    nz = len(znodes)

    xnodes = xnodes / D
    ynodes = ynodes / D
    znodes = znodes / D

    fig, ax = plt.subplots(2, 1, sharey=False, figsize=(7.5, 5))

    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    ax[0] = plt.subplot(gs[0])
    ax[1] = plt.subplot(gs[1])

    fig.subplots_adjust(right=0.5)
    # plot every n
    n = 1
    for i in range(nx):
        if (i % n == 0):
            ax[0].plot([xnodes[i], xnodes[i]], [ynodes[0], ynodes[ny - 1]], 'k', linewidth=0.1)
    for j in range(ny):
        if (j % n == 0):
            ax[0].plot([xnodes[0], xnodes[nx - 1]], [ynodes[j], ynodes[j]], 'k', linewidth=0.1)
    for i in range(nx):
        if (i % n == 0):
            ax[1].plot([xnodes[i], xnodes[i]], [znodes[0], znodes[nz - 1]], 'k', linewidth=0.1)
    for j in range(nz):
        if (j % n == 0):
            ax[1].plot([xnodes[0], xnodes[nx - 1]], [znodes[j], znodes[j]], 'k', linewidth=0.1)

    ax[0].set_ylim(y1, y2)
    ax[0].set_xlim(x1, x2)
    ax[0].set_xticks([])
    ax[1].set_xlim(x1, x2)
    ax[1].set_ylim(z1, z2)

    # Lines marking wake regions
    ax[0].plot(m2[0] / D, m2[1] / D, 'm', linewidth=0.5)
    ax[1].plot(m2[0] / D, [0.0, 0.0, zW2, zW2, 0.0], 'm', linewidth=0.5)
    ax[0].plot(m1[0] / D, m1[1] / D, 'b', linewidth=0.5)
    ax[1].plot(m1[0] / D, [0.0, 0.0, zW2, zW2, 0.0], 'b', linewidth=0.5)

    # Zoomed plot
    axins = zoomed_inset_axes(ax[0], 22, loc='upper right', bbox_to_anchor=(0.99, 0.92), bbox_transform=ax[0].figure.transFigure)
    for i in range(nx):
        if (i % n == 0):
            axins.plot([xnodes[i], xnodes[i]], [ynodes[0], ynodes[ny - 1]], 'k', linewidth=0.1)
    for j in range(ny):
        if (j % n == 0):
            axins.plot([xnodes[0], xnodes[nx - 1]], [ynodes[j], ynodes[j]], 'k', linewidth=0.1)

    margin = 1.1
    axins.set_ylim(yT1 - margin, yT2 + margin)
    axins.set_xlim(xT1 - margin, xT2 + margin)
    axins.set_xticks([xT1, xW1, 0, xW2, xT2])
    axins.set_yticks([yT1, yW1, 0, yW2, yT2])
    axins.plot(m2[0] / D, m2[1] / D, 'm', linewidth=1)
    axins.plot(m1[0] / D, m1[1] / D, 'b', linewidth=1)
    mark_inset(ax[0], axins, loc1=2, loc2=3, fc="none", ec="0.5")

    padding = 1
    bbox = dict(facecolor='white', alpha=1, pad=padding)

    # Wind farm locations.
    WTradius = 0.5 * np.sqrt((xWT.max() - xWT.min()) ** 2 + (yWT.max() - yWT.min()) ** 2)

    WTcircle = plt.Circle((0, 0), WTradius, color='r', fill=False)
    axins.add_artist(WTcircle)
    deg = 124.0 / 180.0 * np.pi
    r1 = 7 * WTradius
    axins.annotate('', xy=(0, 0), xytext=(np.cos(deg) * WTradius, np.sin(deg) * WTradius), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='r')
    axins.annotate('Wind farm radius', fontsize=10, xy=(r1 * np.cos(deg) - 5, r1 * np.sin(deg) - 1), color='r', bbox=bbox, ha='left', va='center')
    axins.plot([np.cos(deg) * WTradius, np.cos(deg) * r1], [np.sin(deg) * WTradius, r1 * np.sin(deg)], '--r', dashes=(5, 2))

    deg = -146.0 / 180.0 * np.pi
    r1 = 4.3 * WTradius
    axins.plot([0.5 * (-WTradius + xW1), 0.5 * (- WTradius + xW1) + r1 * np.cos(deg)], [0.0, r1 * np.sin(deg)], '--b', dashes=(5, 2))
    axins.annotate('', xy=(-WTradius, 0), xytext=(xW1, 0), arrowprops=dict(arrowstyle='<->', color='b', shrinkA=0, shrinkB=0), color='k')
    axins.annotate('grid_m1_w_D', fontsize=10, xy=(0.5 * (WTradius + xW1) + r1 * np.cos(deg), r1 * np.sin(deg) - 1), color='b', bbox=bbox, ha='center', va='center', style='italic')

    deg = -34.0 / 180.0 * np.pi
    r1 = 4.3 * WTradius
    axins.plot([0.5 * (WTradius + xW2), 0.5 * (WTradius + xW2) + r1 * np.cos(deg)], [0.0, r1 * np.sin(deg)], '--b', dashes=(5, 2))
    axins.annotate('', xy=(WTradius, 0), xytext=(xW2, 0), arrowprops=dict(arrowstyle='<->', color='b', shrinkA=0, shrinkB=0), color='k')
    axins.annotate('grid_m1_e_D', fontsize=10, xy=(0.5 * (WTradius + xW2) + r1 * np.cos(deg), r1 * np.sin(deg) - 1), color='b', bbox=bbox, ha='center', va='center', style='italic')

    axins.annotate('', xy=(0, yW2), xytext=(0, WTradius), arrowprops=dict(arrowstyle='<->', color='b', shrinkA=0, shrinkB=0), color='k')
    axins.annotate('grid_m1_n_D', fontsize=10, xy=(1, 0.5 * (WTradius + yW2) + 0.1), color='b', bbox=bbox, ha='left', va='center', style='italic')

    axins.annotate('', xy=(0, yW1), xytext=(0, -WTradius), arrowprops=dict(arrowstyle='<->', color='b', shrinkA=0, shrinkB=0), color='k')
    axins.annotate('grid_m1_s_D', fontsize=10, xy=(1, 0.5 * (-WTradius + yW1) - 0.1), color='b', bbox=bbox, ha='left', va='center', style='italic')

    # Outer margins
    axins.annotate('', xy=(xT1, 0), xytext=(xW1, 0), arrowprops=dict(arrowstyle='<->', color='m', shrinkA=0, shrinkB=0), color='m')
    axins.annotate('grid_m2_w_D', fontsize=10, xy=(xT1 + 0.5, 0.0 + 7.2), color='m', bbox=bbox, ha='left', va='center', style='italic')
    axins.plot([0.5 * (xT1 + xW1), 0.5 * (xT1 + xW1)], [0.0, 7.2], '--m', dashes=(5, 2))
    axins.annotate('', xy=(xW2, 0), xytext=(xT2, 0), arrowprops=dict(arrowstyle='<->', color='m', shrinkA=0, shrinkB=0), color='m')

    axins.annotate('grid_m2_e_D', fontsize=10, xy=(0.5 * (xW2 + xT2) - 1, 7), color='m', bbox=bbox, ha='center', va='center', style='italic')
    axins.plot([0.5 * (xT2 + xW2), 0.5 * (xT2 + xW2)], [0.0, 7.2], '--m', dashes=(5, 2))

    axins.annotate('', xy=(0, yW2), xytext=(0, yT2), arrowprops=dict(arrowstyle='<->', color='m', shrinkA=0, shrinkB=0), color='m')
    axins.annotate('grid_m2_n_D', fontsize=10, xy=(1, 0.5 * (yT2 + yW2) + 0.1), color='m', bbox=bbox, ha='left', va='center', style='italic')

    axins.annotate('', xy=(0, yT1), xytext=(0, yW1), arrowprops=dict(arrowstyle='<->', color='m', shrinkA=0, shrinkB=0), color='m')
    axins.annotate('grid_m2_s_D', fontsize=10, xy=(1, 0.5 * (yT1 + yW1) - 0.1), color='m', bbox=bbox, ha='left', va='center', style='italic')

    # BC

    # place a text boxes with BC's
    x3 = 0.26 * (x2 - x1) + x1
    y3 = 0.26 * (y2 - y1) + y1

    ax[0].annotate('Periodic BC', fontsize=12, xy=(x3, y2), xytext=(padding, -padding), textcoords='offset pixels', bbox=bbox, va='top', ha='center')
    ax[0].annotate('Periodic BC', fontsize=12, xy=(x3, y1), xytext=(padding, padding), textcoords='offset pixels', bbox=bbox, va='bottom', ha='center')
    ax[0].annotate('Inlet BC', fontsize=12, xy=(x1, y3), xytext=(padding, padding), textcoords='offset pixels', bbox=bbox, va='center', ha='left', rotation=90)
    ax[0].annotate('Outlet BC', fontsize=12, xy=(x2, y3), xytext=(-padding, padding), textcoords='offset pixels', bbox=bbox, va='center', ha='right', rotation=90)

    ax[1].annotate('Inlet BC', fontsize=12, xy=(x1, 0.5 * z2), xytext=(padding, padding), textcoords='offset pixels', bbox=bbox, va='center', ha='left', rotation=90)
    ax[1].annotate('Outlet BC', fontsize=12, xy=(x2, 0.5 * z2), xytext=(-padding, padding), textcoords='offset pixels', bbox=bbox, va='center', ha='right', rotation=90)
    ax[1].annotate('Inlet BC', fontsize=12, xy=(x3, z2), xytext=(padding, -padding), textcoords='offset pixels', bbox=bbox, va='top', ha='center')
    ax[1].annotate('Rough wall BC', fontsize=12, xy=(x3, 0), xytext=(padding, padding), textcoords='offset pixels', bbox=bbox, va='bottom', ha='center')

    # grid_radius

    ax[0].annotate('', xy=(x1, 0), xytext=(xT1, 0), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    ax[0].annotate('grid_radius_D', fontsize=10, xy=(0.5 * (x1 + xT1), 0.0), color='k', bbox=bbox, ha='center', va='center', style='italic')

    ax[0].annotate('', xy=(0, y1), xytext=(0, yT1), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    ax[0].annotate('grid_radius_D', fontsize=10, xy=(0.0, 0.5 * (y1 + yT1)), color='k', bbox=bbox, ha='center', va='center', rotation=90, style='italic')

    ax[0].annotate('', xy=(xT2, 0), xytext=(x2, 0), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    ax[0].annotate('grid_radius_D', fontsize=10, xy=(0.5 * (x2 + xT2), 0.0), color='k', bbox=bbox, ha='center', va='center', style='italic')

    ax[0].annotate('', xy=(0, y2), xytext=(0, yT2), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    ax[0].annotate('grid_radius_D', fontsize=10, xy=(0.0, 0.5 * (y2 + yT2)), color='k', bbox=bbox, ha='center', va='center', rotation=90, style='italic')

    ax[1].set_xlabel('$x/D$', rotation=0)
    ax[0].set_ylabel('$y/D$', rotation=0)
    ax[1].yaxis.labelpad = 20
    ax[1].set_ylabel('$z/D$', rotation=0)

    # zlen
    ax[1].annotate('', xy=(0.6 * (x2 - x1) + x1, 0), xytext=(0.6 * (x2 - x1) + x1, z2), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    ax[1].annotate('grid_zlen_D', fontsize=10, xy=(0.6 * (x2 - x1) + x1 + 15, 0.75 * z2), color='k', bbox=bbox, ha='left', va='center', style='italic')

    # Zoomed plot bottom
    axins2 = zoomed_inset_axes(ax[1], 22, loc='upper right', bbox_to_anchor=(0.99, 0.5), bbox_transform=ax[1].figure.transFigure)
    for i in range(nx):
        if (i % n == 0):
            axins2.plot([xnodes[i], xnodes[i]], [znodes[0], znodes[nz - 1]], 'k', linewidth=0.1)
    for j in range(nz):
        if (j % n == 0):
            axins2.plot([xnodes[0], xnodes[nx - 1]], [znodes[j], znodes[j]], 'k', linewidth=0.1)

    margin_x = 2
    margin_z = 0.1
    axins2.set_ylim(0.0 - margin_z, zW2 + margin_z)
    axins2.set_xlim(xT1 - margin_x, xT2 + margin_x)
    axins2.set_xticks([xT1, xW1, 0, xW2, xT2])
    axins2.set_yticks([0, zW2])
    axins2.plot([xT1, xT2, xT2, xT1, xT1], [0.0, 0.0, zW2, zW2, 0.0], 'm', linewidth=1)
    axins2.plot([xW1, xW2, xW2, xW1, xW1], [0.0, 0.0, zW2, zW2, 0.0], 'b', linewidth=1)
    mark_inset(ax[1], axins2, loc1=2, loc2=3, fc="none", ec="0.5")
    axins2.set_aspect(aspect=3)

    axins2.annotate('', xy=(0.5 * (xW2 - xW1) + xW1, 0), xytext=(0.5 * (xW2 - xW1) + xW1, zW2), arrowprops=dict(arrowstyle='<->', color='r', shrinkA=0, shrinkB=0), color='k')
    axins2.annotate('grid_zWakeEnd_D', fontsize=10, xy=(0.5 * (xW2 - xW1) + xW1 + 1, 0.5 * zW2), color='k', bbox=bbox, ha='left', va='center', style='italic')

    fig.savefig(outfile, dpi=300)


def main():
    if __name__ == '__main__':
        plotgrid_flatbox()


main()
