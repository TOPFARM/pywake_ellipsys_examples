import numpy as np
import time
import os
import sys
import xarray
import time
from py_wake.tests import npt
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.examples.data.hornsrev1 import Hornsrev1Site
from scipy import interpolate

import matplotlib.pyplot as plt
import matplotlib.style
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import griddata
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


def run_gabls2_DoubleNREL5MW(run_machine, queue, grid=False, runpre=False, run_no_wf=False, runwf=False,
                             plotpre=False, plot_no_wf=False, plot_wf=False, animate_wf=False):
    from py_wake_ellipsys_examples.data.farms.doublenrel5mw import wt_x, wt_y, type_i
    from py_wake_ellipsys_examples.data.turbines.nrel5mw import NREL5MW
    from py_wake_ellipsys_examples.GABLS2 import GABLS2_path
    from py_wake_ellipsys.utils.fparser import read_fpar_extract_plane, plot_fpar_extract_plane, animate_fpar_extract_plane

    # Two NREL-5MW wind turbines with 5D inter spacing subjected to a
    # transient inflow obtained from a diurnal cycle based on GABLS2
    # If a different time of the day is simulated, one could encouter convergence issues, and
    # one could lower the time step (e3d_dc_dt) or increase the number of subiterations
    # (e3d_dc_submin and e3d_dc_submax).
    wt = NREL5MW()
    Dref = wt.diameter()
    # Sogachev's turb constants:
    kappa = 0.4
    cmu = 0.03
    ce1 = 1.52
    ce2 = 1.833
    pred = kappa ** 2 / (np.sqrt(cmu) * (ce2 - ce1))
    Twallfile = GABLS2_path + 'Twalllow1D_form.dat'
    dc_days = 10.0
    grid_wd = 270.0
    wd = [270.0]
    ws = [9.5]  # Represents geostrophic wind
    zRef = 90.0
    TI = 0.0  # TI is not used in keABLdc model because the actual TI changes in time.
    nroutStart = 600  # Output start, disgarding the first 50 min.

    set_cluster_vars(run_machine, True, queue, 32, 4)

    cnst = Cnst(fcori=8.87e-5, z0=3e-2, ce1=ce1, ce2=ce2,
                prtke=pred, pred=pred, kappa=kappa, CR=4.5, pr=0.74)
    wfgrid = FlatBoxGrid(Dref, dwd=1.0, zFirstCell_D=0.5 / Dref,
                         # We set a zero grid radius to mimize the downstream development of the inflow
                         # and we use the m2 margins to set the distance between the wind farm and the lateral BCs
                         radius_D=0.0, cells2_D=0.1,
                         m2_w_D=0.0, m2_e_D=1e4 / Dref, m2_n_D=1e4 / Dref, m2_s_D=1e4 / Dref,
                         run_wd_con=False)

    e1d = E1D(relaxu=0.7, submin=8, submax=8, nrout=600, dt=1.0, reslim=1e-4)
    e3d = E3D(method='URANS', turbmodel='keABLdc', relaxu=0.7, reslim=1e-4, start_grlvl=1, dt=5.0, submin=8, submax=8, nrout=12)
    predc = PreDC(grid=PrecursorGrid1D(ni=193, zFirstCell=1.02e-4, height=6e3), Twallfile=Twallfile, days=dc_days,
                  alphaMY=0.075, temp_wall=289.0, temp_dTdz=3.5e-3, temp_h=4e3, cluster=Cluster(walltime='2:00:00'))
    wfrun = WFRun(casename='DoubleNREL5MW_GABLS2', tr_tStart=61200, tr_tEnd=64800, tr_nroutStart=nroutStart, cluster=Cluster(walltime='1:00:00'))
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef, zeta=0.0,
                   ad=AD(force='2112'), e1d=e1d, e3d=e3d, wfrun=wfrun,
                   cnst=cnst, pre=predc, run_ws_con=False, run_wd_con=False)
    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create precursor grid
        wfm.create_precursor_grid()
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y, grid_wd=grid_wd)

    if runpre:
        # Run e1d precursor
        wfm.run_precursor_dc(ws)

    if run_no_wf:
        # Run 3D simulation without wind turbines to see how the inflow profiles develop downstream
        # We use extraction points in height that correspond to the precursor grid
        folder = 'grid_precursor_opt/'
        infile = folder + 'distribution.dat'
        print('Wait for precursor grid')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        zpre = np.genfromtxt(infile)
        zprecc = 0.5 * (zpre[:-1, 0] + zpre[1:, 0])
        extractions_line = np.zeros((179, 3))
        extractions_line[:, 2] = zprecc[0:179]
        wfm.set_subattr('ad.force', None)  # Switch off forces
        wfm.grid.origin = [0.0, 0.0]
        wfm.run_windfarm(None, None, wd, ws, None, points=extractions_line)
        wfm.set_subattr('ad.force', '2112')

    if runwf:
        # Run a flow case with the wind farm and save wt out from the first iteration
        wfm.wfrun.tr_nroutStart = 0
        # A plane at hub height (z=zH)
        plane1 = {'plane': 'z', 'cc': True, 'clip': 'm1', 'dist': 90.0}
        # A vertical at a center line (y=0)
        plane2 = {'plane': 'y', 'cc': True, 'clip': 'm1', 'dist': 0.0}
        extraction_planes = [plane1, plane2]
        wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i, planes=extraction_planes)
        wfm.wfrun.tr_nroutStart = nroutStart

    if plotpre:
        # Plot precursor results for the last full day
        # Check output file
        folder = 'run_precursor_dc_z0_%g/' % wfm.cnst.z0
        infile = folder + 'finished'
        print('Wait for precursor')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        # Read inflow precursor
        secondsperday = 24.0 * 3600.0
        nz = wfm.pre.grid.ni - 1
        data, nt = wfm.pre.load_precursor_dc(folder, ws[0], 0.0, secondsperday, nz)
        # Wall temperature
        Twall = np.genfromtxt(Twallfile, skip_header=2)
        Twall_time = np.linspace(0, 24, 241)
        # Temperature variation contours
        itime = 1
        iz = 2
        iu = 3
        iv = 4
        itke = 6
        idtke = 7
        itemp = 8
        ilmax = 9
        iden = 13
        fig, ax = plt.subplots(4, 1, figsize=(6, 15), sharex='col')
        cmap = plt.cm.get_cmap('jet')
        t = data[:, :, itime].flatten() / 3600.0 - (dc_days - 1) * 24  # Time [hours]
        z = data[:, :, iz].flatten() / 1000.0
        tint = np.linspace(0, 24.0, 145)
        zint = np.linspace(z[0], 0.8, 801)
        ax[-1].set_xlabel('time [h]')
        ax[-1].set_xticks([0, 3, 6, 9, 12, 15, 18, 21, 24])
        varnames = ['wind speed [m/s]', r'relative wind direction [$\degree$]',
                    r'turbulent kinetic energy [$\mathsf{m}^2$/$\mathsf{s}^2$]', 'temperature [K]']
        vmins = [0.0, -27.0, 0.0, 270.0]
        vmaxs = [14.0, 63.0, 0.9, 292.0]
        nlevels = [15, 16, 19, 23]
        for i in range(4):
            ax[i].set_ylabel('z [km]')
            ax[i].set_xlim(0, 24)
            levels = np.linspace(vmins[i], vmaxs[i], nlevels[i])
            if i == 0:
                var = np.sqrt(data[:, :, iu] ** 2 + data[:, :, iv] ** 2).flatten()
            elif i == 1:
                var = (np.arctan2(data[:, :, iv], data[:, :, iu]) * 180.0 / np.pi - 45.0).flatten()
            elif i == 2:
                var = data[:, :, itke].flatten()
            elif i == 3:
                var = data[:, :, itemp].flatten()
            varInt = griddata((t, z), var, (tint[None, :], zint[:, None]), method='linear')
            cs = ax[i].contourf(tint, zint, varInt, levels, cmap=cmap, vmax=vmaxs[i], vmin=vmins[i])
            divider1 = make_axes_locatable(ax[i])
            cax1 = divider1.append_axes("top", size="5%", pad=0.2)
            cbar1 = plt.colorbar(cs, cax=cax1, orientation='horizontal', label=varnames[i])
            cbar1.set_label(label=varnames[i], labelpad=-40)
        axtwin2 = ax[3].twinx()
        axtwin2.plot(Twall_time, Twall, '-k')
        axtwin2.set_xlim(0, 24)
        axtwin2.set_ylim(270, 300)
        axtwin2.set_ylabel('wall temperature [k]')
        axtwin2.set_yticklabels([270, 275, 280, 285, 290, 295])
        fig.savefig('GABLS2_pre.pdf')

    if plot_no_wf:
        # Make plot of a comparison between inflow profiles and profiles in the domain center
        wfm.set_subattr('ad.force', None)
        folder = wfm.get_name()
        wfm.set_subattr('ad.force', '2112')
        infile = folder + '/finished'
        print('Wait for grid.1points')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()

        tStartOut = wfm.wfrun.tr_nroutStart * wfm.e3d.dt
        nt = int((wfm.wfrun.tr_tEnd - wfm.wfrun.tr_tStart - tStartOut) / (wfm.e3d.nrout * wfm.e3d.dt)) + 1
        nzplot = 179
        nvar = 15
        # Load profiles from domain, delete columns: x, y and w, for easier plotting
        infile = folder + '/wd%g_ws%g/grid.1points' % (wd[0], ws[0])
        domain_prof = np.reshape(np.delete(np.genfromtxt(infile, skip_header=True), [2, 3, 7], 1), (nt, nzplot, nvar))
        folder = 'run_precursor_dc_z0_%g/' % wfm.cnst.z0
        infile = folder + 'finished'
        print('Wait for precursor')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        # Read inflow precursor
        secondsperday = 24.0 * 3600.0
        nz = wfm.pre.grid.ni - 1
        # data[nt, nz, nvar]
        itime = 1
        iz = 2
        iu = 3
        iv = 4
        itke = 6
        idtke = 7
        itemp = 8
        ilmax = 9
        iden = 13
        inflow_prof, nt = wfm.pre.load_precursor_dc(folder, ws[0], 0.0, secondsperday, nz)

        fig, ax = plt.subplots(1, 4, sharey='all', figsize=(10, 6))
        for i in range(4):
            ax[i].grid(True)
        # Inflow at one hour after start (we could compare earlier profiles to account for 10 km travel time)
        nIn = int(wfm.wfrun.tr_tStart / (wfm.e1d.nrout * wfm.e1d.dt)) + 6
        ax[0].plot(np.sqrt(inflow_prof[nIn, 0:nzplot, iu] ** 2 + inflow_prof[nIn, 0:nzplot, iv] ** 2), inflow_prof[nIn, 0:nzplot, iz], '-k')

        # Domain profiles after 1 hour
        nDo = -1
        ax[0].set_ylabel('z [m]')
        ax[0].set_ylim(0, 1000.0)

        # Wind speed
        wsIn = np.sqrt(inflow_prof[nIn, 0:nzplot, iu] ** 2 + inflow_prof[nIn, 0:nzplot, iv] ** 2)
        wsDo = np.sqrt(domain_prof[nDo, :, iu] ** 2 + domain_prof[nDo, :, iv] ** 2)
        l1, = ax[0].plot(wsIn, inflow_prof[nIn, 0:nzplot, iz], '-k')
        l2, = ax[0].plot(wsDo, domain_prof[nDo, :, iz], '--r')
        ax[0].set_xlabel('ws [m/s]')

        # Relative wind direction
        fint_wd = interpolate.interp1d(inflow_prof[nIn, 0:nzplot, iz], np.arctan(inflow_prof[nIn, 0:nzplot, iv] / inflow_prof[nIn, 0:nzplot, iu]) * 180.0 / np.pi)
        wdH = fint_wd(zRef)
        ax[1].plot(np.arctan(inflow_prof[nIn, 0:nzplot, iv] / inflow_prof[nIn, 0:nzplot, iu]) * 180.0 / np.pi - wdH, inflow_prof[nIn, 0:nzplot, iz], '-k')
        ax[1].plot(np.arctan(domain_prof[nDo, :, iv] / domain_prof[nDo, :, iu]) * 180.0 / np.pi, domain_prof[nDo, :, iz], '--r')
        ax[1].set_xlabel('Relative wd [deg]')

        # TI
        ax[2].set_xlim(0.0, 0.2)
        ax[2].plot(np.sqrt(2.0 / 3.0 * inflow_prof[nIn, 0:nzplot, itke]) / wsIn, inflow_prof[nIn, 0:nzplot, iz], '-k')
        ax[2].plot(np.sqrt(2.0 / 3.0 * domain_prof[nDo, :, itke]) / wsDo, domain_prof[nDo, :, iz], '--r')
        ax[2].set_xlabel('TI')

        # Turbulence length scale
        ax[3].plot(cmu ** 0.75 * inflow_prof[nIn, 0:nzplot, itke] ** 1.5 / inflow_prof[nIn, 0:nzplot, idtke], inflow_prof[nIn, 0:nzplot, iz], '-k')
        ax[3].plot(cmu ** 0.75 * domain_prof[nDo, :, itke] ** 1.5 / domain_prof[nDo, :, idtke], domain_prof[nDo, :, iz], '--r')
        ax[3].set_xlabel('Turbulence length scale [m]')
        fig.legend((l1, l2), ('Inflow', 'Domain (WF center)'), ncol=2, bbox_to_anchor=(0.75, 1.0))
        fig.savefig('GABLS2_profiles_no_wf.pdf')

    if plot_wf:
        # Plot wind turbine power as function of time from the start
        wfm.wfrun_tr_nroutStart = 0
        # PyWake calculates mean power
        WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
        # We can also get the transient values
        WS_eff_ilkn, power_ilkn, ct_ilkn, *dummy = wfm.post_windfarm(transient_out=True)
        # Calculate time steps
        mstep = wfm.wfrun.get_transient_mstep()
        t = np.arange(max(wfm.wfrun.tr_nroutStart, wfm.e3d.nrout), mstep + 1, wfm.e3d.nrout) * wfm.e3d.dt
        fig, ax = plt.subplots(1, 1, figsize=(6, 4))
        l1, = ax.plot(t, power_ilkn[0, 0, 0, :] * 1e-6, '-r')
        l2, = ax.plot(t, power_ilkn[1, 0, 0, :] * 1e-6, '-b')
        l3, = ax.plot([t[0], t[-1]], np.array([power_ilk[0, 0, 0], power_ilk[0, 0, 0]]) * 1e-6, '--r')
        l4, = ax.plot([t[0], t[-1]], np.array([power_ilk[1, 0, 0], power_ilk[1, 0, 0]]) * 1e-6, '--b')
        ax.set_ylabel('WT power [MW]')
        ax.set_xlabel('time [s]')
        ax.grid(True)
        ax.set_xlim(t[0], t[-1])
        fig.tight_layout(rect=[0, 0, 1, 0.8])
        fig.legend((l1, l2, l3, l4), ('WT 1', 'WT 2', 'WT 1 PyWake mean', 'WT 2 PyWake mean'), ncol=2, bbox_to_anchor=(0.85, 1.0))
        fig.savefig('GABLS2_wt_power.pdf')

    if animate_wf:
        # Post process extract out planes
        folder = wfm.get_name()
        infile = '%s/wd%g_ws%g/grid.p01.fpar.u1' % (folder, wd[0], ws[0])
        print('Wait for grid.p01.fpar.u1')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        t, x1, x2, data, nt, n1, n2, tStart, tEnd, version, iprecision, shift, q = read_fpar_extract_plane(infile)
        animate_fpar_extract_plane(data, t, x1 - shift[0], x2 - shift[1], 0, nt - 1, 1, 0.5, 'hub_plane', ext='gif', fps=5, xlabel='$x$ [m]', ylabel='$y$ [m]')
        infile = '%s/wd%g_ws%g/grid.p02.fpar.u1' % (folder, wd[0], ws[0])
        t, x1, x2, data, nt, n1, n2, tStart, tEnd, version, iprecision, shift, q = read_fpar_extract_plane(infile)
        animate_fpar_extract_plane(data, t, x1 - shift[0], x2, 0, nt - 1, 1, 0.5, 'vertical_plane', ext='gif', fps=5, xlabel='$x$ [m]', ylabel='$z$ [m]')


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    run_gabls2_DoubleNREL5MW(run_machine, queue,
                             grid=True,
                             runpre=True,
                             run_no_wf=True,
                             runwf=True,
                             plotpre=True
                             plot_no_wf=True,
                             plot_wf=True,
                             animate_wf=True
                             )
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
