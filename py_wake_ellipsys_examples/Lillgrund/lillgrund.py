import numpy as np
import sys
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *


def run_Lillgrund(run_machine, queue, coarse=False, grid=False, cal=False, run=False,
                  postrows=False, postWFeff=False, plotrows=False, plotWFeff=False,
                  plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.lillgrund import wt_x, wt_y, LillgrundSite
    from py_wake_ellipsys_examples.data.turbines.swt2p3_93 import SWT2p3_93
    from py_wake.validation.validation_lib import GaussianFilter
    from py_wake_ellipsys_examples.Lillgrund.data import data_path
    from py_wake_ellipsys_examples.plotwfres import plotWFrow, plotWFeffrose
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Lillgrund wind farm simulations, as published in DOI: 10.1002/we.1804
    # The grid is similar by selecting a z-distribution with a uniform
    # spaced region in the wake domain. The newer z-distribution is more
    # smooth, but requires more cells. The height of the domain has been
    # increased from 10D to 25D.
    nWT = len(wt_x)
    type_i = np.zeros((nWT))
    wt = SWT2p3_93(zH=65.0)
    Dref = wt.diameter()
    TI = 0.048  # TI based on TKE ~ 0.8 * TI_u = sigma_U/U
    zRef = 65.0
    # A single flow case
    # wd = [120.0]
    # A range of wind directions to perform Gaussian averaging for wd=120
    # wd = np.arange(111.0, 132.0, 3.0)
    # A range of wind directions to perform Gaussian averaging for wd=105, wd=120, wd=207 and wd=222
    # wd = np.append(np.arange(96.0, 132.0, 3.0), np.arange(198.0, 234.0, 3.0))
    # A full wind direction run (takes time!)
    wd = np.arange(0.0, 360.0, 3.0)
    ws = [9.0]
    sigma = 3.3  # Estimated wind direction uncertainty [deg]
    # Old z distribution, which saves cells but has larger expansion ratios at the start and end of the wake domain
    zdistr = 'uniwake'
    # Old results were made with zlen_D=10, which causes some additional artificial blockage
    zlen_D = 25.0
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 1e-2
        grid_cells1_D = 2.0
        adgrid_nr = 5
        grid_bsize = 48
        grid_bsize_cal = 48
        maxnodes = 1
        walltime_wf_run = '0:20:00'
    else:
        e3d_reslim = 1e-5
        grid_cells1_D = 10.0
        adgrid_nr = 64
        grid_bsize_cal = 32
        # If one can afford the resources:
        grid_bsize = 32
        maxnodes = 27
        walltime_wf_run = '30:00:00'
        # Otherwise:
        # grid_bsize = 64
        # maxnodes = 4
        # walltime_wf_run = '100:00:00'
    # Row cases
    cases = [{'name': 'RowB', 'wd': 222.0, 'wts': [14, 13, 12, 11, 10, 9, 8, 7]},
             {'name': 'RowD', 'wd': 222.0, 'wts': [29, 28, 27, np.nan, 26, 25, 24, 23]},
             {'name': 'RowB', 'wd': 207.0, 'wts': [14, 13, 12, 11, 10, 9, 8, 7]},
             {'name': 'RowD', 'wd': 207.0, 'wts': [29, 28, 27, np.nan, 26, 25, 24, 23]},
             {'name': 'Row6', 'wd': 120.0, 'wts': [2, 9, 17, 25, 32, 37, 42, 46]},
             {'name': 'Row4', 'wd': 120.0, 'wts': [4, 11, 19, np.nan, np.nan, 39, 44]},
             {'name': 'Row6', 'wd': 105.0, 'wts': [2, 9, 17, 25, 32, 37, 42, 46]},
             {'name': 'Row4', 'wd': 105.0, 'wts': [4, 11, 19, np.nan, np.nan, 39, 44]}]
    today = date.today()

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1101', grid=ADGrid(nr=adgrid_nr, ntheta=32))
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, zFirstCell_D=0.5 / Dref, radius_D=5e4 / Dref, cells1_D=grid_cells1_D,
                         zlen_D=zlen_D, zdistr=zdistr, bsize=grid_bsize)
    # Wind farm run
    wfrun = WFRun(casename='Lillgrund', cluster=Cluster(walltime=walltime_wf_run))

    e3d = E3D(reslim=e3d_reslim)
    wfm = EllipSys(LillgrundSite(), wt, wfgrid, TI, zRef,
                   ad=ad, e3d=e3d, wfrun=wfrun)

    # Create calibration grid (use smaller block size to speed up calibration)
    wfm.calrun.bsize = grid_bsize_cal

    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create calibration grid (use smaller block size to speed up calibration)
        wfm.create_calibration_grid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)

    if cal:
        # Run force control calibration
        wfm.run_calibration(type_i[0])

    if run:
        # Run flow cases
        wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)

    if postrows:
        # Write results that can be used in the PyWake validation report
        WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
        for case in cases:
            l = np.where(wd == case['wd'])[0][0]
            filename = 'Lillgrund_RANS_wd' + str(int(case['wd'])) + '_' + case['name'] + '.dat'
            f = open(filename, 'w')
            f.write('# EllipSys3D-RANS-AD, k-epsilon-fP\n')
            f.write('# Based on DOI: 10.1002/we.1804, rerun date=%s\n' % today)
            f.write('# GA with sigma=3.3 deg\n')
            f.write('# WT, Pi/P0,     Pi/P0+GA\n')
            for i in range(len(case['wts'])):
                if case['wts'][i] is np.nan:
                    f.write('%i     nan        nan\n' % (i + 1))
                else:
                    P0 = power_ilk[case['wts'][0], l, 0]
                    P = power_ilk[case['wts'][i], l, 0]
                    # Gaussian averaging to represent wind direction uncertainty
                    l1 = l - 3
                    l2 = l + 4
                    PGA = GaussianFilter(power_ilk[case['wts'][i], l1:l2, 0], wd[l1:l2], 3, sigma)
                    P0GA = GaussianFilter(power_ilk[case['wts'][0], l1:l2, 0], wd[l1:l2], 3, sigma)
                    f.write('%-5i %10.8f %10.8f\n' % (i + 1, P / P0, PGA[0][3] / P0GA[0][3]))
            f.close()

    if postWFeff:
        # Write results that can be used in the PyWake validation report
        WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()

        filename = 'Lillgrund_RANS_WFeff.dat'
        f = open(filename, 'w')
        f.write('# EllipSys3D-RANS-AD, k-epsilon-fP\n')
        f.write('# Based on DOI: 10.1002/we.1804, rerun date=%s\n' % today)
        f.write('# GA with sigma=3.3 deg\n')
        f.write('#       Efficiency=sum[P(wd)]/sum[P_noWake(wd)]\n')
        f.write('# wdir, no GA, GA\n')

        # Gaussian averaging assumes that simulated wind directions are cyclic
        if 360.0 - (wd[1] - wd[0]) != wd[-1]:
            print('ERROR: simulated wind directions are not cyclic and we cannot perform GA for all cases!')
            sys.exit()
        PGA = np.zeros(power_ilk.shape)
        for i in range(nWT):
            PGA[i, :, 0] = GaussianFilter(power_ilk[i, :, 0], wd, 3, sigma)

        # Wind farm power without wakes
        WFP0 = wt.power(ws)[0] * nWT
        for l in range(wd.size):
            WFP = power_ilk[:, l, 0].sum()
            WFPGA = PGA[:, l, 0].sum()
            f.write('%5.1f %8.6f %8.6f\n' % (wd[l], WFP / WFP0, WFPGA / WFP0))
        f.close()

    if plotrows:
        for case in cases:
            wd = case['wd']
            datafile = data_path + 'Lillgrund_WFdata_wd' + str(int(case['wd'])) + '_' + case['name'] + '.dat'
            # Compare with old results:
            # oldmodelfile = data_path + 'Lillgrund_RANS_wd' + str(int(case['wd'])) + '_' + case['name'] + '.dat'

            # modelfiles = ['Lillgrund_RANS_wd' + str(int(case['wd'])) + '_' + case['name'] + '.dat', oldmodelfile]
            # modellabels = ['RANS', 'RANS old']
            # modelcolors = ['g', 'r']
            modelfiles = [rans_path + 'Lillgrund_RANS_wd' + str(int(case['wd'])) + '_' + case['name'] + '.dat']
            modellabels = ['RANS']
            modelcolors = ['g']
            outfile = 'Lillgrund_wd' + str(int(wd)) + '_' + case['name'] + '.' + plotfmt
            plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, case['wts'], wt.diameter(), wd)

    if plotWFeff:
        datafile = data_path + 'Lillgrund_WFdata_WFeff.dat'
        # Compare with old results:
        # oldmodelfile = data_path + 'Lillgrund_RANS_WFeff.dat'
        # modelfiles = ['Lillgrund_RANS_WFeff.dat', oldmodelfile]
        # modellabels = ['RANS', 'RANS old']
        # modelcolors = ['g', 'r']
        modelfiles = [rans_path + 'Lillgrund_RANS_WFeff.dat']
        modellabels = ['RANS']
        modells = ['-g']
        modelGAls = ['--g']
        outfile = 'Lillgrund_WFeff.' + plotfmt
        plotWFeffrose(datafile, modelfiles, modellabels, modells, modelGAls, outfile, wt_x, wt_y, wt.diameter(), dataNbins=1)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Run Lillgrund case using a very coarse setup if coarse=True
    run_Lillgrund(run_machine, queue, coarse=True, grid=True, cal=True, run=True,
                  postrows=True, postWFeff=True, plotrows=True, plotWFeff=True,
                  plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
