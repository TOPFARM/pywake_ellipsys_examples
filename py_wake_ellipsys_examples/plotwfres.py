import numpy as np
from copy import copy
import matplotlib.pyplot as plt
import warnings

import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


def get_downstream_label(sDown_D):
    # Convert downstream distance label at 2.5 - > 2p5 or 4.0 -> 4
    s = str(sDown_D)
    return (s.rstrip('0').rstrip('.') if '.' in s else s).replace('.', 'p')


def plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, wt_i_plot, D, wd,
              wt_i_layoutplot=None, dataerrorbar=True, ylabel='$P_i/P_1$', GA=True, figx=9, figy=4,
              xticks=None, xticklabels=None, wfx=0.3, wfy=0.3, fulldata=False):
    """
    Plot wind turbine power in a row of wind turbines and compare with measurements
    The measurements are expected to be read from a data file:
    # wt_nr, P/P0
    The models results are expected to be in a data file including both
    non gaussian averaged (GA) and gaussian averaged results (if GA is True):
    # wt_nr, P/P0, P_GA/P0_GA
    modelfiles is a list of models file names, so multiple models can be compared.
    """
    warnings.filterwarnings("ignore")
    linewidth = 1.5

    if wt_i_layoutplot is None:
        # Marked wts in layout plot
        wt_i_layoutplot = wt_i_plot

    # Load measurement data and model results
    if fulldata:
        # Data and model files contain all wts and wt_i_plot is used to make a subset row plot
        wt_i = wt_i_plot
    else:
        # Data and model contain only a single row of wts
        wt_i = np.arange(0, np.asarray(wt_i_plot).size, 1)
    plotx = np.arange(1, np.asarray(wt_i_plot).size + 1, 1)

    data = np.genfromtxt(datafile, skip_header=True)[wt_i, :]
    models = []
    for modefile in modelfiles:
        models.append(np.genfromtxt(modefile, skip_header=True)[wt_i, :])
    Nmodels = len(models)

    ls = []
    labels = []
    fig, ax = plt.subplots(1, 1, sharey=False, figsize=(figx, figy))
    ax_layout = fig.add_axes([0.7, 0.3, wfx, wfy], frameon=True)
    if dataerrorbar:
        ldata = ax.errorbar(plotx, data[:, 1], yerr=data[:, 2] / np.sqrt(data[:, 3]),
                            color='k', elinewidth=1.0, linewidth=0, marker='o', zorder=0, markersize=4)
    else:
        ldata = ax.scatter(plotx, data[:, 1], color='k', marker='o', zorder=0, s=20)

    ls.append(ldata)
    labels.append('Data')

    for i in range(Nmodels):
        mls, = ax.plot(plotx, models[i][:, 1], color=modelcolors[i], linewidth=linewidth)
        if GA:
            mlsGA, = ax.plot(plotx, models[i][:, 2], color=modelcolors[i], dashes=[5, 2], linewidth=linewidth)
        ls.append(mls)
        if GA:
            ls.append(mlsGA)
        labels.append(modellabels[i])
        if GA:
            labels.append(modellabels[i] + ' GA')
    ax.set_xlim(0.9, models[0][:, 0].size + 0.1)
    ax.grid(True)
    if xticks is None:
        ax.set_xticks(plotx)
    else:
        ax.set_xticks(xticks)
    if xticklabels is not None:
        ax.set_xticklabels(xticklabels)

    if Nmodels > 1:
        if GA:
            ldummy = copy(ls[1])
            ldummy.set_color('w')
            ls.insert(1, ldummy)
            labels.insert(1, '')
        ncol = Nmodels + 1
        ybox = 0.8
    else:
        if GA:
            ncol = 3
        else:
            ncol = 2
        ybox = 0.9

    ax.set_ylabel(ylabel, rotation=0)
    ax.yaxis.labelpad = 20
    ax.set_xlabel('WT nr.')
    fig.tight_layout(rect=[0, 0, 0.75, ybox])
    # Plot the layout in a small plot
    xWT = (np.array(wt_x) - wt_x[0]) / D
    yWT = (np.array(wt_y) - wt_y[0]) / D
    ax_layout.scatter(xWT, yWT, s=2, color='k')
    for i in range(len(wt_i_layoutplot)):
        if not np.isnan(wt_i_layoutplot[i]):
            ax_layout.scatter(xWT[int(wt_i_layoutplot[i])], yWT[int(wt_i_layoutplot[i])], s=2, color='r')
    ax_layout.set_xticks([])
    ax_layout.set_yticks([])
    ax_layout.set_aspect(1)
    ax_layout.set_xlim(xWT.min() - 20, xWT.max() + 10)
    ax_layout.set_ylim(yWT.min() - 10, yWT.max() + 20)
    ax_layout.arrow(xWT.min() - 10, yWT.max() + 10, 6 * np.cos((270.0 - wd) / 180.0 * np.pi),
                    6 * np.sin((270.0 - wd) / 180.0 * np.pi), head_width=3, head_length=1.5, color='r')
    circle1 = plt.Circle((xWT.min() - 10, yWT.max() + 10), 8, color='k', fill=False)
    ax_layout.add_artist(circle1)
    # Move layout plot to the top of the main plot
    pos1 = ax.get_position()
    pos2 = ax_layout.get_position()
    pos3 = [pos2.x0, pos2.y0 + pos1.y0 + pos1.height - (pos2.y0 + pos2.height), pos2.width, pos2.height]
    ax_layout.set_position(pos3)
    fig.tight_layout(rect=[0, 0, 0.75, ybox])
    fig.legend(tuple(ls), tuple(labels), ncol=ncol, loc='upper center', bbox_to_anchor=(0, 0, 1, 1), numpoints=1, scatterpoints=1)
    fig.savefig(outfile)


def plotWFeffrose(datafile, modelfiles, modellabels, modells, modelGAls, outfile, wt_x, wt_y, D, layoutscale=0.15,
                  dataNbins=None, fullrose=True, polar=True, plotGAonly=False, WFeffmin=0.0, WFeffmax=1.0,
                  figx=9, figy=6):
    """
    Plot wind farm efficiency as function of wind direction for a specific wind speed
    and compare with measurements. The measurements are expected to be read from a data file:
    # wd, efficiency
    The models results are expected to be in a data file including both
    non gaussian averaged (GA) and gaussian averaged results:
    # wd, efficiency, efficiency with GA
    modelfiles is a list of models file names, so multiple models can be compared.
    if polar==True, a rose plot is made including the wf layout in the center.
    if polar==False, a rectangular plot is made without the wf layout.
    """
    warnings.filterwarnings("ignore")
    linewidth = 1.5

    # Load measurement data and model results
    data = np.genfromtxt(datafile, skip_header=True)
    models = []
    for modefile in modelfiles:
        models.append(np.genfromtxt(modefile, skip_header=True))
    Nmodels = len(models)

    ls = []
    labels = []
    fig = plt.figure(figsize=(figx, figy))
    rect = [0.1, 0.1, 0.8, 0.7]
    ax = fig.add_axes(rect, polar=polar)
    if polar:
        ax_layout = fig.add_axes(rect, frameon=False)
        wd_convert = np.pi / 180.0
    else:
        wd_convert = 1.0
    if fullrose:
        # Make data periodic
        datap = np.append(data[:, :], [data[0, :]], axis=0)
    else:
        datap = data

    if dataNbins is None:
        # Number of bins is expected to be a column
        ldata = ax.fill_between(datap[:, 0] * wd_convert,
                                datap[:, 1] - datap[:, 2] / np.sqrt(datap[:, 3]),
                                datap[:, 1] + datap[:, 2] / np.sqrt(datap[:, 3]), color='k', alpha=0.3)
    else:
        # Number of bins is a constant set by dataNbins
        ldata = ax.fill_between(datap[:, 0] * wd_convert, datap[:, 1] - datap[:, 2] / np.sqrt(dataNbins),
                                datap[:, 1] + datap[:, 2] / np.sqrt(dataNbins), color='k', alpha=0.3)

    ls.append(ldata)
    labels.append('Data')

    for i in range(Nmodels):
        if fullrose:
            # Make models periodic
            modelp = np.append(models[i][:, :], [models[i][0, :]], axis=0)
        else:
            modelp = models[i]
        if not plotGAonly:
            if modells[i][0:2] == '--':
                dash = [5, 2]
            else:
                dash = [1, 0]
            mls, = ax.plot(modelp[:, 0] * wd_convert, modelp[:, 1], modells[i], dashes=dash, linewidth=linewidth)
            ls.append(mls)
            labels.append(modellabels[i])
        if modelGAls[i][0:2] == '--':
            dash = [5, 2]
        else:
            dash = [1, 0]
        mlsGA, = ax.plot(modelp[:, 0] * wd_convert, modelp[:, 2], modelGAls[i], dashes=dash, linewidth=linewidth)
        ls.append(mlsGA)
        if not plotGAonly:
            labels.append(modellabels[i] + ' GA')
        else:
            labels.append(modellabels[i])
        print('WFeff', modellabels[i], models[i][:, 1].mean(), 'DATA', data[:, 1].mean())

    if Nmodels > 1:
        ldummy = copy(ls[1])
        ldummy.set_color('w')
        ls.insert(1, ldummy)
        labels.insert(1, '')
        ncol = int(Nmodels / 2) + 1
        ybox = 0.8
    else:
        ncol = 3
        ybox = 0.9

    if polar:
        ax.set_theta_zero_location("N")
        ax.set_theta_direction(-1)
        ax.set_rmin(WFeffmin)
        ax.set_rmax(WFeffmax)
        ax.set_rlabel_position(340.0)

        xWTc = 0.5 * (np.array(wt_x).max() + np.array(wt_x).min())
        yWTc = 0.5 * (np.array(wt_y).max() + np.array(wt_y).min())
        scale = layoutscale
        xWT = (np.array(wt_x) - xWTc) / D * scale
        yWT = (np.array(wt_y) - yWTc) / D * scale

        ax_layout.set_xlim(xWT.min() / scale, xWT.max() / scale)
        ax_layout.set_ylim(yWT.min() / scale, yWT.max() / scale)
        ax_layout.set_xlim(xWT.min() / scale, xWT.max() / scale)
        ax_layout.set_ylim(yWT.min() / scale, yWT.max() / scale)
        ax_layout.set_aspect(1)
        ax_layout.scatter(xWT, yWT, s=2, color='k')
        ax_layout.set_xticks([])
        ax_layout.set_yticks([])
    else:
        ax.set_xlabel('Wind direction [deg]')
        ax.set_ylabel('Wind farm efficiency')
        ax.grid(True)
        ax.set_ylim(WFeffmin, WFeffmax)

    fig.legend(tuple(ls), tuple(labels), ncol=ncol, loc='upper center', bbox_to_anchor=(0, 0, 1, 1), numpoints=1, scatterpoints=1)
    fig.savefig(outfile)


def plotflowtransect(datafile, modelfiles, modellabels, modelcolors, outfile, H, modelivar=3, dataerrorbar=False, dataivar=1, ylabel='Speed up factor', ymin=-2.0, ymax=1.0):
    linewidth = 1.5
    data = np.genfromtxt(datafile, skip_header=True)
    models = []
    Nmodels = len(modelfiles)
    for i in range(Nmodels):
        models.append(np.genfromtxt(modelfiles[i], skip_header=True))
    ls = []
    labels = []
    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    if dataerrorbar:
        ldata = ax.errorbar(data[:, 0], data[:, dataivar], yerr=np.array([data[:, dataivar] - data[:, 3],
                            data[:, 3] - data[:, dataivar]]), color='k', elinewidth=1.0, linewidth=0,
                            marker='o', zorder=0, markersize=4)
    else:
        ldata = ax.scatter(data[:, 0], data[:, dataivar], color='k', marker='o', zorder=0, s=20)

    ls.append(ldata)
    labels.append('Data')

    for i in range(Nmodels):
        mls, = ax.plot(models[i][:, 0], models[i][:, modelivar], color=modelcolors[i], linewidth=linewidth)
        ls.append(mls)
        labels.append(modellabels[i])
    ax.grid(True)
    ax.set_xlabel(r'Distance from hill top: $x/H$')
    ax.set_ylabel(ylabel)
    ax.set_xlim(models[0][0, 0], models[0][-1, 0])
    ax.set_ylim(ymin, ymax)

    ax2 = ax.twinx()
    ax2.fill_between(models[0][:, 0], models[0][:, 1] - models[0][:, 2], y2=0.0, color='k')
    ax2.set_ylim(0.0, 3.0)
    ax2.set_ylabel(r'Height: $z/H$')
    ncol = Nmodels + 1
    fig.tight_layout(rect=[0, 0, 1, 0.9])
    fig.legend(tuple(ls), tuple(labels), ncol=ncol, loc='upper center', bbox_to_anchor=(0, 0, 1, 1), numpoints=1, scatterpoints=1)
    fig.savefig(outfile)


def plotflowprofile(datafile, modelfiles, modellabels, modelcolors, outfile, H, modelivar=1, dataivar=1, xlabel='Speed up factor', xmin=0.0, xmax=1.5):
    linewidth = 1.5
    data = np.genfromtxt(datafile, skip_header=True)
    models = []
    Nmodels = len(modelfiles)
    for i in range(Nmodels):
        models.append(np.genfromtxt(modelfiles[i], skip_header=True))
    ls = []
    labels = []
    fig, ax = plt.subplots(1, 1, figsize=(8, 5))
    ldata = ax.scatter(data[:, dataivar], data[:, 0] / H, color='k', marker='o', zorder=0, s=20)

    ls.append(ldata)
    labels.append('Data')

    for i in range(Nmodels):
        mls, = ax.plot(models[i][:, modelivar], models[i][:, 0], color=modelcolors[i], linewidth=linewidth)
        ls.append(mls)
        labels.append(modellabels[i])
    ax.grid(True)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(r'Height: $(z-H)/H$')
    ax.set_ylim(models[0][0, 0], models[0][-1, 0])
    ax.set_xlim(xmin, xmax)
    ncol = Nmodels + 1
    fig.tight_layout(rect=[0, 0, 1, 0.9])
    fig.legend(tuple(ls), tuple(labels), ncol=ncol, loc='upper center', bbox_to_anchor=(0, 0, 1, 1), numpoints=1, scatterpoints=1)
    fig.savefig(outfile)


def plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='wd', norm=0,
                   modeldashes=None, modeliU=1, ylim=None,
                   datafiles=None, lesfiles=None, Dstar=False, dataerrorbar=False, dataU0=1.0,
                   datawd0=0.0, dataiU=1, dataiUstd=2, dataiNbins=3, dataNbins=None,
                   ylabel='$U/U_0$', ylabelrot=0, datadelimiter=None):
    """
    Plot single wake deficit at hub height for several downstream distances. Measurement and LES data is optional
    modelsfiles is a 2D list where the inner list are filenames for different downstream distances and
    the outer list represents different models. The model data is expected to be columns of:
    axis='wd' and norm=0: Relative wd, U/U0
    or
    axis='y' and norm=0: Relative cross distance y/D, U/U0
    or
    axis='wd' and norm=1: Relative wd, (U-U0)/U0
    or
    axis='y' and norm=1: Relative cross distance y/D, (U-U0)/U0
    """

    linewidth = 1.5
    cLES = 'gray'

    nDown = len(sDown_D)
    Nmodels = len(modelfiles)

    if modeldashes is None:
        modeldashes = [(5, 0)] * len(modelfiles)

    ls = []
    labels = []
    fig, ax = plt.subplots(1, nDown, sharex=False, sharey=False, figsize=(3 * nDown, 3))
    # Trick for common x and y labels:
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    for i in range(nDown):
        if datafiles is not None:
            if datafiles[i] != '':
                data = np.genfromtxt(datafiles[i], skip_header=True, delimiter=datadelimiter)
                if dataerrorbar:
                    if dataNbins is None:
                        # Number of bins is expected to be a column
                        ldata = ax[i].errorbar(data[:, 0] - datawd0, data[:, dataiU] / dataU0, yerr=data[:, dataiUstd] / dataU0 /
                                               np.sqrt(data[:, dataiNbins]), color='k', elinewidth=1.0, linewidth=0, marker='o', zorder=0, markersize=4)
                    else:
                        # Number of bins is a constant
                        ldata = ax[i].errorbar(data[:, 0] - datawd0, data[:, dataiU] / dataU0, yerr=data[:, dataiUstd] / dataU0 /
                                               np.sqrt(dataNbins), color='k', elinewidth=1.0, linewidth=0, marker='o', zorder=0, markersize=4)
                else:
                    ldata = ax[i].scatter(data[:, 0] - datawd0, data[:, dataiU] / dataU0, color='k', marker='o', zorder=0, s=10)
    if datafiles is not None:
        ls.append(ldata)
        labels.append('Data')

    for i in range(nDown):
        if lesfiles is not None:
            LES = np.genfromtxt(lesfiles[i], skip_header=True)
            # Shaded area represent the standard error of the mean
            lLES = ax[i].fill_between(LES[:, 0], LES[:, 1] - LES[:, 2] / np.sqrt(LES[:, 3]),
                                      LES[:, 1] + LES[:, 2] / np.sqrt(LES[:, 3]), color=cLES, alpha=0.5)
            if i == 0:
                ls.append(lLES)
                labels.append('LES')

        for j in range(Nmodels):
            model = np.genfromtxt(modelfiles[j][i], skip_header=True)
            lm, = ax[i].plot(model[:, 0], model[:, modeliU], modelcolors[j], linewidth=linewidth, dashes=modeldashes[j])
            if i == 0:
                ls.append(lm)
                labels.append(modellabels[j])

        if Dstar:
            title = '%s %g %s' % ('$x=', sDown_D[i], 'D^*$')
        else:
            title = '%s %g %s' % ('$x=', sDown_D[i], 'D$')
        ax[i].set_title(title)
        if xaxis == 'wd':
            ax[i].set_xticks(np.arange(-30.0, 40.0, 10.0))
            if sDown_D[i] < 7.0:
                ax[i].set_xlim(-30, 30)
            else:
                ax[i].set_xlim(-20, 20)
        elif xaxis == 'y':
            ax[i].set_xlim(-1.5, 1.5)
        ax[i].grid(True)
        if ylim is None:
            if norm == 0:
                ax[i].set_ylim(None, ymax=1.1)
            elif norm == 1:
                ax[i].set_ylim(None, ymax=0.1)
        else:
            ax[i].set_ylim(None, ymax=ylim)

    ax[0].set_ylabel(ylabel, rotation=ylabelrot)
    ax[0].yaxis.labelpad = 20
    if xaxis == 'wd':
        plt.xlabel('Relative wind direction [deg]')
    elif xaxis == 'y':
        # ax[1].set_xlabel('$y/D$')
        plt.xlabel('$y/D$')
    fig.tight_layout(rect=[0, 0, 1, 0.9])

    ncol = Nmodels
    if datafiles is not None:
        ncol = ncol + 1
    if lesfiles is not None:
        ncol = ncol + 1
    fig.legend(tuple(ls), tuple(labels), ncol=ncol, loc='upper center', bbox_to_anchor=(0, 0, 1, 1), numpoints=1, scatterpoints=1)
    fig.savefig(outfile)


def plotWFpower(datafile, modelfiles, modellabels, outfile, wt_x, wt_y, wd, xlabel='$x/D$', ylabel='$y/D$', clabel='$P_i/P_1$'):
    """
    Plot wind turbine power in a wind farm as colored dots orientated in the wind farm layout
    Data is expected to be
    wt_i, Power_wt_i
    Model data is expected to be
    wt_i, Power_wt_i, Power_wt_i_GA
    """

    # Load measurement data and model results
    data = np.genfromtxt(datafile, skip_header=True)
    models = []
    for modefile in modelfiles:
        models.append(np.genfromtxt(modefile, skip_header=True))
    Nmodels = len(models)

    labels = []
    fig, ax = plt.subplots(1, Nmodels + 1, sharex=True, sharey=True, figsize=(4 + 4 * Nmodels, 5))
    cmap = plt.cm.get_cmap('jet', 12)
    vmin = 0.7
    vmax = 1.3
    levels = np.linspace(vmin, vmax, 7)

    ax[0].set_ylabel(ylabel)
    for i in range(Nmodels):
        ax[i].set_title(modellabels[i])
        ax[i].set_aspect('equal')
        ax[i].scatter(wt_x, wt_y, c=models[i][:, 2], s=20, cmap=cmap, vmin=vmin, vmax=vmax, lw=0)
        ax[i].set_xlabel(xlabel)

    ax[-1].set_title('Data')
    ax[-1].set_aspect('equal')
    ax[-1].set_xlabel(xlabel)
    cs = ax[-1].scatter(wt_x, wt_y, c=data[:, 1], s=20, cmap=cmap, vmin=vmin, vmax=vmax, lw=0)

    ax[0].arrow(wt_x.min() - 5, wt_y.max() + 15, 6 * np.cos((270.0 - wd) / 180.0 * np.pi),
                6 * np.sin((270.0 - wd) / 180.0 * np.pi), head_width=4, head_length=4, color='r')
    circle1 = plt.Circle((wt_x.min() - 5, wt_y.max() + 15), 10, color='k', fill=False)
    ax[0].add_artist(circle1)

    fig.colorbar(cs, ax=ax[-1], ticks=levels, label=clabel)
    fig.savefig(outfile)
