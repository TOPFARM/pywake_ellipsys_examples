import numpy as np
import sys
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *


def run_WieringermeerRow(run_machine, queue, coarse=False, grid=False, cal=False, run=False,
                         write=False, plot=False, plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.hornsrev1 import Hornsrev1Site
    from py_wake.validation.ecn_wieringermeer import wt_x, wt_y
    from py_wake_ellipsys_examples.data.turbines.n80 import N80
    from py_wake.validation.validation_lib import GaussianFilter
    from py_wake_ellipsys_examples.plotwfres import plotWFrow
    from py_wake_ellipsys_examples.WieringermeerRow.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Wieringermeer wind turbine row simulations, as published in DOI: 10.1002/we.1804
    # The grid is similar by selecting a z-distribution with a uniform
    # spaced region in the wake domain. The newer z-distribution is more
    # smooth, but requires more cells. The height of the domain has been
    # increased from 10D to 25D, which changes the downstream wind turbine
    # power.
    nWT = len(wt_x)
    type_i = np.zeros((nWT))
    wt = N80()
    Dref = wt.diameter()
    TIs = [0.024, 0.096]  # Low and high TI cases based on TKE ~ 0.8 * TI_u = sigma_U/U
    subcases = ['_TIlow', '_TIhigh']
    case = 'WieringermeerRow'
    zRef = 80.0
    # A single flow case
    # wd = [270.0]
    # A range of wind direction to perform Gaussian averaging for wd=275+-3 deg
    wd = np.arange(263.0, 290.0, 3.0)
    ws = [6.59, 8.35]
    sigma = 2.5  # Estimated wind direction uncertainty [deg]
    # Old z distrubution, which saves cells but has larger expansion ratios at the start and end of the wake domain
    zdistr = 'uniwake'
    # Old results were made with zlen_D=10, which causes some additional artificial blockage
    zlen_D = 25.0
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 1e-2
        grid_cells1_D = 2.0
        adgrid_nr = 5
        grid_bsize_cal = 48
        grid_bsize = 48
        maxnodes = 1
        walltime_wf_run = '0:20:00'
    else:
        e3d_reslim = 1e-5
        grid_cells1_D = 10.0
        adgrid_nr = 64
        grid_bsize_cal = 32
        # If one can afford the resources:
        grid_bsize = 32
        maxnodes = 7
        walltime_wf_run = '0:30:00'
        # Otherwise:
        # grid_bsize = 64
        # maxnodes = 1
        # walltime_wf_run = '10:00:00'

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    linewidth = 1.5
    cRANS = 'g'
    today = date.today()

    # AD
    ad = AD(force='1101', grid=ADGrid(nr=adgrid_nr, ntheta=32))
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, zFirstCell_D=0.5 / Dref, radius_D=5e4 / Dref, cells1_D=grid_cells1_D,
                         zlen_D=25.0, zdistr=zdistr, bsize=grid_bsize,
                         dwd=40.0  # To reduce grid size, because we only simulate 275+-12 deg
                         )
    # Wind farm run
    wfrun = WFRun(casename=case, cluster=Cluster(walltime=walltime_wf_run))

    e3d = E3D(reslim=e3d_reslim)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TIs[0], zRef,
                   ad=ad, e3d=e3d, wfrun=wfrun)
    # Create calibration grid (use smaller block size to speed up calibration)
    wfm.calrun.bsize = grid_bsize_cal

    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create calibration grid
        wfm.create_calibration_grid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)

    if cal:
        # Run force control calibration
        for TI in TIs:
            wfm.TI = TI
            wfm.run_calibration(type_i[0])

    if run:
        # Run flow cases
        for i in range(len(TIs)):
            wfm.TI = TIs[i]
            wfm.run_windfarm(wt_x, wt_y, wd, [ws[i]], type_i)

    if write:
        # Write results that can be used in the PyWake validation report
        for j in range(len(TIs)):
            wfm.TI = TIs[j]

            # Write results that can be used in the PyWake validation report
            WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
            # Gaussian averaging with a variable sigma
            l = np.where(wd == 275.0)[0][0]
            powerGA = np.zeros(power_ilk.shape)
            lGA1 = l - 4
            lGA2 = l + 5
            lAve1 = l - 1
            lAve2 = l + 2
            for i in range(nWT):
                powerGA[i, lGA1:lGA2, 0] = GaussianFilter(power_ilk[i, lGA1:lGA2, 0], wd[lGA1:lGA2], 3, sigma)
            # Average over 3 wds:
            ProwAve = power_ilk[:, lAve1:lAve2, 0].sum(axis=1)
            ProwAveGA = powerGA[:, lAve1:lAve2, 0].sum(axis=1)
            # Output
            filename = case + subcases[j] + '_RANS_wd275_Row.dat'
            f = open(filename, 'w')
            f.write('# EllipSys3D-RANS-AD, k-epsilon-fP\n')
            f.write('# Based on DOI: 10.1002/we.1804, rerun date=%s\n' % today)
            f.write('# GA with sigma=2.5 deg\n')
            f.write('# WT, Pi/P0,     Pi/P0+GA\n')
            for i in range(nWT):
                f.write('%-5i %10.8f %10.8f\n' % (i + 1, ProwAve[i] / ProwAve[0], ProwAveGA[i] / ProwAveGA[0]))
            f.close()

    if plot:
        for j in range(len(TIs)):
            wfm.TI = TIs[j]
            datafile = data_path + 'Wieringermeer%s_WFdata_wd275_Row.dat' % subcases[j]
            # Compare with old results for high TI case:
            # oldmodelfile = data_path + 'Wieringermeer_RANS_wd275_Row.dat'
            # modelfiles = [case + subcases[1] + '_RANS_wd275_Row.dat', oldmodelfile]
            # modellabels = ['RANS', 'RANS old']
            # modelcolors = ['g', 'r']
            modelfiles = [rans_path + case + subcases[j] + '_RANS_wd275_Row.dat']
            modellabels = ['RANS']
            modelcolors = ['g']
            outfile = case + subcases[j] + '_RANS_wd275_Row.' + plotfmt
            plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, np.linspace(0, 4, 5), wt.diameter(), 275.0)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Run with a very coarse setup if coarse=True
    run_WieringermeerRow(run_machine, queue, coarse=False, grid=True, cal=True, run=True,
                         write=True, plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
