import numpy as np
import sys
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *


def run_Hornsrev1(run_machine, queue, coarse=False, grid=False, cal=False, run=False,
                  postrows=False, plotrows=False, plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.hornsrev1 import wt_x, wt_y, Hornsrev1Site
    from py_wake_ellipsys_examples.data.turbines.v80 import V80
    from py_wake.validation.validation_lib import GaussianFilter, sigma_hornsrev
    from py_wake_ellipsys_examples.Hornsrev1.data import data_path
    from py_wake_ellipsys_examples.plotwfres import plotWFrow
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Hornsrev I wind farm simulations, as published in DOI: 10.1002/we.1804
    # The grid is similar by selecting a z-distribution with a uniform
    # spaced region in the wake domain. The newer z-distribution is more
    # smooth, but requires more cells. The height of the domain has been
    # increased from 10D to 25D, which changes the downstream wind turbine
    # power.
    nWT = len(wt_x)
    type_i = np.zeros((nWT))
    wt = V80()
    Dref = wt.diameter()
    TI = 0.056  # TI based on TKE ~ 0.8 * TI_u = sigma_U/U
    zRef = 70.0
    # A single flow case
    # wd = [270.0]
    # A range of wind direction to perform Gaussian averaging for wd=270
    wd = np.arange(255.0, 290.0, 5.0)
    ws = [8.0]
    sigma = 5.0  # Estimated wind direction uncertainty [deg]
    # Old z distribution, which saves cells but has larger expansion ratios at the start and end of the wake domain
    zdistr = 'uniwake'
    # Old results were made with zlen_D=10, which causes some additional artificial blockage
    zlen_D = 25.0
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 1e-2
        grid_cells1_D = 2.0
        adgrid_nr = 5
        grid_bsize = 48
        grid_bsize_cal = 48
        maxnodes = 1
        walltime_wf_run = '0:20:00'
    else:
        e3d_reslim = 1e-5
        grid_cells1_D = 10.0
        adgrid_nr = 64
        grid_bsize_cal = 32
        # If one can afford the resources:
        # grid_bsize = 32
        # maxnodes = 36
        # walltime_wf_run = '30:00:00'
        # Otherwise:
        grid_bsize = 64
        maxnodes = 10
        walltime_wf_run = '10:00:00'
    linewidth = 1.5
    cRANS = 'g'
    today = date.today()

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1101', grid=ADGrid(nr=adgrid_nr, ntheta=32))
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, zFirstCell_D=0.5 / Dref, radius_D=5e4 / Dref, cells1_D=grid_cells1_D,
                         zlen_D=25.0, zdistr=zdistr, bsize=grid_bsize)
    # Wind farm run
    wfrun = WFRun(casename='Hornsrev1', cluster=Cluster(walltime=walltime_wf_run))

    e3d = E3D(reslim=e3d_reslim)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   ad=ad, e3d=e3d, wfrun=wfrun)

    # Create calibration grid (use smaller block size to speed up calibration)
    wfm.calrun.bsize = grid_bsize_cal

    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        wfm.create_calibration_grid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)

    if cal:
        # Run force control calibration
        wfm.run_calibration(type_i[0])

    if run:
        # Run flow cases
        wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)

    if postrows:
        # Write results that can be used in the PyWake validation report
        WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
        # Gaussian averaging with a variable sigma
        l = np.where(wd == 270.0)[0][0]
        powerGA = np.zeros(power_ilk.shape)
        sigma = sigma_hornsrev('vanderLaan', wt_x, wt_y)
        l1 = l - 3
        l2 = l + 4
        for i in range(nWT):
            powerGA[i, :, 0] = GaussianFilter(power_ilk[i, l1:l2, 0], wd[l1:l2], 3, sigma[i])
        # Row average of inner rows
        power_matrix = power_ilk[:, l, 0].reshape(10, 8)
        powerGA_matrix = powerGA[:, l, 0].reshape(10, 8)
        ProwAve = power_matrix[:, 1:6].sum(axis=1)
        ProwAveGA = powerGA_matrix[:, 1:6].sum(axis=1)
        # Output
        filename = 'Hornsrev1_RANS_wd270_InnerRowMean.dat'
        f = open(filename, 'w')
        f.write('# EllipSys3D-RANS-AD, k-epsilon-fP\n')
        f.write('# Based on DOI: 10.1002/we.1804, rerun date=%s\n' % today)
        f.write('# GA with a variable standard deviation\n')
        f.write('# that is a function of the distance\n')
        f.write('# to the wd reference location\n')
        f.write('# WT, Pi/P0,     Pi/P0+GA\n')
        for i in range(10):
            f.write('%-5i %10.8f %10.8f\n' % (i + 1, ProwAve[i] / ProwAve[0], ProwAveGA[i] / ProwAveGA[0]))
        f.close()

    if plotrows:
        datafile = data_path + 'Hornsrev1_WFdata_wd270_InnerRowMean.dat'
        # Compare with old results:
        # oldmodelfile = data_path + 'Hornsrev1_RANS_wd270_InnerRowMean.dat'
        # modelfiles = ['Hornsrev1_RANS_wd270_InnerRowMean.dat', oldmodelfile]
        # modellabels = ['RANS', 'RANS old']
        # modelcolors = ['g', 'r']
        modelfiles = [rans_path + 'Hornsrev1_RANS_wd270_InnerRowMean.dat']
        modellabels = ['RANS']
        modelcolors = ['g']
        outfile = 'Hornsrev1_wd270_InnerRowMean.' + plotfmt
        wt_i_plot = np.linspace(1, 11, 10)
        wt_inner_rows = np.linspace(0, 79, 80).reshape(10, 8)[:, 1:7].flatten().tolist()
        plotWFrow(datafile, modelfiles, modellabels, modelcolors, outfile, wt_x, wt_y, wt_i_plot,
                  wt.diameter(), 270.0, wt_i_layoutplot=wt_inner_rows)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Run Hornsrev1 case using a very coarse setup if coarse=True
    run_Hornsrev1(run_machine, queue, coarse=True, grid=True, cal=True, run=True,
                  postrows=True, plotrows=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
