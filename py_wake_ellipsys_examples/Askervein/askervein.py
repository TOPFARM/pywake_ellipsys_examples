import numpy as np
import time
import os
import sys
import xarray
import time
from datetime import date

from py_wake.tests import npt
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.examples.data.hornsrev1 import Hornsrev1Site


def run_Askervein(run_machine, queue, grid=True, run=True, post=True, netCDF_test=True, write=True, plot=True, plotfmt='pdf', rans_path=''):
    from py_wake_ellipsys_examples.data.turbines.dummy_wt import Dummy
    from py_wake_ellipsys_examples.plotwfres import plotflowtransect, plotflowprofile
    from py_wake_ellipsys_examples.Askervein.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Setup for calculating siting parameters of Askervein hill using a WAsP CFD type setup
    # Inner domain of 4x4 km is set by the wake domain, while the terrain marign are not used
    # The inner domain is not rotated.
    # The input terrain is a WAsP contour map including a single roughness line representing
    # a coast line. The land and sea roughness lenghts are set to 0.02 m and 0.0002 m, respectively
    # grid_radius is 17.5 km and the domain height is set as 0.85 * grid_radius.
    # Two turbulence models are run: the standard k-epsilon model and the k-epsilon-fP model
    # The main difference with the WAsP CFD is the cmu constant. WAsP uses 0.055, we use 0.03.

    # We need to use at least one wt to run the rans wind farm model for now.
    # Here the dummy wt has a rotor diameter of 100 m, which is used a scaling parameter
    wt = Dummy()
    Dref = wt.diameter()
    domain_center = [75222.0, 823587.0]
    hilltop_xy = [75383.0, 823737.65]
    wt_x = np.array([0.0]) + domain_center[0]
    wt_y = np.array([0.0]) + domain_center[1]
    type_i = np.array([0])
    h0_i = np.array([120.0])  # Surface height at hill top
    h_i = wt.hub_height(type_i)  # Hub height of dummy wt
    height = 120.0  # Height to scale results

    # Extraction line A
    transect = np.genfromtxt(data_path + 'extraction_line.dat')

    rundir = os.getcwd()
    if grid:
        os.system('tar -xzf ' + data_path + 'map.tar.gz')
    terrain_map = rundir + '/Askervein.map'
    turbmodels = ['ke', 'kefP']
    wds = [210.0]
    Uref = 10.0
    zRef = 10.0
    z0Inlet = 0.0002
    kappa = 0.4
    cmu = 0.03
    TI = kappa * np.sqrt(2.0 / 3.0) / (cmu ** 0.25 * np.log((zRef + z0Inlet) / z0Inlet))
    today = date.today()

    maxnodes = 3
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='0000', run_pre=True)
    # Wind farm grid
    wfgrid = TerrainGrid(Dref, type='terrainogrid',
                         dwd=360.0, cells1_D=4.8,
                         zFirstCell_D=0.05 / 100.0,
                         zdistr='waspcfd', zWakeEnd_D=10.0,  # 75% of cells are used within the fist 1 km, represented by grid_zWakeEnd_D
                         bsize=48, zlen_D=148.75,
                         terrain_map=terrain_map,
                         terrain_map_farfield_z0=z0Inlet,
                         terrain_map_Lxy_outer=42500.0,
                         terrain_map_dxy_outer=25.0,
                         terrain_map_farfield_h=-1,
                         radius_D=155.0,  # WAsP CFD grid radius is based on grid center
                         m1_e_D=20.0, m1_w_D=20.0,
                         m1_n_D=20.0, m1_s_D=20.0,
                         m2_w_D=0.0, m2_e_D=0.0,
                         m2_n_D=0.0, m2_s_D=0.0,
                         cells2_D=1.0, terrain_surfgen='hypgridsf',
                         terrain_hypsf_blendf=0.5,  # For rotated grids, we would need to use a lower value e.g. 0.001
                         terrain_hyp3d_blendf=0.03,  # For finer grids, we would need to use a lower value e.g. 0.001
                         terrain_ogrid_dr=0.0)  # Use WAsP calculated value
    wfgrid.cluster.walltime = '0:30:00'
    # Wind farm run
    wfrun = WFRun(casename='Askervein', cluster=Cluster(walltime='0:59:00'),
                  write_restart=True)
    # Post flow
    wfpostflow = WFPostFlow(outputformat='netCDF')
    e3d = E3D(turbmodel=turbmodels[0])
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   ad=ad, e3d=e3d, wfrun=wfrun, wfpostflow=wfpostflow, run_wd_con=False,
                   run_grid=grid, run_wf=run)

    # Make grid and run simulations
    # One could also use create_windfarm_grid() and run_windfarm() but then one should take care of grid_wd
    for i in range(len(turbmodels)):
        wfm.set_subattr('e3d.turbmodel', turbmodels[i])
        if i == 1:
            wfm.run_grid = False
        WS_eff_ilk, TI_effilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x, wt_y, h_i, type_i, wds, [Uref], h0_i=h0_i)

    if post:
        # Post process points, where third column is the height above terrain
        # Reference profile
        profileRS = np.zeros((101, 3))
        xRS = 74300.0
        yRS = 820980.0
        profileRS[:, 0] = xRS
        profileRS[:, 1] = yRS
        profileRS[:, 2] = np.linspace(1.0, 101.0, 101)
        # Hill top profile
        profileHT = np.zeros((101, 3))
        profileHT[:, 0] = hilltop_xy[0]
        profileHT[:, 1] = hilltop_xy[1]
        profileHT[:, 2] = np.linspace(1.0, 101.0, 101)
        transect2 = np.copy(transect)
        transect2[:, 2] = 10.0
        points = np.append(transect2[:, 0:3], profileRS, axis=0)
        points = np.append(points, profileHT, axis=0)
        # Post process points, where third column is the height above terrain
        for i in range(len(turbmodels)):
            wfm.set_subattr('e3d.turbmodel', turbmodels[i])
            wfm.post_windfarm_flow(wds, [Uref], precursor=True, points=points, points_z_as_normal=True)

    if netCDF_test:
        # Get same output but then from netCDF file representing inner part of the o-grid
        # Open netCDF data file
        j = 0
        wfm.set_subattr('e3d.turbmodel', turbmodels[j])
        folder = wfm.get_name()
        infile = '%s/post_precursor_flow_points_wd%g_ws%g/extracted.dat' % (folder, wds[0], Uref)
        print('Wait for extracted file')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        flowdata = np.genfromtxt(infile, skip_header=True)

        infile = '%s/post_precursor_flow_points_wd%g_ws%g/flowdata_terrain.nc' % (folder, wds[0], Uref)
        print('Wait for netCDF file')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()

        data = xarray.open_dataset(infile)
        # Interpolation points should include the terrain height
        profileRS = np.zeros((101, 3))
        xRS = 74300.0
        yRS = 820980.0
        hRS = data['hs1'].interp(xs1=xRS, ys1=yRS, method='linear')
        profileRS[:, 0] = xRS
        profileRS[:, 1] = yRS
        profileRS[:, 2] = np.linspace(1.0, 101.0, 101) + np.array([hRS])
        # Hill top profile
        profileHT = np.zeros((101, 3))
        profileHT[:, 0] = hilltop_xy[0]
        profileHT[:, 1] = hilltop_xy[1]
        hHT = data['hs1'].interp(xs1=hilltop_xy[0], ys1=hilltop_xy[1], method='linear')
        profileHT[:, 2] = np.linspace(1.0, 101.0, 101) + np.array([hHT])
        # Find terrain height if transect for testing, even though we have already saved the values.
        transect2 = np.copy(transect)
        npoints = transect2[:, 0].size
        hi = np.zeros(npoints)
        hi[:] = np.nan
        N_h_maps = 2
        for j in range(N_h_maps):
            if np.isnan(hi).any():
                for i in range(npoints):
                    if np.isnan(hi[i]):
                        if j == 0:
                            hi[i] = data['hs%i' % (j + 1)].interp(xs1=transect2[i, 0], ys1=transect2[i, 1], method='linear')
                        elif j == 1:
                            hi[i] = data['hs%i' % (j + 1)].interp(xs2=transect2[i, 0], ys2=transect2[i, 1], method='linear')
        # f=open('extraction_line.dat', 'w')
        # for i in range(npoints):
        #    f.write('%8.2f %9.2f %11.8f %4.1f\n' % (transect2[i, 0], transect2[i, 1], hi[i] + 10.0, 10.0))
        # f.close()
        transect2[:, 2] = hi + 10.0
        points = np.append(transect2[:, 0:3], profileRS, axis=0)
        points = np.append(points, profileHT, axis=0)

        # Python does not have interpolation tools for 3D curvilinear grids, so we use an EllipSys based tool (InverseMap)
        from pyellipsys.inversemap import InverseMap
        IM = InverseMap()
        Ui = IM.interp(data['x'], data['y'], data['z'], data['U'], points[:, 0], points[:, 1], points[:, 2])
        # A new interpolation with the same interpolation points can be used more quickly as
        Vi = IM.interp(data['x'], data['y'], data['z'], data['V'], points[:, 0], points[:, 1], points[:, 2], make_inversemap=False, locate_points=False)
        # The reference profile is outside the inner grid, so it cannot be found in the netCDF file.
        # Check difference with extracted.dat, should be less than 1e-7
        ntest = 121  # The last two points are not found by IM because cell centered grid is stored
        absdiffU = np.abs(Ui[0:ntest] - flowdata[0:ntest, 5])
        print('diff', np.mean(absdiffU), np.max(absdiffU), np.min(absdiffU))
        absdiffV = np.abs(Vi[0:ntest] - flowdata[0:ntest, 6])
        print('diff', np.mean(absdiffV), np.max(absdiffV), np.min(absdiffV))
        npt.assert_array_almost_equal(np.max(absdiffU), np.array(0.0), 7)
        npt.assert_array_almost_equal(np.max(absdiffV), np.array(0.0), 7)

    if write:
        # Write speed up factor and TI to a file for transect A and speed up factor at hill top
        for j in range(len(turbmodels)):
            wfm.set_subattr('e3d.turbmodel', turbmodels[j])
            folder = wfm.get_name()
            infile = '%s/post_precursor_flow_points_wd%g_ws%g/extracted.dat' % (folder, wds[0], Uref)
            print('Wait for extracted.dat')
            while not os.path.exists(infile):
                time.sleep(5)
                sys.stdout.write('.', )
                sys.stdout.flush()
            # Speed up profile at hill top
            nt = transect[:, 0].size
            nprof = 101
            flowdataRS = np.genfromtxt(infile, skip_header=True)[nt:(nt + nprof), :]
            flowdataHT = np.genfromtxt(infile, skip_header=True)[(nt + nprof):(nt + 2 * nprof), :]
            windspeedRS = np.sqrt(flowdataRS[:, 5] ** 2 + flowdataRS[:, 6] ** 2)
            windspeedHT = np.sqrt(flowdataHT[:, 5] ** 2 + flowdataHT[:, 6] ** 2)
            speedup = (windspeedHT - windspeedRS) / windspeedRS
            f = open('Askervein_prof_' + turbmodels[j] + '_wd' + str(int(wds[0])) + '_speedup.dat', 'w')
            f.write('# EllipSys3D-RANS, %s\n' % turbmodels[j])
            f.write('# Rerun date=%s\n' % today)
            f.write('# H = %g m\n' % height)
            f.write('# height above terrain: normal/H, speed up factor\n')
            for i in range(nprof):
                f.write('%8.6f %8.6f\n' % ((flowdataHT[i, 3] - h0_i) / height, speedup[i]))
            f.close()
            # Transect
            wsRS = windspeedRS[9]
            nt = transect[:, 0].size
            flowdata = np.genfromtxt(infile, skip_header=True)[0:nt, :]
            deg_ext = 270.0 - np.arctan((flowdata[0, 2] - flowdata[-1, 2]) / (flowdata[0, 1] - flowdata[-1, 1])) * 180.0 / np.pi
            deg = (270.0 - deg_ext) * np.pi / 180.0
            xrot = (flowdata[:, 1] - hilltop_xy[0]) * np.cos(deg) + (flowdata[:, 2] - hilltop_xy[1]) * np.sin(deg)
            windspeed = np.sqrt(flowdata[:, 5] ** 2 + flowdata[:, 6] ** 2)
            speedup = (windspeed - wsRS) / wsRS
            TI = np.sqrt(2.0 / 3.0 * flowdata[:, 10]) / wsRS
            f = open('Askervein_A_' + turbmodels[j] + '_wd' + str(int(wds[0])) + '_speedup_TI.dat', 'w')
            f.write('# EllipSys3D-RANS, %s\n' % turbmodels[j])
            f.write('# Rerun date=%s\n' % today)
            f.write('# H = %g m\n' % height)
            f.write('# Distance from hill top: x/H, height: z/H, height above terrain: normal/H, speed up factor,  local TI\n')
            for i in range(nt):
                f.write('%8.6f %8.6f %8.6f %8.6f %8.6f\n' % (xrot[i] / height, flowdata[i, 3] / height, flowdata[i, 4] / height, speedup[i], TI[i]))
            f.close()

    if plot:
        # Transect A speed up and TI
        datafile = data_path + 'measurements/measurements_transectA.dat'
        modelfiles = []
        for j in range(len(turbmodels)):
            modelfiles.append(rans_path + 'Askervein_A_' + turbmodels[j] + '_wd' + str(int(wds[0])) + '_speedup_TI.dat')
        modellabels = [r'RANS $k$-$\varepsilon$', r'RANS $k$-$\varepsilon$-$f_P$']
        modelcolors = ['r', 'g']
        outfile = 'Askervein_A_speedup.' + plotfmt
        plotflowtransect(datafile, modelfiles, modellabels, modelcolors, outfile, height)
        outfile = 'Askervein_A_TI.' + plotfmt
        plotflowtransect(datafile, modelfiles, modellabels, modelcolors, outfile, height, modelivar=4, dataivar=2, ylabel='TI', ymin=0.0, ymax=0.25)
        # Speed up profile at hill top
        datafile = data_path + 'measurements/measurements_profile.dat'
        modelfiles = []
        for j in range(len(turbmodels)):
            modelfiles.append(rans_path + 'Askervein_prof_' + turbmodels[j] + '_wd' + str(int(wds[0])) + '_speedup.dat')
        modellabels = [r'RANS $k$-$\varepsilon$', r'RANS $k$-$\varepsilon$-$f_P$']
        modelcolors = ['r', 'g']
        outfile = 'Askervein_prof_speedup.' + plotfmt
        plotflowprofile(datafile, modelfiles, modellabels, modelcolors, outfile, height)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    run_Askervein(run_machine, queue, grid=True, run=True, post=True, netCDF_test=True, write=True, plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
