import numpy as np
from scipy import interpolate
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.style
matplotlib.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
matplotlib.use('Agg')


def plotContourExtracted(extractedfile, ix1, ix2, ivar, nx1, nx2, delta, outname='contour', ext='png'):
    cmap = plt.get_cmap('jet')
    flowdata = np.genfromtxt(extractedfile, skip_header=True)
    mins = np.zeros((3))
    maxs = np.zeros((3))
    ws = np.sqrt(flowdata[:, 5] ** 2 + flowdata[:, 6] ** 2)
    mins[0] = flowdata[:, ix1].min()
    mins[1] = flowdata[:, ix2].min()
    mins[2] = ws.min()
    maxs[0] = flowdata[:, ix1].max()
    maxs[1] = flowdata[:, ix2].max()
    maxs[2] = ws.max()
    vmin = np.floor(mins[2] / delta) * delta
    vmax = np.ceil(maxs[2] / delta) * delta
    ncontours = int((vmax - vmin) / delta + 1)
    levels = np.linspace(vmin, vmax, ncontours)
    fig, ax = plt.subplots(1, 1, sharey=False, figsize=(7.5, 5))
    ax.ticklabel_format(useOffset=False)
    ax.grid(True)
    x1int = np.linspace(mins[0], maxs[0], int(nx1))
    x2int = np.linspace(mins[1], maxs[1], int(nx2))
    x1 = flowdata[:, ix1].flatten()
    x2 = flowdata[:, ix2].flatten()
    var = ws.flatten()
    varint = interpolate.griddata((x1, x2), var, (x1int[None, :], x2int[:, None]), method='linear')
    cs = ax.contourf(x1int, x2int, varint, levels, cmap=cmap, vmax=vmax, vmin=vmin)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(cs, label='$U$ [m/s]', orientation='vertical', cax=cax)
    ax.set_xlim(mins[0], maxs[0])
    ax.set_ylim(mins[1], maxs[1])
    ax.set_xlabel('$x$ [km]')
    ax.set_ylabel('$y$ [km]')
    ax.set_aspect('equal')
    fig.savefig(outname + '.' + ext, dpi=300)
