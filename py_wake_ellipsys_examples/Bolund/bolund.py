import pytest
import numpy as np
import time
import os
import sys
import xarray
import time
from datetime import date
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.optimize import curve_fit

from py_wake.tests import npt
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.examples.data.hornsrev1 import Hornsrev1Site

import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


def flinear(x, a, b):
    return a * x + b


def loglawfit(z, U):
    popt, pcov = curve_fit(flinear, np.log(z), U)
    kappa = 0.4
    uStar = popt[0] * kappa
    z0 = np.exp(-popt[1] * kappa / uStar)
    return z0, uStar


def loglaw(z0, uStar, z):
    return uStar / 0.4 * np.log(z / z0)


def Uloglawfit(zdata, Udata, zInt):
    z0, uStar = loglawfit(zdata, Udata)
    print(z0, uStar)
    return loglaw(z0, uStar, zInt)


def run_Bolund(run_machine, queue, grid=True, run=True, write=True, post=True, plot=True, plotfmt='pdf', rans_path=''):
    from py_wake_ellipsys_examples.data.turbines.dummy_wt import Dummy
    from py_wake_ellipsys.wind_farm_models.ellipsys_lib.terraingrid import write_box_grd
    from py_wake_ellipsys_examples.plotwfres import plotflowtransect
    from py_wake_ellipsys_examples.Bolund.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Setup for calculating speed up factor at Bolund, see:
    # DOI 10.1007/s10546-011-9636-y
    # DOI 10.1007/s10546-011-9637-x
    # Bolund is a small hill with a height of 12 m and a very steep escarpment.

    # We need to use at least one wt to run the rans wind farm model for now.
    # Here the dummy wt has a rotor diameter of 12 m, which is used a scaling parameter
    # and equal to the hill height.
    wt = Dummy(D=12.0, zH=12.0)
    Dref = wt.diameter()
    domain_center = [-40.0, 0.0]
    hilltop_xy = [0.0, 0.0]
    wt_x = np.array([0.0]) + domain_center[0]
    wt_y = np.array([0.0]) + domain_center[1]
    height = 12.0  # Height to scale results
    type_i = np.array([0])
    h_i = wt.hub_height(type_i)

    m_mean = np.genfromtxt(data_path + 'measurements/Dir_239.dat', skip_header=True)
    m_std = np.genfromtxt(data_path + 'measurements/Dir_STD_239.dat', skip_header=True)

    os.system('tar -xzf ' + data_path + 'surface.tar.gz')
    grid_terrain_map_inner = os.getcwd() + '/Bolund.grd'
    grid_terrain_map_inner_z0 = os.getcwd() + '/Bolund_roughness.grd'

    # Extract terrain height over extraction line A
    nA = 251
    lineA = np.linspace(-100.0, 150.0, nA)
    deg = (270.0 - 239.0) * np.pi / 180.0
    xext = lineA * np.cos(deg)
    yext = lineA * np.sin(deg)
    zInt = np.array([2.0, 5.0])

    # Write background grd file with outer height
    zouter = 0.75
    z0Outer = 0.0003
    write_box_grd('height_outer.grd', zouter, -1e6, 1e6, -1e7, 1e7)
    write_box_grd('z0_outer.grd', z0Outer, -1e6, 1e6, -1e7, 1e7)
    grid_terrain_map_outer = os.getcwd() + '/height_outer.grd'
    grid_terrain_map_outer_z0 = os.getcwd() + '/z0_outer.grd'

    grid_wds = np.arange(0.0, 360.0, 10.0)

    wds = [239.0]
    Uref = 10.0
    zRef = 10.0
    z0Inlet = 0.0003
    kappa = 0.4
    cmu = 0.03
    TI = kappa * np.sqrt(2.0 / 3.0) / (cmu ** 0.25 * np.log((zRef + z0Inlet) / z0Inlet))
    today = date.today()

    maxnodes = 4
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='0000', run_pre=True)
    # Wind farm grid
    wfgrid = TerrainGrid(Dref, type='terrainogrid',
                         dwd=1.0, cells1_D=24.0, radius_D=5000.0 / Dref,
                         zFirstCell_D=0.05 / Dref, zdistr='waspcfd75', zWakeEnd_D=5.0, zlen_D=50.0,
                         cells2_D=6.0,
                         bsize=48,
                         terrain_h_grds=[grid_terrain_map_inner, grid_terrain_map_outer],
                         terrain_z0_grds=[grid_terrain_map_inner_z0, grid_terrain_map_outer_z0],
                         m1_e_D=5.0, m1_w_D=2.0,
                         m1_n_D=6.0, m1_s_D=5.0,
                         m2_w_D=10.0, m2_e_D=15.0,
                         m2_n_D=5.0, m2_s_D=5.0,
                         terrain_coarsening_levels=[0.25, 0.75],
                         terrain_surfgen='hypgridsf',
                         # Only generate surface grid for quick testing
                         # grid_terrain_surf_only=True,
                         # The split of the inner surface grids is made for x=0.4 such that the split is
                         # more aligned and close to the cliff:
                         terrain_hypsf_split='x',
                         terrain_hypsf_split_ratio=0.4,
                         # This is required for the hypgridsf to converge properly for the current case:
                         terrain_hypsf_divide=0.25,
                         terrain_hypsf_ghost=0.5,
                         terrain_hypsf_blendf=0.001,  # Blend factor need to be lowered for rotated inner grids
                         terrain_hyp3d_proj=False,  # For large and finer grids, we need to switch this to True
                         terrain_hyp3d_failstop=False)
    wfgrid.cluster.walltime = '0:30:00'
    # Wind farm run
    wfrun = WFRun(casename='Bolund', cluster=Cluster(walltime='0:59:00'),
                  write_restart=True)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   ad=ad, wfrun=wfrun, run_wd_con=False,
                   run_grid=grid, run_wf=run)

    # Make grid and run simulations
    # One could also use create_windfarm_grid() and run_windfarm() but then one should take care of grid_wd
    WS_eff_ilk, TI_effilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x, wt_y, h_i, type_i, wds, [Uref])

    if post:
        # Post process a mast positions and line A where the third coordinate is normal
        # Mast positions
        mast_points = m_mean[:, 3:6]
        nmast = len(mast_points)
        # Reference location M0 taken at 2 m and 5 m agl:
        xref = -181.3
        yref = -102.5
        points = np.zeros((nmast + 2 * nA + 2, 3))
        points[0:nmast, :] = mast_points
        points[nmast:nmast + nA, 0] = xext
        points[nmast:nmast + nA, 1] = yext
        points[nmast:nmast + nA, 2] = zInt[0]
        points[nmast + nA:nmast + nA * 2, 0] = xext
        points[nmast + nA:nmast + nA * 2, 1] = yext
        points[nmast + nA:nmast + nA * 2, 2] = zInt[1]
        points[-2, :] = np.array([xref, yref, zInt[0]])
        points[-1, :] = np.array([xref, yref, zInt[1]])
        wfm.post_windfarm_flow(wds, [Uref], precursor=True, points=points, points_z_as_normal=True)

    if write:
        # Plot Speedup factor over Line A
        # Check output file
        folder = wfm.get_name(grid_wd=wds[0])
        infile = '%s/post_precursor_flow_points_wd%g_ws%g/extracted.dat' % (folder, wds[0], Uref)
        print(infile)
        print('Wait for extracted.dat')
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()

        # Write measurements to a file in a simpler format
        flowdata = np.genfromtxt(infile, skip_header=True)
        # Mast positions
        nmast = len(m_mean)

        mU = np.zeros((5, 2))
        point_per_mast = [0, 2, 3, 5, 3, 3]
        m_z = m_mean[:, 5] - m_mean[:, 6]
        # It is unclear which uStar is used to normalize the data, we use the normalized wind speed for now
        uStar0 = 0.3559
        # m_ws = m_mean[:, 8] * uStar0
        # m_ws = m_mean[:, 8] * m_mean[:, 7]
        m_ws = m_mean[:, 8]

        # Obtain reference wind speed by loglaw fit from reference mast M0.
        i1 = 0
        i2 = 2
        m0U = Uloglawfit(m_z[i1:i2], m_ws[i1:i2] * uStar0, zInt) / uStar0

        i_z5 = [3, 8, 11, 14]
        i_z2 = [2, 6, 10, 13]
        mSpeedUp = np.array([(m_ws[i_z5] - m0U[0]) / m0U[0], (m_ws[i_z2] - m0U[0]) / m0U[0]])
        print('5 m', m_z[i_z5])
        print('2 m', m_z[i_z2])
        # The speedup factor at M2 for z = 5m seems to be different compared the published article (Fig, 4 in DOI 10.1007/s10546-011-9637-x)
        print(mSpeedUp)

        # Mast distance from reference point
        deg_ext = 270.0 - np.arctan((m_mean[0, 4] - m_mean[15, 4]) / (m_mean[0, 3] - m_mean[15, 3])) * 180.0 / np.pi
        deg = (270.0 - deg_ext) * np.pi / 180.0
        xrot = (m_mean[0:16, 3] - hilltop_xy[0]) * np.cos(deg) + (m_mean[0:16, 4] - hilltop_xy[1]) * np.sin(deg)
        m_xrot = np.array([xrot[0], xrot[2], xrot[5], xrot[10], xrot[13]])

        for i in range(2):
            if i == 1:
                # 2 m
                Unorm = np.sqrt(flowdata[-2, 5] ** 2 + flowdata[-2, 6] ** 2)
                i1 = nmast
                i2 = nmast + nA
                k = 0
            elif i == 0:
                # 5 m
                Unorm = np.sqrt(flowdata[-1, 5] ** 2 + flowdata[-1, 6] ** 2)
                i1 = nmast + nA
                i2 = nmast + nA * 2
                k = 1
            deg_ext = 270.0 - np.arctan((flowdata[i1, 2] - flowdata[i2 - 1, 2]) / (flowdata[i1, 1] - flowdata[i2 - 1, 1])) * 180.0 / np.pi
            deg = (270.0 - deg_ext) * np.pi / 180.0
            xrot = (flowdata[i1:i2, 1] - hilltop_xy[0]) * np.cos(deg) + (flowdata[i1:i2, 2] - hilltop_xy[1]) * np.sin(deg)
            speedup = (np.sqrt(flowdata[i1:i2, 5] ** 2 + flowdata[i1:i2, 6] ** 2) - Unorm) / Unorm
            TI = np.sqrt(2.0 / 3.0 * flowdata[i1:i2, 10]) / Unorm
            z = flowdata[i1:i2, 3]

            f = open('Bolund_A_z' + str(int(zInt[k])) + '_wd' + str(int(wds[0])) + '_speedup_TI.dat', 'w')
            f.write('# EllipSys3D-RANS, k-epsilon-fP\n')
            f.write('# Rerun date=%s\n' % today)
            f.write('# H = %g m\n' % height)
            f.write('# Distance from hill top: x/H, height: z/H, height above terrain: normal/H, speed up factor,  local TI\n')
            for j in range(speedup.size):
                f.write('%8.6f %8.6f %8.6f %8.6f %8.6f\n' % (xrot[j] / height, z[j] / height, zInt[k] / height, speedup[j], TI[j]))
            f.close()

            f = open('Bolund_data_A_z' + str(int(zInt[k])) + '_wd' + str(int(wds[0])) + '_speedup.dat', 'w')
            f.write('# H = %g m\n' % height)
            f.write('# Distance from hill top: x/H, speed up factor\n')
            for j in range(0, 4):
                f.write('%8.6f %8.6f\n' % (m_xrot[j + 1] / height, mSpeedUp[i, j]))
            f.close()

    if plot:
        # Plot Speedup factor over Line A for two different heights above terrain
        for i in range(2):
            datafile = data_path + 'measurements/post/Bolund_data_A_z' + str(int(zInt[i])) + '_wd' + str(int(wds[0])) + '_speedup.dat'
            modelfiles = []
            modelfiles.append(rans_path + 'Bolund_A_z' + str(int(zInt[i])) + '_wd' + str(int(wds[0])) + '_speedup_TI.dat')
            modellabels = ['RANS']
            modelcolors = ['g']
            outfile = 'Bolund_A_z' + str(int(zInt[i])) + '_speedup.' + plotfmt
            plotflowtransect(datafile, modelfiles, modellabels, modelcolors, outfile, height)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    # queue = 'windq'
    starttime = time.time()
    run_Bolund(run_machine, queue, grid=True, run=True, post=True, write=True, plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
