import numpy as np
import sys
import os
import time
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysWTs


def run_NystedRoedsand2(run_machine, queue, coarse=False, grid=False, prelib=False, pre=False, cal=False,
                        run=False, write=False, plot=False, plotfmt='pdf', rans_path=''):
    from py_wake.examples.data.hornsrev1 import wt_x, wt_y, Hornsrev1Site
    from py_wake_ellipsys_examples.data.farms.nysted import wt_x as n_wt_x
    from py_wake_ellipsys_examples.data.farms.nysted import wt_y as n_wt_y
    from py_wake_ellipsys_examples.data.farms.roedsand2 import wt_x as r_wt_x
    from py_wake_ellipsys_examples.data.farms.roedsand2 import wt_y as r_wt_y
    from py_wake_ellipsys_examples.data.turbines.bonus2p3 import Bonus_2p3
    from py_wake_ellipsys_examples.data.turbines.swt2p3_93 import SWT2p3_93
    from py_wake.validation.validation_lib import GaussianFilter
    from py_wake_ellipsys_examples.plotwfres import plotWFrow, plotWFeffrose
    from py_wake_ellipsys.wind_farm_models.ellipsys_lib.terraingrid import write_box_grd
    from py_wake_ellipsys_examples.NystedRoedsand2.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Nysted-Roedsand2 double wind farm simulations, based on DOI: 10.1088/1742-6596/625/1/012026
    # The grid is similar by selecting a z-distribution with a uniform
    # spaced region in the wake domain. The newer z-distribution is more
    # smooth, but requires more cells. The height of the domain has been
    # increased from 10D to 25D, which changes the downstream wind turbine
    # power. A flat terrian o-grid is used by selecting grid_type='terrainogrid'
    # with a flat terrain input .grd file and wind directions are simulated by
    # changing the inflow rather than rotating the wind farm layout. The o-grid
    # results in a much smaller grid  compared to using a Cartesian grid where
    # the wind farm layout is rotated to simulate wind directions. A disadvantage
    # of our chosen setup it that we cannot save iterations by running consecutive
    # wind directions.
    wt_x = r_wt_x + n_wt_x
    wt_y = r_wt_y + n_wt_y
    nWT_n = len(n_wt_x)
    nWT_r = len(r_wt_x)
    type_i = np.array([1] * nWT_r + [0] * nWT_n)
    nWT = nWT_n + nWT_r
    bonus_2p3 = Bonus_2p3()
    swt2p3_93 = SWT2p3_93(zH=68.5)
    wt = EllipSysWTs.from_WindTurbines([bonus_2p3, swt2p3_93])
    TI = 0.07  # TI based on TKE ~ 0.8 * TI_u = sigma_U/U
    zRef = 68.5
    Dref = 82.4
    # A single flow case
    # wd = [97.0]
    # A range of wind direction to perform Gaussian averaging
    wd = np.arange(17.0, 152.0, 5.0)
    ws = [8.0]
    sigma = 5.0  # Estimated wind direction uncertainty [deg]
    # Old z distrubution, which saves cells but has larger expansion ratios at the start and end of the wake domain
    zdistr = 'uniwake'
    # Old results were made with zlen_D=10, which causes some additional artificial blockage
    zlen_D = 25.0
    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        e3d_reslim = 1e-2
        maxit = 10
        tolOpt = 1e-1
        grid_cells1_D = 2.0
        adgrid_nr = 5
        grid_bsize = 48
        grid_bsize_cal = 48
        maxnodes = 3
        walltime_wf_run = '1:00:00'
    else:
        e3d_reslim = 1e-5
        maxit = 50
        tolOpt = 1e-3
        grid_cells1_D = 8.0
        adgrid_nr = 10
        grid_bsize_cal = 32
        grid_bsize = 64
        maxnodes = 18
        walltime_wf_run = '36:00:00'
    linewidth = 1.5
    cRANS = 'g'
    today = date.today()
    # Run Roedsand2 without and with Nysted to calculate the effect of Nysted on Roedsand2
    cases = ['Roedsand2', 'NystedRoedsand2']
    # Run three turbulence models to show the effect of ABL height and wind veer
    turb_models = ['kefP', 'keABLplfP', 'keABLclfP']
    # For ABL models
    fcori = 2.0 * 7.292115 * 10 ** (-5) * np.sin(54.5 / 180.0 * np.pi)
    z0 = 0.007  # As used in paper for ABL model with Coriolis
    # For terainogrid without terrain
    write_box_grd('flatterrain.grd', 0.0, -1e6, 1e6, -1e7, 1e7)

    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='1001', grid=ADGrid(nr=adgrid_nr, ntheta=32))
    # Wind farm grid
    wfgrid = TerrainGrid(Dref, type='terrainogrid', zFirstCell_D=0.5 / Dref,
                         radius_D=5e4 / Dref, cells1_D=grid_cells1_D,
                         zlen_D=zlen_D, zdistr=zdistr, bsize=grid_bsize,
                         # dwd=90.0,  # To reduce the wind farm grid
                         # We use grid_wd=90, so all margins are rotated CW by 180 deg
                         m1_w_D=5.0, m1_e_D=20.0,  # For eastern wds
                         m1_n_D=20.0, m1_s_D=18.0,  # North/South margins should also resolve wakes for NE and NS wds
                         terrain_h_grds=[os.getcwd() + '/flatterrain.grd'],
                         terrain_surfgen='sfhill',
                         terrain_3dgen='extrude3d',
                         cluster=Cluster(walltime='1:00:00'))
    # Wind farm run
    wfrun = WFRun(casename=cases[0], cluster=Cluster(walltime=walltime_wf_run))
    # Constants
    cnst = Cnst(fcori=fcori, z0=z0)
    # ABL inflow precursor optimizer
    preopt = PreOpt(grid=PrecursorGrid1D(), maxit=maxit, tolOpt=tolOpt,
                    cluster=Cluster(walltime='0:30:00'))
    e3d = E3D(reslim=e3d_reslim)
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   cnst=cnst, ad=ad, pre=preopt, e3d=e3d, wfrun=wfrun,
                   run_wd_con=False, run_ws_con=False)
    wfm.calrun.cluster.walltime = '2:00:00'
    # Run step by step:
    if grid:
        # Create precursor grid
        wfm.create_precursor_grid()
        wfm.create_precursorlib_grid()
        # Create AD grid
        for type in np.unique(type_i):
            print(type)
            wfm.create_adgrid(type)
        # Create calibration grid (use smaller block size to speed up calibration)
        for type in np.unique(type_i):
            wfm.create_calibration_grid(type)
        # Create wind farm grids
        for case in cases:
            wfm.wfrun.casename = case
            if case == 'Roedsand2':
                wfm.windTurbines = SWT2p3_93(zH=68.5)
                wfm.create_windfarm_grid(r_wt_x, r_wt_y)
            elif case == 'NystedRoedsand2':
                wfm.windTurbines = EllipSysWTs.from_WindTurbines([bonus_2p3, swt2p3_93])
                wfm.create_windfarm_grid(wt_x, wt_y, make_grid=True)

    if prelib:
        # Inflow precursor for ABL models
        # Create precursor lib
        Ro0 = 10**np.arange(5.0, 10.2, 0.2)
        Rol = 10**np.append(np.arange(2.0, 3.5, 0.1), np.arange(3.5, 4.7, 0.05))
        wfm.set_subattr('e3d.turbmodel', 'keABLclfP')
        wfm.run_precursor_lib(Ro0, Rol, 'cori')
        Ro0_tilde = 10**np.arange(5.0, 10.2, 0.2)
        Rol_tilde = 10**np.append(np.arange(2.0, 3.5, 0.1), np.arange(3.5, 4.7, 0.05))
        wfm.set_subattr('e3d.turbmodel', 'keABLplfP')
        wfm.run_precursor_lib(Ro0_tilde, Rol_tilde, 'press')

    if pre:
        # We need an inflow profile for each wind speed in the force calibration run
        ws_pre = np.arange(4.0, 11.0, 1.0)
        wfm.set_subattr('e3d.turbmodel', 'keABLclfP')
        wfm.run_precursor_opt(ws_pre)
        wfm.set_subattr('e3d.turbmodel', 'keABLplfP')
        wfm.run_precursor_opt(ws_pre)

    if cal:
        # Run force control calibration
        # Set lower cut out wind speed for faster force calibration simulations using an ABL inlow.
        # This is possible because we only interested in a 8.0 m/s flow case.
        windTurbines = wfm.windTurbines
        windTurbines.cutout = [10.0, 10.0]
        wfm.windTurbines = windTurbines
        for turb_model in turb_models:
            wfm.set_subattr('e3d.turbmodel', turb_model)
            if turb_model == 'kefP':
                wfm.run_ws_con = True
                wfm.pre = None
            else:
                wfm.run_ws_con = False
                wfm.pre = preopt
            for type in np.unique(type_i):
                wfm.run_calibration(type)
        windTurbines.cutout = [25.0, 25.0]
        wfm.windTurbines = windTurbines

    if run:
        # Run flow cases
        for turb_model in turb_models:
            wfm.set_subattr('e3d.turbmodel', turb_model)
            if turb_model == 'kefP':
                wfm.pre = None
            else:
                wfm.pre = preopt
            for case in cases:
                wfm.wfrun.casename = case
                if case == 'Roedsand2':
                    wfm.windTurbines = SWT2p3_93(zH=68.5)
                    wfm.run_windfarm(r_wt_x, r_wt_y, wd, ws, np.zeros(nWT_r))
                elif case == 'NystedRoedsand2':
                    wfm.windTurbines = EllipSysWTs.from_WindTurbines([bonus_2p3, swt2p3_93])
                    wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)

    if write:
        # Write results of wind farm efficiency of Roedsand2 with and without Nysted
        for n in range(len(turb_models)):
            wfm.set_subattr('e3d.turbmodel', turb_models[n])
            for m in range(len(cases)):
                wfm.wfrun.casename = cases[m]
                WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
                # Gaussian averaging
                powerGA = np.zeros(power_ilk.shape)
                for i in range(nWT_r):
                    powerGA[i, :, 0] = GaussianFilter(power_ilk[i, :, 0], wd, 3, sigma)

                # Output
                filename = cases[m] + '_' + turb_models[n] + '_RANS_WFeff.dat'
                f = open(filename, 'w')
                f.write('# EllipSys3D-RANS-AD, k-epsilon-fP\n')
                f.write('# Based on DOI: 10.1002/we.1804, rerun date=%s\n' % today)
                f.write('# GA with sigma=%g deg\n' % sigma)
                f.write('#       Efficiency=sum[P(wd)]/sum[P_noWake(wd)]\n')
                f.write('# wdir, no GA, GA\n')
                # Wind farm power without wakes
                WFP0 = wt.power(ws, type=1)[0] * nWT_r
                print('WFP0', WFP0)
                for l in range(3, wd.size - 3):
                    WFP = power_ilk[0:nWT_r, l, 0].sum()
                    print(l, WFP)
                    WFPGA = powerGA[0:nWT_r, l, 0].sum()
                    f.write('%5.1f %8.6f %8.6f\n' % (wd[l], WFP / WFP0, WFPGA / WFP0))
                f.close()

    if plot:
        datafile = data_path + 'measurements/NystedRoedsand2_WFdata_WFeff.dat'
        modelfiles = []
        modellabels = []
        # casenames = ['Roedsand2', 'Roedsand2 + Nysted']
        casenames = ['R2', 'R2 + N']
        turbnames = ['ASL', 'ABLp', 'ABLc']
        for n in range(len(turb_models)):
            for m in range(len(casenames)):
                filename = rans_path + cases[m] + '_' + turb_models[n] + '_RANS_WFeff.dat'
                modelfiles.append(filename)
                modellabels.append(casenames[m] + ' ' + turbnames[n])
        modells = ['--g', '-g', '--r', '-r', '--b', '-b']
        # Plot WF efficiency in a polar plot including wf layout
        outfile = 'Roedsand2_WFeff_polar.' + plotfmt
        plotWFeffrose(datafile, modelfiles, modellabels, modells, modells, outfile, wt_x, wt_y, wt.diameter(1), layoutscale=0.2, fullrose=False, plotGAonly=True, WFeffmin=0.2, WFeffmax=0.9)
        # Plot WF efficiency in a rectangular plot without wf layout
        outfile = 'Roedsand2_WFeff_rect.' + plotfmt
        plotWFeffrose(datafile, modelfiles, modellabels, modells, modells, outfile, wt_x, wt_y, wt.diameter(1), fullrose=False, polar=False, plotGAonly=True, WFeffmin=0.4, WFeffmax=1.0, figx=10.0, figy=5.0)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Run with a very coarse setup if coarse=True (which still takes an hour to run).
    run_NystedRoedsand2(run_machine, queue,
                        coarse=True,
                        grid=True,
                        prelib=True,
                        pre=True,
                        cal=True,
                        run=True,
                        write=True,
                        plot=True,
                        plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
