import numpy as np
import time
import os
import sys
import shutil
import xarray
import datetime
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.examples.data.hornsrev1 import Hornsrev1Site
from py_wake_ellipsys_examples.data.turbines.v27swift import V27SWIFT
from py_wake_ellipsys_examples.SWiFT_wakebench.lidar import SWiFTlidar_path
from py_wake_ellipsys_examples.plotwfres import plotSingleWake, get_downstream_label


def run_SWiFTuni_AirF(run_machine, queue, coarse=False, grid=False, run=False, post=False):
    UH = 7.0
    TI = 0.0
    Re = 1e6
    D = 27.0
    costilt = np.cos(4.05 / 180.0 * np.pi)
    sintilt = np.sin(4.05 / 180.0 * np.pi)
    viscosity = 1.225 * D * UH / Re
    wt = V27SWIFT()
    wt_x = [0.0]
    wt_y = [0.0]

    if coarse:
        cD = 5.0
        nr = 32
        ntheta = 64
    else:
        cD = 20.0
        nr = 95
        ntheta = 180

    maxnodes = 2
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    cnst = Cnst(mu=viscosity)
    e3d = E3D(turbmodel='koSST', reslim=1e-7, relaxu=0.6)
    ad = AD(force='3110', grid=ADGrid(nr=nr, ntheta=ntheta), adres_out=True,
            airfoil_tipcor='shen_pirrung', airfoil_tipcor_shen_c2=29)
    wfgrid = FlatBoxGrid(D, type='uniform', zFirstCell_D=0.5 / D, radius_D=10.0, cells1_D=cD,
                         zlen_D=10.0, m1_w_D=3, m1_e_D=15, m1_s_D=1.5, m1_n_D=1.5)
    wfrun = WFRun(casename='SWiFTuni', write_restart=True, cluster=Cluster(walltime='1:00:00'))
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, 32.1,
                   cnst=cnst, ad=ad, e3d=e3d, wfrun=wfrun,
                   run_ws_con=False)
    # Run step by step:
    type_i = np.array([0])
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)
    if run:
        # Run flow case
        wfm.run_windfarm(wt_x, wt_y, [270.0], [UH], type_i, tilt_ilk=4.05)
    if post:
        WS_eff_ilk, power_ilk, ct_ilk, *dummy = wfm.post_windfarm()
        folder = wfm.get_name() + '/wdCon_ws%g' % UH
        ###
        # Check aerodynamic performance
        ###
        os.chdir(folder)
        os.system('tail -1 ADres.dat > ADres_last.dat')
        data = np.genfromtxt('ADres_last.dat')
        # Calculate thrust coefficient normal to rotor plane (including tilt)
        Fn_x = float(data[12])
        Fn_z = float(data[14])
        Fn = Fn_x * costilt + Fn_z * sintilt
        rpm = float(data[17])
        pitch = float(data[18])
        omega = rpm / 30.0 * np.pi
        print(power_ilk[0][0][0], Fn, rpm, pitch)
        UHn = UH * costilt
        print('Cp:', power_ilk[0][0][0] / (0.5 * 1.225 * 27.0 ** 2 * np.pi * 0.25 * (UHn) ** 3))
        print('Ct:', -Fn / (0.5 * 1.225 * 27.0 ** 2 * np.pi * 0.25 * (UHn) ** 2))
        os.chdir('../')


def run_SWiFT(run_machine, queue, coarse=False, grid=False, prelib=False, pre=False,
              run=False, post=False, write=False, plot=False, plotfmt='pdf', rans_path='SWiFT/'):
    from py_wake_ellipsys_examples.SWiFT_wakebench.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    D = 27.0
    zRef = 32.1
    turbmodels = ['keMOfP', 'keABLclfP']
    nturb = len(turbmodels)
    cases = ['neutral', 'stable', 'unstable']
    ncases = len(cases)
    ad_forces = ['0000', '3110']
    nadfor = len(ad_forces)
    TIs = [0.088, 0.029, 0.1]
    UHs = [8.7, 4.8, 6.6]
    alphas = [0.14, 0.5, 0.14]
    # For keMOfP:
    zetas = [0.0, 32.1 / 13.5, 32.1 / -112.0]
    # For keABLcfP:
    z0s = [0.01, 0.01, 0.02]
    fcori = 8.07079e-5

    wt_x = [0.0]
    wt_y = [0.0]

    # Output planes
    xDown = [-2.5 * D, 1 * D, 2 * D, 3 * D, 4 * D, 5 * D, 6 * D, 7 * D, 8 * D]
    xDownString = ['-2.5', '1', '2', '3', '4', '5', '6', '7', '8']
    ny = 81
    nz = 80

    if coarse:
        cD = 5.0
        nr = 32
        ntheta = 64
        e3d_reslim = 1e-2
        e1d_maxit = 10
        e1d_tolOpt = 1e-1
    else:
        cD = 20.0
        nr = 95
        ntheta = 180
        e3d_reslim = 1e-6
        e1d_maxit = 50
        e1d_tolOpt = 1e-2

    wt = V27SWIFT()

    maxnodes = 4
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    cnst = Cnst(fcori=fcori)
    e3d = E3D(turbmodel='koSST', reslim=e3d_reslim)
    wfgrid = FlatBoxGrid(D, zFirstCell_D=0.5 / D, radius_D=10.0, cells1_D=cD,
                         zlen_D=10.0, m1_w_D=3, m1_e_D=15, m1_s_D=1.5, m1_n_D=1.5)
    ad = AD(force=ad_forces[1], grid=ADGrid(nr=nr, ntheta=ntheta), adres_out=True,
            airfoil_tipcor='shen_pirrung', airfoil_tipcor_shen_c2=29)
    preopt = PreOpt(maxit=e1d_maxit, tolOpt=e1d_tolOpt, grid=PrecursorGrid1D())
    wfrun = WFRun(casename='SWiFT', write_restart=True, cluster=Cluster(walltime='0:20:00'))
    Cluster.maxnodes = 4
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TIs[0], zRef,
                   cnst=cnst, ad=ad, e3d=e3d, pre=preopt, wfrun=wfrun,
                   run_ws_con=False)

    # Run step by step:
    type_i = np.array([0])
    if grid:
        # Create precursor grids
        wfm.create_precursorlib_grid()
        wfm.create_precursor_grid()
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create wind farm grid
        wfm.create_windfarm_grid(wt_x, wt_y)

    if prelib:
        # Inflow precursor for ABL models
        # Create precursor lib
        Ro0 = 10**np.linspace(6.5, 7.7, 7)
        Rol = 10**np.linspace(2.5, 4.5, 21)
        e3d.turbmodel = 'keABLclfP'
        wfm.e3d = e3d
        wfm.run_precursor_lib(Ro0, Rol, 'cori')

    if pre:
        # We need an inflow profile for each wind speed case
        e3d.turbmodel = 'keABLclfP'
        wfm.e3d = e3d
        # Run an optimization for each case:
        for j in range(ncases):
            wfm.TI = TIs[j]
            cnst.z0 = z0s[j]
            wfm.cnst = cnst
            wfm.run_precursor_opt([UHs[j]])

    if run:
        # Run flow cases
        for i in range(len(turbmodels)):
            e3d.turbmodel = turbmodels[i]
            wfm.e3d = e3d
            for j in range(ncases):
                if turbmodels[i] == 'keMOfP':
                    wfm.pre = None
                    wfm.zeta = zetas[j]
                elif turbmodels[i] == 'keABLclfP':
                    wfm.zeta = 0.0
                    cnst.z0 = z0s[j]
                    wfm.cnst = cnst
                    wfm.pre = preopt
                wfm.TI = TIs[j]
                for k in range(nadfor):
                    ad.force = ad_forces[k]
                    wfm.ad = ad
                    wfm.run_windfarm(wt_x, wt_y, [270.0], [UHs[j]], type_i, tilt_ilk=4.05)

    if post:
        # Post wake planes
        ny = 81
        nz = 80
        delta = D / 20.0
        xyz = np.zeros((ny * nz * len(xDown), 3))
        k = 0
        for x in xDown:
            for i in range(0, ny):
                y = -2.0 * D + i * delta + wt_y[0]
                for j in range(0, nz):
                    z = delta * (j + 1)
                    xyz[k, 0] = x + wt_x[0]
                    xyz[k, 1] = y
                    xyz[k, 2] = z
                    k = k + 1
        for i in range(len(turbmodels)):
            e3d.turbmodel = turbmodels[i]
            wfm.e3d = e3d
            for j in range(ncases):
                wfm.TI = TIs[j]
                if turbmodels[i] == 'keMOfP':
                    wfm.zeta = zetas[j]
                else:
                    wfm.zeta = 0.0
                for k in range(nadfor):
                    ad.force = ad_forces[k]
                    wfm.ad = ad
                    wfm.post_windfarm_flow([270.0], [UHs[j]], points=xyz)

    if write:
        date = datetime.datetime.today().strftime('%Y%m%d')
        # Power loss factor based on OpenFAST
        lossfactor = 0.887
        denfactor = 1.0640320278 / 1.225
        A = D ** 2 * 0.25 * np.pi
        costilt = np.cos(4.05 / 180.0 * np.pi)
        sintilt = np.sin(4.05 / 180.0 * np.pi)
        models = ['DTU_VANDERLAAN_RANS-MOST', 'DTU_VANDERLAAN_RANS-ABL']
        outcases = ['neutral_evolution', 'stable_evolution', 'unstable_dynamics']
        # Results for docs
        folder = 'SWiFT'
        if os.path.exists(folder):
            # Remove dir
            shutil.rmtree(folder)
        os.mkdir(folder)
        # Results for wakebench
        folder = 'results'
        if os.path.exists(folder):
            # Remove dir
            shutil.rmtree(folder)
        os.mkdir(folder)
        os.chdir(folder)
        for case in outcases:
            os.mkdir(case)
        os.chdir('../')
        yi = np.linspace(-2, 2, ny)
        zi = np.linspace(1.0 / 20.0, 4, nz)
        for i in range(ncases):
            TI = TIs[i]
            UH = UHs[i]
            case = cases[i]
            outcase = outcases[i]
            for j in range(nturb):
                if turbmodels[j] == 'keMOfP':
                    zeta = zetas[i]
                else:
                    zeta = 0.0

                ad.force = '3110'
                wfm.ad = ad
                e3d.turbmodel = turbmodels[j]
                wfm.e3d = e3d
                wfm.TI = TI
                wfm.zeta = zeta
                folder = wfm.get_name() + '/wdCon_ws%g' % UH
                ###
                # Write aero file
                ###
                os.chdir(folder)
                os.system('tail -1 ADres.dat > ADres_last.dat')
                data = np.genfromtxt('ADres_last.dat')
                Fn_x = float(data[12])
                Fn_z = float(data[14])
                Fn = Fn_x * costilt + Fn_z * sintilt
                P = float(data[16])
                rpm = float(data[17])
                pitch = float(data[18])
                omega = rpm / 30.0 * np.pi
                print('RANS: Pgen', P * 0.001 * lossfactor * denfactor, 'RPM', rpm, 'Fn', Fn, 'pitch', pitch)
                if i == 0:
                    # Neutral
                    print('Data: Pgen=', 79.07, 'RPM', 42.76)
                elif i == 1:
                    # Stable
                    print('Data: Pgen=', 13.28, 'RPM', 25.02)
                elif i == 2:
                    # Unstable
                    print('Data: Pgen=', 49.09, 'RPM', 35.88)
                os.chdir('../../')
                # Write to aerodata to file
                outfile = 'results/' + outcase + '/' + models[j] + '_' + date + '_wtg_response_steady_state.txt'
                os.system('cp ' + SWiFTlidar_path + '../templates/wtg_response_steady_state.txt ' + outfile)
                f = open(outfile, 'a')
                f.write('hub_wind_speed_[m_s-1],%2.1f\n' % UH)
                f.write('rotor_power_[kW],%8.5f\n' % (P * 0.001 * denfactor))
                f.write('rotor_torque_[N_m],%6.2f\n' % (P / omega * denfactor))
                f.write('rotor_speed_[rpm],%8.6f\n' % rpm)
                f.write('blade_pitch_[deg],%8.6f\n' % pitch)
                f.write('blade_root_flap_moment_[N_m],NaN\n')
                f.write('blade_root_edge_moment_[N_m],NaN\n')
                f.write('generator_power_[kW],%8.5f\n' % (P * 0.001 * lossfactor * denfactor))
                f.write('generator_torque_[N_m],NaN\n')
                f.write('aero_thrust_force_[N],%6.2f\n' % (-Fn * denfactor))
                f.write('aero_thrust_coefficient_[-],%8.6f\n' % (-Fn / (0.5 * 1.225 * A * (UH * costilt) ** 2)))
                f.write('total_thrust_force_[N],%6.2f\n' % (-Fn * denfactor))
                f.write('###############################################################################\n')
                f.write('# Please add more lines if you want to provide other variables.\n')
                f.write('###############################################################################\n')
                f.close()
                ###
                # Write wake planes
                ###
                infile = folder + '/../post_flow_points_wd270_ws%g/extracted.dat' % UHs[i]
                print('Wait for extracted.dat')
                while not os.path.exists('%s/../post_flow_points_wd270_ws%g/extracted.dat' % (folder, UHs[i])):
                    time.sleep(5)
                    sys.stdout.write('.', )
                    sys.stdout.flush()
                outpath = 'results/' + outcase + '/'
                extract_wake_to_netcfd(models[j], infile, outpath, wt=1)
                # NoAD cases, save one plane at x=-2,5D
                ad.force = '0000'
                wfm.ad = ad
                folder2 = wfm.get_name()
                infile = '%s/post_flow_points_wd270_ws%g/extracted.dat' % (folder2, UHs[i])
                print('Wait for extracted.dat')
                while not os.path.exists('%s/post_flow_points_wd270_ws%g/extracted.dat' % (folder2, UHs[i])):
                    time.sleep(5)
                    sys.stdout.write('.', )
                    sys.stdout.flush()
                outpath = 'results/' + outcase + '/'
                extract_wake_to_netcfd(models[j], infile, outpath, wt=0)

                # Write wake deficit results at hub height and as function of height
                outpath = 'results/' + outcase + '/'
                infile = outpath + models[j] + "_" + date + "_uvw_" + xDownString[0] + "D_2D.nc"
                data = xarray.open_dataset(infile)
                UMin2p5D = np.copy(data['U'])
                for ip in range(0, len(xDownString)):
                    infile = outpath + models[j] + "_" + date + "_uvw_" + xDownString[ip] + "D_2D.nc"
                    data = xarray.open_dataset(infile)
                    zm, ym = np.meshgrid(data['z'] / D, data['y'] / D)
                    y = ym.flatten()
                    z = zm.flatten()
                    U = (data['U'] - UMin2p5D) / UMin2p5D
                    if xDownString[ip] in ['2', '3', '4', '5']:
                        WakeU_y = U.interp(y=yi * D, z=32.1)[0, 0]
                        label = get_downstream_label(xDownString[ip])
                        filename = 'SWiFT/SWiFT_' + case + '_RANS_' + turbmodels[j] + '_hub_' + label + 'D.dat'
                        f = open(filename, 'w')
                        f.write('# EllipSys3D-RANS, %s\n' % turbmodels[j])
                        f.write('# Based on DOI: 10.1002/we.2543, rerun date=%s\n' % date)
                        f.write('# Cross distance y/D, (U-Umin2p5)/Umin2p5\n')
                        for k in range(ny):
                            f.write('%8.6f %8.6f\n' % (yi[k], WakeU_y[k]))
                        f.close()
                        # Vertical line
                        WakeU_z = U.interp(y=0.0, z=zi * D)[0, 0]
                        filename = 'SWiFT/SWiFT_' + case + '_RANS_' + turbmodels[j] + '_vert_' + label + 'D.dat'
                        f = open(filename, 'w')
                        f.write('# EllipSys3D-RANS, %s\n' % turbmodels[j])
                        f.write('# Based on DOI: 10.1002/we.2543, rerun date=%s\n' % date)
                        f.write('# Vertical distance (z-zH)/D, (U-Umin2p5)/Umin2p5\n')
                        for k in range(nz):
                            f.write('%8.6f %8.6f\n' % ((zi[k] * D - 32.1) / D, WakeU_z[k]))
                        f.close()
    if plot:
        sDown_D = np.array([2, 3, 4, 5])
        Nbins = [6, 6, 5]
        # plot wake deficit at hub height
        turbmodel1 = turbmodels[0]
        turbmodel2 = turbmodels[1]
        for i in range(ncases):
            datafiles = []
            modelfiles = []
            modelfilesdown1 = []
            modelfilesdown2 = []
            for j in range(sDown_D.size):
                label = get_downstream_label(sDown_D[j])
                if (i != 2) or (i == 2 and j == 1):
                    lidarfile = SWiFTlidar_path + cases[i] + '_' + str(float(sDown_D[j])) + 'D_ffor_lateral_vd.csv'
                    datafiles.append(lidarfile)
                else:
                    datafiles.append('')
                filename = rans_path + 'SWiFT_' + cases[i] + '_RANS_' + turbmodel1 + '_hub_' + label + 'D.dat'
                modelfilesdown1.append(filename)
                filename = rans_path + 'SWiFT_' + cases[i] + '_RANS_' + turbmodel2 + '_hub_' + label + 'D.dat'
                modelfilesdown2.append(filename)
            modelfiles.append(modelfilesdown1)
            modelfiles.append(modelfilesdown2)
            modellabels = ['RANS MO', 'RANS ABL']
            modelcolors = ['g', 'b']
            outfile = 'SWiFT_' + cases[i] + '_hub' + '.' + plotfmt
            plotSingleWake(modelfiles, modellabels, modelcolors, sDown_D, outfile, xaxis='y', norm=1,
                           datafiles=datafiles, dataerrorbar=True, dataNbins=1, dataiUstd=4,
                           ylabel=r'$(U-U_0)/U_0$', ylabelrot=90, datadelimiter=',')


def extract_wake_to_netcfd(participant_id, infile, outpath, wt=1):
    date = datetime.datetime.today().strftime('%Y%m%d')

    # Load data
    extracted = np.genfromtxt(infile, skip_header=True)

    D = 27.0
    nx = 9
    ny = 81
    nz = 80
    nt = 1

    xAD = 0.0
    yAD = 0.0

    y_1d = np.linspace(-2 * D, 2 * D, ny)
    z_1d = np.linspace(D / 20.0, 4 * D, nz)
    t_1d = np.zeros((nt))

    # Reshape
    iu = 5
    iv = 6
    iw = 7
    itke = 10
    idtke = 11
    U = extracted[:, iu].reshape((nt, nx, ny, nz))
    V = extracted[:, iv].reshape((nt, nx, ny, nz))
    W = extracted[:, iw].reshape((nt, nx, ny, nz))
    TKE = extracted[:, itke].reshape((nt, nx, ny, nz))
    DTKE = extracted[:, idtke].reshape((nt, nx, ny, nz))

    # print("Shape of U is {0}".format(U.shape))
    # print("Shape of V is {0}".format(V.shape))
    # print("Shape of W is {0}".format(W.shape))

    # Use xarray to create DataArrays, then Dataset
    xDownString = ['-2.5', '1', '2', '3', '4', '5', '6', '7', '8']
    xDown = [-2.5 * D, 1 * D, 2 * D, 3 * D, 4 * D, 5 * D, 6 * D, 7 * D, 8 * D]
    for ip in range(0, nx):
        x_1d = np.zeros((1)) + xDown[ip]
        U2 = np.zeros((nt, 1, ny, nz))
        V2 = np.zeros((nt, 1, ny, nz))
        W2 = np.zeros((nt, 1, ny, nz))
        TKE2 = np.zeros((nt, 1, ny, nz))
        DTKE2 = np.zeros((nt, 1, ny, nz))

        U2[:, 0, :, :] = U[:, ip, :, :]
        V2[:, 0, :, :] = V[:, ip, :, :]
        W2[:, 0, :, :] = W[:, ip, :, :]
        TKE2[:, 0, :, :] = TKE[:, ip, :, :]
        DTKE2[:, 0, :, :] = DTKE[:, ip, :, :]

        U3 = xarray.DataArray(data=U2,
                              dims=("t", "x", "y", "z"),
                              coords={"t": t_1d, "x": x_1d, "y": y_1d, "z": z_1d},
                              name="U",
                              attrs={"long_name": "streamwise wind component", "units": "m/s"})

        V3 = xarray.DataArray(data=V2,
                              dims=("t", "x", "y", "z"),
                              coords={"t": t_1d, "x": x_1d, "y": y_1d, "z": z_1d},
                              name="V",
                              attrs={"long_name": "cross-stream wind component", "units": "m/s"})

        W3 = xarray.DataArray(data=W2,
                              dims=("t", "x", "y", "z"),
                              coords={"t": t_1d, "x": x_1d, "y": y_1d, "z": z_1d},
                              name="W",
                              attrs={"long_name": "vertical wind component", "units": "m/s"})

        TKE3 = xarray.DataArray(data=TKE2,
                                dims=("t", "x", "y", "z"),
                                coords={"t": t_1d, "x": x_1d, "y": y_1d, "z": z_1d},
                                name="TKE",
                                attrs={"long_name": "turbulent kinetic energy", "units": "m^2/s^2"})

        DTKE3 = xarray.DataArray(data=DTKE2,
                                 dims=("t", "x", "y", "z"),
                                 coords={"t": t_1d, "x": x_1d, "y": y_1d, "z": z_1d},
                                 name="DTKE",
                                 attrs={"long_name": "destruction of turbulent kinetic energy (epsilon)", "units": "m^2/s^3"})
        dataset = xarray.Dataset(data_vars={"U": U3, "V": V3, "W": W3, "TKE": TKE3, "DTKE": DTKE3})
        # print(dataset)
        # Save as NetCDF
        if wt == 0 and ip == 0:
            dataset.to_netcdf(path=outpath + participant_id + "_" + date + "_uvw_" + xDownString[ip] + "D_NoWT_2D.nc", mode='w')
        elif wt == 1:
            dataset.to_netcdf(path=outpath + participant_id + "_" + date + "_uvw_" + xDownString[ip] + "D_2D.nc", mode='w')


if __name__ == '__main__':
    # run_machine = 'docker'
    # run_machine = 'local'
    # run_machine = 'jess'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    # Check aerodynamic performance with OpenFAST in a uniform flow
    '''
    run_SWiFTuni_AirF(run_machine, queue, coarse=False,
                      grid=True,
                      run=True,
                      post=True)
    '''
    # Run cases
    run_SWiFT(run_machine, queue, coarse=False,
              grid=True,
              prelib=True,
              pre=True,
              run=True,
              post=True,
              write=True,
              plot=True, plotfmt='png')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
