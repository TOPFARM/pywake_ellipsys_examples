import numpy as np
import sys
import os
import time
import xarray
from datetime import date
from copy import copy
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
import matplotlib.pyplot as plt
import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1
plt.rcParams.update({'font.size': 10})
mpl.rcParams['lines.linewidth'] = 1.5
from matplotlib.lines import Line2D


def MixedOrderAnalysis(filenames, plotlist=None):
    """
    Mixed order analysis of grid errors, see https://doi.org/10.2514/2.2013 and https://doi.org/10.1002/we.1607
    filenames is list of files where each file represent a different grid solution (for example a rotor averaged velocity) as function of x/D
    plotlist is an optional list downstream distances (x/D) for which a mixed order plot is made to investigate the order of convergence.
    Example: plotlist=[2.5, 5.0, 7.5]. If plotlist=None, then it is not used
    IMPORTANT: Richardson extrapolation only makes sense if the grid solution converges! If not, then use finer grids.
    """
    for i in range(len(filenames)):
        datai = np.genfromtxt(filenames[i], skip_header=True)
        if i == 0:
            data = np.zeros((datai.shape[0], len(filenames) + 1))
            data[:, 0] = datai[:, 0]
        data[:, i + 1] = datai[:, 1]
    xVec = data[:, 0]
    uuVec = data[:, 1:]
    # Number of solutions to compare (can be number of slices)
    N = len(data[:, 0])
    Ngrids = len(data[0, 1:])
    # Refinement ratio
    r = 2
    # Richardson extrapolated value
    RE = np.zeros((1, N))
    # g values
    xStore = np.zeros((N, Ngrids + 1))
    # Errors
    errorStore = np.zeros((Ngrids, N + 1))
    # Coefficient matrix (h=1) for Ngrids sizes with refinement factor 2
    A = np.zeros((Ngrids, Ngrids))
    for i in range(Ngrids):
        for j in range(Ngrids):
            A[i, j] = 2 ** (i * j)

    # Absolute error function
    def f(x, g, n):
        return abs(g) * x ** n

    # Total absolute error function
    def ftotal(x, gvec):
        f = 0
        for i in range(len(gvec)):
            f = f + gvec[i] * x ** (i + 1)
        return abs(f)

    # Output data
    # Richardson extrapolated value and grid solutions
    f1 = open('RE.dat', 'w')
    # Grid errors
    f2 = open('Errors.dat', 'w')
    f1.write('# x/D')
    f2.write('# x/D')
    for i in range(Ngrids):
        f1.write(', L%i' % (Ngrids - i))
        f2.write(', L%i' % (Ngrids - i))
    f1.write(', RE\n')
    f2.write('\n')
    for i in range(0, N):
        # Right hand side b
        b = uuVec[i, ::-1]
        # Solve Ax =b
        x = np.linalg.solve(A, b)
        # Richardson extrapolated value
        RE = x[0]
        # Store errors
        xStore[i, 0] = i
        xStore[i, 1:] = np.transpose(x)
        errorStore[:, i + 1] = b - RE
        errorStore[:, 0] = A[1, :]
        # Plot absolute errors per downstream distance
        if plotlist is not None:
            if xVec[i] in plotlist:
                fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                ax.grid(True)
                ax.set_title('x/D=%g' % xVec[i])
                xlog = np.logspace(-1, 2, 100, endpoint=True)
                ax.set_xlim(10 ** (-1), 10 ** (2))
                ax.loglog(xlog, ftotal(xlog, x[1:]), '-r', label='Total error')
                ax.loglog(xlog, f(xlog, x[1], 1), '-g', label='1st order')
                ax.loglog(xlog, f(xlog, x[2], 2), '-b', label='2nd order')
                # ax.loglog(xlog, f(xlog, x[3], 3), '-m', label='3rd order')
                ax.scatter(errorStore[:, 0], np.abs(errorStore[:, i + 1]), label='D_n - RE')
                ax.set_xlabel('Normalized grid size $h$')
                ax.set_ylabel('Absolute error')
                ax.legend(loc=4, scatterpoints=1)
                fig.savefig('MOA_xD%g.pdf' % xVec[i])
        f1.write('%14.6e' % (xVec[i]))
        f2.write('%14.6e' % (xVec[i]))
        for j in range(Ngrids):
            f1.write('%14.6e' % (uuVec[i, j]))
            f2.write('%14.6e' % (errorStore[Ngrids - j - 1, i + 1]))
        f1.write('%14.6e\n' % (RE))
        f2.write('\n')
    f1.close()
    f2.close()


def run_GridStudy(run_machine, queue, coarse=True, grid=False, prelib=False, pre=False, pre_temp_study=False, run=False, post=False, write=False, write_rotor_average=False, mixedOrderAnalysis=False,
                  plot=False):
    from py_wake.examples.data.hornsrev1 import Hornsrev1Site
    from py_wake_ellipsys.utils.wtgeneric_cnst import WTgen_Cnst
    from py_wake_ellipsys.utils.ad_integration import AD_integrated_xline

    # Grid refinement study for a general single turbine wake. A plot is generated similar to Fig. 5 in https://doi.org/10.1088/1742-6596/1256/1/012011
    # The rotor averaged streamwise velocity is used to compare the grids
    # The grid error and Richardson extrapolated value are based on a mixed order analysis
    # Grid levels, cells per rotor diameter to be investigated (should be minimal three levels with factor 2 refinement)
    # Quick run (although D/2 is probably too coarse to be in the grid convergence region):
    # grid_cells1_Ds = [2.0, 4.0, 8.0]
    # More relevant for most cases:
    grid_cells1_Ds = [4.0, 8.0, 16.0]
    # or four grid size:
    # grid_cells1_Ds = [4.0, 8.0, 16.0, 32.0]
    wt_x = [0.0]
    wt_y = [0.0]
    type_i = np.zeros((1))
    Dref = 100.0
    zRef = 100.0
    tsr = 8.0
    ct = 0.8
    cp = 0.45
    wt = WTgen_Cnst(D=Dref, zH=zRef, ctval=ct, cpval=cp, tsrval=tsr)
    TI = 0.05
    ws = [10.0]
    wd = [270.0]
    today = date.today()

    maxnodes = 5
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force='2112', grid=ADGrid(nr=64, ntheta=32))
    # Wind farm grid small rotor
    Dref = wt.diameter()
    wfgrid = FlatBoxGrid(Dref,
                         cells1_D=grid_cells1_Ds[0],
                         m1_w_D=4.0, m1_e_D=25.0, m1_n_D=1.5, m1_s_D=1.5,
                         radius_D=100.0, bsize=32, zlen_D=25.0)
    e3d = E3D(reslim=1e-6)
    # Wind farm run
    wfrun = WFRun(casename='gridStudy', write_restart=True,
                  cluster=Cluster(walltime='1:59:00'))
    # Post flow
    wfpostflow = WFPostFlow(outputformat='netCDF')
    wfm = EllipSys(Hornsrev1Site(), wt, wfgrid, TI, zRef,
                   e3d=e3d, ad=ad, wfrun=wfrun, wfpostflow=wfpostflow)

    # Run step by step:
    if grid:
        # Create AD grid
        wfm.create_adgrid(type_i[0])
        # Create wind farm grids
        for p in range(len(grid_cells1_Ds)):
            wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[p])
            wfm.create_windfarm_grid(wt_x, wt_y)

    if run:
        # Run flow cases
        for p in range(len(grid_cells1_Ds)):
            wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[p])
            wfm.run_windfarm(wt_x, wt_y, wd, ws, type_i)

    if post:
        # Post process flow
        for p in range(len(grid_cells1_Ds)):
            wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[p])
            wfm.post_windfarm_flow(wd, ws)

    if write_rotor_average:
        # Write rotor averaged results as function of downstream distances
        for p in range(len(grid_cells1_Ds)):
            wfm.set_subattr('grid.cells1_D', grid_cells1_Ds[p])
            folder = wfm.get_name()
            infile = '%s/post_flow_wd%g_ws%g/flowdata.nc' % (folder, wd[0], ws[0])
            print('Wait for flowdata.nc', infile)
            while not os.path.exists(infile):
                time.sleep(5)
                sys.stdout.write('.', )
                sys.stdout.flush()
            data = xarray.open_dataset(infile)
            vars_ADint, pos = AD_integrated_xline(wt_x[0] - 3 * Dref, wt_y[0], zRef, 25.0 * Dref, 0.1 * Dref, 10, 32, Dref, vars=['U', 'tke'], flowdata=data)
            outfile = 'RANS_%s_%gcD_ADint.dat' % (wfm.wfrun.subcasename, wfm.grid.cells1_D)
            f = open(outfile, 'w')
            f.write('# EllipSys3D-RANS, %s\n' % wfm.e3d.turbmodel)
            f.write('# Based on DOI: 10.1002/we.1736, rerun date=%s\n' % today)
            f.write('# x/D, U/U0, TI (sqrt(2/3 tke)/U0)\n')
            U = vars_ADint[0] / ws[0]
            wakeTI = np.sqrt(2.0 / 3.0 * vars_ADint[1] / ws[0]) - TI
            for i in range(pos.shape[0]):
                f.write('%8.6f %8.6f %8.6f\n' % (pos[i, 0] / Dref, U[i], wakeTI[i]))
            f.close()

    if mixedOrderAnalysis:
        filenames = []
        for p in range(len(grid_cells1_Ds)):
            filenames.append('RANS_%s_%gcD_ADint.dat' % (wfm.wfrun.subcasename, grid_cells1_Ds[p]))
        MixedOrderAnalysis(filenames)

    if plot:
        fig, ax = plt.subplots(1, 2, sharex=True, figsize=(8, 3))
        colors = ['r', 'b', 'g', 'c', 'm', 'orange']
        # Grid solution and Richardson Extrapolated value
        infile = 'RE.dat'
        RE = np.genfromtxt(infile, skip_header=True)
        # Error
        infile = 'Errors.dat'
        gridError = np.genfromtxt(infile, skip_header=True)
        ls = []
        labels = []
        for p in range(len(grid_cells1_Ds)):
            l1, = ax[0].plot(RE[:, 0], RE[:, p + 1], color=colors[p])
            ax[1].plot(gridError[:, 0], gridError[:, p + 1] * 100.0, color=colors[p])
            ls.append(l1)
            labels.append(r'$\Delta=D/%g$' % grid_cells1_Ds[p])
        l1, = ax[0].plot(RE[:, 0], RE[:, -1], '--k')
        ax[1].plot(gridError[:, 0], gridError[:, 0] * 0.0, '--k')
        ls.append(l1)
        labels.append('RE')
        ax[0].grid(True)
        ax[0].set_ylabel(r'$\frac{\int_A{U dA}}{U_\infty \int_A{dA}}$', rotation=0, size=12)
        ax[0].yaxis.labelpad = 20
        ax[0].set_xlabel(r'$x/D$')
        ax[1].grid(True)
        ax[1].set_xlabel(r'$x/D$')
        ax[1].set_ylabel('Discretization error', size=12)
        vals = ax[1].get_yticks()
        ax[1].set_yticklabels(['{:3.1f}%'.format(x) for x in vals])
        ax[0].set_xlim(-2.5, 25)
        fig.tight_layout(rect=[0, 0, 1, 0.9])
        fig.legend(tuple(ls), tuple(labels), ncol=len(grid_cells1_Ds) + 1, bbox_to_anchor=(0.97, 1.0))
        props = dict(boxstyle='round', facecolor='w', edgecolor='w', alpha=1.0, pad=0.1, zorder=15)
        ax[0].text(1, 0.95, '(a)', fontsize=14, bbox=props, zorder=16)
        ax[1].text(1, 4.5, '(b)', fontsize=14, bbox=props, zorder=16)
        filename = 'gridStudy.pdf'
        fig.savefig(filename)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq,workq'
    starttime = time.time()
    run_GridStudy(run_machine, queue,
                  grid=True,
                  run=True,
                  post=True,
                  write_rotor_average=True,
                  mixedOrderAnalysis=True,
                  plot=True
                  )
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
