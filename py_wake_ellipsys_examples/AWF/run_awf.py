import numpy as np
import sys
import os
import time
import xarray
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.site._site import UniformWeibullSite

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


class UniformSite(UniformWeibullSite):
    def __init__(self, A, k, nsector=12.0, shear=None):
        UniformWeibullSite.__init__(self, np.ones(int(nsector)) / float(nsector), [A] * int(nsector), [k] * int(nsector), .1, shear=shear)


def set_coarse(wfm):
    wfm.set_subattr('e3d.reslim', 1e-2)
    wfm.set_subattr('e3d.relaxturb', 0.5)
    wfm.set_subattr('pre.tolOpt', 1e-1)
    wfm.set_subattr('pre.maxit', 1)
    wfm.set_subattr('pre.G1', 8.5)
    wfm.set_subattr('pre.ABLscale1', 2e-5)
    wfm.set_subattr('grid.cells1_D', 0.5)
    wfm.set_subattr('grid.cells2_D', 0.5)
    wfm.set_subattr('awf.grid.cells_xy_D', 0.5)
    wfm.set_subattr('grid.z_cells1_D', 1.0)
    wfm.set_subattr('grid.bsize', 48)
    wfm.set_subattr('ad.grid.nr', 5)
    wfm.set_subattr('ad.grid.ntheta', 8)
    Cluster.maxnodes = 1
    wfm.wfrun.cluster.walltime = '0:20:00'


def run(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, run_awf_ctcp=False, awf_cal=False, run=False, postflow=False, plot=False):
    from py_wake_ellipsys.utils.wtgeneric import WTgen
    from py_wake_ellipsys.utils.flatboxgridutils import get_wf_center

    maxnodes = 10
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # A rectangular wind farm simulated as an Actuator Wind Farm model
    # Input parameters are based on the Dudgeon wind farm as used in https://doi.org/10.5194/wes-2022-112
    # Here, we use a simple loglaw inflow representing a neutral atmospheric surface layer. This is unrealistic for large wts
    # that are expected to operate beyond the surface layer but it provides a numerically robust setup compared to using
    # a inflow including an ABL height.
    Dref = 154.0
    # zH_D = 0.7
    # zH_D * Dref
    zH = 102.0
    ws_rated = 10.0
    ws_cutin = 4.0
    ws_cutout = 25.0
    ct_rated = 0.8
    cp_rated = 0.45
    tsr_rated = 8.0
    rho = 1.225
    wt = WTgen(ti=0, D=Dref, zH=zH, P_rated=-1, ws_rated=ws_rated, ct_rated=ct_rated, cp_rated=cp_rated, tsr_rated=tsr_rated,
               rho=rho, ws_cutin=ws_cutin, ws_cutout=ws_cutout, dws=1.0,
               ct_method='power_law', ct_exp=3.2)
    # To reduce calibration simulations:
    wt.cutout = [12.0]
    Site = UniformSite(A=10.0, k=2.4)

    # List of square wind farm cases
    n = 8
    s = 8.0  # WT inter spacing in rotor diameters
    # s = 4.0  # WT inter spacing in rotor diameters
    # A square layout
    wt_x = np.zeros((n ** 2))
    wt_y = np.zeros((n ** 2))
    for i in range(n):
        for j in range(n):
            k = i * n + j
            wt_x[k] = i * Dref * s
            wt_y[k] = j * Dref * s

    Lx = s * (n - 1) * Dref
    nWT = len(wt_x)

    # Rotate the layout for testing binning vs gaussian method
    # deg = 45.0 / 180.0 * np.pi
    deg = 0.0
    cos = np.cos(deg)
    sin = np.sin(deg)
    wt_x_r = np.zeros((n ** 2))
    wt_y_r = np.zeros((n ** 2))
    wt_c = 0.5 * Lx
    for k in range(n * n):
        wt_x_r[k] = (wt_x[k] - wt_c) * cos - (wt_y[k] - wt_c) * sin + wt_c
        wt_y_r[k] = (wt_x[k] - wt_c) * sin + (wt_y[k] - wt_c) * cos + wt_c
    awf_wt_x = wt_x_r
    awf_wt_y = wt_y_r
    awf_wt_type_i = np.zeros((nWT), dtype=int)
    awf_wf_type_i = np.zeros((nWT), dtype=int)

    xWFc, yWFc = get_wf_center(np.asarray(awf_wt_x), np.asarray(awf_wt_y), True, 270.0)

    TI = 0.044
    zRef = zH
    # A single flow case
    wd = np.array([270.0])
    ws = np.array([8.0])

    # method = 'binning'  # A method more similar to WRF
    method = 'gauss'
    casename = 'AWF%ix%i-%gD_deg%g' % (n, n, s, deg)

    # If wind farm CT and CP are known they can be directly defined:
    # CTCPwf = np.array([[[[0.473043]]], [[[0.20852]]]])
    # CTCPwt = np.zeros((2, 1, nWT, len(wd), len(ws)))
    # CTCPwt[0, 0, :, 0, 0] = 0.473043
    # CTCPwt[1, 0, :, 0, 0] = 0.20852
    # awf_ctcp = awf.ctcp_to_netcdf(CTCPwf, CTCPwt, ws, wd)
    # If it is not known then a RANS-AD simulation will be used (takes time)
    awf_ctcp = None

    # We fix the following dimensionless number descibing the inflow:
    Rozi = 72.7695
    Nf = 112.309

    # AD
    ad = AD(force=None)
    # AWF force distribution grid
    awfgrid = AWFGrid(cells_xy_D=0.5,  # Horizontal spacing in AWF model
                      cells_z_D=6.0,  # Vertical spacing in AWF model
                      nwt_method=method  # Horizontal force distribution method reflection a wt density
                      )
    # AWF setup
    awf = AWF(grid=awfgrid, force='1',  # Variable AWF forces
              ctcp=awf_ctcp,
              ctcp_ad_force='2111',  # For CTwf and CPwf calculation step using RANS-AD
              # For variable AWF forces, we need to cover a range of wds and wss in order for the controller to interpolate over a look up table:
              ctcp_wd=np.array([265.0, 270.0, 275.0]),
              ctcp_ws=np.array([7.0, 8.0, 9.0]))

    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, cells1_D=0.5, zWakeEnd_D=3.0, bsize=64, zlen_D=25.0, dwd=10.0,
                         z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 30 m margin (150 m)
                         cells_out_factor=150.0,  # To avoid too large cells in x at the outlet that could cause numerical convergence problems.
                         cells2_D=0.5,  # For AWF cal simulation resolution (TODO: could be moved to a new variable called grid_awf_cal_cells1_D)
                         # To resolve wind farm with 4 * WF lengths
                         m1_w_D=60.0, m1_e_D=260.0, m1_n_D=35.0, m1_s_D=35.0,
                         origin=[xWFc, yWFc],
                         cluster=Cluster(walltime='0:59:00'))
    # Constants
    cnst = Cnst(pr=0.74)
    # Calculate fcori based on latitude
    latitude = 53.22
    cnst.lat2fcori(latitude)
    # EllipSys3D input
    e3d = E3D(turbmodel='keABLctfP', start_grlvl=1)  # We start on first grid level because the ABLct inflow model crashes when starting from a coarser grid level.
    # EllipSys1D input
    e1d = E1D(relaxu=0.6, relaxturb=0.6)
    # ABL inflow precursor optimizer
    # Inflow based on a prescribed temperature profle including an inversion
    preopt = PreOpt(tolOpt=1e-4, G1=5.0, ABLscale1=1e-4, TIamb=1e-5, Camb=1e-7,
                    temp_wall=285.0, temp_h=1000.0, temp_dTdz=5e-3, temp_zT_zi=0.2)
    # Wind farm run
    wfrun = WFRun(casename=casename, write_restart=True, cluster=Cluster(walltime='0:59:00'))
    # Post flow
    wfpostflow = WFPostFlow(outputformat='netCDF', single_precision_netCDF=True)

    wfm = EllipSys(Site, wt, wfgrid, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, awf=awf, pre=preopt, wfrun=wfrun, wfpostflow=wfpostflow,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_awf_ctcp=run_awf_ctcp, run_awf_cal=awf_cal, run_wf=run)

    # Cal grid adjustments
    wfm.calrun.grid.bsize = 32

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    actuator_type = [casename] * len(awf_wt_x)  # actuator_type is used to declare if a wt is an AD model or part of an AWF model
    h_i = [zRef] * len(awf_wt_x)
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(awf_wt_x, awf_wt_y, h_i, awf_wt_type_i, wd, ws, actuator_type=actuator_type)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.post_windfarm_flow(wd, ws)

    if plot:
        infile = 'run_AWF8x8-8D_deg0_NoAD_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid270/post_flow_wd270_ws8/flowdata.nc' % wfm.grid.cells1_D
        data = xarray.open_dataset(infile)

        fig, ax = plt.subplots(1, 1, sharex='col', sharey='row', figsize=(10.0, 3.0))

        delta = 0.25
        delta2 = 2.0
        xi = np.arange(-s * 6 - 10.0, s * 9 + 20 + delta, delta) * Dref
        xi = np.append(xi, np.arange(s * 9 + 20 + delta2, s * 9 + 20 + 260.0 + delta2, delta2) * Dref)
        yi = np.arange(-s * 6 - 10.0, s * 9 + 10.0 + delta, delta) * Dref
        cmap = plt.cm.get_cmap('jet')

        Xi, Yi = np.meshgrid(xi, yi)
        UAD = data['U'].interp(x=xi, y=yi, z=zH)
        vmin = 0.4
        vmax = 1.05
        levels = np.linspace(vmin, vmax, int((vmax - vmin) / 0.05 + 1))
        cs = ax.contourf(Xi / Lx, Yi / Lx, UAD.T, levels, cmap=cmap, vmax=vmax, vmin=vmin)
        fig.colorbar(cs, ax=ax, label=r'$U/U_\infty$', orientation='horizontal', location='top', aspect=50)
        filename = 'Wake.pdf'
        fig.savefig(filename)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    # queue = 'windq'
    queue = 'workq,windq,fatq,windfatq'
    starttime = time.time()
    run(run_machine, queue,
        coarse=True,
        grid=True,
        pre=True,
        cal=True,
        run_awf_ctcp=True,
        awf_cal=True,
        run=True,
        postflow=True,
        plot=True)
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
