import warnings
warnings.filterwarnings("ignore")  # Suppress xarray DeprecationWarning: distutils
import numpy as np
from numpy import newaxis as na
import time
import os
import sys
import xarray
import time
from copy import copy
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *
from py_wake.examples.data.hornsrev1 import Hornsrev1Site
import matplotlib.pyplot as plt
import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


def set_coarse(wfm):
    wfm.set_subattr('e3d.reslim', 1e-2)
    wfm.set_subattr('grid.cells1_D', 0.5)
    wfm.set_subattr('grid.z_cells1_D', 5.0)
    Cluster.maxnodes = 1
    wfm.wfrun.cluster.walltime = '0:20:00'


def keSogCnst(wfm):
    cnst = wfm.cnst
    cnst.cmu = 0.03
    cnst.kappa = 0.4
    cnst.ce1 = 1.52
    cnst.ce2 = 1.833
    cnst.pred = cnst.kappa ** 2 / (np.sqrt(cnst.cmu) * (cnst.ce2 - cnst.ce1))
    cnst.prtke = cnst.pred
    wfm.cnst = cnst


def kefPCnst(wfm):
    cnst = wfm.cnst
    cnst.ce1 = -1
    cnst.ce2 = 1.92
    cnst.prtke = 1.00
    cnst.pred = 1.30
    wfm.cnst = cnst


def run_Falster(run_machine, queue, coarse=False, grid=False, run=False, post=False, write=False, plot=False, plotfmt='pdf', rans_path=''):
    from py_wake_ellipsys.utils.canopy import CAN2CANunf, plot_CAN, read_CAN
    from py_wake_ellipsys_examples.Falster.data import data_path
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path

    # Falster forest edge based on https://orbit.dtu.dk/en/publications/7a10848b-e6ad-485d-ada6-a4135609cdf9
    # Horizontal spacing in CAN file. It is recommended to use the same horizontal spacing in CFD model (grid_Dref / grid_cells1_D = delta)
    delta = 5
    # delta = 10
    CANfile = 'grid_%im.CAN' % delta
    # 500x500 m^2 CAN file for Westerly wind direction cases with delta m horizontal spacing ana vertical spacing of dz=1 m
    os.system('cp ' + data_path + 'CAN/' + CANfile + ' .')
    # Convert formatted CAN file to binary format
    CAN2CANunf(CANfile, 'grid.CANunf', canfileformat=1, shift2center=True)
    # Read CAN file and set origin to center of CAN file
    nx, ny, xmin, xmax, ymin, ymax, FH, PAD, nlevelslist, totalnlevels = read_CAN(CANfile, canfileformat=1)
    grid_ref = [0.5 * (xmin + xmax), 0.5 * (ymin + ymax)]
    # Met mast positions
    masts = ['m1', 'm2']
    m1_utm = np.array([695273.1, 6072663.7])
    m2_utm = np.array([695343.1, 6072643.8])
    canfile = 'grid.CANunf'
    Uref = 1.0
    zRef = 30.9
    Dref = 20.0
    kappa = 0.4
    cmu = 0.03
    # TI based on a given roughness
    # z0Inlet = 0.03
    # TI = kappa * np.sqrt(2.0 / 3.0) / (cmu ** 0.25 * np.log((zRef + z0Inlet) / z0Inlet))
    # Or use TI of m1 at z=zref, corrected for induction
    # TI = np.sqrt(2.0 / 3.0 * 0.0421 * 0.885 ** 2)
    TI = 0.14
    # Cases (different turbulence model setups:
    # 1) ke Sogachev
    # 2) same as 1) but without additional epsilon source
    # 3) kefP model
    subcases = ['_SogCnst_Seps', '_SogCnst', '']
    turbmodels = ['ke', 'ke', 'kefP']
    today = date.today()

    maxnodes = 7
    set_cluster_vars(run_machine, True, queue, 32, maxnodes)

    # AD
    ad = AD(force=None)
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, origin=[0.0, 0.0],
                         cells1_D=4.0, z_cells1_D=20.0,
                         m1_w_D=15.0, m1_e_D=40.0,
                         m1_s_D=15.0, m1_n_D=15.0,
                         zWakeEnd_D=2.0, radius_D=125.0)
    # Wind farm run
    wfrun = WFRun(casename='Falster', subcasename=subcases[0], cluster=Cluster(walltime='1:00:00'),
                  write_restart=True)
    # Forest input
    forest = Forest(canfile=canfile, cd=0.2)
    #                ke=True)

    e3d = E3D(turbmodel=turbmodels[0], nstepmin=2)
    #         relaxp=0.5, relaxu=0.5, relaxturb=0.5)
    # Post flow
    wfpostflow = WFPostFlow(outputformat='netCDF', single_precision_netCDF=True, AWF_out=True)
    wfm = EllipSys(Hornsrev1Site(), None, wfgrid, TI, zRef,
                   ad=ad, e3d=e3d, wfrun=wfrun, forest=forest, wfpostflow=wfpostflow)

    # Run flow cases
    # The met mast alignment wd is 285.9 degrees. The measurement data was extracted
    # for 280-300 degrees
    wd = [285.0]
    ws = [1.0]

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    if grid:
        # Create grid
        wfm.create_windfarm_grid(None, None)

    if run:
        # Run
        for i in range(len(subcases)):
            wfm.set_subattr('e3d.turbmodel', turbmodels[i])
            wfm.wfrun.subcasename = subcases[i]
            if subcases[i][0:8] == '_SogCnst':
                keSogCnst(wfm)
            else:
                kefPCnst(wfm)
            if subcases[i] == '_SogCnst_Seps':
                wfm.set_subattr('forest.ke', True)
            else:
                wfm.set_subattr('forest.ke', False)
            wfm.run_windfarm(None, None, wd, ws, None)

    if post:
        # Store 3D flow data as a netCDF file
        for i in range(len(subcases)):
            wfm.set_subattr('e3d.turbmodel', turbmodels[i])
            wfm.wfrun.subcasename = subcases[i]
            wfm.post_windfarm_flow(wd, ws)

    if write:
        # Met mast position in local coords for wd = 270.0
        m1 = m1_utm - grid_ref
        m2 = m2_utm - grid_ref
        for p in range(len(subcases)):
            wfm.wfrun.subcasename = subcases[p]
            wfm.set_subattr('e3d.turbmodel', turbmodels[p])
            for l in range(len(wd)):
                deg = (270.0 - wd[l]) / 180.0 * np.pi
                cos = np.cos(deg)
                sin = np.sin(deg)
                m1wd = np.array([m1[0] * cos + m1[1] * sin, -m1[0] * sin + m1[1] * cos])
                m2wd = np.array([m2[0] * cos + m2[1] * sin, -m2[0] * sin + m2[1] * cos])
                # Check output of netCDF file
                folder = wfm.get_name()
                infile = folder + '/post_flow_wd%g_ws1/flowdata.nc' % wd[l]
                print('Wait for netCDF file', infile)
                while not os.path.exists(infile):
                    time.sleep(5)
                    sys.stdout.write('.', )
                    sys.stdout.flush()
                data = xarray.open_dataset(infile)
                m1U = data['U'].interp(x=m1wd[0], y=m1wd[1])
                m2U = data['U'].interp(x=m2wd[0], y=m2wd[1])
                Uref = m1U.interp(z=zRef)
                print('Uref', Uref)
                m1TI = np.sqrt(2.0 / 3.0 * data['tke'].interp(x=m1wd[0], y=m1wd[1]))
                m2TI = np.sqrt(2.0 / 3.0 * data['tke'].interp(x=m2wd[0], y=m2wd[1]))
                Ures = [m1U, m2U]
                TIres = [m1TI, m2TI]
                for j in range(2):
                    outfile = 'Falster_%s%s_%s_wd%g.dat' % (masts[j], wfm.wfrun.subcasename, wfm.e3d.turbmodel, wd[l])
                    f = open(outfile, 'w')
                    f.write('# EllipSys3D-RANS, %s\n' % wfm.e3d.turbmodel)
                    f.write('# Rerun date=%s\n' % today)
                    f.write('# Uref=%8.6f m/s (z=%g m at M1)\n' % (Uref, zRef))
                    f.write('# z[m], U/U_ref [-], TI_k [-]\n')
                    for i in range(len(data['z'])):
                        f.write('%8.6f %8.6f %8.6f\n' % (data['z'][i], Ures[j][i] / Uref, TIres[j][i] / Uref))
                    f.close()
    if plot:
        datafiles = [data_path + 'measurements/m1.dat', data_path + 'measurements/m2.dat']
        data = [np.genfromtxt(datafiles[0], skip_header=True), np.genfromtxt(datafiles[1], skip_header=True)]
        fig, ax = plt.subplots(1, 2, sharey=True, figsize=(8, 5))
        colors = ['r', 'b']
        dashes = [(None, None), (3, 4), (5, 2)]
        ls = []
        labels = []
        mast_labels = ['M1', 'M2']
        model_labels = [r'$k$-$\varepsilon$ Sogachev', r'$k$-$\varepsilon$ Sogachev, $S_\varepsilon=0$', r'$k$-$\varepsilon$-$f_P$']
        for j in range(2):
            ldata = ax[0].errorbar(data[j][:, 1], data[j][:, 0], xerr=data[j][:, 3], color=colors[j], elinewidth=1.0, linewidth=0, marker='o', zorder=0, markersize=4)
            ax[1].errorbar(np.sqrt(2.0 / 3.0 * data[j][:, 2]), data[j][:, 0], xerr=data[j][:, 4], color=colors[j], elinewidth=1.0, linewidth=0, marker='o', zorder=0, markersize=4)
            for i in range(len(subcases)):
                modelfile = rans_path + 'Falster_%s%s_%s_wd%g.dat' % (masts[j], subcases[i], turbmodels[i], wd[0])
                model = np.genfromtxt(modelfile, skip_header=True)
                lmodel, = ax[0].plot(model[:, 1], model[:, 0], color=colors[j], dashes=dashes[i], lw=2)
                if i == 0:
                    ldummy = copy(lmodel)
                    ldummy.set_color('w')
                    ls.append(ldummy)
                    labels.append(mast_labels[j])
                    ls.append(ldata)
                    labels.append('Data')
                ls.append(lmodel)
                labels.append(model_labels[i])
                ax[1].plot(model[:, 2], model[:, 0], color=colors[j], dashes=dashes[i], lw=2)
            ax[j].grid(True)
        ax[0].set_xlim(0.0, 1.4)
        ax[0].set_ylim(0.0, 60.0)
        ax[0].set_ylabel(r'$z$ [m]')
        ax[0].set_xlabel(r'$U/U_{\rm ref}$')
        ax[1].set_xlabel(r'$I_k$')
        fig.tight_layout(rect=[0.0, 0.0, 1.0, 0.65])
        fig.legend(tuple(ls), tuple(labels), ncol=2, loc='upper center', bbox_to_anchor=(0.0, 0, 1.05, 0.975), numpoints=1, scatterpoints=1, handlelength=1)

        outfile = 'Falster_prof.' + plotfmt
        fig.savefig(outfile)

        # Plot Plant Area Intensity starting from z=2 m
        outfile = 'Falster_PAI.' + plotfmt
        orig_cmap = plt.cm.gray
        colormap = orig_cmap(np.linspace(0.0, 0.8, 21))
        cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colormap)
        plot_CAN(CANfile, outfile, canfileformat=1, norm=None, clabel='PAI [m^3/m^3]', wt_x=[m1_utm[0], m2_utm[0]], wt_y=[m1_utm[1], m2_utm[1]], wt_names=['M1', 'M2'], wt_colors=colors, wt_size=200, wt_marker='*', cmap=cmap, startlevel=2)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    queue = 'windq'
    starttime = time.time()
    run_Falster(run_machine, queue, plotfmt='pdf',
                coarse=True,
                grid=True,
                run=True,
                post=True,
                write=True,
                plot=True,
                )
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
