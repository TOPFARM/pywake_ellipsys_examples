import numpy as np
from numpy import newaxis as na
import sys
import os
import time
import xarray
from copy import copy
from datetime import date
from py_wake_ellipsys.wind_farm_models.ellipsys import EllipSys
from py_wake_ellipsys.wind_farm_models.ellipsys_lib.ellipsys_wind_turbines import EllipSysWTs
from py_wake_ellipsys.wind_farm_models.ellipsys_lib import *

from scipy import interpolate
from py_wake.site._site import UniformWeibullSite
from py_wake_ellipsys.utils.get_layout_info import get_min_dist, calculate_wf_area
from py_wake.validation.validation_lib import GaussianFilter
from py_wake_ellipsys_examples.DudgeonCluster.data import data_path
from py_wake_ellipsys.utils.flatboxgridutils import get_margin_coords, get_wf_center
from py_wake_ellipsys_examples.data.turbines.dummy_wt import Dummy
from py_wake_ellipsys.utils.wtgeneric import WTgen
try:
    from py_wake_ellipsys_examples.DudgeonCluster.confidential.turbines import S6MW, S3_6MW
    confidential_data = True
except Exception:
    confidential_data = False
from py_wake.deficit_models import TurboGaussianDeficit
from py_wake.deficit_models import SelfSimilarityDeficit2020
from py_wake.ground_models import Mirror
from py_wake.superposition_models import LinearSum, SquaredSum
from py_wake.wind_farm_models.engineering_models import PropagateDownwind, All2AllIterative
from py_wake.rotor_avg_models.gaussian_overlap_model import GaussianOverlapAvgModel
from py_wake.flow_map import XYGrid
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
# Latex font
plt.rcParams['font.family'] = 'STIXGeneral'
plt.title(r'ABC123 vs $\mathrm{ABC123}^{123}$')
plt.rcParams["legend.scatterpoints"] = 1
plt.rcParams["legend.numpoints"] = 1


class UniformSite(UniformWeibullSite):
    def __init__(self, A, k, nsector=12.0, shear=None):
        UniformWeibullSite.__init__(self, np.ones(int(nsector)) / float(nsector), [A] * int(nsector), [k] * int(nsector), 0.044 / 0.8, shear=shear)


class TurbOPark_all2all(All2AllIterative):
    def __init__(self, site, windTurbines):
        wake_deficitModel = TurboGaussianDeficit(groundModel=Mirror(),
                                                 rotorAvgModel=GaussianOverlapAvgModel())
        All2AllIterative.__init__(self, site, windTurbines,
                                  wake_deficitModel=wake_deficitModel,
                                  superpositionModel=SquaredSum(),
                                  blockage_deficitModel=SelfSimilarityDeficit2020(groundModel=Mirror(), superpositionModel=LinearSum()))


class TurbOPark_all2all_recal(All2AllIterative):
    def __init__(self, site, windTurbines):
        wake_deficitModel = TurboGaussianDeficit(groundModel=None, A=.06,  # Recal changes
                                                 rotorAvgModel=GaussianOverlapAvgModel())
        All2AllIterative.__init__(self, site, windTurbines,
                                  wake_deficitModel=wake_deficitModel,
                                  superpositionModel=SquaredSum(),
                                  blockage_deficitModel=SelfSimilarityDeficit2020(groundModel=Mirror(), superpositionModel=LinearSum()))


# Global setup
Site = UniformSite(A=10.0, k=2.4)
if confidential_data:
    print('Use confidential data')
    S3_6MW = S3_6MW()
    S6MW = S6MW()
    filename = data_path + 'confidential/dudgeon.dat'
    du_layout = np.genfromtxt(filename)
    wt_x_du = list(du_layout[:, 1])
    wt_y_du = list(du_layout[:, 2])
    filename = data_path + 'confidential/sheringhamshoal.dat'
    shs_layout = np.genfromtxt(filename)
    wt_x_shs = list(shs_layout[:, 0])
    wt_y_shs = list(shs_layout[:, 1])
else:
    print('Use open data')
    # If Siemens turbines are not available then we use generic WTs
    S3_6MW = WTgen(D=107.0, zH=80.0, P_rated=3600.0, ct_rated=0.8, cp_rated=0.45, tsr_rated=8.0, ws_cutin=4.0, ws_cutout=12.0, name='WTgen3p6MW')
    S6MW = WTgen(D=154.0, zH=102.0, P_rated=6000.0, ct_rated=0.8, cp_rated=0.45, tsr_rated=8.0, ws_cutin=4.0, ws_cutout=12.0, name='WTgen6MW')
    # Openstreetmap.org data
    from py_wake_ellipsys_examples.data.farms.dudgeon import wt_x as wt_x_du
    from py_wake_ellipsys_examples.data.farms.dudgeon import wt_y as wt_y_du
    from py_wake_ellipsys_examples.data.farms.dudgeon import wt_y as wt_y_du
    from py_wake_ellipsys_examples.data.farms.sheringhamshoal import wt_x as wt_x_shs
    from py_wake_ellipsys_examples.data.farms.sheringhamshoal import wt_y as wt_y_shs

wt = EllipSysWTs.from_WindTurbines([S6MW, S3_6MW])

from py_wake_ellipsys_examples.data.farms.dudgeon import wt_names as du_wtnames
from py_wake_ellipsys_examples.data.farms.dudgeon import row1, row2
from py_wake_ellipsys_examples.data.farms.racebank import wt_x as wt_x_rb
from py_wake_ellipsys_examples.data.farms.racebank import wt_y as wt_y_rb

nWT_du = len(wt_x_du)
nWT_shs = len(wt_x_shs)
nWT_rb = len(wt_x_shs)

# From West to East:
row1_i = np.zeros((len(row1)), dtype=int)
for i in range(len(row1)):
    row1_i[i] = np.where(row1[i] == np.asarray(du_wtnames))[0]
# From South to North
row2_i = np.zeros((len(row2)), dtype=int)
for i in range(len(row2)):
    row2_i[i] = np.where(row2[i] == np.asarray(du_wtnames))[0]
rows_i = [row1_i, row2_i]

TI = 0.044
zRef = 102.0
Dref = 107.0
ws = [8.0]
latitude = 53.22  # 0.5 * (lat_min + lat_max) of WT position of all three WFs
fcori = 2.0 * 7.292115 * 10 ** (-5) * np.sin(latitude / 180.0 * np.pi)
sigmaGA = 5.0

# Global setup
# AD
ad = AD(force='1001')
# Wind farm grid
wfgrid = FlatBoxGrid(Dref, zlen_D=10.0,
                     bsize=64,
                     dwd=30.0,
                     z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                     m1_e_D=50.0, m1_n_D=10.0, m1_s_D=10.0,
                     cluster=Cluster(walltime='0:59:00'))
# Constants
cnst = Cnst(fcori=fcori, pr=0.74)
# EllipSys3D input
e3d = E3D(turbmodel='keABLctfP', start_grlvl=1)
# EllipSys1D input
e1d = E1D(relaxu=0.8, relaxturb=0.5)
# ABL inflow precursor optimizer
preopt = PreOpt(tolOpt=1e-4, G1=5.0, ABLscale1=1e-4, TIamb=1e-5, Camb=1e-7,
                temp_wall=285.0, temp_h=1000.0, temp_dTdz=5e-3, temp_zT_zi=0.2)
# Post flow
wfpostflow = WFPostFlow(outputformat='netCDF', single_precision_netCDF=True)

################################################################################
# Double wind farm case
################################################################################
type_i_du = [0] * nWT_du
wt_x_dushs = wt_x_du + wt_x_shs
wt_y_dushs = wt_y_du + wt_y_shs
type_i_dushs = [0] * nWT_du + [1] * nWT_shs
h_i_dushs = [zRef] * (nWT_du + nWT_shs)
wd_dushs = np.arange(220.0, 250.0 + 5.0, 5.0)
grid_wd_dushs = 235.0  # We use a different grid orientation to save cells

# WRF results
WRF_transect = np.genfromtxt(data_path + 'WRF/WRF_transectv2I.dat')

################################################################################
# Wind farm cluster (3 WFs)
################################################################################
wt_x_dushsrb = wt_x_du + wt_x_shs + wt_x_rb
wt_y_dushsrb = wt_y_du + wt_y_shs + wt_y_rb
type_i_dushsrb = [0] * nWT_du + [1] * nWT_shs + [0] * nWT_rb
h_i_dushsrb = [zRef] * (nWT_du + nWT_shs + nWT_rb)
wd_dushsrb = np.arange(185.0, 300.0 + 5.0, 5.0)
wdpost_dushsrb = np.arange(205.0, 285.0 + 5.0, 5.0)


def inpect_wf_properties():
    smin = get_min_dist(wt_x_du, wt_y_du) / 154.0
    area = calculate_wf_area(wt_x_du, wt_y_du, method='ConcaveHull', ConcaveHull_alpha=0.001, plot=True, outfile='du_polygon.pdf') / 154.0 ** 2
    # area = calculate_wf_area(wt_x_du, wt_y_du, method='ConvexHull', plot=True, outfile='du_polygon.pdf') / 154.0 ** 2
    print('smin du', smin, 'area', area * 154 ** 2 * 1e-6, 'density', area / nWT_du, 'smean_jens', np.sqrt(area) / (np.sqrt(nWT_du) - 1))
    smin = get_min_dist(wt_x_rb, wt_y_rb) / 154.0
    area = calculate_wf_area(wt_x_rb, wt_y_rb, method='ConcaveHull', ConcaveHull_alpha=0.001, plot=True, outfile='rb_polygon.pdf') / 154.0 ** 2
    # area = calculate_wf_area(wt_x_rb, wt_y_rb, method='ConvexHull', plot=True, outfile='rb_polygon.pdf') / 154.0 ** 2
    print('smin rb', smin, 'area', area * 154 ** 2 * 1e-6, 'density', area / nWT_rb, 'smean_jens', np.sqrt(area) / (np.sqrt(nWT_rb) - 1))
    smin = get_min_dist(wt_x_shs, wt_y_shs) / 107.0
    area = calculate_wf_area(wt_x_shs, wt_y_shs, plot=True, outfile='shs_polygon.pdf') / 107.0 ** 2
    print('smin shs', smin, 'area', area * 107 ** 2 * 1e-6, 'density', area / nWT_shs, 'smean_jens', np.sqrt(area) / (np.sqrt(nWT_shs) - 1))


def add_subplot_axes(ax, rect, facecolor='w'):
    fig = plt.gcf()
    box = ax.get_position()
    width = box.width
    height = box.height
    inax_position = ax.transAxes.transform(rect[0:2])
    transFigure = fig.transFigure.inverted()
    infig_position = transFigure.transform(inax_position)
    x = infig_position[0]
    y = infig_position[1]
    width *= rect[2]
    height *= rect[3]
    subax = fig.add_axes([x, y, width, height], facecolor=facecolor)
    x_labelsize = subax.get_xticklabels()[0].get_size()
    y_labelsize = subax.get_yticklabels()[0].get_size()
    x_labelsize *= rect[2] ** 0.5
    y_labelsize *= rect[3] ** 0.5
    subax.xaxis.set_ticks([])
    subax.yaxis.set_ticks([])
    subax.xaxis.set_ticklabels([])
    subax.yaxis.set_ticklabels([])
    return subax


def set_coarse(wfm):
    wfm.set_subattr('e3d.reslim', 1e-2)
    wfm.set_subattr('grid.cells1_D', 0.5)
    wfm.set_subattr('grid.cells2_D', 0.5)
    wfm.set_subattr('awf.grid.cells_xy_D', 0.5)
    wfm.set_subattr('e3d.relaxturb', 0.5)
    wfm.set_subattr('grid.z_cells1_D', 1.0)
    wfm.set_subattr('grid.bsize', 48)
    wfm.set_subattr('ad.grid.nr', 5)
    wfm.set_subattr('ad.grid.ntheta', 8)
    Cluster.maxnodes = 3
    # Cluster.maxnodes = 1
    wfm.wfrun.cluster.walltime = '0:20:00'


def write_row_WSeff(wfm, outfile, WS_eff_ilk, row_i, wd, GA=False, l=3, k=0, Uref=8.0):
    if confidential_data:
        calfile = 'calibration_S6MW-154_Fix_keABLctfP_%gcD_Ti0.044_zeta0/S6MW-154_Fix_keABLctfP_%gcD_Ti0.044_zeta0.dat' % (wfm.grid.cells1_D, wfm.grid.cells1_D)
    else:
        calfile = 'calibration_WTgen6MW_Fix_keABLctfP_%gcD_Ti0.044_zeta0/WTgen6MW_Fix_keABLctfP_%gcD_Ti0.044_zeta0.dat' % (wfm.grid.cells1_D, wfm.grid.cells1_D)
    caldata = np.genfromtxt(calfile, skip_header=2)
    calws = np.arange(4.0, 13.0, 1.0)
    fint = interpolate.interp1d(caldata[:-1, 0], calws)
    Uint_row = fint(WS_eff_ilk[row_i, l, k])
    f = open(outfile, 'w')
    f.write('# EllipSys3D-RANS %s\n' % wfm.e3d.turbmodel)
    f.write('# Based on DOI: 10.5194/wes-8-819-2023, rerun date=%s\n' % today)
    if GA:
        Uint_row_GA = np.zeros((len(row_i), len(wd)))
        for ll in range(0, len(wd)):
            Uint_row_GA[:, ll] = fint(WS_eff_ilk[row_i, ll, k])
        for j in range(len(row_i)):
            Uint_row_GA[j, :] = GaussianFilter(Uint_row_GA[j, :], wd, 3, sigmaGA)
        f.write('# GA with sigma=%6.4f deg\n' % sigmaGA)
        # f.write('# P15=%10.2f, P15GA=%10.2f\n' % (ProwAve[14], ProwAveGA[14]))
        f.write('# WTname, WS_eff/U0, WS_eff_GA/U0\n')
        for i in range(len(row_i)):
            f.write('%s %10.8f %10.8f\n' % (du_wtnames[row_i[i]], Uint_row[i] / Uref, Uint_row_GA[i, l] / Uref))
    else:
        f.write('# WTname, WS_eff/U0\n')
        for i in range(len(row_i)):
            f.write('%s %10.8f\n' % (du_wtnames[row_i[i]], Uint_row[i] / Uref))
    f.close()


def write_WFpower(wfm, outfile, WFpower):
    f = open(outfile, 'w')
    f.write('# EllipSys3D-RANS %s\n' % wfm.e3d.turbmodel)
    f.write('# Based on DOI: 10.5194/wes-8-819-2023, rerun date=%s\n' % today)
    # f.write('# P15=%10.2f, P15GA=%10.2f\n' % (ProwAve[14], ProwAveGA[14]))
    f.write('# wd, WFpower [W]\n')
    for l in range(len(wd_dushs)):
        f.write('%g %10.8e\n' % (wd_dushs[l], WFpower[l]))
    f.close()


def write_transect(wfm, path, outfile, xWFc, yWFc, zRef, Uref=1.0):
    wsAD_line_GA = np.zeros((len(WRF_transect[:, 0]), len(wd_dushs)))
    for l in range(len(wd_dushs)):
        infile = path + '/post_flow_wd%g_ws8/flowdata.nc' % wd_dushs[l]
        print('Wait for netCDF file', infile)
        while not os.path.exists(infile):
            time.sleep(5)
            sys.stdout.write('.', )
            sys.stdout.flush()
        data = xarray.open_dataset(infile)
        # Rotate extraction points
        deg = (270.0 - wd_dushs[l]) / 180.0 * np.pi
        cos = np.cos(deg)
        sin = np.sin(deg)
        xWRF = WRF_transect[:, 3] - xWFc
        yWRF = WRF_transect[:, 4] - yWFc
        xWRF_rot = (WRF_transect[:, 3] - xWFc) * cos + (WRF_transect[:, 4] - yWFc) * sin
        yWRF_rot = -(WRF_transect[:, 3] - xWFc) * sin + (WRF_transect[:, 4] - yWFc) * cos
        UAD_line = np.diagonal(data['U'].interp(x=xWRF_rot, y=yWRF_rot, z=zRef))
        VAD_line = np.diagonal(data['V'].interp(x=xWRF_rot, y=yWRF_rot, z=zRef))
        wsAD_line_GA[:, l] = np.sqrt(UAD_line ** 2 + VAD_line ** 2)
        if l == 3:
            wsAD_line = np.sqrt(UAD_line ** 2 + VAD_line ** 2)
    for j in range(len(WRF_transect[:, 0])):
        wsAD_line_GA[j, :] = GaussianFilter(wsAD_line_GA[j, :], wd_dushs, 3, sigmaGA)
    f = open(outfile, 'w')
    f.write('# EllipSys3D-RANS %s\n' % wfm.e3d.turbmodel)
    f.write('# Based on DOI: 10.5194/wes-8-819-2023, rerun date=%s\n' % today)
    f.write('# GA with sigma=%6.4f deg\n' % sigmaGA)
    # f.write('# P15=%10.2f, P15GA=%10.2f\n' % (ProwAve[14], ProwAveGA[14]))
    f.write('# x [m], y [m], ws/U0, ws_GA/U0\n')
    for j in range(len(WRF_transect)):
        f.write('%10.8e %10.8e %10.8e %10.8e\n' % (xWRF_rot[j], yWRF_rot[j], wsAD_line[j] / Uref, wsAD_line_GA[j, 3] / Uref))
    f.close()


def write_flowdata(wfm, infile, outfile, zRef, coarse, dx=0.0, dy=0.0, Uref=1.0):
    if coarse:
        gridname = 'grid_DoubleWF_2AD_0.5cD_wdGrid235'
    else:
        gridname = 'grid_DoubleWF_2AD_8cD_wdGrid235'
    m1, m2 = get_margin_coords('flatbox', 'boxf90', gridname)
    delta2 = 40.0
    # delta2 = 400.0
    xi2 = np.arange(m1[0, 0], m1[0, 1] + delta2, delta2)
    yi2 = np.arange(m1[1, 0], m1[1, 2] + delta2, delta2)
    print('Wait for netCDF file', infile)
    while not os.path.exists(infile):
        time.sleep(5)
        sys.stdout.write('.', )
        sys.stdout.flush()
    data = xarray.open_dataset(infile)
    UAD = data['U'].interp(x=xi2 + dx, y=yi2 + dy, z=zRef) / Uref
    VAD = data['V'].interp(x=xi2 + dx, y=yi2 + dy, z=zRef) / Uref
    wsAD = np.sqrt(UAD ** 2 + VAD ** 2)
    wsAD = wsAD.rename('WS_eff')
    wsAD['x'] = wsAD['x'] - dx
    wsAD['y'] = wsAD['y'] - dy
    encoding = {}
    encoding['x'] = {'dtype': 'float32'}
    encoding['y'] = {'dtype': 'float32'}
    encoding['WS_eff'] = {'dtype': 'float32'}
    wsAD.to_netcdf(path=outfile, encoding=encoding)


def write_flowdata_cluster(wfm, infile, outfile, xi, yi, zRef, Uref=1.0):
    print('Wait for netCDF file', infile)
    while not os.path.exists(infile):
        time.sleep(5)
        sys.stdout.write('.', )
        sys.stdout.flush()
    data = xarray.open_dataset(infile)
    U = data['U'].interp(x=xi, y=yi, z=zRef) / Uref
    U['x'] = U['x']
    U['y'] = U['y']
    encoding = {}
    encoding['x'] = {'dtype': 'float32'}
    encoding['y'] = {'dtype': 'float32'}
    encoding['U'] = {'dtype': 'float32'}
    U.to_netcdf(path=outfile, encoding=encoding)


def run_TurboPark(coarse=False, write=False):
    # Original model
    # from py_wake.literature import TurbOPark
    Ti_u = TI / 0.8
    # wf_model_org = TurbOPark(Site, wt)  # This is still PyWake but with different model settings.

    # kWake = 0.38 * Ti_u / 0.8 + 4e-3
    wfm = EllipSys(Site, wt, FlatBoxGrid(Dref), TI, zRef)
    wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x_dushs, wt_y_dushs, type_i_dushs, 235.0, grid_wd=235.0)

    wf_model_pyorg = TurbOPark_all2all(Site, wt)
    wf_model_pyrecal = TurbOPark_all2all_recal(Site, wt)

    wd_GAU = np.arange(270.0 - 18.0, 270.0 + 18.0 + 1.0, 1.0)
    sim_res_GAUpyorg = wf_model_pyorg(wt_x_cfd, wt_y_cfd, wd=wd_GAU, ws=ws, type=type_i_dushs, TI=Ti_u)
    sim_res_GAUpyrecal = wf_model_pyrecal(wt_x_cfd, wt_y_cfd, wd=wd_GAU, ws=ws, type=type_i_dushs)

    if write:
        lGAU = np.where(wd_GAU == 270.0)[0][0]
        l1 = lGAU - 15 + 3
        l2 = lGAU + 15 + 1 + 3
        WS_effGApyorg = np.zeros((len(rows_i[0]), len(wd_GAU), 1))
        WS_effGApyrecal = np.zeros((len(rows_i[0]), len(wd_GAU), 1))
        for j in range(len(rows_i[0])):
            WS_effGApyorg[j, :, 0] = GaussianFilter(sim_res_GAUpyorg.WS_eff_ilk[rows_i[0][j], :, 0], wd_GAU, 15, sigmaGA)
            WS_effGApyrecal[j, :, 0] = GaussianFilter(sim_res_GAUpyrecal.WS_eff_ilk[rows_i[0][j], :, 0], wd_GAU, 15, sigmaGA)
        TB_WS_effmean_pyorg = np.mean(sim_res_GAUpyorg.WS_eff_ilk[rows_i[0], lGAU - 2:lGAU + 3, 0], axis=1)
        TB_WS_effGAmean_pyorg = np.mean(WS_effGApyorg[:, lGAU - 2:lGAU + 3, 0], axis=1)
        TB_WS_effmean_pyrecal = np.mean(sim_res_GAUpyrecal.WS_eff_ilk[rows_i[0], lGAU - 2:lGAU + 3, 0], axis=1)
        TB_WS_effGAmean_pyrecal = np.mean(WS_effGApyrecal[:, lGAU - 2:lGAU + 3, 0], axis=1)

        f = open('DuShS_TurbOPark_Row1_WSeff_wd235.dat', 'w')
        f.write('# TurbOPark original and revised\n')
        f.write('# Based on DOI: 10.5194/wes-8-819-2023, rerun date=%s\n' % today)
        f.write('# GA with sigma=%6.4f deg\n' % sigmaGA)
        f.write('# WTname, WS_eff_org_GA/U0, WS_eff_ref_GA/U0\n')
        for i in range(len(row1_i)):
            f.write('%s %10.8f %10.8f\n' % (du_wtnames[row1_i[i]], TB_WS_effGAmean_pyorg[i] / ws, TB_WS_effGAmean_pyrecal[i] / ws))
        f.close()

        deg = (270.0 - 235.0) / 180.0 * np.pi
        cos = np.cos(deg)
        sin = np.sin(deg)
        xWFc, yWFc = get_wf_center(np.asarray(wt_x_dushs), np.asarray(wt_y_dushs), True, 235.0)
        xWRF = WRF_transect[:, 3] - xWFc
        yWRF = WRF_transect[:, 4] - yWFc
        xWRF_rot = (WRF_transect[:, 3] - xWFc) * cos + (WRF_transect[:, 4] - yWFc) * sin
        yWRF_rot = -(WRF_transect[:, 3] - xWFc) * sin + (WRF_transect[:, 4] - yWFc) * cos

        fmline_pyorg = sim_res_GAUpyorg.flow_map(XYGrid(x=xWRF_rot, y=yWRF_rot, h=zRef), wd=wd_GAU, ws=8.0)
        fmline_pyrecal = sim_res_GAUpyrecal.flow_map(XYGrid(x=xWRF_rot, y=yWRF_rot, h=zRef), wd=wd_GAU, ws=8.0)
        WSline_eff_pyorg = np.zeros((len(xWRF_rot), len(wd_GAU)))
        WSline_eff_pyrecal = np.zeros((len(xWRF_rot), len(wd_GAU)))
        for l in range(len(wd_GAU)):
            WSline_eff_pyorg[:, l] = np.diagonal(fmline_pyorg.WS_eff_xylk[:, :, l, 0])
            WSline_eff_pyrecal[:, l] = np.diagonal(fmline_pyrecal.WS_eff_xylk[:, :, l, 0])
        WSline_effGA_pyorg = np.zeros((len(xWRF_rot), len(wd_GAU)))
        WSline_effGA_pyrecal = np.zeros((len(xWRF_rot), len(wd_GAU)))
        for j in range(len(xWRF_rot)):
            WSline_effGA_pyorg[j, :] = GaussianFilter(WSline_eff_pyorg[j, :], wd_GAU, 15, sigmaGA)
            WSline_effGA_pyrecal[j, :] = GaussianFilter(WSline_eff_pyrecal[j, :], wd_GAU, 15, sigmaGA)
        WSline_effGAmean_pyorg = np.mean(WSline_effGA_pyorg[:, lGAU - 2:lGAU + 3], axis=1)
        WSline_effGAmean_pyrecal = np.mean(WSline_effGA_pyrecal[:, lGAU - 2:lGAU + 3], axis=1)

        f = open('DuShS_TurbOPark_WStransect_wd235.dat', 'w')
        f.write('# TurbOPark original and revised\n')
        f.write('# Based on DOI: 10.5194/wes-8-819-2023, rerun date=%s\n' % today)
        f.write('# GA with sigma=%6.4f deg\n' % sigmaGA)
        f.write('# x [m], y [m], WS_org_GA/U0, WS_rev_GA/U0\n')
        for j in range(len(WRF_transect)):
            f.write('%10.8e %10.8e %10.8e %10.8e\n' % (xWRF_rot[j], yWRF_rot[j], WSline_effGAmean_pyorg[j] / ws, WSline_effGAmean_pyrecal[j] / ws))
        f.close()

        if coarse:
            gridname = 'grid_DoubleWF_2AD_0.5cD_wdGrid235'
        else:
            gridname = 'grid_DoubleWF_2AD_8cD_wdGrid235'
        m1, m2 = get_margin_coords('flatbox', 'boxf90', gridname)
        delta1 = 40.0
        # delta2 = 400.0
        xi1 = np.arange(m1[0, 0], m1[0, 1] + delta1, delta1)
        yi1 = np.arange(m1[1, 0], m1[1, 2] + delta1, delta1)

        fm = sim_res_GAUpyrecal.flow_map(XYGrid(x=xi1, y=yi1, h=zRef), wd=270, ws=8.0)
        U = fm.WS_eff_xylk[:, :, 0, 0] / ws
        encoding = {}
        encoding['x'] = {'dtype': 'float32'}
        encoding['y'] = {'dtype': 'float32'}
        encoding['WS_eff'] = {'dtype': 'float32'}
        U.to_netcdf(path='DuShS_TurbOPark_flowdata.nc', encoding=encoding)


def find_Rozi_Nf(run_machine, queue, Uref, coarse=False, grid=False, pre=False, post=False):

    set_cluster_vars(run_machine, True, queue, 32, 1)

    # AD
    ad = AD(force=None)
    # Wind farm grid
    wfgrid = FlatBoxGrid(Dref, zlen_D=10.0, origin=[0.0, 0.0], bsize=32,
                         z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                         cells1_D=8.0)
    # Constants
    cnst = Cnst(fcori=fcori, pr=0.74)
    # EllpSys3d input
    e3d = E3D(turbmodel='keABLctfP')
    # EllipSys1D input
    e1d = E1D(relaxu=0.8, relaxturb=0.5)
    # ABL inflow precursor optimizer
    preopt = PreOpt(tolOpt=1e-4, G1=5.0, ABLscale1=1e-4, TIamb=1e-5, Camb=1e-7,
                    temp_wall=285.0, temp_h=1000.0, temp_dTdz=5e-3, temp_zT_zi=0.2)
    wfm = EllipSys(Site, wt, wfgrid, TI, zRef,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, pre=preopt)

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)
    if grid:
        wfm.create_precursor_grid()
    if pre:
        wfm.run_precursor_opt([Uref])
    if post:
        folder = wfm.pre.get_name() + '/'
        G, *dummy = wfm.pre.read_opt_result(folder, Uref)
        print('G', G)
        Rozi = G / (wfm.cnst.fcori * wfm.pre.temp_h)
        Nf = np.sqrt(wfm.pre.temp_dTdz * np.abs(wfm.cnst.g) / wfm.pre.temp_wall) / np.abs(wfm.cnst.fcori)
    else:
        Rozi = np.nan
        Nf = np.nan
    return Rozi, Nf


def run_DoubleWF_1AD(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, run=False, post=False, postflow=False, write=False):
    # Use dummy wts for shs in order to get the same grid as the double WF simulations
    wt_dummy = Dummy(D=107.0, zH=80.0, cutin=4.0, cutout=5.0)
    wt = EllipSysWTs.from_WindTurbines([S6MW, wt_dummy])

    set_cluster_vars(run_machine, True, queue, 32, 30)
    # Wind farm run
    wfrun = WFRun(casename='DoubleWF_DuOnlyAD', write_restart=True, cluster=Cluster(walltime='29:59:00'))
    wfm = EllipSys(Site, wt, wfgrid, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, pre=preopt, wfrun=wfrun, wfpostflow=wfpostflow,
                   run_wt_input_check_stop=False,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_wf=run, run_post=post)

    wfm.calrun.cluster.walltime = '1:00:00'
    wfm.wfpostflow.cluster.walltime = '0:59:00'

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    # Cal grid adjustments
    wfm.calrun.grid.m1_w_D = 3.0
    wfm.calrun.grid.m1_e_D = 18.0
    wfm.calrun.grid.m1_n_D = 3.0
    wfm.calrun.grid.m1_s_D = 3.0
    wfm.calrun.grid.bsize = 32

    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushs, wt_y_dushs, h_i_dushs, type_i_dushs, wd_dushs, ws, grid_wd=grid_wd_dushs)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.post_windfarm_flow(wd_dushs, ws, grid_wd=grid_wd_dushs)

    if write:
        # Output
        write_row_WSeff(wfm, 'DuOnly_RANSAD_Row1_WSeff_wd235.dat', WS_eff_ilk, row1_i, wd_dushs)
        write_WFpower(wfm, 'DuOnly_RANSAD_WFpower.dat', power_ilk[0:nWT_du, :, :].sum(axis=0).sum(axis=1))


def run_DoubleWF_2AD(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, run=False, post=False, postflow=False, write=False):
    set_cluster_vars(run_machine, True, queue, 32, 13)
    # set_cluster_vars(run_machine, True, queue, 32, 30)
    # Wind farm run
    wfrun = WFRun(casename='DoubleWF_2AD', write_restart=True, cluster=Cluster(walltime='29:59:00'))
    wfm = EllipSys(Site, wt, wfgrid, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, pre=preopt, wfrun=wfrun,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_wf=run, run_post=post)
    wfm.calrun.cluster.walltime = '1:00:00'
    wfm.wfpostflow.cluster.walltime = '0:59:00'

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    # Cal grid adjustments
    wfm.calrun.grid.m1_w_D = 3.0
    wfm.calrun.grid.m1_e_D = 18.0
    wfm.calrun.grid.m1_n_D = 3.0
    wfm.calrun.grid.m1_s_D = 3.0
    wfm.calrun.grid.bsize = 32

    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushs, wt_y_dushs, h_i_dushs, type_i_dushs, wd_dushs, ws, grid_wd=grid_wd_dushs)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.post_windfarm_flow(wd_dushs, ws, grid_wd=grid_wd_dushs)

    if write:
        # Output
        write_row_WSeff(wfm, 'DuShS_RANSAD_Row1_WSeff_wd235.dat', WS_eff_ilk, row1_i, wd_dushs, GA=True)
        write_WFpower(wfm, 'DuShS_RANSAD_WFpower.dat', power_ilk[0:nWT_du, :, :].sum(axis=0).sum(axis=1))
        xWFc, yWFc = get_wf_center(np.asarray(wt_x_dushs), np.asarray(wt_y_dushs), wfm.run_wd_con or wfm.run_enforce_rot_layout, 235.0)
        path = 'run_DoubleWF_2AD_Fix_con_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid235' % wfm.grid.cells1_D
        write_transect(wfm, path, 'DuShS_RANSAD_WStransect_wd235.dat', xWFc, yWFc, zRef=zRef)
        infile = 'run_DoubleWF_2AD_Fix_con_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid235/post_flow_wd235_ws8/flowdata.nc' % wfm.grid.cells1_D
        write_flowdata(wfm, infile, 'DuShS_RANSAD_flowdata.nc', zRef, coarse)


def run_DoubleWF_1AD_1AWF(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, awf_ct=False, awf_cal=False, run=False, post=False, postflow=False, write=False):
    xWFc, yWFc = get_wf_center(np.asarray(wt_x_du), np.asarray(wt_y_du), True, grid_wd_dushs)
    set_cluster_vars(run_machine, True, queue, 32, 30)

    # Wind farm grid
    wfgrid_1AD_1AWF = FlatBoxGrid(Dref, zlen_D=10.0,
                                  bsize=64,
                                  dwd=30.0,
                                  z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                                  m1_e_D=50.0, m1_n_D=10.0, m1_s_D=10.0,
                                  origin=[xWFc, yWFc],
                                  m2_w_D=200.0,
                                  m2_e_D=0.0,
                                  m2_n_D=0.0 + 25.0,
                                  m2_s_D=20.0 + 25.0,
                                  cells2_D=0.5,
                                  cluster=Cluster(walltime='0:59:00'))
    # Wind farm run
    wfrun = WFRun(casename='DoubleWF_1AD_1AWF', write_restart=True, cluster=Cluster(walltime='29:59:00'))
    # AWF
    awfgrid = AWFGrid(cells_xy_D=0.5, cells_z_D=6.0, gauss_thresshold=0.01,
                      nwt_method='gauss')
    awf = AWF(grid=awfgrid, force='1',
              ctcp_wd=np.arange(215.0, 255.0 + 5.0, 5.0),
              ctcp_ws=np.arange(7.0, 9.0 + 1.0, 1.0))
    wfm = EllipSys(Site, wt, wfgrid_1AD_1AWF, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, awf=awf, pre=preopt, wfrun=wfrun, wfpostflow=wfpostflow,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_awf_ctcp=awf_ct, run_awf_cal=awf_cal, run_wf=run, run_post=post)
    wfm.calrun.cluster.walltime = '1:00:00'
    wfm.wfpostflow.cluster.walltime = '0:59:00'

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    # Cal grid adjustments
    wfm.calrun.grid.m1_w_D = 3.0
    wfm.calrun.grid.m1_e_D = 18.0
    wfm.calrun.grid.m1_n_D = 3.0
    wfm.calrun.grid.m1_s_D = 3.0
    wfm.calrun.grid.bsize = 32

    actuator_type = ['ad'] * nWT_du + ['shs'] * nWT_shs  # actuator_type is used to declare if a wt is an AD model or part of an AWF model
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushs, wt_y_dushs, h_i_dushs, type_i_dushs, wd_dushs, ws, grid_wd=grid_wd_dushs,
                                                                        actuator_type=actuator_type)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.post_windfarm_flow(wd_dushs, ws, grid_wd=grid_wd_dushs)

    if write:
        # Output
        write_row_WSeff(wfm, 'DuShS_RANSADAWF_Row1_WSeff_wd235.dat', WS_eff_ilk, row1_i, wd_dushs, GA=True)
        write_WFpower(wfm, 'DuShS_RANSADAWF_WFpower.dat', power_ilk[0:nWT_du, :, :].sum(axis=0).sum(axis=1))
        xWFc, yWFc = get_wf_center(np.asarray(wt_x_du), np.asarray(wt_y_du), wfm.run_wd_con or wfm.run_enforce_rot_layout, 235.0)
        path = 'run_DoubleWF_1AD_1AWF_Fix_con_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid235' % wfm.grid.cells1_D
        write_transect(wfm, path, 'DuShS_RANSADAWF_WStransect_wd235.dat', xWFc, yWFc, zRef=zRef)
        infile = 'run_DoubleWF_1AD_1AWF_Fix_con_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid235/post_flow_wd235_ws8/flowdata.nc' % wfm.grid.cells1_D
        wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x_dushs, wt_y_dushs, type_i_dushs, np.asarray(235.0), grid_wd=235.0)
        wt_x_cfd2, wt_y_cfd2 = wfm.get_wt_position_cfd(wt_x_du, wt_y_du, type_i_du, np.asarray(235.0), grid_wd=235.0)
        dx = wt_x_cfd2[0] - wt_x_cfd[0]
        dy = wt_y_cfd2[0] - wt_y_cfd[0]
        write_flowdata(wfm, infile, 'DuShS_RANSADAWF_flowdata.nc', zRef, coarse, dx=dx, dy=dy)


def run_WFcluster_2AWF(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, awf_ct=False, awf_cal=False, run=False, post=False, postflow=False, write=False):
    xWFc, yWFc = get_wf_center(np.asarray(wt_x_du), np.asarray(wt_y_du), True, grid_wd_dushs)
    set_cluster_vars(run_machine, True, queue, 32, 30)

    # Wind farm grid
    wfgrid_2AWF = FlatBoxGrid(Dref, zlen_D=10.0,
                              bsize=32,
                              dwd=30.0,
                              z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                              radius_D=500.0,
                              m1_w_D=260.0,
                              m1_e_D=120.0,
                              m1_n_D=120.0,
                              m1_s_D=120.0,
                              cells1_D=0.5,
                              origin=[xWFc, yWFc],
                              cluster=Cluster(walltime='0:59:00'))
    # Wind farm run
    wfrun = WFRun(casename='DoubleWF_2AWF', write_restart=True, cluster=Cluster(walltime='29:59:00'))
    # AWF
    awfgrid = AWFGrid(cells_xy_D=0.5, cells_z_D=6.0, gauss_thresshold=0.01,
                      nwt_method='gauss')
    awf = AWF(grid=awfgrid, force='1', to_aep=True, ctcp_ad_force='1001',
              ctcp_wd=np.arange(215.0, 255.0 + 5.0, 5.0),
              ctcp_ws=np.arange(7.0, 9.0 + 1.0, 1.0))
    wfm = EllipSys(Site, wt, wfgrid_2AWF, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=AD(force=None), awf=awf, pre=preopt, wfrun=wfrun, wfpostflow=wfpostflow,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_awf_ctcp=awf_ct, run_awf_cal=awf_cal, run_wf=run, run_post=post)

    wfm.calrun.cluster.walltime = '1:00:00'
    wfm.wfpostflow.cluster.walltime = '0:59:00'

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    # Cal grid adjustments
    wfm.calrun.grid.m1_w_D = 3.0
    wfm.calrun.grid.m1_e_D = 18.0
    wfm.calrun.grid.m1_n_D = 3.0
    wfm.calrun.grid.m1_s_D = 3.0
    wfm.calrun.grid.bsize = 32

    # AWF ctcp and cal grids adjustments
    wfm.awf.ctcpgrid.m1_e_D = 50.0
    wfm.awf.ctcpgrid.m1_n_D = 10.0
    wfm.awf.ctcpgrid.m1_s_D = 10.0
    wfm.awf.calgrid.bsize = 32

    grid_wd = 235.0  # We use different grid orientation to save cells
    actuator_type = ['du'] * nWT_du + ['shs'] * nWT_shs  # actuator_type is used to declare if a wt is an AD model or part of an AWF model
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushs, wt_y_dushs, h_i_dushs, type_i_dushs, wd_dushs, ws, grid_wd=grid_wd,
                                                                        actuator_type=actuator_type)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.post_windfarm_flow(wd_dushs, ws, grid_wd=grid_wd)

    if write:
        # Output
        write_WFpower(wfm, 'DuShS_RANSAWF_WFpower.dat', power_ilk[0:nWT_du, :, :].sum(axis=0).sum(axis=1))
        path = 'run_DoubleWF_2AWF_NoAD_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid235' % wfm.grid.cells1_D
        write_transect(wfm, path, 'DuShS_RANSAWF_WStransect_wd235.dat', xWFc, yWFc, zRef=zRef)
        infile = 'run_DoubleWF_2AWF_NoAD_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid235/post_flow_wd235_ws8/flowdata.nc' % wfm.grid.cells1_D
        wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x_dushs, wt_y_dushs, type_i_dushs, np.asarray(235.0), grid_wd=235.0)
        wt_x_cfd2, wt_y_cfd2 = wfm.get_wt_position_cfd(wt_x_du, wt_y_du, type_i_du, np.asarray(235.0), grid_wd=235.0)
        dx = wt_x_cfd2[0] - wt_x_cfd[0]
        dy = wt_y_cfd2[0] - wt_y_cfd[0]
        write_flowdata(wfm, infile, 'DuShS_RANSAWF_flowdata.nc', zRef, coarse, dx=dx, dy=dy)


def run_WFcluster_3AD(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, run=False, post=False):
    set_cluster_vars(run_machine, True, queue, 32, 150)

    # Wind farm grid
    wfgrid_3AD = FlatBoxGrid(Dref, zlen_D=10.0,
                             bsize=64,
                             z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                             m1_e_D=50.0, m1_n_D=10.0, m1_s_D=10.0,
                             cluster=Cluster(walltime='0:59:00'))

    # Wind farm run
    wfrun = WFRun(casename='WFcluster_3AD', write_restart=True, shmmax=4, cluster=Cluster(walltime='29:59:00'))
    wfm = EllipSys(Site, wt, wfgrid_3AD, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, pre=preopt, wfrun=wfrun, wfpostflow=wfpostflow,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_wf=run, run_post=post)

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    wd = [235.0]
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushsrb, wt_y_dushsrb, h_i_dushsrb, type_i_dushsrb, wd, ws, selected_vars=['U'])


def run_WFcluster_1AD_2AWF(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, awf_ct=False, awf_cal=False, run=False, post=False, postflow=False, write=False):
    set_cluster_vars(run_machine, True, queue, 32, 34)

    # Wind farm grid
    wfgrid_1AD_2AWF = FlatBoxGrid(Dref, zlen_D=10.0,
                                  bsize=64,
                                  z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                                  m1_e_D=50.0, m1_n_D=10.0, m1_s_D=10.0,
                                  m2_w_D=400.0,
                                  m2_e_D=0.0,
                                  m2_n_D=400.0,
                                  m2_s_D=400.0,
                                  cells2_D=0.5,
                                  cluster=Cluster(walltime='0:59:00'))
    # Wind farm run
    wfrun = WFRun(casename='WFcluster_1AD_2AWF', write_restart=True, cluster=Cluster(walltime='29:59:00'))
    # AWF
    awfgrid = AWFGrid(cells_xy_D=0.5, cells_z_D=6.0, gauss_thresshold=0.01,
                      nwt_method='gauss')
    awf = AWF(grid=awfgrid, force='1',
              ctcp_wd=np.arange(175.0, 310.0 + 5.0, 5.0),
              ctcp_ws=np.arange(7.0, 9.0 + 1.0, 1.0))
    wfm = EllipSys(Site, wt, wfgrid_1AD_2AWF, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=ad, awf=awf, pre=preopt, wfrun=wfrun,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_awf_ctcp=awf_ct, run_awf_cal=awf_cal, run_wf=run, run_post=post)

    wfm.calrun.cluster.walltime = '1:00:00'
    wfm.wfpostflow.cluster.walltime = '0:59:00'

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    # Cal grid adjustments
    wfm.calrun.grid.m1_w_D = 3.0
    wfm.calrun.grid.m1_e_D = 18.0
    wfm.calrun.grid.m1_n_D = 3.0
    wfm.calrun.grid.m1_s_D = 3.0
    wfm.calrun.grid.bsize = 32

    # AWF ctcp and cal grids adjustments
    wfm.awf.ctcpgrid.m1_e_D = 50.0
    wfm.awf.ctcpgrid.m1_n_D = 10.0
    wfm.awf.ctcpgrid.m1_s_D = 10.0
    wfm.awf.calgrid.bsize = 32

    actuator_type = ['ad'] * nWT_du + ['shs'] * nWT_shs + ['rb'] * nWT_rb  # actuator_type is used to declare if a wt is an AD model or part of an AWF model
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushsrb, wt_y_dushsrb, h_i_dushsrb, type_i_dushsrb, wd_dushsrb, ws, actuator_type=actuator_type)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.wfpostflow.selected_vars = ['U']
        wfm.post_windfarm_flow(wdpost_dushsrb, ws)

    if write:
        wdPost = [[205.0, 210.0, 215.0, 220.0, 225.0, 230.0, 235.0, 240.0, 245.0, 250.0], [250.0, 255.0, 260.0, 265.0, 270.0, 275.0, 280.0, 285.0]]
        for p in range(2):
            for ll in range(len(wdPost[p])):
                l = np.where(wd_dushsrb == wdPost[p][ll])[0][0]
                l1 = l - 3
                l2 = l + 4
                write_row_WSeff(wfm, 'DuCluster_RANSADAWF_Row%i_WSeff_wd%g.dat' % (p + 1, wdPost[p][ll]), WS_eff_ilk[:, l1:l2, :], rows_i[p], wd_dushsrb[l1:l2], GA=True)
        wdunique = np.unique([j for sub in wdPost for j in sub])
        gridname = 'grid_WFcluster_1AD_2AWF_%gcD_wdGrid270' % wfm.grid.cells1_D
        m1, m2 = get_margin_coords(wfm.grid.type, 'boxf90', gridname)
        delta1 = 100.0
        # delta1 = 1000.0
        xi1 = np.arange(m2[0, 0] + 5000.0, m2[0, 1] + delta1, delta1)
        yi1 = np.arange(m2[1, 0] + 20000.0, m2[1, 2] + delta1 - 5000.0, delta1)
        delta2 = 40.0
        # delta2 = 400.0
        xi2 = np.arange(m1[0, 0], m1[0, 1] + delta2, delta2)
        yi2 = np.arange(m1[1, 0], m1[1, 2] + delta2, delta2)
        for l in range(len(wdunique)):
            infile = 'run_WFcluster_1AD_2AWF_Fix_con_keABLctfP_%gcD_Ti0.044_zeta0_wdGrid270/post_flow_wd%g_ws8/flowdata.nc' % (wfm.grid.cells1_D, wdunique[l])
            write_flowdata_cluster(wfm, infile, 'DuCluster_RANSADAWF_flowdata_outer_wd%g.nc' % wdunique[l], xi1, yi1, zRef)
            write_flowdata_cluster(wfm, infile, 'DuCluster_RANSADAWF_flowdata_inner_wd%g.nc' % wdunique[l], xi2, yi2, zRef)


def run_WFcluster_3AWF(run_machine, queue, coarse=False, grid=False, pre=False, cal=False, awf_ct=False, awf_cal=False, run=False, post=False, postflow=False):
    xWFc, yWFc = get_wf_center(np.asarray(wt_x_dushsrb), np.asarray(wt_y_dushsrb), True, 270.0)
    if coarse:
        gridname = 'grid_WFcluster_3AD_0.5cD_wdGrid270'
    else:
        gridname = 'grid_WFcluster_3AD_8cD_wdGrid270'
    m1, m2 = get_margin_coords('flatbox', 'boxf90', gridname)

    set_cluster_vars(run_machine, True, queue, 32, 30)

    # Wind farm grid
    wfgrid_3AWF = FlatBoxGrid(Dref, zlen_D=10.0,
                              bsize=32,
                              z_cells1_D=6.0,  # Results in 64 cells in height, while still having D/8 in the rotor area + 142 m margin (S 6MW wt)
                              radius_D=500.0,
                              m1_w_D=np.abs(m1[0][0]) / Dref,
                              m1_e_D=np.abs(m1[0][1]) / Dref,
                              m1_n_D=np.abs(m1[1][2]) / Dref,
                              m1_s_D=np.abs(m1[1][0]) / Dref,
                              cells1_D=0.5,
                              origin=[xWFc, yWFc],
                              cluster=Cluster(walltime='0:59:00'))
    # Wind farm run
    wfrun = WFRun(casename='WFcluster_3AWF', write_restart=True, cluster=Cluster(walltime='29:59:00'))
    # AWF
    awfgrid = AWFGrid(cells_xy_D=0.5, cells_z_D=6.0, gauss_thresshold=0.01,
                      nwt_method='gauss')
    awf = AWF(grid=awfgrid, force='1', ctcp_ad_force='1001',
              ctcp_wd=np.arange(215.0, 255.0 + 5.0, 5.0),
              ctcp_ws=np.arange(7.0, 9.0 + 1.0, 1.0))
    wfm = EllipSys(Site, wt, wfgrid_3AWF, TI, zRef, Rozi=Rozi, Nf=Nf,
                   cnst=cnst, e1d=e1d, e3d=e3d, ad=AD(force=None), awf=awf, pre=preopt, wfrun=wfrun, wfpostflow=wfpostflow,
                   run_grid=grid, run_pre=pre, run_cal=cal, run_awf_ctcp=awf_ct, run_awf_cal=awf_cal, run_wf=run, run_post=post)

    wfm.calrun.cluster.walltime = '1:00:00'
    wfm.wfpostflow.cluster.walltime = '0:59:00'

    if coarse:
        # Quick run using a coarse grid and loose convergence criterium
        set_coarse(wfm)

    # Cal grid adjustments
    wfm.calrun.grid.m1_w_D = 3.0
    wfm.calrun.grid.m1_e_D = 18.0
    wfm.calrun.grid.m1_n_D = 3.0
    wfm.calrun.grid.m1_s_D = 3.0
    wfm.calrun.grid.bsize = 32

    # AWF ctcp and cal grids adjustments
    wfm.awf.ctcpgrid.m1_e_D = 50.0
    wfm.awf.ctcpgrid.m1_n_D = 10.0
    wfm.awf.ctcpgrid.m1_s_D = 10.0
    wfm.awf.calgrid.bsize = 32

    actuator_type = ['du'] * nWT_du + ['shs'] * nWT_shs + ['rb'] * nWT_rb  # actuator_type is used to declare if a wt is an AD model or part of an AWF model
    WS_eff_ilk, TI_eff_ilk, power_ilk, *dummy = wfm.calc_wt_interaction(wt_x_dushsrb, wt_y_dushsrb, h_i_dushsrb, type_i_dushsrb, wd_dushsrb, ws, actuator_type=actuator_type)

    if postflow:
        # Store 3D flow data as a netCDF file
        wfm.post_windfarm_flow(wdpost_dushsrb, ws)


def ducluster_make_plots(coarse=False, plot_DoubleWF_WFpower=False, plot_DoubleWF_row=False, plot_WFcluster=False, plotfmt='pdf', rans_path='', extract=False):
    if rans_path is None:
        # Use old results, default is running new local results
        rans_path = data_path
    if extract:
        # Extract old RANS data
        os.system('tar -xzf ' + data_path + 'DuShS_flowdata.tar.gz')

    Uref = 8.0
    if plot_DoubleWF_WFpower:

        # A plot for wind farm power as function of ws
        fig, ax = plt.subplots(1, 2, sharex='row', sharey='none', figsize=(8.0, 4.0))
        for i in range(2):
            ax[i].grid(True)
        labels = ['(a)', '(b)']
        label_y = [-1.6, 1.1]
        for j in range(2):
            ax[j].text(220.5, label_y[j], labels[j], color='k')

        AD_WFpower_du_without_shs = np.genfromtxt(rans_path + 'DuOnly_RANSAD_WFpower.dat')[:, 1]
        AD_WFpower_du_with_shs = np.genfromtxt(rans_path + 'DuShS_RANSAD_WFpower.dat')[:, 1]
        AWF_WFpower_du_with_shs = np.genfromtxt(rans_path + 'DuShS_RANSADAWF_WFpower.dat')[:, 1]
        AWFpower_ilk = np.genfromtxt(rans_path + 'DuShS_RANSAWF_WFpower.dat')[:, 1]
        l1, = ax[0].plot(wd_dushs, (AD_WFpower_du_with_shs - AD_WFpower_du_without_shs) / AD_WFpower_du_without_shs * 100.0, '-k', lw=2)
        l2, = ax[0].plot(wd_dushs, (AWF_WFpower_du_with_shs - AD_WFpower_du_without_shs) / AD_WFpower_du_without_shs * 100.0, '-g', lw=2)
        # l3, = ax[0].plot(wd_dushs, (AWFpower_ilk[0, :, 0] - AD_WFpower_du_without_shs) / AD_WFpower_du_without_shs * 100.0, '-m', lw=2)
        l3, = ax[0].plot(wd_dushs, (AWFpower_ilk - AD_WFpower_du_without_shs) / AD_WFpower_du_without_shs * 100.0, '-m', lw=2)

        ax[1].plot(wd_dushs, (AWF_WFpower_du_with_shs - AD_WFpower_du_with_shs) / AD_WFpower_du_without_shs * 100.0, '-g', lw=2)
        # ax[1].plot(wd_dushs, (AWFpower_ilk[0, :, 0] - AD_WFpower_du_with_shs) / AD_WFpower_du_without_shs * 100.0, '-m', lw=2)
        ax[1].plot(wd_dushs, (AWFpower_ilk - AD_WFpower_du_with_shs) / AD_WFpower_du_without_shs * 100.0, '-m', lw=2)

        ax[0].set_ylabel(r'Dudgeon power loss [%]')
        ax[1].set_ylabel(r'Difference with RANS-AD [%]')
        ax[0].set_xlabel(r'Wind direction [$^\circ$]')
        ax[1].set_xlabel(r'Wind direction [$^\circ$]')

        fig.tight_layout(rect=[0.0, 0.0, 1.0, 0.8])
        fig.legend((l1, l2, l3), ('RANS-AD', r'RANS-AD-AWF, $\Delta=2D$', r'RANS-AWF, $\Delta=2D$'), ncol=3, loc='upper center', bbox_to_anchor=(0.0, 0, 1.05, 0.975), numpoints=1, scatterpoints=1, handlelength=1)
        filename = 'DuShS_Ti0p44_WFpowerloss.' + plotfmt
        fig.savefig(filename)

    if plot_DoubleWF_row:
        # WT row plot
        du_Uint_keABLcfP_row = np.genfromtxt(rans_path + 'DuOnly_RANSAD_Row1_WSeff_wd235.dat')[:, 1]
        Uint_keABLcfP_row = np.genfromtxt(rans_path + 'DuShS_RANSAD_Row1_WSeff_wd235.dat')[:, 1]
        Uint_keABLcfP_row_GA = np.genfromtxt(rans_path + 'DuShS_RANSAD_Row1_WSeff_wd235.dat')[:, 2]
        awf_Uint_keABLcfP_row = np.genfromtxt(rans_path + 'DuShS_RANSADAWF_Row1_WSeff_wd235.dat')[:, 1]
        awf_Uint_keABLcfP_row_GA = np.genfromtxt(rans_path + 'DuShS_RANSADAWF_Row1_WSeff_wd235.dat')[:, 2]
        TB_WS_effGAmean_pyorg = np.genfromtxt(rans_path + 'DuShS_TurbOPark_Row1_WSeff_wd235.dat')[:, 1]
        TB_WS_effGAmean_pyrecal = np.genfromtxt(rans_path + 'DuShS_TurbOPark_Row1_WSeff_wd235.dat')[:, 2]

        RANSAD_transect = np.genfromtxt(rans_path + 'DuShS_RANSAD_WStransect_wd235.dat')
        RANSADAWF_transect = np.genfromtxt(rans_path + 'DuShS_RANSADAWF_WStransect_wd235.dat')
        RANSAWF_transect = np.genfromtxt(rans_path + 'DuShS_RANSAWF_WStransect_wd235.dat')
        TB_transect = np.genfromtxt(rans_path + 'DuShS_TurbOPark_WStransect_wd235.dat')

        # WRF results
        deg = (270.0 - 235.0) / 180.0 * np.pi
        cos = np.cos(deg)
        sin = np.sin(deg)
        xWFc, yWFc = get_wf_center(np.asarray(wt_x_dushs), np.asarray(wt_y_dushs), True, 235.0)
        xWRF = WRF_transect[:, 3] - xWFc
        yWRF = WRF_transect[:, 4] - yWFc
        xWRF_rot = (WRF_transect[:, 3] - xWFc) * cos + (WRF_transect[:, 4] - yWFc) * sin
        yWRF_rot = -(WRF_transect[:, 3] - xWFc) * sin + (WRF_transect[:, 4] - yWFc) * cos
        sWRF = np.sqrt((xWRF_rot - xWRF_rot[-1]) ** 2 + (yWRF_rot - yWRF_rot[-1]) ** 2)

        wdPost = [[235.0]]
        colors = ['k', 'g', 'm']

        fig, ax = plt.subplots(1, 5, sharex='row', sharey='row', figsize=(10.0, 11.0))
        fig.subplots_adjust(hspace=0.05)

        ldata = []
        datacolor = ['gray', 'k', 'c', 'r', 'g', 'm']
        rect1 = [3.3, 0.215, 2.6, 0.2]
        ax3 = add_subplot_axes(ax[0], rect1)
        rect2 = [0.0, 0.215, 2.6, 0.2]
        ax4 = add_subplot_axes(ax[0], rect2)
        rect3 = [1.70, -0.06, 2.6, 0.2]
        ax5 = add_subplot_axes(ax[0], rect3)

        row_data = np.genfromtxt(data_path + 'measurements/Row1_nstab3_wd235-235_ws7.5-8.5.dat')

        datalabels = []
        lstyle = []

        # Wake shape
        ax3.set_title('Wake shape from WT power')
        ldata1 = ax3.fill_between(row_data[::-1, 0], (row_data[:, 2] - row_data[:, 3] / np.sqrt(row_data[:, 4])) / np.mean(row_data[:, 2]), y2=(row_data[:, 2] + row_data[:, 3] / np.sqrt(row_data[:, 4])) / np.mean(row_data[:, 2]), color='k', alpha=0.25)
        ax3.plot(np.arange(0, len(row1_i), 1), Uint_keABLcfP_row[::-1] / np.mean(Uint_keABLcfP_row[::-1]), '-k', lw=2)
        ax3.plot(np.arange(0, len(row1_i), 1), awf_Uint_keABLcfP_row[::-1] / np.mean(awf_Uint_keABLcfP_row[::-1]), '-g', lw=2)
        ax3.plot(np.arange(0, len(row1_i), 1), Uint_keABLcfP_row_GA[::-1] / np.mean(Uint_keABLcfP_row_GA), '-k', lw=2, dashes=(5, 3))
        ax3.plot(np.arange(0, len(row1_i), 1), awf_Uint_keABLcfP_row_GA[::-1] / np.mean(awf_Uint_keABLcfP_row_GA), '-g', lw=2, dashes=(5, 3))
        ax3.plot(np.arange(0, len(row1_i), 1), TB_WS_effGAmean_pyorg[::-1] / np.mean(TB_WS_effGAmean_pyorg), '-r', lw=2)
        ax3.plot(np.arange(0, len(row1_i), 1), TB_WS_effGAmean_pyrecal[::-1] / np.mean(TB_WS_effGAmean_pyrecal), '-r', lw=2, dashes=(5, 3))
        ax3.set_xlim(-0.5, len(row1_i) - 0.5)
        ax3.set_xticks(np.arange(0, len(row1_i), 1))
        ax3.set_ylim(0.94, 1.06)
        ax3.set_yticks(np.arange(0.94, 1.06, 0.02))
        ax3.set_yticklabels([0.94, 0.96, 0.98, 1.0, 1.02, 1.04, 1.06])
        ax3.set_xticks(np.arange(0, 12, 1))
        ax3.set_xticklabels(np.asarray(du_wtnames)[row1_i][::-1])
        ax3.grid(True)
        ax3.set_ylabel(r'$U_{\rm wt}/U_{\rm ref}$')
        ax3.set_xlabel('Wind turbine')
        ax3.text(0.2, 1.045, '(g)')

        # Wake magnitude
        ll2, = ax4.plot(np.arange(0, len(row1_i), 1), du_Uint_keABLcfP_row[::-1], '-', color='gray', lw=2)
        ll3, = ax4.plot(np.arange(0, len(row1_i), 1), Uint_keABLcfP_row[::-1], '-k', lw=2)
        ll4, = ax4.plot(np.arange(0, len(row1_i), 1), awf_Uint_keABLcfP_row[::-1], '-g', lw=2)
        ll7, = ax4.plot(np.arange(0, len(row1_i), 1), Uint_keABLcfP_row_GA[::-1], '-k', lw=2, dashes=(5, 3))
        ll8, = ax4.plot(np.arange(0, len(row1_i), 1), awf_Uint_keABLcfP_row_GA[::-1], '-g', lw=2, dashes=(5, 3))
        ll5, = ax4.plot(np.arange(0, len(row1_i), 1), TB_WS_effGAmean_pyorg[::-1], '-r', lw=2)
        ll6, = ax4.plot(np.arange(0, len(row1_i), 1), TB_WS_effGAmean_pyrecal[::-1], '-r', lw=2, dashes=(5, 3))
        print('maxdiff RANS', (awf_Uint_keABLcfP_row[::-1] - Uint_keABLcfP_row[::-1]))
        ax4.set_xlim(-0.5, len(row1_i) - 0.5)
        ax4.set_xticks(np.arange(0, len(row1_i), 1))
        ax4.set_ylim(0.86, 1.0)
        ax4.set_yticks(np.arange(0.86, 1.0, 0.02))
        ax4.set_yticklabels([0.86, 0.88, 0.90, 0.92, 0.94, 0.96, 0.98, 1.0])
        ax4.set_xticks(np.arange(0, 12, 1))
        ax4.set_xticklabels(np.asarray(du_wtnames)[row1_i][::-1])
        ax4.grid(True)
        ax4.set_ylabel(r'$U_{\rm wt}/U_\infty$')
        ax4.set_xlabel('Wind turbine')
        ax4.set_title('Wake magnitude from WT power')
        ax4.text(0.2, 0.985, '(f)')

        # Transect
        ax5.set_title('Wake magnitude upstream of Dudgeon')
        lWRF = ax5.fill_between(sWRF * 1e-3, (WRF_transect[:, 5] / WRF_transect[:, 8] - WRF_transect[:, 9] / np.sqrt(WRF_transect[:, 7])), y2=(WRF_transect[:, 5] / WRF_transect[:, 8] + WRF_transect[:, 9] / np.sqrt(WRF_transect[:, 7])), color='b', alpha=0.5)
        ax5.plot(sWRF * 1e-3, RANSAD_transect[:, 2], '-', color=colors[0], lw=2)
        ax5.plot(sWRF * 1e-3, RANSADAWF_transect[:, 2], '-', color=colors[1], lw=2)
        ax5.plot(sWRF * 1e-3, RANSAWF_transect[:, 2], '-', color=colors[2], lw=2)
        ax5.plot(sWRF * 1e-3, TB_transect[:, 2], '-', color='r', lw=2, zorder=1)
        ax5.plot(sWRF * 1e-3, TB_transect[:, 3], '--', color='r', lw=2, zorder=1, dashes=(5, 3))
        # Add Gaussian averaged RANS result for upstream wake
        ax5.plot(sWRF * 1e-3, RANSAD_transect[:, 3], '-', color=colors[0], lw=2, dashes=(5, 3))
        ax5.plot(sWRF * 1e-3, RANSADAWF_transect[:, 3], '-', color=colors[1], lw=2, dashes=(5, 3))
        ax5.plot(sWRF * 1e-3, RANSAWF_transect[:, 3], '-', color=colors[2], lw=2, dashes=(5, 3))
        ax5.set_ylim(0.84, 1.02)
        ax5.set_yticks(np.arange(0.84, 1.02, 0.02))
        ax5.set_yticklabels([0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96, 0.98, 1.0, 1.02])
        ax5.set_xticks(np.arange(0.0, 15.0, 1.0))
        ax5.set_xticklabels([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14])
        ax5.grid(True)
        ax5.set_ylabel(r'$\sqrt{U^2+V^2}/U_\infty$')
        ax5.set_xlabel('Distance along transect [km]')
        ax5.text(2.15, 1.005, '(h)')

        ldummy = copy(ll2)
        ldummy.set_color('w')
        lstyle.append(ldummy)
        datalabels.append('TurbOPark')
        lstyle.append(ldummy)
        datalabels.append('')
        lstyle.append(ll5)
        datalabels.append('Du + ShS, GA, original')
        lstyle.append(ll6)
        datalabels.append('Du + ShS, GA, revised')
        lstyle.append(ldummy)
        datalabels.append('RANS-AD')
        lstyle.append(ll2)
        datalabels.append('Du')
        lstyle.append(ll3)
        datalabels.append('Du + ShS')
        lstyle.append(ll7)
        datalabels.append('Du + ShS, GA')
        lstyle.append(ldummy)
        datalabels.append('RANS-AD-AWF')
        lstyle.append(ldummy)
        datalabels.append('')
        lstyle.append(ll4)
        datalabels.append('Du + ShS')
        lstyle.append(ll8)
        datalabels.append('Du + ShS, GA')
        lstyle.append(ldummy)
        datalabels.append('RANS-AWF')
        lstyle.append(ldummy)
        datalabels.append('')
        ll9 = copy(ll4)
        ll9.set_color('m')
        lstyle.append(ll9)
        datalabels.append('Du + ShS')
        ll10 = copy(ll8)
        ll10.set_color('m')
        lstyle.append(ll10)
        datalabels.append('Du + ShS, GA')
        lstyle.append(ldummy)
        datalabels.append('')
        lstyle.append(ldummy)
        datalabels.append('')
        lstyle.append(lWRF)
        datalabels.append('WRF')
        lstyle.append(ldata1)
        datalabels.append('Data')

        # Contours plots
        WRF_mean = np.genfromtxt(data_path + 'WRF/WRF_meanv2I.dat')
        xWRF_mean_rot = (WRF_mean[:, 2] - xWFc) * cos + (WRF_mean[:, 3] - yWFc) * sin
        yWRF_mean_rot = -(WRF_mean[:, 2] - xWFc) * sin + (WRF_mean[:, 3] - yWFc) * cos
        cmap = plt.cm.get_cmap('jet')
        wfm = EllipSys(Site, wt, FlatBoxGrid(Dref), TI, zRef)
        wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x_dushs, wt_y_dushs, type_i_dushs, np.asarray(wdPost[0][0]), grid_wd=235.0)
        dxplot = -wt_x_cfd[row1_i[0]] * 1e-3
        dyplot = -wt_y_cfd[row1_i[0]] * 1e-3
        infiles = []
        infiles.append('DuShS_TurbOPark_flowdata.nc')
        infiles.append('DuShS_RANSAD_flowdata.nc')
        infiles.append('DuShS_RANSADAWF_flowdata.nc')
        infiles.append('DuShS_RANSAWF_flowdata.nc')
        titles = ['TurbOPark', 'RANS-AD', 'RANS-AD-AWF', 'RANS-AWF']
        vmin = 0.3
        vmax = 1.05
        levels = np.linspace(vmin, vmax, int((vmax - vmin) / 0.05 + 1))
        for i in range(len(infiles)):
            data = xarray.open_dataset(infiles[i])
            Xi, Yi = np.meshgrid(data['x'], data['y'])
            if i == 0:
                xi1 = data['x']
                yi1 = data['y']
            ax[i].set_title(titles[i])
            ax[i].set_aspect('equal')
            if i == 0:
                cs = ax[i].contourf(data['y'] * 1e-3 + dyplot, data['x'] * 1e-3 + dxplot, data['WS_eff'].T, levels, cmap=cmap, vmax=vmax, vmin=vmin)
            else:
                cs = ax[i].contourf(Yi * 1e-3 + dyplot, Xi * 1e-3 + dxplot, data['WS_eff'].T, levels, cmap=cmap, vmax=vmax, vmin=vmin)
            if i < 3:
                ax[i].scatter(wt_y_cfd[row1_i][::-1] * 1e-3 + dyplot, wt_x_cfd[row1_i][::-1] * 1e-3 + dxplot, s=5, color='m', facecolors='none', edgecolors='m')
            ax[i].set_xlim(yi1[0] * 1e-3 + dyplot, yi1[-1] * 1e-3 + dyplot)
            ax[i].set_ylim(xi1[0] * 1e-3 + dxplot, xi1[-1] * 1e-3 + dxplot)
            ax[i].set_ylim(ax[i + 1].get_ylim()[::-1])
        xWRF = WRF_transect[:, 3] - xWFc
        yWRF = WRF_transect[:, 4] - yWFc
        xWRF_rot = (WRF_transect[:, 3] - xWFc) * cos + (WRF_transect[:, 4] - yWFc) * sin
        yWRF_rot = -(WRF_transect[:, 3] - xWFc) * sin + (WRF_transect[:, 4] - yWFc) * cos
        for i in range(5):
            ax[i].scatter(yWRF_rot * 1e-3 + dyplot, xWRF_rot * 1e-3 + dxplot, zorder=10, s=5, color='b', facecolors='none', edgecolors='b')
        # WRF
        ax[4].set_title('WRF')
        ax[4].set_aspect('equal')
        Uref_WRF = 1.0
        cs = ax[4].contourf(np.reshape(yWRF_mean_rot, (182, 91)) * 1e-3 + dyplot, np.reshape(xWRF_mean_rot, (182, 91)) * 1e-3 + dxplot, np.reshape(WRF_mean[:, 4], (182, 91)) / np.reshape(WRF_mean[:, 5], (182, 91)), levels, cmap=cmap, vmax=vmax, vmin=vmin)
        ax[4].set_xlim(yi1[0] * 1e-3 + dyplot, yi1[-1] * 1e-3 + dyplot)
        ax[4].set_ylim(xi1[0] * 1e-3 + dxplot, xi1[-1] * 1e-3 + dxplot)
        ax[4].set_ylim(ax[4].get_ylim()[::-1])
        fig.colorbar(cs, ax=ax.ravel().tolist(), label=r'$\sqrt{U^2+V^2}/U_\infty$', orientation='horizontal', location='top', aspect=50, anchor=(0.5, 1.5))
        ax[0].set_ylabel(r'$y-y_{A05}$ [km]')
        ax[2].set_xlabel(r'$x-x_{A05}$ [km]')
        labels = ['(a)', '(b)', '(c)', '(d)', '(e)']
        for j in range(5):
            ax[j].text(-13.0, -19.5, labels[j], color='w')
            ax[j].annotate(r"$\phi=235^\circ$", xy=(3.5, -15), xytext=(3.5, -19.5), horizontalalignment="center", arrowprops=dict(arrowstyle="->", color='w'), color='w')

        fig.tight_layout(rect=[0.0, 0.55, 1.0, 0.9])
        fig.legend(tuple(lstyle), tuple(datalabels), ncol=5, loc='upper center', bbox_to_anchor=(0.01, 0, 1, 0.6), numpoints=1, scatterpoints=1, handlelength=1)
        filename = 'DuShS_Ti0p44.' + plotfmt
        fig.savefig(filename, dpi=600)

    if plot_WFcluster:
        if coarse:
            gridname = write_WFpower + 'grid_WFcluster_1AD_2AWF_0.5cD_wdGrid270'
        else:
            gridname = write_WFpower + 'grid_WFcluster_1AD_2AWF_8cD_wdGrid270'
        m1, m2 = get_margin_coords('flatbox', 'boxf90', gridname)
        cmap = plt.cm.get_cmap('jet')
        axsub = []
        wdPost = [[205.0, 210.0, 215.0, 220.0, 225.0, 230.0, 235.0, 240.0, 245.0, 250.0], [250.0, 255.0, 260.0, 265.0, 270.0, 275.0, 280.0, 285.0]]
        rows_i = [row1_i, row2_i]
        titles = ['Southern front row', 'Western front row']
        legend_y = [0.935, 0.945]
        sublabels = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)']
        for p in range(2):
            # x_bottom, y_bottom, width, height
            rects1 = []
            rect11 = [0.69, 0.0, 1.0, 1.0]
            rects2 = []
            rect21 = [1.025, 0.0, 1.0, 1.0]
            for ll in range(len(wdPost[p])):
                rects1.append(rect11)
                rects2.append(rect21)
                rectsall = [rects1, rects2]

            fig, ax = plt.subplots(len(wdPost[p]), 2, sharex='col', figsize=(8.0, len(wdPost[p]) * 1.5), gridspec_kw={'width_ratios': [2.0, 1]})
            fig.subplots_adjust(hspace=0.05)

            ldata = []
            datacolor = ['k', 'gray', 'c', 'r', 'g', 'm']
            for ll in range(len(wdPost[p])):
                rect = patches.Rectangle((0.3 * len(rows_i[p]), 1.033), 0.4 * len(rows_i[p]), 0.024, linewidth=1, edgecolor='white', facecolor='white', alpha=1.0, zorder=3)
                Uint_keABLcfP_row = np.genfromtxt(rans_path + 'DuCluster_RANSADAWF_Row%i_WSeff_wd%g.dat' % ((p + 1), wdPost[p][ll]))[:, 1]
                Uint_keABLcfP_rowGA = np.genfromtxt(rans_path + 'DuCluster_RANSADAWF_Row%i_WSeff_wd%g.dat' % ((p + 1), wdPost[p][ll]))[:, 2]
                filenames = []
                filenames.append(data_path + 'measurements/Row%i_nstab3_wd%g-%g_ws7.5-8.5.dat' % (p + 1, wdPost[p][ll], wdPost[p][ll]))
                row_data = []
                for n in range(len(filenames)):
                    row_data.append(np.genfromtxt(filenames[n]))
                datalabels = ['Data']
                lstyle = []
                if p == 0:
                    ax[ll, 0].set_xticklabels(np.asarray(du_wtnames)[rows_i[p]][::-1])
                    for n in range(len(filenames)):
                        ldata1 = ax[ll, 0].fill_between(row_data[n][::-1, 0], (row_data[n][:, 2] - row_data[n][:, 3] / np.sqrt(row_data[n][:, 4])) / np.mean(row_data[n][:, 2]), y2=(row_data[n][:, 2] + row_data[n][:, 3] / np.sqrt(row_data[n][:, 4])) / np.mean(row_data[n][:, 2]), color='k', alpha=0.25)
                        lstyle.append(ldata1)
                    ll2, = ax[ll, 0].plot(np.arange(0, len(rows_i[p]), 1), Uint_keABLcfP_row[::-1] / np.mean(Uint_keABLcfP_row), '-g', lw=2)
                    ll3, = ax[ll, 0].plot(np.arange(0, len(rows_i[p]), 1), Uint_keABLcfP_rowGA[::-1] / np.mean(Uint_keABLcfP_rowGA[::-1]), '--g', lw=2)
                elif p == 1:
                    ax[ll, 0].set_xticklabels(np.asarray(du_wtnames)[rows_i[p]])
                    for n in range(len(filenames)):
                        ldata1 = ax[ll, 0].fill_between(row_data[n][:, 0], (row_data[n][:, 2] - row_data[n][:, 3] / np.sqrt(row_data[n][:, 4])) / np.mean(row_data[n][:, 2]), y2=(row_data[n][:, 2] + row_data[n][:, 3] / np.sqrt(row_data[n][:, 4])) / np.mean(row_data[n][:, 2]), color='k', alpha=0.25)
                        lstyle.append(ldata1)
                    ll2, = ax[ll, 0].plot(np.arange(0, len(rows_i[p]), 1), Uint_keABLcfP_row / np.mean(Uint_keABLcfP_row), '-g', lw=2)
                    ll3, = ax[ll, 0].plot(np.arange(0, len(rows_i[p]), 1), Uint_keABLcfP_rowGA / np.mean(Uint_keABLcfP_rowGA), '--g', lw=2)
                ax[ll, 0].set_xlim(-0.5, len(rows_i[p]) - 0.5)
                ax[ll, 0].set_xticks(np.arange(0, len(rows_i[p]), 1))
                ax[ll, 0].set_ylim(0.94, 1.06)
                ax[ll, 0].set_yticks(np.arange(0.95, 1.05, 0.025))
                ax[ll, 0].set_yticklabels(["0.95", "", "1.0", "", "1.05"])
                ax[ll, 0].grid(True)
                ax[ll, 0].text(0.4 * len(rows_i[p]), 1.045, r'$\phi=%g\pm2.5^\circ$' % wdPost[p][ll], va='center', zorder=10)
                ax[ll, 0].text(0.32 * len(rows_i[p]), 1.045, sublabels[ll], va='center', zorder=10)
                ax[ll, 0].add_patch(rect)

                wt_x_cfd, wt_y_cfd = wfm.get_wt_position_cfd(wt_x_du, wt_y_du, type_i_du, np.asarray(wdPost[p][ll]), grid_wd=270.0)
                vmin = 0.3
                vmax = 1.05
                levels = np.linspace(vmin, vmax, int((vmax - vmin) / 0.05 + 1))
                for i in range(2):
                    ax1 = add_subplot_axes(ax[ll, 0], rectsall[i][ll])
                    ax1.set_aspect('equal')
                    if i == 0:
                        flowdata = xarray.open_dataset(rans_path + 'DuCluster_RANSADAWF_flowdata_outer_wd%g.nc' % wdPost[p][ll])
                        Xi, Yi = np.meshgrid(flowdata['x'], flowdata['y'])
                        cs = ax1.contourf(Yi * 1e-3, Xi * 1e-3, flowdata['U'].T, levels, cmap=cmap, vmax=vmax, vmin=vmin)
                        if p == 0:
                            ax1.annotate(r"$\phi=%g^\circ$" % wdPost[p][ll], xy=(0, -10), xytext=(0, -35), horizontalalignment="center", color='w', zorder=10)
                            ax1.annotate(r"", xy=(-12, -20), xytext=(-12, -35), horizontalalignment="center", arrowprops=dict(arrowstyle="->", color='w'), color='w', zorder=10)
                            ax1.set_xlim(m2[1, 0] * 1e-3 + 30.0, m2[1, 2] * 1e-3 - 5.0)
                            ax1.set_ylim(m2[0, 0] * 1e-3 + 5.0, m2[0, 1] * 1e-3)
                        elif p == 1:
                            ax1.annotate(r"$\phi=%g^\circ$" % wdPost[p][ll], xy=(-12, -20), xytext=(-12, -38), horizontalalignment="center", color='w', zorder=10)
                            ax1.annotate(r"", xy=(-25, -20), xytext=(-25, -35), horizontalalignment="center", arrowprops=dict(arrowstyle="->", color='w'), color='w', zorder=10)
                            ax1.set_xlim(m2[1, 0] * 1e-3 + 20.0, m2[1, 2] * 1e-3 - 20.0)
                            ax1.set_ylim(m2[0, 0] * 1e-3 + 5.0, m2[0, 1] * 1e-3)
                    elif i == 1:
                        flowdata = xarray.open_dataset(rans_path + 'DuCluster_RANSADAWF_flowdata_inner_wd%g.nc' % wdPost[p][ll])
                        Xi, Yi = np.meshgrid(flowdata['x'], flowdata['y'])
                        cs = ax1.contourf(Yi * 1e-3, Xi * 1e-3, flowdata['U'].T, levels, cmap=cmap, vmax=vmax, vmin=vmin)
                        ax1.scatter(wt_y_cfd[rows_i[p]][::-1] * 1e-3, wt_x_cfd[rows_i[p]][::-1] * 1e-3, s=5, color='m', facecolors='none', edgecolors='m')
                    ax1.set_ylim(ax1.get_ylim()[::-1])
                    if i == 1:
                        divider1 = make_axes_locatable(ax1)
                        cax1 = divider1.append_axes("right", size="10%", pad=0.05)
                        cbar1 = plt.colorbar(cs, cax=cax1, label=r'$U/U_\infty$', orientation='vertical')
                ax[ll, 1].axis('off')
            if p == 0:
                ax[5, 0].set_ylabel(r'$U_{\rm wt}/U_{\rm ref}$')
                ax[5, 0].yaxis.set_label_coords(-0.15, 1.0)
            elif p == 1:
                ax[4, 0].set_ylabel(r'$U_{\rm wt}/U_{\rm ref}$')
                ax[4, 0].yaxis.set_label_coords(-0.15, 1.0)
            ax[-1, 0].set_xlabel('Wind turbine')
            lstyle.append(ll2)
            datalabels.append('RANS')
            lstyle.append(ll3)
            datalabels.append('RANS GA')
            fig.legend(tuple(lstyle), tuple(datalabels), ncol=3, loc='upper center', bbox_to_anchor=(0, 0, 1, legend_y[p]), numpoints=1, scatterpoints=1)
            filename = 'DuCluster_Ti0p44_Row%i.%s' % (p + 1, plotfmt)
            fig.savefig(filename, dpi=600)


if __name__ == '__main__':
    # run_machine = 'local'
    run_machine = 'sophia'
    # queue = 'windq,workq,fatq,windfatq,rome'
    queue = 'windq'
    today = date.today()
    starttime = time.time()

    # Inspect wf layout properties
    # inpect_wf_properties()

    # Find non-dimensional numbers for running a scaled ABL inflow profile based on physical profile (using a chosen Uref=8.0 m/s),
    # allowing for consecutive ws runs, where Uinflow=1.0 m/s. The additional non-dimensional numbers represent the added physics
    # (Coriolis, inversion, ABL height). When they are set to constants, then we still have Reynolds number similarity,
    #  allowing for efficient consecutive ws runs.
    # Rozi, Nf = find_Rozi_Nf(run_machine, queue, 8.0, coarse=False, grid=True, pre=True, post=True)
    Rozi, Nf = find_Rozi_Nf(run_machine, queue, 8.0, coarse=False, grid=False, pre=False, post=True)
    print('Rozi', Rozi, 'Nf', Nf)

    ################################################################################
    # Double wind farm case
    ################################################################################
    # Run du+shs with ADs
    run_DoubleWF_2AD(run_machine, queue, coarse=True, grid=True, pre=True, cal=True, run=True, post=True, postflow=True, write=True)
    # Run with only du as ADs, with the same grid as the 2AD case to look at the wind farm induction (by using dummy wts for shs
    run_DoubleWF_1AD(run_machine, queue, coarse=True, grid=True, pre=True, cal=True, run=True, post=True, postflow=True, write=True)
    # Run du+shs as du:AWF, shs:AWF
    # Does not make force calibration files and AWT CT when using NoAD and wrong grid_cells1_D
    run_WFcluster_2AWF(run_machine, queue, coarse=True, grid=True, pre=False, cal=True, awf_ct=True, awf_cal=True, run=True, post=True, postflow=True, write=True)
    # Run du+shs as du:AD, shs:AWF
    run_DoubleWF_1AD_1AWF(run_machine, queue, coarse=True, grid=True, pre=False, cal=False, awf_ct=False, awf_cal=False, run=True, post=True, postflow=True, write=True)
    # Run du+shs usign TurbOPark original and revised
    run_TurboPark(coarse=True, write=True)

    ################################################################################
    # Wind farm cluster (3 WFs)
    ################################################################################
    # All ADs -> Enormous grid!
    # run_WFcluster_3AD(run_machine, queue, coarse=True, grid=True, pre=False, cal=False, run=False, post=False)
    # du:AWF, shs:AWF, rb:AWF, requires 3AD wind farm grid for grid margins
    # run_WFcluster_3AWF(run_machine, queue, coarse=True, grid=True, pre=False, cal=True, awf_ct=True, awf_cal=True, run=True, post=True, postflow=True)
    # run_WFcluster_3AWF(run_machine, queue, coarse=True, awf_cal=True, run=True, post=True, postflow=True)

    # du:AD, shs:AWF, rb:AWF
    run_WFcluster_1AD_2AWF(run_machine, queue, coarse=True, grid=True, pre=False, cal=False, awf_ct=True, awf_cal=True, run=True, post=True, postflow=True, write=True)

    # Create plots and animation
    # DuShS_Ti0p44_WFpowerloss figure differs from article because the RANSAD wind farm power results included the power of 1 ShS turbine
    # DuShS_Ti0p44 figure differs slightly from artcle for RANSAD Du only because the wrong wd was extracted (225 instead 235)
    ducluster_make_plots(coarse=True, plot_DoubleWF_WFpower=True, plot_DoubleWF_row=True, plot_WFcluster=True, plotfmt='pdf')
    endtime = time.time()
    print('Total time:', endtime - starttime, 'sec')
